package com.groupon.web.api.dto;

import java.util.Map;

/**
 * User: prachi.nagpal
 */
public class RequestDTO {

	private String apiName;
	private String baseUrl;
	private String endPoint;
	private String methodType;
	private String headerJson;
	private String pathParamJson;
	private String queryParamJson;
	private String requestBody;
	private Map<String, String> formParams;
	private int expectedResponseCode;
	private String expectedResponseJson;

	public RequestDTO(String baseUrl) {
		this.baseUrl = baseUrl;
	}

	public String getMethodType() {
		return methodType;
	}

	public void setMethodType(String methodType) {
		this.methodType = methodType;
	}

	public String getEndPoint() {
		return endPoint;
	}

	public void setEndPoint(String endPoint) {
		this.endPoint = endPoint;
	}

	public String getHeaderJson() {
		return headerJson;
	}

	public void setHeaderJson(String headerJson) {
		this.headerJson = headerJson;
	}

	public String getPathParamJson() {
		return pathParamJson;
	}

	public void setPathParamJson(String pathParamJson) {
		this.pathParamJson = pathParamJson;
	}

	public String getQueryParamJson() {
		return queryParamJson;
	}

	public void setQueryParamJson(String queryParamJson) {
		this.queryParamJson = queryParamJson;
	}

	public int getExpectedResponseCode() {
		return expectedResponseCode;
	}

	public void setExpectedResponseCode(int responseCode) {
		this.expectedResponseCode = responseCode;
	}

	public String getExpectedResponseJson() {
		return expectedResponseJson;
	}

	public void setExpectedResponseJson(String responseParamJson) {
		this.expectedResponseJson = responseParamJson;
	}

	public String getRequestBody() {
		return requestBody;
	}

	public void setRequestBody(String requestBody) {
		this.requestBody = requestBody;
	}

	public String getApiName() {
		return apiName;
	}

	public void setApiName(String apiName) {
		this.apiName = apiName;
	}

	public String getBaseUrl() {
		return baseUrl;
	}

	public Map<String, String> getFormParams() {
		return formParams;
	}

	public void setFormParams(Map<String, String> formParams) {
		this.formParams = formParams;
	}

	@Override
	public String toString() {
		return "RequestDTO{" +
				"apiName='" + apiName + '\'' +
				", baseUrl='" + baseUrl + '\'' +
				", endPoint='" + endPoint + '\'' +
				", methodType='" + methodType + '\'' +
				", headerJson='" + headerJson + '\'' +
				", pathParamJson='" + pathParamJson + '\'' +
				", queryParamJson='" + queryParamJson + '\'' +
				", requestBody='" + requestBody + '\'' +
				", formParams=" + formParams +
				", responseCode=" + expectedResponseCode +
				", expectedResponseJson='" + expectedResponseJson + '\'' +
				'}';
	}
}
