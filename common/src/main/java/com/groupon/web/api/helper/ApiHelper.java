package com.groupon.web.api.helper;


import com.groupon.web.api.dto.RequestDTO;
import com.groupon.web.common.utils.ExcelUtil;
import org.apache.commons.collections.MapUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.http.HttpEntity;
import org.apache.http.HttpEntityEnclosingRequest;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpDelete;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpPut;
import org.apache.http.client.methods.HttpRequestBase;
import org.apache.http.client.utils.URIBuilder;
import org.apache.http.entity.StringEntity;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.protocol.HTTP;
import org.apache.http.util.EntityUtils;
import org.apache.log4j.Logger;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;


/**
 * This class contains helper functions which are being used in testng class
 *
 */
/**
 * 
 * @author prachi.nagpal
 *
 */
public class ApiHelper {

    private static final Logger LOGGER = Logger.getLogger(ApiHelper.class);
    public static final String CONTENT_TYPE = HTTP.CONTENT_TYPE;
    public static final String ACCEPT = "Accept";

    /**
	 * This function is used to get the response of the api, which we get through curl hit 
	 * 
	 *
     *
     * @param requestDTO : requestDTO containing request information
     * @return : it returns HttpResponse containing status code & response data
	 */
	public HttpRequestBase prepareHTTPRequest(RequestDTO requestDTO) throws Exception {
        if(requestDTO != null && StringUtils.isNotBlank(requestDTO.getBaseUrl())) {
        	JSONParser parser = new JSONParser();
            //set path params
            assignPathParams(requestDTO, parser);
            //set url endpoint
            String url = requestDTO.getBaseUrl();
            if(StringUtils.isNotEmpty(requestDTO.getEndPoint())) {
                url += "/" + requestDTO.getEndPoint();
            }
            URIBuilder uriBuilder = new URIBuilder(url);
            //set query params
            assignQueryParams(uriBuilder, requestDTO.getQueryParamJson(), parser);
            //prepare http request
            HttpRequestBase httpRequest = prepareHttpRequest(requestDTO.getMethodType());
            httpRequest.setURI(uriBuilder.build());
            //add headers
			assignHeaders(httpRequest, requestDTO.getHeaderJson(), parser);
            //add request body
			addRequestBody(httpRequest, requestDTO.getRequestBody());
			//add form parameters
			addFormParams(httpRequest, requestDTO.getFormParams());

            return httpRequest;
        }
        throw new IllegalArgumentException("RequestDTO("+requestDTO+") or Base Url cannot be empty");
	}

	private void addFormParams(HttpRequestBase httpRequest, Map<String, String> formParams) throws UnsupportedEncodingException {
		if(MapUtils.isNotEmpty(formParams) && httpRequest instanceof HttpPost) {
			List<NameValuePair> parameters = new ArrayList<NameValuePair>(formParams.size());
			for (Map.Entry<String, String> entry : formParams.entrySet()) {
				parameters.add(new BasicNameValuePair(entry.getKey(), entry.getValue()));
			}
			((HttpPost) httpRequest).setEntity(new UrlEncodedFormEntity(parameters));
		}
	}

	private void addRequestBody(HttpRequestBase httpRequest, String requestBody) throws UnsupportedEncodingException {
        if(StringUtils.isNotBlank(requestBody) && httpRequest instanceof HttpEntityEnclosingRequest) {
            final StringEntity entity = new StringEntity(requestBody);
            entity.setContentType(httpRequest.getFirstHeader(CONTENT_TYPE));
            ((HttpEntityEnclosingRequest) httpRequest).setEntity(entity);
        }
    }

    private void assignHeaders(HttpRequestBase httpRequest, String headerJson, JSONParser parser) throws ParseException {
    	//default headers
    	httpRequest.addHeader(CONTENT_TYPE, "application/json");
    	if(HttpPost.METHOD_NAME.equalsIgnoreCase(httpRequest.getMethod()) || HttpPut.METHOD_NAME.equalsIgnoreCase(httpRequest.getMethod())) {
    		httpRequest.addHeader(ACCEPT, "application/json");
    	}
    	
    	//custom header
        if(StringUtils.isNotBlank(headerJson)) {
            final JSONObject headerMap = (JSONObject) parser.parse(headerJson);
            for (Object k : headerMap.keySet()) {
                final String key = (String) k;
                final String value = (String) headerMap.get(key);
                if(StringUtils.isNotEmpty(value)) {
                    httpRequest.setHeader(key, value);
                }
            }
        }
    }

    private HttpRequestBase prepareHttpRequest(String methodType) {
        if(HttpPost.METHOD_NAME.equalsIgnoreCase(methodType)) {
            return new HttpPost();
        }
        else if(HttpPut.METHOD_NAME.equalsIgnoreCase(methodType)) {
             return new HttpPut();
        }
        else if(HttpDelete.METHOD_NAME.equalsIgnoreCase(methodType)) {
            return new HttpDelete();
        }
        else {
            return new HttpGet();
        }
    }

    private void assignQueryParams(URIBuilder uriBuilder, String queryParamJson, JSONParser parser) throws ParseException {
        if(StringUtils.isNotBlank(queryParamJson)) {
            final JSONObject queryParamMap = (JSONObject) parser.parse(queryParamJson);
            for (Object k : queryParamMap.keySet()) {
                final String key = (String) k;
                final String value = (String) queryParamMap.get(key);
                if(StringUtils.isNotEmpty(value)) {
                    uriBuilder.addParameter(key, value);
                }
            }
        }
    }

    private void assignPathParams(RequestDTO requestDTO, JSONParser parser) throws ParseException {
        final String pathParamJson = requestDTO.getPathParamJson();
        if(StringUtils.isNotBlank(pathParamJson)) {
            String endPoint = requestDTO.getEndPoint();
            final JSONObject pathParamMap = (JSONObject) parser.parse(pathParamJson);
            for (Object k : pathParamMap.keySet()) {
                final String key = (String) k;
                final String value = (String)pathParamMap.get(key);
                if(StringUtils.isNotEmpty(value)) {
                    endPoint = endPoint.replace("{"+key+"}", value);
                }
            }
            requestDTO.setEndPoint(endPoint);
        }
    }


    /**
	 * This function is used to get records from xls file and use the data as data provider
	 * 
	 * @param xlFilePath : takes file name of the excel along with its path
	 * @param sheetNum : sheet number
	 * @return : it returns an array which is used as a data provider in test case
	 */
	public String[][] getTableArray(String xlFilePath, int sheetNum) {
		/*List<List<String>> values = new ArrayList<List<String>>();
		int col = 7;
		try {
			FileInputStream file = new FileInputStream(xlFilePath);
			Workbook wb = Workbook.getWorkbook(file);
			Sheet sheet = wb.getSheet(sheetNum);
			int ci = 0;
			while (true) {
				ci++;
				String endpoint = sheet.getCell(0, ci).getContents();
				if(StringUtils.isNotBlank(endpoint) && !endpoint.equalsIgnoreCase("null")) {
					LOGGER.info("Row:"+ci+", Endpoint:"+endpoint);
					List<String> colValues = new ArrayList<String>(col);
					for (int cj = 0; cj < col; cj++){
						colValues.add(sheet.getCell(cj, ci).getContents());
					}
					values.add(colValues);
				}
				else {
					LOGGER.info("Reading request urls done");
					break;
				}
			}
		} catch (Exception e) {
			throw new RuntimeException("Error in reading request urls", e);
		}
		if(values.size() > 0) {
			String[][] tabArray = new String[values.size()][col];
			int i = 0;
			for(List<String> colValues : values) {
				tabArray[i] = colValues.toArray(new String[0]);
				i++;
			}
			return (tabArray);
		}
		else {
			throw new RuntimeException("No request urls found");
		}*/

        try {
            return ExcelUtil.readExcelData(xlFilePath, sheetNum, false);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }

    }

	
	/**
	 * This function is used to get the "successful" field value from response json
	 *  
	 * @param httpResponse: HttpResponse object
	 * @return : it returns a string containing response body
	 */
	public String getResponseBody(HttpResponse httpResponse) throws IOException {
        final HttpEntity entity = httpResponse.getEntity();
        return entity != null ? EntityUtils.toString(entity) : null;
    }
	
}
