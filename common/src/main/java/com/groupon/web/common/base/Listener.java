/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.groupon.web.common.base;

import org.testng.ITestContext;
import org.testng.ITestListener;
import org.testng.ITestResult;

/**
 * 
 * @author prachi.nagpal
 *
 */
public class Listener implements ITestListener{

	public void onTestStart(ITestResult result) {
        System.out.println("Started Test: "+result.getName());
    }

    public void onTestSuccess(ITestResult result) {
        System.out.println("Finished Test: "+result.getName()+" :PASSED");
    }

    public void onTestFailure(ITestResult result) {
        System.out.println("Finished Test: "+result.getName()+" :FAILED");
    }

    public void onTestSkipped(ITestResult result) {
        System.out.println("Finished Test: "+result.getName()+" :SKIPPED");
    }

    public void onTestFailedButWithinSuccessPercentage(ITestResult result) {
        System.out.println("Finished Test: "+result.getName()+" :FAILED BUT WITHIN SUCCESS PERCENTAGE");
    }

    public void onStart(ITestContext context) {
        System.out.println("Tests Started: "+context.getName());
    }

    public void onFinish(ITestContext context) {
        System.out.println("Tests Finished: "+context.getName());
    }
}

