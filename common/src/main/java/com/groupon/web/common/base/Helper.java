/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.groupon.web.common.base;

import com.groupon.web.common.utils.WaitUtility;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;

import java.util.ArrayList;
import java.util.List;

/**
 * 
 * @author prachi.nagpal
 *
 */
public class Helper 
{

    public WebDriver createWebDriver(String browser, WebDriver driver)
    {
        System.setProperty("webdriver.chrome.driver","ChromeDriver\\chromedriver.exe");
	System.setProperty("webdriver.ie.driver","IEDriver\\IEDriverServer.exe");
	if (browser.equalsIgnoreCase("Firefox")) 
        {
            driver = new FirefoxDriver();
        }
	else if (browser.equalsIgnoreCase("Chrome"))
        {
            driver = new ChromeDriver();
        }
	else if (browser.equalsIgnoreCase("InternetExplorer")) 
        {
            driver = new InternetExplorerDriver();
        }
	driver.manage().window().maximize();
	return driver;
    }
    
    /**
	 * This method is used to log in by providing the user credentials
	 * 
	 * @param driver
	 *            - the reference of the WebDriver object initialized in the
	 *            TestNG class
	 * @param id
	 *            String - email address used to login to the vendor panel
	 * @param password
	 *            String - the corresponding password of the vendor
	 */
	public void login(WebDriver driver, String id, String password) 
        {
            WaitUtility.waitForElementToAppeaerById("j_username", driver);
            driver.findElement(By.id("j_username")).clear();
            driver.findElement(By.id("j_username")).sendKeys(id);
            driver.findElement(By.name("j_password")).clear();
            driver.findElement(By.name("j_password")).sendKeys(password);
            driver.findElement(By.xpath(".//*[@id='loginform']/div[1]/div[4]/input")).click();
        }
        
        public void waitForElementBylinkText(String linkText, WebDriver driver)
        {
            WaitUtility.waitForElementToAppeaerBylinkText(linkText, driver);
        }
    
        public void waitForElementById(String id, WebDriver driver)
        {
            WaitUtility.waitForElementToAppeaerById(id, driver);
        }
        
        public ArrayList<String> getPincodeCourierMappingDataFromUI(List<WebElement> table_data, int totalPages,
                WebDriver driver, String pagerClass, String waitId)
        {
            String temp = "";
            ArrayList<String> values_from_ui = new ArrayList<String>();
            
            for(int j=1;j<=totalPages;j++)
            {
                driver.findElement(By.className(pagerClass)).clear();
                driver.findElement(By.className(pagerClass)).sendKeys(String.valueOf(j));
                driver.findElement(By.className(pagerClass)).sendKeys(Keys.RETURN);
                WaitUtility.waitUntilElemetIsDisAppearedById(waitId, driver);
                List<WebElement> tbody_containt = driver.findElements(By.xpath("id('shippingLocationGrid')/tbody/tr"));
                for (int i=2;i<=tbody_containt.size();i++)
                {
                    temp = "";
                    try
                    {
                        temp = driver.findElement(By.xpath("id('shippingLocationGrid')/tbody/tr["+i+"]/td[1]")).getText()+"|"+
                               driver.findElement(By.xpath("id('shippingLocationGrid')/tbody/tr["+i+"]/td[2]")).getText()+"|"+
                               driver.findElement(By.xpath("id('shippingLocationGrid')/tbody/tr["+i+"]/td[3]")).getText()+"|"+
                               driver.findElement(By.xpath("id('shippingLocationGrid')/tbody/tr["+i+"]/td[4]")).getText();
                        values_from_ui.add(temp);
                    }
                    catch(IndexOutOfBoundsException e)
                    {
                        return null;
                    }
                }  
            }
            return values_from_ui;
        }
        
        public ArrayList<String> getSupcBannedZipmapping(List<WebElement> table_data, int totalPages,
                WebDriver driver, String pagerClass, String waitId)
        {
            String temp = "";
            ArrayList<String> values_from_ui = new ArrayList<String>();
            
            for(int j=1;j<=totalPages;j++)
            {
                driver.findElement(By.className(pagerClass)).clear();
                driver.findElement(By.className(pagerClass)).sendKeys(String.valueOf(j));
                driver.findElement(By.className(pagerClass)).sendKeys(Keys.RETURN);
                WaitUtility.waitUntilElemetIsDisAppearedById(waitId, driver);
                List<WebElement> tbody_containt = driver.findElements(By.xpath("id('shippingReachGrid')/tbody/tr"));
                for (int i=2;i<=tbody_containt.size();i++)
                {
                    temp = "";
                    try
                    {
                        temp = driver.findElement(By.xpath("id('shippingReachGrid')/tbody/tr["+i+"]/td[1]")).getText()+"|"+
                               driver.findElement(By.xpath("id('shippingReachGrid')/tbody/tr["+i+"]/td[3]")).getText()+"|"+
                               driver.findElement(By.xpath("id('shippingReachGrid')/tbody/tr["+i+"]/td[7]")).getText()+"|"+
                               driver.findElement(By.xpath("id('shippingReachGrid')/tbody/tr["+i+"]/td[9]")).getText();
                        values_from_ui.add(temp);
                    }
                    catch(IndexOutOfBoundsException e)
                    {
                        return null;
                    }
                }  
            }
            return values_from_ui;
        }
        
        public ArrayList<String> getCategoryBannedZipmapping(List<WebElement> table_data, int totalPages,
                WebDriver driver, String pagerClass, String waitId)
        {
            String temp = "";
            ArrayList<String> values_from_ui = new ArrayList<String>();
            
            for(int j=1;j<=totalPages;j++)
            {
                driver.findElement(By.className(pagerClass)).clear();
                driver.findElement(By.className(pagerClass)).sendKeys(String.valueOf(j));
                driver.findElement(By.className(pagerClass)).sendKeys(Keys.RETURN);
                WaitUtility.waitUntilElemetIsDisAppearedById(waitId, driver);
                List<WebElement> tbody_containt = driver.findElements(By.xpath("id('shippingReachGrid')/tbody/tr"));
                for (int i=2;i<=tbody_containt.size();i++)
                {
                    temp = "";
                    try
                    {
                        temp = driver.findElement(By.xpath("id('shippingReachGrid')/tbody/tr["+i+"]/td[1]")).getText()+"|"+
                               driver.findElement(By.xpath("id('shippingReachGrid')/tbody/tr["+i+"]/td[2]")).getText()+"|"+
                               driver.findElement(By.xpath("id('shippingReachGrid')/tbody/tr["+i+"]/td[7]")).getText()+"|"+
                               driver.findElement(By.xpath("id('shippingReachGrid')/tbody/tr["+i+"]/td[9]")).getText();
                        values_from_ui.add(temp);
                    }
                    catch(IndexOutOfBoundsException e)
                    {
                        return null;
                    }
                }  
            }
            return values_from_ui;
        }
        
        public ArrayList<String> getFcBannedZipmapping(List<WebElement> table_data, int totalPages,
                WebDriver driver, String pagerClass, String waitId)
        {
            String temp = "";
            ArrayList<String> values_from_ui = new ArrayList<String>();
            
            for(int j=1;j<=totalPages;j++)
            {
                driver.findElement(By.className(pagerClass)).clear();
                driver.findElement(By.className(pagerClass)).sendKeys(String.valueOf(j));
                driver.findElement(By.className(pagerClass)).sendKeys(Keys.RETURN);
                WaitUtility.waitUntilElemetIsDisAppearedById(waitId, driver);
                List<WebElement> tbody_containt = driver.findElements(By.xpath("id('shippingReachGrid')/tbody/tr"));
                for (int i=2;i<=tbody_containt.size();i++)
                {
                    temp = "";
                    try
                    {
                        temp = driver.findElement(By.xpath("id('shippingReachGrid')/tbody/tr["+i+"]/td[1]")).getText()+"|"+
                               driver.findElement(By.xpath("id('shippingReachGrid')/tbody/tr["+i+"]/td[6]")).getText()+"|"+
                               driver.findElement(By.xpath("id('shippingReachGrid')/tbody/tr["+i+"]/td[7]")).getText()+"|"+
                               driver.findElement(By.xpath("id('shippingReachGrid')/tbody/tr["+i+"]/td[9]")).getText();
                        values_from_ui.add(temp);
                    }
                    catch(IndexOutOfBoundsException e)
                    {
                        return null;
                    }
                }  
            }
            return values_from_ui;
        }
        
        public void waitUntilElemetIsDisAppearedById(String id, WebDriver driver)
        {
            WaitUtility.waitUntilElemetIsDisAppearedById(id, driver);
        }
        
        public int getCurrentCountOfDataInGrid(List<WebElement> table_data, WebDriver driver, String pagerClass)
        {
            String temp = "";
            ArrayList<String> values_from_ui = new ArrayList<String>();
            List<WebElement> tbody_containt = driver.findElements(By.xpath("id('shippingLocationGrid')/tbody/tr"));
            for (int i=2;i<=tbody_containt.size();i++)
            {
                temp = "";
                try
                {
                    temp = driver.findElement(By.xpath("id('shippingLocationGrid')/tbody/tr["+i+"]/td[1]")).getText()+"|"+
                           driver.findElement(By.xpath("id('shippingLocationGrid')/tbody/tr["+i+"]/td[2]")).getText()+"|"+
                           driver.findElement(By.xpath("id('shippingLocationGrid')/tbody/tr["+i+"]/td[3]")).getText()+"|"+
                           driver.findElement(By.xpath("id('shippingLocationGrid')/tbody/tr["+i+"]/td[4]")).getText();
                     values_from_ui.add(temp);
                }
                catch(IndexOutOfBoundsException e)
                {
                    return 0;
                }
            }
            return values_from_ui.size();
        }
        
        public ArrayList<String> getCategoryShippingModeMappingFromUI(WebDriver driver)
        {
            String temp = "";
            ArrayList<String> values_from_ui = new ArrayList<String>();
            List<WebElement> tbody_containt = driver.findElements(By.xpath("id('shippingModeGrid')/tbody/tr"));
            for (int i=2;i<=tbody_containt.size();i++)
            {
                temp = "";
                try
                {
                    temp = driver.findElement(By.xpath("id('shippingModeGrid')/tbody/tr["+i+"]/td[1]")).getText()+"|"+
                           driver.findElement(By.xpath("id('shippingModeGrid')/tbody/tr["+i+"]/td[2]")).getText()+"|"+
                           driver.findElement(By.xpath("id('shippingModeGrid')/tbody/tr["+i+"]/td[4]")).getText();
                    System.out.println("temp:"+temp);
                     values_from_ui.add(temp);
                }
                catch(IndexOutOfBoundsException e)
                {
                    return null;
                }
            }
            return values_from_ui;
        }
}
