package com.groupon.web.common.utils;

/**
 * 
 * @author prachi.nagpal
 *
 */

public class PerfLogMessage implements LogMessage {

	private String method;
	private String pageUrl;
	private Long responseStatus;
	private String responseUrl;
	private String responseType;
	private String responseMIMEType;
	private String requestHeaders;

	public String getMethod() {
		return method;
	}

	public void setMethod(String method) {
		this.method = method;
	}

	public String getPageUrl() {
		return pageUrl;
	}

	public void setPageUrl(String pageUrl) {
		this.pageUrl = pageUrl;
	}

	public Long getResponseStatus() {
		return responseStatus;
	}

	public void setResponseStatus(Long responseStatus) {
		this.responseStatus = responseStatus;
	}

	public String getResponseUrl() {
		return responseUrl;
	}

	public void setResponseUrl(String responseUrl) {
		this.responseUrl = responseUrl;
	}

	public String getResponseType() {
		return responseType;
	}

	public void setResponseType(String responseType) {
		this.responseType = responseType;
	}

	public String getResponseMIMEType() {
		return responseMIMEType;
	}

	public void setResponseMIMEType(String responseMIMEType) {
		this.responseMIMEType = responseMIMEType;
	}

	public String getRequestHeaders() {
		return requestHeaders;
	}

	public void setRequestHeaders(String requestHeaders) {
		this.requestHeaders = requestHeaders;
	}
	
	//TODO:ToString tab delimited
	@Override
	public String toString() {

		String errorStr = this.getMethod() + "\t" + this.getPageUrl() + "\t" + this.getResponseStatus() + "\t" + this.getResponseType() + this.getResponseMIMEType() + this.getRequestHeaders();
		return errorStr;


	}

}
