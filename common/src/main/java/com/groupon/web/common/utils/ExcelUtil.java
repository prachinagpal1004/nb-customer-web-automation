package com.groupon.web.common.utils;

import org.apache.commons.lang3.ArrayUtils;
import org.apache.log4j.Logger;
import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.Font;
import org.apache.poi.ss.usermodel.IndexedColors;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;
import org.apache.poi.xssf.usermodel.XSSFCellStyle;
import org.junit.Assert;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.Iterator;
import java.util.List;

public class ExcelUtil {

    private static final Logger LOGGER = Logger.getLogger(ExcelUtil.class);

    public static String[][] readExcelData(String fileName, String sheetName, boolean includeHeaders) {
        LOGGER.info("Read Excel Data:"+fileName+":"+sheetName);
        InputStream is = ExcelUtil.class.getClassLoader().getResourceAsStream(fileName);
        final Workbook workbook;
        String[][] tabArray;
        try {
            workbook = getWorkbook(is);
            Sheet sheet = getSheet(workbook, sheetName);
            tabArray = getSheetContent(includeHeaders, sheet);
            is.close();
        } catch (IOException e) {
            LOGGER.error("Error in Excel Data read", e);
            throw new RuntimeException(e);
        }
        LOGGER.info("Excel Data read:"+ ArrayUtils.toString(tabArray));
        return tabArray;
    }

    public static String[][] readExcelData(String fileName, int sheetNum, boolean includeHeaders) throws IOException {
        InputStream is = ExcelUtil.class.getClassLoader().getResourceAsStream(fileName);
        final Workbook workbook = getWorkbook(is);
        Sheet sheet = getSheet(workbook, sheetNum);
        String[][] tabArray = getSheetContent(includeHeaders, sheet);
        is.close();
        return tabArray;
    }

    public static void createExcelFile(String fileName, String sheetName, boolean deleteIfExists) throws IOException, URISyntaxException {
        final URL url = ExcelUtil.class.getClassLoader().getResource(fileName);
        boolean create = url == null || deleteIfExists;
        if(create) {
            if(url != null) {
                final File file = new File(url.toURI());
                if(file.exists()) {
                    if(!file.delete()) {
                        throw new RuntimeException("Old File exists, Please delete it first");
                    }
                }
            }
            final String parentDir = fileName.substring(0, fileName.indexOf("/"));
            final String childFileName = fileName.substring(fileName.indexOf("/") + 1);
            System.out.println(parentDir);
            System.out.println(childFileName);
            final URL parentResource = ExcelUtil.class.getClassLoader().getResource(parentDir);
            if(parentResource == null) {
                throw new RuntimeException("output directory not exists in resources folder");
            }
            final String fqFileName = new File(parentResource.toURI()).getAbsolutePath() + File.separator + childFileName;
            System.out.println(fqFileName);
            final File file = new File(fqFileName);
            if(!file.createNewFile()) {
                throw new RuntimeException("Create file failed at path:"+fqFileName);
            }

            HSSFWorkbook workbook = new HSSFWorkbook();
            workbook.createSheet(sheetName);
            FileOutputStream out = new FileOutputStream(file);
            workbook.write(out);
            out.close();
        }
    }

    public static void setColValues(String fileName, String sheetName, int rowNum, List<String> values, int seed) throws IOException, URISyntaxException {
        final URL resource = ExcelUtil.class.getClassLoader().getResource(fileName);
        if(resource == null) {
            throw new IllegalArgumentException("Given File Name not exists:"+fileName);
        }
        File file = new File(resource.toURI());
        final FileInputStream is = new FileInputStream(file);
        final Workbook workbook = getWorkbook(is);
        final Sheet sheet = getSheet(workbook, sheetName);
        Row row = sheet.getRow(rowNum);
        if(row == null) {
            row = sheet.createRow(rowNum);
        }
        int colNum = seed;
        for (String value : values) {
            Cell cell = row.getCell(colNum);
            if(cell == null) {
                cell = row.createCell(colNum);
            }
           
            cell.setCellValue(value);
            colNum++;
        }
        is.close();

        FileOutputStream os = new FileOutputStream(file);
        workbook.write(os);
        os.close();

    }

    public static void setColValues(String fileName, String sheetName, List<List<String>> rows, int seed) throws IOException, URISyntaxException {
        final URL resource = ExcelUtil.class.getClassLoader().getResource(fileName);
        if(resource == null) {
            throw new IllegalArgumentException("Given File Name not exists:"+fileName);
        }
        File file = new File(resource.toURI());
        final FileInputStream is = new FileInputStream(file);
        final Workbook workbook = getWorkbook(is);
        final Sheet sheet = getSheet(workbook, sheetName);
        int rowNum = 0;
        for (List<String> values : rows) {
            Row row = sheet.getRow(rowNum);
            if(row == null) {
                row = sheet.createRow(rowNum);
            }
            int colNum = seed;
            for (String value : values) {
                Cell cell = row.getCell(colNum);
                if(cell == null) {
                    cell = row.createCell(colNum);
                }
                cell.setCellValue(value);
                colNum++;
            }
            rowNum++;
        }
        is.close();
        FileOutputStream os = new FileOutputStream(file);
        workbook.write(os);
        os.close();

    }

    private static Workbook getWorkbook(InputStream is) throws IOException {
        Workbook workbook;
        try {
            workbook = WorkbookFactory.create(is);
        } catch (InvalidFormatException e) {
            throw new IOException(e);
        }
        return workbook;
    }

    private static Sheet getSheet(Workbook workbook, String sheetName) {
        return workbook.getSheet(sheetName);
    }

    private static Sheet getSheet(Workbook workbook, int sheetNum) {
        return workbook.getSheetAt(sheetNum);
    }

    private static String[][] getSheetContent(boolean includeHeaders, org.apache.poi.ss.usermodel.Sheet sheet) {
        int headPos = 0;
        if(! includeHeaders)
            headPos = 1;
        if(sheet==null){
        	Assert.assertNotNull("Sheet is Null",sheet);
        }
        int physicalNumberOfRows = sheet.getPhysicalNumberOfRows();
		int physicalNumberOfCells = sheet.getRow(0).getPhysicalNumberOfCells();
		String[][] tabArray = new String[physicalNumberOfRows - headPos][physicalNumberOfCells];

        Iterator<Row> rowIterator = sheet.rowIterator();
        if (!includeHeaders)
            rowIterator.next();

        int i=0;
        boolean end = false;
        while (rowIterator.hasNext()){
            Row row	=	rowIterator.next();
            int j=0;
            Iterator<Cell> cellIterator	= row.cellIterator();
            while(cellIterator.hasNext())
            {
                HSSFCell cell	=	(HSSFCell) cellIterator.next();
                if(cell.getCellType() == Cell.CELL_TYPE_BLANK)
                {
                    if(j == 0) {
                        end = true;
                        break;
                    }
                    tabArray[i][j] = null;
                }
                else {
                    cell.setCellType(Cell.CELL_TYPE_STRING);
                    if(cell.toString().trim().equals("null"))
                    {
                        tabArray[i][j]	=	null;
                    }
                    else if(j == 0 && cell.toString().trim().equalsIgnoreCase("br")) {
                        break;
                    }
                    else{
                        tabArray[i][j]	=	cell.getStringCellValue();
                    }
                }
                j++;
            }
            if(end) {
                break;
            }
            i++;
        }
        return tabArray;
    }
}
