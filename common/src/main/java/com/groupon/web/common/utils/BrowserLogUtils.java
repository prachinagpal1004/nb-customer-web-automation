package com.groupon.web.common.utils;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.testng.Assert;

/**
 * 
 * @author prachi.nagpal
 *
 */

public class BrowserLogUtils {

	private static final Logger LOGGER = Logger.getLogger(BrowserLogUtils.class);

	private static List<JSError> finalChromeJSErrors = new ArrayList<JSError>();
	private static List<JSError> perfLogs = new ArrayList<JSError>();

	public static void copyChromeJSError(List<JSError> logEntries) {
		synchronized (finalChromeJSErrors) {
			finalChromeJSErrors.addAll(new ArrayList<JSError>(logEntries));
		}
	}

	public static void copyPerfLogs(List<JSError> perfLogEntries) {
		synchronized (perfLogs) {
			perfLogs.addAll(new ArrayList<JSError>(perfLogEntries));
		}
	}

	public static void writeChromeJSError(String fileLocation) {
		System.out.println("Checking JS errors for CHROME");
		File file = new File(fileLocation);
		cleanJSFile(file);
		if(finalChromeJSErrors.isEmpty()) {
			LOGGER.info("No JS errors");
		}
		else {
			writeJSlogFile(file, finalChromeJSErrors, null);
			LOGGER.info("Browser Logs written in : " + file.getAbsolutePath());
		}
	}

	public static void writePerfLogs(String fileLocation, List<? extends LogMessage> logs, String header) {
		System.out.println("Checking PerfLogs");
		if(logs == null) {
			logs = perfLogs;
		}
		File file = new File(fileLocation);
		cleanJSFile(file);
		if(logs.isEmpty()) {
			LOGGER.info("No Performance Logs");
		}
		else {
			writeJSlogFile(file, logs, header);
			LOGGER.info("Performance Logs written in : " + file.getAbsolutePath());
		}
	}

	public static void cleanJSFile(File file) {

		// delete if file exists
		if (file.exists()){
			if(!file.delete()) {
				Assert.fail("File exists but not able to delete it");
			}
		}
		try {
            File parentFile = new File(file.getParent());
            if(!parentFile.exists()) {
                parentFile.mkdirs();
            }
			file.createNewFile();
		} catch (IOException e) {
			LOGGER.error("Error cleaning JS file:"+file.getName(), e);
		}

	}

	/**
	 * Writing JS errors into their respective browser's log file
	 */
	private static void writeJSlogFile(File file, List<? extends LogMessage> logs, String header){
		if(header == null) {
			header = "URL" + "\t" + "LOG_LEVEL" + "\t" + "TIMESTAMP" + "\t" + "MESSAGE";
		}
		try {
			FileWriter fw = new FileWriter(file.getAbsoluteFile(), true);
			BufferedWriter bw = new BufferedWriter(fw);
			if(header != null) {
				fw.append(header);
				fw.write("\n");
			}

			for(LogMessage log : logs) {
				bw.write(log.toString());
				bw.write("\n");
			}
			bw.close();
		} 
		catch (IOException i) {
            LOGGER.error("Error writing JS log file:" + file.getName(), i);
		}

	}

	/**
	 * Test 404
	 * @param filterMethodName
	 * @return
	 */
	public static List<PerfLogMessage> readPerfLogs(String brand, String fileLocation, String filterMethodName){
		List<PerfLogMessage> perfLogMessages = new ArrayList<PerfLogMessage>();
		JSONParser parser = new JSONParser();
		BufferedReader br = null;
		try {
            String cvsSplitBy = "\t";
			br = new BufferedReader(new FileReader(fileLocation));
			br.readLine();
			String line = "";
			
			while ((line = br.readLine()) != null) {

				// use tab as separator
				String[] perfLog = line.split(cvsSplitBy);

				try {
					JSONObject jsonObject =	(JSONObject) parser.parse(perfLog[3]);

					if(jsonObject != null) {
						JSONObject messageJson = (JSONObject)jsonObject.get("message");
						String method = (String)messageJson.get("method");

						if(method != null) {
							boolean allow = filterMethodName != null ? method.equalsIgnoreCase(filterMethodName) : true;

							if(allow) {
								JSONObject paramJson = (JSONObject)messageJson.get("params");
								JSONObject responseJson = (JSONObject)paramJson.get("response");
								Long status = (Long)responseJson.get("status");

								if(status != null) {
									PerfLogMessage perfLogMessage = new PerfLogMessage();
									perfLogMessage.setMethod(method);
									perfLogMessage.setPageUrl(perfLog[0]);
									perfLogMessage.setResponseStatus(status);
									perfLogMessage.setResponseUrl((String)responseJson.get("url"));
									perfLogMessage.setResponseType((String)paramJson.get("type"));
									perfLogMessage.setResponseMIMEType((String)responseJson.get("mimeType"));

									JSONObject responseHeaders = (JSONObject)responseJson.get("headers");
									if(responseHeaders != null) {
										perfLogMessage.setRequestHeaders(responseHeaders.toJSONString());
									}

									JSONObject requestHeaders = (JSONObject)responseJson.get("requestHeaders");
									if(requestHeaders != null) {
										perfLogMessage.setRequestHeaders(requestHeaders.toJSONString());
									}
									LOGGER.info("Performance Log Message: " + perfLogMessage);
									LOGGER.info("\n");

									perfLogMessages.add(perfLogMessage);
								}
							}
						}

					}

				} catch (org.json.simple.parser.ParseException e) {
					throw new RuntimeException(e);
				}

			}

		} catch (FileNotFoundException e) {
			throw new RuntimeException(e);
		} catch (IOException e) {
			throw new RuntimeException(e);
		} finally {
			if (br != null) {
				try {
					br.close();
				} catch (IOException e) {
					throw new RuntimeException(e);
				}
			}
		}
		return perfLogMessages;
	}

    public static String getLogDirPathForGivenBrand() {
        final String separator = File.separator;
        StringBuilder sb = new StringBuilder(System.getProperty("user.dir"));
        sb.append(separator).append("logs").append(separator);
        return sb.toString();
    }

}

