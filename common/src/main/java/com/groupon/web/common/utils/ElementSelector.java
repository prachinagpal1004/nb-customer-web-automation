package com.groupon.web.common.utils;

/**
 *
 * @author prachi.nagpal
 *
 */
public class ElementSelector {

    private String key;
    private String id;
    private String css;
    private String xpath;
    private String name;
    private String className;
    private String linkText;

    public ElementSelector(String key) {
        this.key = key;
    }

    public String getKey() {
        return key;
    }

    public String getLinkText() {
        return linkText;
    }

    public void setLinkText(String linkText) {
        this.linkText = linkText;
    }

    public String getClassName() {
        return className;
    }

    public void setClassName(String className) {
        this.className = className;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getXpath() {
        return xpath;
    }

    public void setXpath(String xpath) {
        this.xpath = xpath;
    }

    public String getCss() {
        return css;
    }

    public void setCss(String css) {
        this.css = css;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    @Override
    public String toString() {
        return "ElementSelector{" +
                "key='" + key + '\'' +
                ", id='" + id + '\'' +
                ", css='" + css + '\'' +
                ", xpath='" + xpath + '\'' +
                ", name='" + name + '\'' +
                ", className='" + className + '\'' +
                ", linkText='" + linkText + '\'' +
                '}';
    }
}
