package com.groupon.web.common.utils;

import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class DatePickerFromCalenderUtility {

	WebDriver driver;

	public String datePickerFromCalender() throws IOException{  

	       DateFormat dateFormat2 = new SimpleDateFormat("dd"); 
           Date date2 = new Date();

           String today = dateFormat2.format(date2); 

           //find the calendar
           WebElement dateWidget = driver.findElement(By.id("datepicker"));  
           List<WebElement> columns=dateWidget.findElements(By.tagName("td"));  

           //comparing the text of cell with today's date and clicking it.
           for (WebElement cell : columns)
           {
              if (cell.getText().equals(today))
              {
                 cell.click();
                 break;
              }
           }
		return today;
	}
}