/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.groupon.web.common.utils;

/**
 * 
 * @author prachi.nagpal
 *
 */
/*
public class DatabaseUtility
{
    protected static Connection connection = null;
    private static void createConnection() throws IOException, ClassNotFoundException
    {
	Config config = Config.getInstance();
	String dbUserName = config.getConfig("DataBase.UserName");
	String dbPassWord = config.getConfig("DataBase.Password");
	String dbEndpoint = config.getConfig("DataBase.EndPoint");
	String dbName = config.getConfig("DataBase.DBName");

	String URL = "jdbc:mysql://" + dbEndpoint + "/" + dbName;

	try
        {
            Class.forName("com.mysql.jdbc.Driver");
            connection = DriverManager.getConnection(URL, dbUserName,dbPassWord);

	} 
        catch (SQLException e)
        {
            e.printStackTrace();
	}
    }
    
    public static ArrayList<String> getPincodeCourierMappingData(String query) throws IOException, ClassNotFoundException, SQLException
    {
        String temp;
        createConnection();
        Statement stmt = connection.createStatement();
        ResultSet records = stmt.executeQuery(query);
        ArrayList<String> arrayList = new ArrayList<String>();
        while(records.next())
        {
            temp = "";
            temp = records.getString(1)+"|"+records.getString(2)+"|"+records.getString(3)+"|"+records.getString(4);
            temp = temp.replaceAll("\\s","");
            arrayList.add(temp);
        }
        connection.close();
        return arrayList;
    }
    
    public static String getSingleStringRecord(String query) throws IOException, ClassNotFoundException, SQLException
    {
        createConnection();
        Statement stmt = connection.createStatement();
        ResultSet records = stmt.executeQuery(query);
        String temp;
        if(records.next())
        {
            temp = records.getString(1);
        }
        else
        {
            temp =  null;
        }
        connection.close();
        return temp;
    }
    
    public static ArrayList<String> getCourierShippingModeMapping(String query) throws IOException, ClassNotFoundException, SQLException
    {
        String temp;
        createConnection();
        Statement stmt = connection.createStatement();
        ResultSet records = stmt.executeQuery(query);
        ArrayList<String> arrayList = new ArrayList<String>();
        while(records.next())
        {
            temp = "";
            temp = records.getString(1)+"|"+records.getString(2)+"|"+records.getString(3);
            temp = temp.replaceAll("\\s","");
            arrayList.add(temp);
        }
        connection.close();
        return arrayList;
    }
    
    public static ArrayList<String> getRecordsAsSingleList(String query) throws IOException, ClassNotFoundException, SQLException
    {
        createConnection();
        Statement stmt = connection.createStatement();
        ResultSet records = stmt.executeQuery(query);
        ArrayList<String> arrayList = new ArrayList<String>();
        while(records.next())
        {
            arrayList.add(records.getString(1));
        }
        connection.close();
        return arrayList;
    }
    
    public static void executeDeleteQuery(String query) throws IOException, ClassNotFoundException, SQLException
    {
        createConnection();
        Statement stmt = connection.createStatement();
        stmt.executeUpdate(query);
        connection.close();
    }
    
    public static ArrayList<String> getBannedzipPatternviaSupc(String query) throws IOException, ClassNotFoundException, SQLException
    {
        createConnection();
        String temp;
        Statement stmt = connection.createStatement();
        ResultSet records = stmt.executeQuery(query);
        ArrayList<String> arrayList = new ArrayList<String>();
        while(records.next())
        {
            temp = "";
            temp = records.getString(1)+"|"+records.getString(2)+"|"+records.getString(3)+"|"+records.getString(4);
            temp = temp.replaceAll("\\s","");
            arrayList.add(temp);
        }
        connection.close();
        return arrayList;
    }
}
*/
