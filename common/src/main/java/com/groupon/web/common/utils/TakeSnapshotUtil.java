package com.groupon.web.common.utils;

import java.io.File;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import org.apache.commons.io.FileUtils;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;

public class TakeSnapshotUtil{

	public static void fullPageScreenshot(WebDriver driver) throws Exception { 
		DateFormat dateFormat = new SimpleDateFormat("yyyy_MMM_dd HH_mm_ss");   
		Date date = new Date();           
		String time=dateFormat.format(date);  
		System.out.println(time);       
		File f = ((TakesScreenshot)driver).getScreenshotAs(OutputType.FILE);        
		//FileUtils.copyFile(f, new File("~/webPageTestReport_"+time+".png")); // Linux machine path      
		FileUtils.copyFile(f,new File(System.getProperty("user.dir")+File.separator+"target"+File.separator+"webPageTestReport_"+time));
		
	}
	
}
