package com.groupon.web.common.exception;

/**
 * 
 * @author prachi.nagpal
 *
 */
public class NotAuthorizedException extends Exception {

    public NotAuthorizedException(String message) {
        super(message);
    }

    public NotAuthorizedException(String message, Throwable cause) {
        super(message, cause);
    }

    public NotAuthorizedException(Throwable cause) {
        super(cause);
    }
}
