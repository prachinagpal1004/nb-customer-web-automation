package com.groupon.web.common.base;

import java.io.File;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.logging.Level;

import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Platform;
import org.openqa.selenium.UnexpectedAlertBehaviour;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxProfile;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.logging.LogType;
import org.openqa.selenium.logging.LoggingPreferences;
import org.openqa.selenium.remote.CapabilityType;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;

/**
 * 
 * @author prachi.nagpal
 *
 */

public class DriverFactory {

	private static final Logger LOGGER = Logger.getLogger(DriverFactory.class);
	public static final String HTTP = "http://";
	public static final String FIREFOX = "firefox";
	public static final String WEB = "web";
	public static final String MOBILE = "mobile";
	public static final String CHROME = "chrome";
	public static final String IE = "ie";

	public WebDriver create(String browser, boolean remote, String remoteUrl, String platform) throws Exception {

		WebDriver driver = null;
		URL remoteLocator = null;
		if(remote) {
			remoteLocator = prepareRemoteURL(remoteUrl);
		}

		if(platform.equalsIgnoreCase(WEB)){

			if (browser.equalsIgnoreCase(FIREFOX)) {
				DesiredCapabilities capabilities = DesiredCapabilities.firefox();
				updateCommonCapabilities(capabilities);
				if(remote) {
					FirefoxProfile ffProfile = new FirefoxProfile();
					ffProfile.setAssumeUntrustedCertificateIssuer(false);
					driver = new RemoteWebDriver(remoteLocator, capabilities);
				}
				else {
					FirefoxProfile ffProfile = new FirefoxProfile();
					ffProfile.setAssumeUntrustedCertificateIssuer(true);
					ffProfile.setAcceptUntrustedCertificates(true);
					//ffProfile.setPreference("dom.max_script_run_time", 60);
					capabilities.setCapability(FirefoxDriver.PROFILE, ffProfile);
					//capabilities.setCapability(CapabilityType.UNEXPECTED_ALERT_BEHAVIOUR, UnexpectedAlertBehaviour.IGNORE);
					driver = new FirefoxDriver(capabilities);
				}
			}
			else if (browser.equalsIgnoreCase(CHROME)) {
				DesiredCapabilities capabilities = DesiredCapabilities.chrome();
				updateCommonCapabilities(capabilities);

				/*
			//enable tracing
			Map<String, Object> perfLogPrefs = new HashMap<String, Object>();
			perfLogPrefs.put("traceCategories", "browser,devtools.timeline,devtools");
			ChromeOptions options = new ChromeOptions();
			options.setExperimentalOption("perfLoggingPrefs", perfLogPrefs);
			capabilities.setCapability(ChromeOptions.CAPABILITY, options);
				 */

				if(remote) {
					driver = new RemoteWebDriver(remoteLocator, capabilities);
				}
				else {
					final Platform OS = Platform.getCurrent();
					String driverPath = "win" + File.separator + "chromedriver.exe"; //default windows
					if(Platform.MAC.equals(OS)) {
						driverPath = "mac" + File.separator + "chromedriver";
					}
					System.setProperty("webdriver.chrome.driver", getDriverConfigDir() + File.separator + driverPath);
					driver = new ChromeDriver(capabilities);
				}
				if(Platform.MAC.equals(Platform.getCurrent())) {
					JavascriptExecutor js = ((JavascriptExecutor)driver);
					final String windowName = Thread.currentThread().getName();
					js.executeScript("window.open('','" + windowName + "','width=2000,height=800,top=0,left=0')");
					driver.close();
					driver.switchTo().window(windowName);
				}
			}
			else if (browser.equalsIgnoreCase(IE)) {
				System.setProperty("webdriver.ie.driver", getDriverConfigDir() + File.separator + "IEDriverServer.exe");
				DesiredCapabilities capabilities = DesiredCapabilities.internetExplorer();
				capabilities.setJavascriptEnabled(true);
				if(remote) {
					capabilities.setCapability("ignoreProtectedModeSettings",true);
					capabilities.setCapability(InternetExplorerDriver.INTRODUCE_FLAKINESS_BY_IGNORING_SECURITY_DOMAINS, true);
					driver = new RemoteWebDriver(remoteLocator, capabilities);
				}
				else {
					driver = new InternetExplorerDriver(capabilities);

				}
			}
			else {
				throw new IllegalArgumentException("Invalid Browser:"+browser);
			}

			driver.manage().window().maximize();

		}

		else if (platform.equalsIgnoreCase(MOBILE)){
			
			DesiredCapabilities capabilities = new DesiredCapabilities();
			capabilities.setCapability("platformName", "Android");
			capabilities.setCapability("platformVersion", "5.0.2");
			capabilities.setCapability("deviceName", "TA934001HI");
			capabilities.setCapability("browserName", "Chrome");

//			capabilities.setCapability("platformName", "iOS");
//			capabilities.setCapability("platformVersion", "9.3.1");
//			capabilities.setCapability("deviceName", "iPhone 6 (9.3)");
//			capabilities.setCapability("browserName", "Safari");
			
			URL url = new URL("http://127.0.0.1:4723/wd/hub");
			driver = new RemoteWebDriver(url, capabilities);

		}

		return driver;
	}

	private void updateCommonCapabilities(DesiredCapabilities capabilities) {
		capabilities.setJavascriptEnabled(true);
		LoggingPreferences loggingprefs = new LoggingPreferences();
		loggingprefs.enable(LogType.BROWSER, Level.ALL);
		loggingprefs.enable(LogType.PERFORMANCE, Level.ALL);
		capabilities.setCapability(CapabilityType.LOGGING_PREFS, loggingprefs);
	}

	private URL prepareRemoteURL(String remoteUrl) {
		if(StringUtils.isBlank(remoteUrl)) {
			throw new IllegalArgumentException("Remote Url must be provided for remote execution");
		}
		if(!remoteUrl.startsWith(HTTP)) {
			remoteUrl = HTTP + remoteUrl;
		}
		if(!remoteUrl.substring(HTTP.length()-1).contains(":")) {
			throw new IllegalArgumentException("Remote Url must contain the port information");
		}
		remoteUrl = remoteUrl + "/wd/hub";
		LOGGER.info("Create Remote Webdriver Instance with remoteURl:" + remoteUrl);
		try {
			return new URL(remoteUrl);
		} catch (MalformedURLException e) {
			throw new RuntimeException("Error in initialize Remote URL for remote execution:"+remoteUrl, e);
		}
	}

	private String getDriverConfigDir() {
		return getClass().getResource("/driver").getFile();
	}

	public void destroy(WebDriver driver) {
		driver.close();
		driver.quit();
	}

}
