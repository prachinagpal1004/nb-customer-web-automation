package com.groupon.web.common.utils;

import java.io.IOException;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;

import com.google.api.services.sheets.v4.Sheets;
import com.google.api.services.sheets.v4.model.AppendValuesResponse;
import com.google.api.services.sheets.v4.model.UpdateValuesResponse;
import com.google.api.services.sheets.v4.model.ValueRange;

public class GoogleSheetUtils {
	
	private static final Logger logger = Logger.getLogger(GoogleSheetUtils.class);
	
	public static Map<String, Object> updateValues(String sheetId, String range, List<List<Object>> values) throws IOException {
		logger.info("Update values for SheetId:"+sheetId+", Range:"+range);
		Sheets service = ReadAndWriteGoogleSheet.getSheetsService();
		ValueRange requestBody = new ValueRange();
		requestBody.setValues(values);

		UpdateValuesResponse response = service.spreadsheets().values()
				.update(sheetId, range, requestBody)
				.setValueInputOption("USER_ENTERED")
				.execute();
		return response;
	}
	
	public static Map<String, Object> appendValues(String sheetId, String range, List<List<Object>> values) throws IOException {
		logger.info("Append values for SheetId:"+sheetId+", Range:"+range);
		Sheets service = ReadAndWriteGoogleSheet.getSheetsService();
		ValueRange requestBody = new ValueRange();
		requestBody.setValues(values);
		
		AppendValuesResponse response = service.spreadsheets().values()
				.append(sheetId, range, requestBody)
				.setValueInputOption("USER_ENTERED")
				.execute();
		return response;
	}
	
	public static List<List<Object>> getValues(String sheetId, String range) throws IOException {
		logger.info("Get values for SheetId:"+sheetId+", Range:"+range);
		Sheets service = ReadAndWriteGoogleSheet.getSheetsService();
		ValueRange response = service.spreadsheets().values()
				.get(sheetId, range)
				.execute();
		return response.getValues();
	}

}
