/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.groupon.web.common.config;

import org.apache.log4j.Logger;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.net.URL;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

/**
 * 
 * @author prachi.nagpal
 *
 */
public class Config {

    private static final Logger LOGGER = Logger.getLogger(Config.class);
	private static final String DEFAULT = "";
    public static final String GLOBAL_FILE_NAME = "global";
    public static final String COMMON_FILE_NAME = "common";
    private static Map<String, Config> instances = new HashMap<String, Config>();
	private Properties globalConfigs;
	private Properties envConfigs;
    public static String environment;
	public static String ext = ".cfg";

	public static void loadConfigFile(String env, String id) {
        LOGGER.info("Load Config File for environment: "+env);
		if(id == null) {
			id = DEFAULT;
		}
        if (instances.containsKey(id)) {
			return;
		}

        try {
            Config instance = new Config();
            String configLocation = instance.getConfigDirectory();

            //Load Global Config
            String globalConfigFile = configLocation + File.separator + GLOBAL_FILE_NAME + ext;
            loadGlobalConfigFiles(instance, globalConfigFile);

            //Load Environment specific config
            String commonConfigFile = configLocation + File.separator + id + File.separator + COMMON_FILE_NAME + ext;
            String envConfigFile = configLocation + File.separator + id + File.separator + env + ext;
            loadEnvConfigFiles(instance, commonConfigFile, envConfigFile);
            environment = env;
            instances.put(id, instance);

		} catch (IOException e) {
			throw new RuntimeException("Error in initializing Config files for env:"+env, e);
		}
	}

    private static void loadGlobalConfigFiles(Config instance, String... globalConfigFiles) throws IOException {
        LOGGER.info("Global Config File Location:"+Arrays.asList(globalConfigFiles));
        instance.globalConfigs = new Properties();
        for (String globalConfigFile : globalConfigFiles) {
            instance.globalConfigs.load(new FileInputStream(globalConfigFile));
        }

    }

    private static void loadEnvConfigFiles(Config instance, String... fqFileNames) throws IOException {
        LOGGER.info("Environment Config File Location:"+ Arrays.asList(fqFileNames));
        instance.envConfigs = new Properties();
        for (String fqFileName : fqFileNames) {
            instance.envConfigs.load(new FileInputStream(fqFileName));
        }

    }

    public static Config getInstance() {
		return getInstance(DEFAULT);
	}
	
	public static Config getInstance(String id) {
        final Config instance = instances.get(id);
        if (instance == null) {
            throw new RuntimeException("Initialize AppConfig");
        }
		return instance;
	}

	public String getConfig(String key) {
		Object value = null;

		value = envConfigs.get(key);
		if (value == null) {
			value = globalConfigs.get(key);
		}
		if (value != null) {
			return value.toString();
		} else {
			return null;
		}
	}

	private String getConfigDirectory() {
        URL resource = getClass().getResource("/config");
		return resource.getFile();
	}

	public static void main(String[] args) throws Exception {
		Config appConfig = Config.getInstance("samsung");
		System.out.println("appConfig.getConfig(key)===="
				+ appConfig.getConfig("key"));
	}
}
