package com.groupon.web.common.utils;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

/**
 *
 * @author prachi.nagpal
 *
 */
public class SelectorUtils {

    private static Map<String, Map<String, Map<String, ElementSelector>>> props = new HashMap<String, Map<String, Map<String, ElementSelector>>>();

    public static void loadData(String fileName, String... sheetNames) throws IOException {
        Map<String, Map<String, ElementSelector>> sheetData = props.get(fileName);
        if(sheetData == null) {
            sheetData = new HashMap<String, Map<String, ElementSelector>>();
        }
        for (String sheet : sheetNames) {
            Map<String, ElementSelector> selectorData = sheetData.get(sheet);
            if(selectorData == null) {
                selectorData = new HashMap<String, ElementSelector>();
            }
            selectorData.clear();
            final String[][] rows = ExcelUtil.readExcelData(fileName, sheet, false);
            for (String[] row : rows) {
                ElementSelector selector = prepareElementSelector(row);
                selectorData.put(selector.getKey(), selector);
            }
            sheetData.put(sheet, selectorData);
        }
        props.put(fileName, sheetData);
    }

    public static ElementSelector getSelector(String fileName, String sheetName, String key) {
        final Map<String, Map<String, ElementSelector>> fileData = props.get(fileName);
        return fileData != null ? fileData.containsKey(sheetName) ? fileData.get(sheetName).get(key) : null : null;
    }

    private static ElementSelector prepareElementSelector(String[] row) {
        ElementSelector selector = new ElementSelector(row[0]);
        selector.setId(row[1]);
        selector.setCss(row[2]);
        selector.setXpath(row[3]);
        selector.setName(row[4]);
        selector.setClassName(row[5]);
        selector.setLinkText(row[6]);
        return selector;
    }


}
