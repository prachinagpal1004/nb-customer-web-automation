package com.groupon.web.common.utils;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Date;

import com.groupon.web.common.config.Config;
import com.relevantcodes.extentreports.ExtentReports;
import com.relevantcodes.extentreports.NetworkMode;
import org.apache.log4j.Logger;

public class ExtentManagerUtil {

	private static final Logger logger = Logger.getLogger(ExtentManagerUtil.class);
	private static ExtentReports extent;

	public static ExtentReports getInstance() {
		if (extent == null) {
			String reportPath=null;
			File file= new File(System.getProperty("user.dir")+File.separator+
					Config.getInstance().getConfig("TestReport")+File.separator+
					Config.getInstance().getConfig("ReportPath"));
			if(!file.exists()){
				file.mkdirs();
			}

			SimpleDateFormat s = new SimpleDateFormat("dd-MM-yyyy HH:mm");
			String timestamp = s.format(new Date()).replace(" ", "_").replace(":", "-"); //for report file name

			reportPath = file.getPath()+File.separator+
					Config.getInstance().getConfig("ReportPath")+"_"+timestamp+".html";
			
			logger.info("Init Extent Reports, Report Path:"+reportPath);
			extent = new ExtentReports(reportPath, true, NetworkMode.ONLINE);
			//            
			//            // optional
			//            extent.config().documentTitle("Automation Report")
			//            .reportName("Regression")
			//            .reportHeadline("App");
			// optional
			extent.addSystemInfo("Environment", Config.environment.toUpperCase());
		}
		return extent;
	}


}