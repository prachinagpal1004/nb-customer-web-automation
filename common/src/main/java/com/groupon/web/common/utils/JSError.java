package com.groupon.web.common.utils;

import org.openqa.selenium.logging.LogEntry;
/**
 * 
 * @author prachi.nagpal
 *
 */

public class JSError implements LogMessage {

	private String url;
	private LogEntry chromeError;

	public JSError(String url, LogEntry chromeError) {
		this.url = url;
		this.chromeError = chromeError;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public LogEntry getChromeError() {
		return chromeError;
	}

	public void setChromeError(LogEntry chromeError) {
		this.chromeError = chromeError;
	}

	@Override
	public String toString() {

		String errorStr = this.getUrl() + "\t" + chromeError.getLevel() + "\t" + chromeError.getTimestamp() + "\t" + chromeError.getMessage();
		return errorStr;
	}
}
