package com.groupon.web.common.utils;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class DataProviderUtil {

	private static Map<String, Map<String, String[][]>> dataProvider = new HashMap<String, Map<String, String[][]>>();

	private static synchronized void prepareMap(String fileName, String sheetName) throws IOException{
        Map<String, String[][]> sheetData = dataProvider.get(fileName);
        if(sheetData == null) {
            sheetData = new HashMap<String, String[][]>();
        }
        if(!sheetData.containsKey(sheetName)) {
            sheetData.put(sheetName, ExcelUtil.readExcelData(fileName, sheetName, false));
        }
        dataProvider.put(fileName, sheetData);
	}
	public static String[][] provideData(String fileName, String sheetName, String dataProviderName){
		String[][] tabArray;
		try {
			prepareMap(fileName, sheetName);
		} catch (IOException e) {
			throw new RuntimeException(e);
		}
		tabArray = extractDataForDataProvider(dataProvider.get(fileName).get(sheetName), dataProviderName);
		return tabArray;
	}

	public static synchronized void clearTestData(String fileName){
		dataProvider.get(fileName).clear();
		dataProvider.remove(fileName);
	}

    public static synchronized void clearTestData(){
		dataProvider.clear();
	}

	private static String[][]  extractDataForDataProvider(String[][] tabArray,String dataProviderName){

		List<String[]> rowsToKeep = new ArrayList<String[]>();
		for(int i =0; i<tabArray.length; i++){
			if (tabArray[i][0] != null && tabArray[i][0].equals(dataProviderName)){
				String testData = tabArray[i][2];
				String[] tokens = clean(testData.split(","));
				rowsToKeep.add(tokens);
			}
		}
		
		String[][] subArray = new String[rowsToKeep.size()][];
		int len = rowsToKeep.size();
		
		for(int i=0; i < len; i++){
			subArray[i] = rowsToKeep.get(i);
		}

		return subArray;
	}
	
	private static String[] clean(String[] tokens) {
		for(int i = 0; i < tokens.length; i++) {
			if(tokens[i].equals("\"\"") || tokens[i].equals("''")) {
				tokens[i] = null;
			}
		}
		return tokens;
	}


}
