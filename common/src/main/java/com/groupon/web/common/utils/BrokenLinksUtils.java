package com.groupon.web.common.utils;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.HttpClients;
import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class BrokenLinksUtils {

	private static final Logger logger = Logger.getLogger(BrokenLinksUtils.class);

	public BrokenLinksUtils() {
	}

	public static void findBrokenLinksOnCurrentPage(WebDriver webDriver) {
		// Find total No of links on page and print In console.
		logger.info("*********** BROKEN LINKS FOR PAGE:"+ webDriver.getCurrentUrl()+" *************");
		List<WebElement> total_links = webDriver.findElements(By.tagName("a"));
		logger.info("Total Number of links found on page : = " + total_links.size());
		boolean isValid = false;
		List<String> brokenLinks = new ArrayList<String>();
		for (int i = 0; i < total_links.size(); i++) {
			WebElement anchor = total_links.get(i);
			String url = anchor.getAttribute("href");
			if (StringUtils.isNotBlank(url) && !url.startsWith("javascript:")) {
				isValid = getResponseCode(url);
				if (!isValid) {
//					logger.info("Broken Link ------>" + "Anchor Text:"+anchor.getText() + ", Href:"+url);
					brokenLinks.add("Anchor Text:"+anchor.getText() + ", Broken Url:"+url);
				}
			}
		}
		if(brokenLinks.isEmpty()) {
			logger.info("No Broken Links found!!!");
		}
		else {
			logger.info(brokenLinks.size() + " Broken links found!!!");
			for (String brokenLink : brokenLinks) {
				logger.info(brokenLink);
			}
			
		}
	}

	// Function to get response code of link URL.
	// Link URL Is valid If found response code = 200.
	// Link URL Is Invalid If found response code = 404 or 505.
	private static boolean getResponseCode(String chkurl) {
		boolean validResponse = false;
		try {
			// Get response code of URL
			HttpResponse urlresp = HttpClients.createDefault().execute(new HttpGet(chkurl));
			int resp_Code = urlresp.getStatusLine().getStatusCode();
			if ((resp_Code == 404) || (resp_Code == 505)) {
				validResponse = false;
			} else {
				validResponse = true;
			}
		} catch (Exception e) {
			logger.error("Error in checking response code:", e);
		}
		return validResponse;
	}
}
