package com.groupon.web.common.utils;

import com.groupon.web.common.page.BasePage;
import org.apache.pdfbox.pdfparser.PDFParser;
import org.apache.pdfbox.util.PDFTextStripper;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.testng.Assert;
import org.testng.Reporter;

import java.io.BufferedInputStream;
import java.io.IOException;
import java.net.URL;
import java.util.concurrent.TimeUnit;

public class PDFUtil extends BasePage{


	public String readPDF() throws IOException{  

		waitVar.until(ExpectedConditions.urlContains("voucherpdf"));
		URL url = new URL(getWebDriver().getCurrentUrl());   
		System.out.println("Voucher URL : " + url);		
		BufferedInputStream fileToParse=new BufferedInputStream(url.openStream());  
		//parse()  --  This will parse the stream and populate the COSDocument object.   
		//COSDocument object --  This is the in-memory representation of the PDF document  
		PDFParser parser = new PDFParser(fileToParse);  
		parser.parse();  
		//getPDDocument() -- This will get the PD document that was parsed. When you are done with this document 
		//you must call    close() on it to release resources  
		//PDFTextStripper() -- This class will take a pdf document and strip out all of the text and ignore the formatting and such.  
		String output=new PDFTextStripper().getText(parser.getPDDocument());  
		System.out.println("PDF is read");  
		Assert.assertTrue(output.length()>0,"Text is extracted successfully");
		parser.getPDDocument().close();   
		Reporter.log("base template reading is done");  
		getWebDriver().manage().timeouts().implicitlyWait(500, TimeUnit.SECONDS); 

		return output;
	} 

}
