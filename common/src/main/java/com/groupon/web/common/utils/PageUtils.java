package com.groupon.web.common.utils;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.List;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.NoSuchFrameException;
import org.openqa.selenium.StaleElementReferenceException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;

/**
 * 
 * @author prachi.nagpal
 *
 */
public class PageUtils{

	private static final Logger logger = Logger.getLogger(PageUtils.class);
	private static int invalidImageCount;
	private static int invalidLinksCount;


	/**
	 * Prepare Random Email Id for SignUp
	 * @return
	 */
	public static String getRandomUserEmail() {
		String userName = "";
		userName = String.valueOf(System.currentTimeMillis());
		return "qa"+userName+"@test.com";
	}

	/**
	 * Prepare Random Name for SignUp
	 * @return
	 */
	public static String getRandomName() {
		String name = "";
		name = String.valueOf(System.currentTimeMillis());
		return "test"+name;
	}

	/**
	 * Prepare Random Mobile Number for SignUp
	 * @return
	 */
	public static String getRandomMobileNumber() {
		String mobile = "";
		mobile = String.valueOf(System.currentTimeMillis());
		return mobile.substring(mobile.length()-10);

	}
	public static void scrollToTop(WebDriver driver){

		/*JavascriptExecutor jse = (JavascriptExecutor)driver;
		jse.executeScript("scroll(250, 0)");*/
		new Actions(driver).keyDown(Keys.CONTROL).sendKeys(Keys.HOME).perform();
		logger.info("Browser Scrolled to Top");

	}

	public static void scrollToEnd(WebDriver driver){

		driver.findElement(By.cssSelector("body")).sendKeys(Keys.END);
		//new Actions(driver).keyDown(Keys.CONTROL).sendKeys(Keys.END).build().perform();
		logger.info("Browser Scrolled to End");

	}

	public static void switchToFrame(WebDriver driver,WebElement frameElement) {
		try {
			if (WaitUtility.isElementPresent(driver, frameElement)) {
				driver.switchTo().frame(frameElement);
				System.out.println("Navigated to frame with element "+ frameElement);
			} else {
				System.out.println("Unable to navigate to frame with element "+ frameElement);
			}
		} catch (NoSuchFrameException e) {
			System.out.println("Unable to locate frame with element " + frameElement + e.getStackTrace());
		} catch (StaleElementReferenceException e) {
			System.out.println("Element with " + frameElement + "is not attached to the page document" + e.getStackTrace());
		} catch (Exception e) {
			System.out.println("Unable to navigate to frame with element " + frameElement + e.getStackTrace());
		}
	}

	public static void verifyAllLinksInPage(WebDriver driver){
		List<WebElement> no = driver.findElements(By.tagName("a"));
		int nooflinks = no.size(); 
		System.out.println("No. of Links in Page: " + nooflinks);
		for (WebElement pagelink : no)
		{
			String linktext = pagelink.getText();
			String link = pagelink.getAttribute("href"); 
			System.out.println(linktext+" ->");
			System.out.println(link);
		}
	}

	public static void scrollDownThisNumberOfTimes(WebDriver driver, int times){

		for(int i=0; i<=times ; i++){
			driver.findElement(By.cssSelector("body")).sendKeys(Keys.DOWN);
		}
	}

	/**
	 * Verify all images in the page are visible and giving sttaus 200 OK
	 * @param driver
	 */
	public static void validateInvalidImages(WebDriver driver) {
		try {
			invalidImageCount = 0;
			List<WebElement> imagesList = driver.findElements(By.tagName("img"));
			System.out.println("Total no. of images are " + imagesList.size());
			for (WebElement imgElement : imagesList) {
				if (imgElement != null) {
					verifyimageActive(imgElement);
				}
			}
			System.out.println("Total no. of invalid images are "	+ invalidImageCount);
		} catch (Exception e) {
			e.printStackTrace();
			System.out.println(e.getMessage());
		}
	}

	public static void verifyimageActive(WebElement imgElement) {
		try {
			HttpClient client = HttpClientBuilder.create().build();
			HttpGet request = new HttpGet(imgElement.getAttribute("src"));
			HttpResponse response = client.execute(request);
			// verifying response code he HttpStatus should be 200 if not,
			// increment as invalid images count
			if (response.getStatusLine().getStatusCode() != 200)
				invalidImageCount++;
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public static void findBrokenLinks(WebDriver driver){
		
		// Get all links web driver  
		List<WebElement> links = driver.findElements(By.tagName("a"));  

		try {
			invalidLinksCount = 0;
			List<WebElement> anchorTagsList = driver.findElements(By
					.tagName("a"));
			System.out.println("Total no. of links are "
					+ anchorTagsList.size());
			for (WebElement anchorTagElement : anchorTagsList) {
				if (anchorTagElement != null) {
					String url = anchorTagElement.getAttribute("href");
					if (url != null && !url.contains("javascript")) {
						verifyURLStatus(url);
					} else {
						invalidLinksCount++;
					}
				}
			}

			System.out.println("Total no. of invalid links are "
					+ invalidLinksCount);

		} catch (Exception e) {
			e.printStackTrace();
			System.out.println(e.getMessage());
		}

	}  

	public static void verifyURLStatus(String URL) throws MalformedURLException, IOException {         
		HttpClient client = HttpClientBuilder.create().build();
		HttpGet request = new HttpGet(URL);
		try {
			HttpResponse response = client.execute(request);
			// verifying response code and The HttpStatus should be 200 if not,
			// increment invalid link count
			////We can also check for 404 status code like response.getStatusLine().getStatusCode() == 404
			if (response.getStatusLine().getStatusCode() != 200)
				invalidLinksCount++;
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

}

