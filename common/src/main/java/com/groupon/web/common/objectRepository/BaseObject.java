package com.groupon.web.common.objectRepository;

import org.openqa.selenium.WebDriver;

/**
 *
 * @author prachi.nagpal
 *
 */
public abstract class BaseObject {

    WebDriver driver;

    public BaseObject(WebDriver driver) {
        this.driver	=	driver;
    }
}
