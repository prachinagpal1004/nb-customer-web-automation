package com.groupon.web.common.exception;
/**
 * 
 * @author prachi.nagpal
 *
 */
public class AuthorizedException extends Exception{
	
	public AuthorizedException(String message) {
        super(message);
    }

    public AuthorizedException(String message, Throwable cause) {
        super(message, cause);
    }

    public AuthorizedException(Throwable cause) {
        super(cause);
    }

}
