package com.groupon.web.common.utils;
/**
 * 
 * @author prachi.nagpal
 *
 */
public interface LogMessage {
	
	public String toString();

}
