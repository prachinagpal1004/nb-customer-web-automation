package com.groupon.web.common.page;

import java.awt.Toolkit;
import java.awt.datatransfer.StringSelection;
import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import org.apache.commons.io.FileUtils;
import org.apache.log4j.Logger;
import org.junit.Assert;
import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.Cookie;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.Point;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.logging.Logs;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.FluentWait;
import org.openqa.selenium.support.ui.Wait;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.ITestResult;

import com.google.common.base.Function;
import com.groupon.web.common.base.DriverFactory;
import com.groupon.web.common.config.Config;
import com.groupon.web.common.utils.BrokenLinksUtils;
import com.groupon.web.common.utils.WaitUtility;

/**
 * 
 * @author prachi.nagpal
 *
 */
public class BasePage {

	private static final Logger logger = Logger.getLogger(BasePage.class);
	private static ThreadLocal<WebDriver> WEB_DRIVERS_PROVIDER = new ThreadLocal<WebDriver>();
	public static String baseUrl;
	public static String platform;
	protected WebDriverWait waitVar;

	public BasePage() {
		Long waitTime = Long.parseLong(Config.getInstance().getConfig("webdriver.wait.default"));
		waitVar = new WebDriverWait(getWebDriver(), waitTime);
	}


	protected static WebDriver getWebDriver() {
		return WEB_DRIVERS_PROVIDER.get();
	}

	private static void setWebDriver(WebDriver driver) {
		WEB_DRIVERS_PROVIDER.set(driver);
	}

	public static synchronized void initializeDriver(String browser, boolean remote, String remoteUrl, String platform) throws Exception{
		if(getWebDriver() == null){
			setWebDriver(invokeBrowser(browser, remote, remoteUrl, platform));
		}
		//		if(baseUrl.contains("iwanto") || baseUrl.contains("nearbuy")){
		//			closeLightbox();
		//		}

	}


	protected static void closeDriver(){
		try{
			getWebDriver().close();
		}catch(Exception ex){
			System.err.println(ex);
		}
	}

	public static void quitDriver(){
		try{
			closeDriver();
			getWebDriver().quit();
			WEB_DRIVERS_PROVIDER.remove();
		}catch(Exception ex){
			System.err.println(ex);
		}
	}

	public static String getCurrentUrl() {
		return getWebDriver().getCurrentUrl();
	}

	public static Logs getDriverLogs() {
		return getWebDriver().manage().logs();
	}

	private static WebDriver invokeBrowser (String browser, boolean remote, String remoteUrl, String domain) throws Exception {

		//		// In case authentication pop up is present
		//		String authPopupEnabled = Config.getInstance().getConfig("auth.popup.enabled");
		//				if(authPopupEnabled != null && Boolean.valueOf(authPopupEnabled)) {
		//					(new Thread(new BrowserAuthenticationWindow())).start();
		//				}
		platform = domain;
		baseUrl = Config.getInstance().getConfig("base.url");
		WebDriver driver = new DriverFactory().create(browser, remote, remoteUrl, platform);
		driver.get(baseUrl);
		//        if(Config.environment.equals("staging")) {
		//            new WebDriverWait(driver, 20).until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector(".secondary_menu li>a[href$='login']")));
		//        }
		return driver;
	}


	/*boolean append	=	false;
		String xPort = System.getProperty("Importal.xvfb.id",":1");
		ChromeDriverService chromeDriverService = null;
		System.out.println("Value of port" +xPort);
		File f	=	new File(System.getProperty("user.dir")+System.getProperty("file.separator")+"resources");
		if(!f.exists())
			append	=	true;

		System.out.println("browser = "+ browser);
		if(browser.equalsIgnoreCase("firefox"))
		{
			DesiredCapabilities dc=new DesiredCapabilities();
			dc.setCapability(CapabilityType.ACCEPT_SSL_CERTS,true);

			if (os.equalsIgnoreCase("linux")){
				FirefoxBinary fb = new FirefoxBinary();
				fb.setEnvironmentProperty("DISPLAY", xPort);

				driver =new FirefoxDriver(fb, null, dc);
			}else {

				driver =new FirefoxDriver(dc);
			}
		}
		else if(browser.equalsIgnoreCase("chrome"))
		{
			if(os.equalsIgnoreCase("win"))
			{
				if(!append)
					System.setProperty("webdriver.chrome.driver",
							System.getProperty("user.dir")+System.getProperty("file.separator")+PropertiesUtil.getConstantProperty("WindowsChromeDriver"));
				else
					System.setProperty("webdriver.chrome.driver",
							System.getProperty("user.dir")+System.getProperty("file.separator")+PropertiesUtil.getConstantProperty("ProjectName")+System.getProperty("file.separator")+PropertiesUtil.getConstantProperty("WindowsChromeDriver"));
			}
			//download chromedriver for mac
			else if(os.equalsIgnoreCase("mac")) {
				//System.out.println("Inside Chrome");
				System.setProperty("webdriver.chrome.driver",
						System.getProperty("user.dir")+System.getProperty("file.separator")+PropertiesUtil.getConstantProperty("MacChromeDriver"));

			}else if(os.equalsIgnoreCase("linux")) {
				System.out.println("Inside Chrome for linux");
				chromeDriverService = new ChromeDriverService.Builder().usingDriverExecutable(new File(PropertiesUtil.getConstantProperty("LinuxChromeDriver"))).usingAnyFreePort().withEnvironment(ImmutableMap.of("DISPLAY", xPort)).build();
				chromeDriverService.start();
				System.out.println("Chrome server started");
			}

			ChromeOptions options = new ChromeOptions();
			options.addArguments("--test-type");
			if (chromeDriverService != null)
				driver = new ChromeDriver(chromeDriverService, options);
			else 
				driver = new ChromeDriver(options);
		}
		else if(browser.equalsIgnoreCase("ie"))
		{
			if(!append)
				System.setProperty("webdriver.chrome.driver",
						System.getProperty("user.dir")+System.getProperty("file.separator")+PropertiesUtil.getConstantProperty("IEDriver"));
			else
				System.setProperty("webdriver.chrome.driver",
						System.getProperty("user.dir")+System.getProperty("file.separator")+PropertiesUtil.getConstantProperty("ProjectName")+System.getProperty("file.separator")+PropertiesUtil.getConstantProperty("IEDriver"));	
			driver = new InternetExplorerDriver();
		}
		else if(browser.equalsIgnoreCase("safari"))
			driver	=	new SafariDriver();

		driver.manage().window().maximize();*/


	//		if(browser.equalsIgnoreCase("chrome")&&os.equalsIgnoreCase("mac"))
	//		BrowserUtil.setWindowToFullSize("testwindow",true);


	/**
	 * Select city from popup
	 * @param city
	 * @return
	 */
	public BasePage selectCityFromLocationPopUp(String city){
		verifyLocationPopUpBox();
		Actions builder = new Actions(getWebDriver());   
		logger.info("Select a city & close location PopUp");
		waitVar.until(ExpectedConditions.visibilityOf(getWebDriver().findElement(By.cssSelector(".modal-show .search"))));
		/*
		 *  Click outside popUp & assert
		 */
//		WebElement outsideCityPopUp = getWebDriver().findElement(By.cssSelector(".modal_wrapper.modal-show"));
//		builder.moveToElement(outsideCityPopUp, 40, 25).click().build().perform();
		WebElement cityPopUpModal = getWebDriver().findElement(By.cssSelector(".modal-show .search"));
		Assert.assertTrue(cityPopUpModal.isDisplayed());
		logger.info("Clicking outside popup is not closing. Verified");
		
		Assert.assertFalse("Cross button is visible", getWebDriver().findElement(By.cssSelector(".search button.flt-right")).isDisplayed());

		getWebDriver().findElement(By.linkText(city)).click();
		waitVar.until(ExpectedConditions.invisibilityOfElementLocated(By.cssSelector(".modal-show .search")));
		WebElement visibleCityText = getWebDriver().findElement(By.cssSelector(".txt-primary.city-name"));
		waitVar.until(ExpectedConditions.elementToBeClickable(visibleCityText));
		Assert.assertTrue(city.equals(visibleCityText.getText()));
		logger.info("Clicked the city which user wants to select");

		return this;
	}

	/**
	 * Verify location 
	 * @return
	 */
	public BasePage verifyLocationPopUpBox(){

		logger.info("Verifying Location Pop Up Box text");
		WaitUtility.waitForLoad(getWebDriver());
		try {
			Thread.sleep(4000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		WebElement locationPopUpElement = getWebDriver().findElement(By.cssSelector(".modal-show .search"));
		waitVar.until(ExpectedConditions.visibilityOf(locationPopUpElement));
		//waitVar.until(ExpectedConditions.visibilityOf(getWebDriver().findElement(By.cssSelector(".modal__body[_ngcontent-mlb-10]"))));
		List<WebElement> cityPopUpText = getWebDriver().findElements(By.cssSelector(".modal-show .search"));
		for(WebElement text : cityPopUpText){
			Assert.assertTrue(text.isDisplayed());
		}
		List<WebElement> locations = getWebDriver().findElements(By.cssSelector(".margin-top-xl a"));
		Assert.assertEquals(locations.size(), 12);
		Assert.assertFalse(getWebDriver().findElement(By.cssSelector(".search .btn-close")).isDisplayed());
		for(WebElement city : locations){
			Assert.assertTrue(city.isDisplayed());
			Assert.assertTrue(city.isEnabled());
		}
//		List<WebElement> hotelsLinks = getWebDriver().findElements(By.cssSelector(".icon-sort"));
//		for(WebElement link : hotelsLinks){
//			Assert.assertTrue(link.isDisplayed());
//			Assert.assertTrue(link.isEnabled());
//			logger.info(link.getText());
//		}
//		List<WebElement> topAndOtherCitiesHeading = getWebDriver().findElements(By.cssSelector(".modal__body h6"));
//		for(WebElement heading : topAndOtherCitiesHeading){
//			Assert.assertTrue(heading.isDisplayed());
//			logger.info(heading.getText());
//		}

		return this;
	}
	
	public BasePage refresh() {
		getWebDriver().navigate().refresh();
		return this;
	}


	public static void setClipboardData(String str){
		StringSelection ss = new StringSelection(str);
		Toolkit.getDefaultToolkit().getSystemClipboard().setContents(ss, null);
		System.out.println("done");
	}

	protected void takeScreenShot(ITestResult result ){

		String failureImageFileName =  result.getName() + new SimpleDateFormat("MM-dd-yyyy_HH-ss").format(new GregorianCalendar().getTime())+ ".png";

		try {
			File scrFile = ((TakesScreenshot) getWebDriver()).getScreenshotAs(OutputType.FILE);
			FileUtils.copyFile(scrFile, new File(System.getProperty("user.dir")+System.getProperty("file.separator")+Config.getInstance().getConfig("ScreenshotPath")+System.getProperty("file.separator")+failureImageFileName)); 

		} catch (IOException e1) {
			e1.printStackTrace();
		}
	}

	/**
	 * Scrolling the page for getting element in focus
	 * @param ele 
	 */

	protected void scrollElementIntoView(WebElement ele) {
		((JavascriptExecutor) getWebDriver()).executeScript("arguments[0].scrollIntoView(false);", ele);
		((JavascriptExecutor) getWebDriver()).executeScript("window.scrollBy(0,300);", "");
		sleepThread(1000);
	}

	protected String acceptAlertAndReturnMessage(){
		Alert alert = getWebDriver().switchTo().alert();
		String message = alert.getText();  
		alert.accept();
		return message;
	}

	protected void hoverOnElement(WebElement ele){

		Actions action = new Actions(getWebDriver());
		action.moveToElement(ele).build().perform();
	}

	//	public static void setWindowToFullSize(String windowName,boolean closeCurrentWindow)
	//	{
	//		JavascriptExecutor js = (JavascriptExecutor)driver;
	//		js.executeScript("window.open('','"+windowName+"','width=400,height=200')");
	//		if(closeCurrentWindow)
	//		driver.close();
	//		driver.switchTo().window(windowName);
	//		js.executeScript("window.moveTo(0,0);");
	//		js.executeScript("window.resizeTo(1280,800);");	
	//	}

	/**********************/	
	protected void fn_SelectRightClickOption(WebElement we, int optionIndex) throws InterruptedException{
		Actions actObj=new Actions(getWebDriver());
		actObj.contextClick(we).build().perform();
		Thread.sleep(500);
		for(int i=1; i <= optionIndex; i++){
			actObj.sendKeys(Keys.ARROW_DOWN);
		}
		actObj.sendKeys(Keys.ENTER);
	}

	protected void fn_OpenURL(String url){
		System.out.println(getWebDriver());
		getWebDriver().get(url);
	}

	protected void fn_Click(WebElement weObj){
		boolean visiblestatus=fn_IsVisiible(weObj);
		boolean enablestatus=fn_IsEnable(weObj);
		if(enablestatus==true && visiblestatus==true)
			weObj.click();
	}

	protected void fn_MouseOver(WebElement weObj){
		boolean visiblestatus=fn_IsVisiible(weObj);
		boolean enablestatus=fn_IsEnable(weObj);
		if(enablestatus==true && visiblestatus==true)
			new Actions(getWebDriver()).moveToElement(weObj).build().perform();
	}

	protected void fn_MouseClick(WebElement weObj){
		boolean visiblestatus=fn_IsVisiible(weObj);
		boolean enablestatus=fn_IsEnable(weObj);
		if(enablestatus==true && visiblestatus==true)
			new Actions(getWebDriver()).click(weObj).build().perform();
	}

	protected void fn_JavaScriptClick(WebElement we){
		JavascriptExecutor executor = (JavascriptExecutor)getWebDriver();
		executor.executeScript("arguments[0].click();", we);
	}

	protected void fn_WindowScroll(){
		JavascriptExecutor executor = (JavascriptExecutor)getWebDriver();
		executor.executeScript("window.scrollTo(0,150);");
	}

	protected void fn_WindowScrollToWebElement(WebElement we){
		Point p= we.getLocation();
		JavascriptExecutor executor = (JavascriptExecutor)getWebDriver();
		executor.executeScript("window.scrollTo("+p.getX()+","+p.getY()+");");
	}

	protected void fn_InputStatic(WebElement weObj, String valToInput){
		boolean visiblestatus=fn_IsVisiible(weObj);
		boolean enablestatus=fn_IsEnable(weObj);
		if(enablestatus==true && visiblestatus==true)
			weObj.sendKeys(valToInput);
	}

	protected boolean fn_IsVisiible(WebElement eleObj){
		int height = eleObj.getSize().getHeight();
		if(height>0){
			return true; 
		}else{
			return false;
		}
	}

	protected boolean fn_IsEnable(WebElement eleObj){
		boolean status=eleObj.isEnabled();
		return status;
	}  

	protected void fn_ExplicitWait_Visibility(WebElement we, int TimeOut){
		WebDriverWait WWT=new WebDriverWait(getWebDriver(), TimeOut);
		WWT.until(ExpectedConditions.visibilityOf(we));
	}

	protected void fn_ExplicitWait_ClickAble(WebElement we, int TimeOut){
		WebDriverWait WWT=new WebDriverWait(getWebDriver(), TimeOut);
		WWT.until(ExpectedConditions.elementToBeClickable(we));
	}  

	protected void fn_ValidateText_Static(WebElement we, String ExpectedValue){
		String ActualText= we.getText();
		if(ExpectedValue.trim().equalsIgnoreCase(ActualText)){
			System.out.println("Passed");
		}else{
			System.out.println("Failed");
		}
	} 

	protected void fn_ValidateTextContains_Static(WebElement we, String ExpectedValue){
		boolean ActualStatus= we.getText().contains(ExpectedValue);
		if(ActualStatus==true){
			System.out.println("Passed");
		}else{
			System.out.println("Failed");
		}
	}  

	protected void fn_ValidateAttribute_Static(WebElement we, String ExpectedValue){
		boolean ActualStatus= we.getText().contains(ExpectedValue);
		if(ActualStatus==true){
			System.out.println("Passed");
		}else{
			System.out.println("Failed");
		}
	}

	protected void switchToPopup(String title){
		Set<String> gmailWindows = getWebDriver().getWindowHandles();
		for(String gmailWindow : gmailWindows){

			getWebDriver().switchTo().window(gmailWindow);
			if(getWebDriver().getTitle().contains(title))
				break;
		}
	}

	protected void switchToParentWindow(Set<String> windows){

		for(String window : windows){
			getWebDriver().switchTo().window(window);
			//System.out.println("Window name in external ::" +window + "-" + driver.getTitle() );
			break;
		}
	}


	protected WebElement fluentWait(final By locator) 
	{
		Wait<WebDriver> wait = new FluentWait<WebDriver>(getWebDriver()).withTimeout(50, TimeUnit.SECONDS).pollingEvery(5, TimeUnit.SECONDS).ignoring(NoSuchElementException.class);

		WebElement webElement = wait.until(new Function<WebDriver, WebElement>()
		{
			public WebElement apply(WebDriver driver) 
			{
				return driver.findElement(locator);

			}});
		return webElement;
	};

	protected WebElement fluentWaitForElementTextLength(final WebElement webelement,int totalTime, int pollingTime) 
	{
		Wait<WebDriver> wait = new FluentWait<WebDriver>(getWebDriver()).withTimeout(totalTime, TimeUnit.SECONDS).pollingEvery(pollingTime, TimeUnit.SECONDS).ignoring(NoSuchElementException.class);

		WebElement webElement= wait.until(new Function<WebDriver, WebElement>()
		{
			public WebElement apply(WebDriver driver) 
			{
				if(webelement.getText().length()>0)	
					return webelement; 
				//return driver.findElement(By.id("invalid"));
				return webelement;


			}});
		return webElement;
	};


	protected void sleepThread(long milliseconds)
	{
		try {
			Thread.sleep(milliseconds);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}

	protected void explicitWaitByVisibilityOfElement( int seconds,WebElement el){
		WebDriverWait wait = new WebDriverWait(getWebDriver(), seconds);
		wait.until(ExpectedConditions.visibilityOf(el));
		//return wait;
	}

	protected void explicitWaitByPresenceOfElement( int seconds,By by){
		WebDriverWait wait = new WebDriverWait(getWebDriver(), seconds);
		wait.until(ExpectedConditions.presenceOfElementLocated(by));
		//return wait;
	}


	protected WebDriverWait explicitWaitByElementToBeClickable(int seconds, WebElement elt){
		WebDriverWait wait = new WebDriverWait(getWebDriver(), seconds);
		wait.until(ExpectedConditions.elementToBeClickable(elt));
		return wait;
	}


	protected WebElement fluentWait(final WebElement locator,
			final int timeoutSeconds) {
		FluentWait<WebDriver> wait = new FluentWait<WebDriver>(getWebDriver())
				.withTimeout(timeoutSeconds, TimeUnit.SECONDS)
				.pollingEvery(500, TimeUnit.MILLISECONDS)
				.ignoring(NoSuchElementException.class);

		return wait.until(new Function<WebDriver, WebElement>() {
			public WebElement apply(WebDriver webDriver) {
				return locator;
			}
		});
	}

	public String getCookievalue(String cookieName){
		Cookie cookie = getWebDriver().manage().getCookieNamed(cookieName);  
		String cookieVal = cookie.getValue();
		return cookieVal;
	}

	protected static void closeLightbox(){

		System.out.println("In Lightbox");
		Assert.assertNotNull(getWebDriver().findElement(By.cssSelector(".nearbuy_lghtbox .pop-up-container")));
		new WebDriverWait(getWebDriver(), 10).until(ExpectedConditions.elementToBeClickable(By.cssSelector(".nearbuy_lghtbox .visit")));
		Assert.assertTrue(getWebDriver().findElement(By.cssSelector(".nearbuy_lghtbox .pop-up-container")).isDisplayed());
		getWebDriver().findElement(By.cssSelector(".nearbuy_lghtbox .cross")).click();
		new WebDriverWait(getWebDriver(), 10).until(ExpectedConditions.invisibilityOfElementLocated(By.cssSelector(".nearbuy_lghtbox .pop-up-container")));
		System.out.println("Lightbox Closed");
	}

	/**
	 * Verify Data Layer Objects in all the pages
	 * @param expectedValuesMap
	 */
	protected void verifyDataLayer(Map<String, Object> expectedValuesMap, String dataLayerNumber){

		//verifyGTMdomLoad();
		logger.info("*******dataLayer*******");

		//Object obj = ((JavascriptExecutor) getWebDriver()).executeScript("return window['dataLayer'][" + dataLayerNumber + "]");
		Object obj = ((JavascriptExecutor) getWebDriver()).executeScript("return window['dataLayer']");
		Assert.assertNotNull(obj);
		List l = (List)obj;
		for(Object o : l) {
			System.out.println((Map)o);
		}
		//		Map<String, Object> actualValuesMap = new HashMap<String, Object>((Map)obj);
		//		logger.info("Actual Values Map:"+actualValuesMap);
		//		for (Entry<String, Object> entry : actualValuesMap.entrySet()) {
		//			logger.info(entry.getKey()+":" + entry.getValue());
		//		}
		//		for (Entry<String, Object> entry : expectedValuesMap.entrySet()) {
		//			String key = entry.getKey();
		//			Object expectedValue = entry.getValue();
		//			Object actualValue = actualValuesMap.get(key);
		//			Assert.assertEquals(expectedValue.toString(), actualValue.toString());
		//		}
		logger.info("*******dataLayer Verifications Ends*******");
	}

	/**
	 * Verify GTM DOM is loaded or not
	 */
	protected void verifyGTMdomLoad() {

		logger.info("*******gtmDom & gtmLoad*******");

		Object gtmDOM = ((JavascriptExecutor) getWebDriver()).executeScript("return window['google_tag_manager'].dataLayer.gtmDom;");
		logger.info("gtmDom Fired : " + gtmDOM); // true when DOM Ready has fired
		Assert.assertTrue((Boolean)gtmDOM);
		Object gtmLOAD = ((JavascriptExecutor) getWebDriver()).executeScript("return window['google_tag_manager'].dataLayer.gtmLoad;");
		logger.info("gtmLoad Fired : " + gtmLOAD); // true when Window Loaded has fired
		Object isArrayDataLayer = ((JavascriptExecutor) getWebDriver()).executeScript("return Array.isArray(dataLayer);");
		logger.info("DataLayer is an array : " + isArrayDataLayer); // true when Window Loaded has fired
		logger.info("*******gtmDom & gtmLoad Verifications Ends*******");
	}
	
	public void printAllBrokenLinksForCurrentPage() {
		BrokenLinksUtils.findBrokenLinksOnCurrentPage(getWebDriver());
	}

	//	public static synchronized void selectCityFromLocationPopUp(String city){
	//		
	//		logger.info("Select a city & close location PopUp");
	//		waitVar.until(ExpectedConditions.visibilityOf(getWebDriver().findElement(By.cssSelector(".drop_down_shadow"))));
	//		getWebDriver().findElement(By.linkText(city)).click();
	//		waitVar.until(ExpectedConditions.invisibilityOfElementLocated(By.id("divisions")));
	//	}
}