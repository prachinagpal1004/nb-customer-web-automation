package com.groupon.web.common.utils;

import java.awt.AWTException;
import java.awt.Robot;
import java.awt.event.KeyEvent;

public class BrowserAuthenticationWindow implements Runnable {

    private Robot robot;

	public void run() {
        entercredentials();
	}

	public void entercredentials() {
		try {
            System.out.println("Enter QA Authentication Credentials");
            robot = new Robot();
            robot.delay(25000);
            robot.mouseMove(1005,10);
           // robot.mousePress(InputEvent.BUTTON1_MASK);
            enterText("kibana", false);
            enterSpecialChar(KeyEvent.VK_TAB);
            enterText("Krypton90!", true);
			enterSpecialChar(KeyEvent.VK_ENTER);
            System.out.println("Entered QA Authentication Credentials");
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

	public void enterText(String text, boolean caseSensitive) throws AWTException {
		byte[] bytes = text.getBytes();

		for (byte b : bytes) {
			int bytecode = b;
            boolean capital = false;
			// keycode only handles [A-Z] (which is ASCII decimal [65-90])
            if (bytecode> 64 && bytecode< 91) {
                capital = true;
            }
            else if (bytecode> 96 && bytecode< 123) {
                bytecode = bytecode - 32;
            }
            if("!".equals(new String(new byte[] {b}))) {
                robot.delay(40);
                robot.keyPress(KeyEvent.VK_SHIFT);
                robot.keyPress(KeyEvent.VK_1);
                robot.keyRelease(KeyEvent.VK_1);
                robot.keyRelease(KeyEvent.VK_SHIFT);
            }
            else {
                robot.delay(40);
                if(caseSensitive && capital) {
                    robot.keyPress(KeyEvent.VK_SHIFT);
                }
                robot.keyPress(bytecode);
                robot.keyRelease(bytecode);
                if(caseSensitive && capital) {
                    robot.keyRelease(KeyEvent.VK_SHIFT);
                }
            }
		}
	}

	private void enterSpecialChar(int s) throws AWTException {
		Robot robot = new Robot();
		robot.delay(40);
		robot.keyPress(s);
		robot.keyRelease(s);
	}
	
}
