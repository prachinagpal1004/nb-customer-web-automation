package com.groupon.web.common.utils;

import com.relevantcodes.extentreports.ExtentReports;
import com.relevantcodes.extentreports.ExtentTest;
import com.relevantcodes.extentreports.LogStatus;
import org.apache.log4j.Logger;
import org.testng.ITestContext;
import org.testng.ITestListener;
import org.testng.ITestResult;

/**
 * 
 * @author prachi.nagpal
 *
 */
public class TestListener implements ITestListener {

	private static final Logger logger = Logger.getLogger(TestListener.class);
	public static String screen=null;
	public String testNameFromXml=null;
	private ExtentReports extent = null;
	private static ThreadLocal<ExtentTest> extentTestThreadLocal = new ThreadLocal<ExtentTest>();

	public void onTestStart(ITestResult result) {
		logger.info("Started Test: " + result.getName());
		extentTestThreadLocal.set(extent.startTest(result.getName(), "Test Run on " + testNameFromXml));
		extentTestThreadLocal.get();			
	}

	public void onTestSuccess(ITestResult result) {
		logger.info("Finished Test: " + result.getName() + " :PASSED");
        if(extentTestThreadLocal.get() == null) {
            logger.error("TestListener.onTestStart() didn't got called. Check dataProvider,etc for issue.");
            return;
        }
        extentTestThreadLocal.get().log(LogStatus.PASS, result.getName() + " Passed");
		//extentTestThreadLocal.get().addScreenCapture(screen);
		extent.endTest(extentTestThreadLocal.get());

	}

	public void onTestFailure(ITestResult result) {
		logger.info("Finished Test: " + result.getName() + " :FAILED");
        if(extentTestThreadLocal.get() == null) {
            logger.error("TestListener.onTestStart() didn't got called. Check dataProvider,etc for issue.");
            return;
        }
        extentTestThreadLocal.get().log(LogStatus.FAIL, result.getName() + " Failed");
		if(result.getThrowable() != null)
			extentTestThreadLocal.get().log(LogStatus.FAIL, result.getThrowable());
		//extentTestThreadLocal.get().addScreenCapture(screen);
		extent.endTest(extentTestThreadLocal.get());
	}

	public void onTestSkipped(ITestResult result) {
		logger.info("Finished Test: " + result.getName() + " :SKIPPED");
        if(extentTestThreadLocal.get() == null) {
            logger.error("TestListener.onTestStart() didn't got called. Check dataProvider,etc for issue.");
            return;
        }
		extentTestThreadLocal.get().log(LogStatus.SKIP, result.getName() + " Skipped");
		extent.endTest(extentTestThreadLocal.get());
	}

	public void onTestFailedButWithinSuccessPercentage(ITestResult result) {
		logger.info("Finished Test: " + result.getName()+ " :FAILED BUT WITHIN SUCCESS PERCENTAGE");
        if(extentTestThreadLocal.get() == null) {
            logger.error("TestListener.onTestStart() didn't got called. Check dataProvider,etc for issue.");
            return;
        }
        extentTestThreadLocal.get().log(LogStatus.WARNING, result.getName() + " Failed");
		if(result.getThrowable() != null)
			extentTestThreadLocal.get().log(LogStatus.WARNING, result.getThrowable());
		extent.endTest(extentTestThreadLocal.get());
	}

	public void onStart(ITestContext context) {
		logger.info("onStart:"+context.getName());
		extent = ExtentManagerUtil.getInstance();
		testNameFromXml=context.getName();
	}

	public void onFinish(ITestContext context) {
		logger.info("onFinish:"+context.getName());
		if(extent != null) {
			extent.flush();
			extent.close();
			extent = null;
		}
	}

	public static void takeScreenshot(String screenPath){
		screen=screenPath;
		//logger.info("screenpath "+screenPath); 
		//logger.info("screen "+screen); 
	}

	public static void setLog(LogStatus type, String logMessage){
		extentTestThreadLocal.get().log(type, logMessage);
	}

	//		public static void initExtentManager() {
	//			extent = ExtentManagerUtil.getInstance();
	//		}
	//		public static void closeExtentManager() {
	//			extent.flush();
	//		}

}

