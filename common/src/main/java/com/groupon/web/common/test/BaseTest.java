package com.groupon.web.common.test;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.openqa.selenium.logging.LogEntry;
import org.openqa.selenium.logging.LogType;
import org.openqa.selenium.logging.Logs;
import org.testng.ITestContext;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Listeners;
import org.testng.annotations.Optional;
import org.testng.annotations.Parameters;

import com.groupon.web.common.config.Config;
import com.groupon.web.common.page.BasePage;
import com.groupon.web.common.utils.BrowserAuthenticationWindow;
import com.groupon.web.common.utils.BrowserLogUtils;
import com.groupon.web.common.utils.DataProviderUtil;
import com.groupon.web.common.utils.JSError;
import com.groupon.web.common.utils.SelectorUtils;
import com.groupon.web.common.utils.TestListener;

/**
 * 
 * @author prachi.nagpal
 *
 */
@Listeners(value=TestListener.class)
public class BaseTest {

	private static final Logger logger = Logger.getLogger(BaseTest.class);

	protected static String baseUrl;

	private List<JSError> chromeJSErrors = new ArrayList<JSError>();
	private List<JSError> perfLogs = new ArrayList<JSError>();

	private String city;

	@BeforeSuite(alwaysRun=true)
	@Parameters({"env","threadCount"})
	public void beforeSuite(@Optional("prod") String env, @Optional("5") int threadCount, ITestContext context) {
		logger.info("Before Suite");
		Config.loadConfigFile(env, null);
		//TestListener.initExtentManager();
		//loadSelectors();
		baseUrl = Config.getInstance().getConfig("base.url");
		logger.info("BaseUrl : "+baseUrl);
		if(Config.environment.equals("staging")) {
			threadCount = 1;//no parallel in staging
		}
		context.getSuite().getXmlSuite().setThreadCount(threadCount);
	}

	private void loadSelectors() {
		try {
			final String selectorFileName = Config.getInstance().getConfig("page.selector.file");
			final String sheets = Config.getInstance().getConfig("page.selector.file.sheets");
			SelectorUtils.loadData(selectorFileName, StringUtils.split(sheets));
		} catch (IOException e) {
			throw new RuntimeException(e);
		}
	}

	@BeforeClass(alwaysRun=true)
	@Parameters({"browser","remote","remoteUrl", "platform", "city"})
	public void beforeClass(
			@Optional("firefox") String browser, 
			@Optional("false") boolean remote, 
			@Optional("127.0.0.1:4444") String remoteUrl, 
			@Optional("web") String platform,
			@Optional("New Delhi") String city) throws Exception {
		logger.info("Inside beforeClass:"+getClassName());
		BasePage.initializeDriver(browser, remote, remoteUrl, platform);
		if(baseUrl.contains("iwanto") || baseUrl.contains("nearbuy")){
			new BasePage().selectCityFromLocationPopUp(city);
		}
		extractJSLogs();
		this.city = city;
	}

	@AfterClass(alwaysRun=true)
	public void afterClass() {
		logger.info("Inside afterClass:"+getClassName());
		extractJSLogs();
		copyJSLogs();
		BasePage.quitDriver();
		this.city = null;
	}

	@AfterSuite(alwaysRun=true)
	public void afterSuite() {
		logger.info("After suite called...");
		writeJSLogs();
		DataProviderUtil.clearTestData();
		//	TestListener.closeExtentManager();
	}

	public String getClassName() {
		return this.getClass().getName();
	}

	/**
	 * Extract JS logs on every page
	 */
	protected void extractJSLogs(){
		String currentUrl = BasePage.getCurrentUrl();
		Logs driverLogs = BasePage.getDriverLogs();
		List<LogEntry> errors = driverLogs.get(LogType.BROWSER).getAll();
		for (LogEntry error : errors) {
			this.chromeJSErrors.add(new JSError(currentUrl, error));
		}

		/*List<LogEntry> perfErrors = driverLogs.get(LogType.PERFORMANCE).getAll();
        for (LogEntry entry : perfErrors) {
            this.perfLogs.add(new JSError(currentUrl, entry));
        }*/
	}

	/**
	 * Copy JS logs
	 */
	protected void copyJSLogs() {

		BrowserLogUtils.copyChromeJSError(this.chromeJSErrors);
		BrowserLogUtils.copyPerfLogs(this.perfLogs);
		this.chromeJSErrors.clear();
		this.perfLogs.clear();
	}

	/**
	 * Write JS logs to File
	 */
	protected void writeJSLogs() {
		String logDirPath = BrowserLogUtils.getLogDirPathForGivenBrand();
		BrowserLogUtils.writeChromeJSError(logDirPath+"JSerrorLogs.csv");
		BrowserLogUtils.writePerfLogs(logDirPath+"PerformanceLogs.csv", null, null);
	}

	protected void authPopup() {
		String authPopupEnabled = Config.getInstance().getConfig("auth.popup.enabled");
		System.out.println("Property from config is taken");
		if(authPopupEnabled != null && Boolean.valueOf(authPopupEnabled)) {
			(new Thread(new BrowserAuthenticationWindow())).start();
			System.out.println("Thread has started");
		}
	}

	public String getCity() {
		return city;
	}

}
