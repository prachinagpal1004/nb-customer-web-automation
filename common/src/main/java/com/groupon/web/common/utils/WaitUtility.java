/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.groupon.web.common.utils;

import java.util.List;
import java.util.concurrent.TimeUnit;

import org.apache.commons.lang3.StringUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.TimeoutException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import junit.framework.Assert;

/**
 * 
 * @author prachi.nagpal
 *
 */
public class WaitUtility
{

	private static WebElement myDynamicElement;
	public static void waitForElementToAppeaerById(final String id, WebDriver driver)
	{
		myDynamicElement = (new WebDriverWait(driver, 20)).until(new ExpectedCondition<WebElement>()
				{
			public WebElement apply(WebDriver d)
			{
				return d.findElement(By.id(id));
			}
				});
	}

	public static void waitForElementToAppeaerByXpath(final String xpath, WebDriver driver)
	{

		myDynamicElement = (new WebDriverWait(driver, 20)).until(new ExpectedCondition<WebElement>()
				{
			public WebElement apply(WebDriver d)
			{
				return d.findElement(By.xpath(xpath));
			}
				});
	}

	public static void waitForElementToAppeaerBylinkText(final String linkText, WebDriver driver)
	{

		myDynamicElement = (new WebDriverWait(driver, 20)).until(new ExpectedCondition<WebElement>()
				{
			public WebElement apply(WebDriver d)
			{
				return d.findElement(By.linkText(linkText));
			}
				});
	}

	public static void waitUntilElemetIsDisAppearedById(final String id, WebDriver driver)
	{
		boolean loading = false;
		do 
		{
			try
			{
				myDynamicElement = (new WebDriverWait(driver, 15)).until(new ExpectedCondition<WebElement>()
						{
					public WebElement apply(WebDriver d)
					{
						return d.findElement(By.id("load_shippingLocationGrid"));
					}
						});
				loading = myDynamicElement.isDisplayed();
			}
			catch (TimeoutException e) 
			{
				return;
			}
		}while(loading == true);
	}


	/**
	 * Wait for an attribute to appear or have a value
	 * @param xpath
	 * @param attribute
	 * @param driver
	 */
	public static void waitForAnAttributeToCome(final String xpath, final String attribute, WebDriver driver){
		new WebDriverWait(driver, 50).until(new ExpectedCondition<Boolean>() {
			public Boolean apply(WebDriver driver) {
				WebElement element = driver.findElement(By.xpath(xpath));
				String enabled = element.getAttribute(attribute);
				if(enabled.length()>0) 
					return true;
				else
					return false;
			}
		});
	}

    public static void waitForAttributeContainsGivenValue(final By locator, final String attribute, final String value, WebDriver driver, WebDriverWait waitVar){
        if(waitVar == null) {
            waitVar = new WebDriverWait(driver, 50);
        }

        waitVar.until(prepareConditionForAttrValueContains(locator, null, attribute, value));
    }

    public static void waitForAttributeNotContainsGivenValue(final By locator, final String attribute, final String value, WebDriver driver, WebDriverWait waitVar){
        if(waitVar == null) {
            waitVar = new WebDriverWait(driver, 50);
        }

        waitVar.until(ExpectedConditions.not(prepareConditionForAttrValueContains(locator, null, attribute, value)));
    }

    public static void waitForAttributeContainsGivenValue(final WebElement element, final String attribute, final String value, WebDriver driver, WebDriverWait waitVar){
        if(waitVar == null) {
            waitVar = new WebDriverWait(driver, 50);
        }

        waitVar.until(prepareConditionForAttrValueContains(null, element, attribute, value));
    }

    public static void waitForAttributeNotContainsGivenValue(final WebElement element, final String attribute, final String value, WebDriver driver, WebDriverWait waitVar){
        if(waitVar == null) {
            waitVar = new WebDriverWait(driver, 50);
        }

        waitVar.until(ExpectedConditions.not(prepareConditionForAttrValueContains(null, element, attribute, value)));
    }

    private static ExpectedCondition<Boolean> prepareConditionForAttrValueContains(final By locator, final WebElement element, final String attribute, final String value) {
        return new ExpectedCondition<Boolean>() {
            public Boolean apply(WebDriver driver) {
                WebElement webElement = element == null ? driver.findElement(locator) : element;
                String attrValue = webElement.getAttribute(attribute);
                if (StringUtils.isNotBlank(attrValue) && attrValue.contains(value))
                    return true;
                else
                    return false;
            }
        };
    }

    public static void waitForLoad(WebDriver driver) {
		ExpectedCondition<Boolean> pageLoadCondition = new
				ExpectedCondition<Boolean>() {
			public Boolean apply(WebDriver driver) {
				return ((JavascriptExecutor)driver).executeScript("return document.readyState").equals("complete");
			}
		};
		WebDriverWait wait = new WebDriverWait(driver, 70);
		wait.until(pageLoadCondition);
	}

	/**
	 * Is Elemnet Present
	 * @param driver
	 * @param locator
	 * @return
	 */
	public static boolean isElementPresentByLocator(WebDriver driver, By locator) {
		return getElementByLocator(driver, locator) != null;
	}

	public static WebElement getElementByLocator(WebDriver driver, By locator) {
		try {
			WebElement element = driver.findElement(locator);
			return element;
		}
		catch(NoSuchElementException e) {
			return null;
		}
	}

    public static List<WebElement> getElements(WebDriver driver, By locator) {
		try {
			return driver.findElements(locator);
		}
		catch(NoSuchElementException e) {
			return null;
		}
	}

	/**
	 * Wait for progress bar to load 
	 * @param driver
	 * @param waitVar
	 */
	public static void waitForProgressBarToLoad(WebDriver driver, WebDriverWait waitVar){

//		WebElement progressBarElement = driver.findElement(By.id("progressbar"));
		waitVar.until(ExpectedConditions.visibilityOfElementLocated(By.id("progressbar")));//visibilityOf(progressBarElement));
		waitVar.until(ExpectedConditions.invisibilityOfElementLocated(By.id("progressbar")));
	}
	
	/**
	 * Wait for URL to change
	 * @param driver
	 * @param waitVar
	 */
	public static void waitForURLtoChange(WebDriver driver, WebDriverWait waitVar){
		
		String currentURL = null;
		final String previousURL = driver.getCurrentUrl();
		 ExpectedCondition<Boolean> e = new ExpectedCondition<Boolean>() {
	          public Boolean apply(WebDriver d) {
	            return (d.getCurrentUrl() != previousURL);
	          }
	        };

	    waitVar.until(e);
	    currentURL = driver.getCurrentUrl();
	    System.out.println("New URL : " +currentURL);
	}
	
	/**
	 * Is alert present or not on page
	 * @param driver
	 * @return
	 */
	public static boolean isAlertPresent(WebDriver driver){
	    boolean foundAlert = false;
	    WebDriverWait wait = new WebDriverWait(driver, 10 /*timeout in seconds*/);
	    try {
	        wait.until(ExpectedConditions.alertIsPresent());
	        foundAlert = true;
	    } catch (TimeoutException eTO) {
	        foundAlert = false;
	    }
	    return foundAlert;
	}
	
	public static boolean isPresent(WebDriver driver, By locator){
		Boolean isPresent = driver.findElements(locator).size() > 0;
		return isPresent;
	}
	
	public static boolean hasClass(WebElement el, String className ) {
	    return el.getAttribute( "class" ).contains(className);
	}
	
	public static boolean isElementPresent(WebDriver driver, WebElement webelement) {	
		boolean exists = false;

		driver.manage().timeouts().implicitlyWait(0, TimeUnit.MILLISECONDS);

		try {
			webelement.getTagName();
			exists = true;
		} catch (NoSuchElementException e) {
			// nothing to do.
		}

		driver.manage().timeouts().implicitlyWait(1000, TimeUnit.MILLISECONDS);

		return exists;
	}
	
	public static void waitForTextToAppear(WebDriver driver, String textToAppear, WebElement element) {
	    WebDriverWait wait = new WebDriverWait(driver,1000);
	    wait.until(ExpectedConditions.textToBePresentInElement(element, textToAppear));
	}
}
