package com.groupon.web.api.test;

import com.groupon.web.api.dto.RequestDTO;
import com.groupon.web.api.helper.ApiHelper;
import com.groupon.web.common.config.Config;
import junit.framework.Assert;
import org.apache.commons.lang3.StringUtils;
import org.apache.http.Header;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpRequestBase;
import org.apache.http.entity.ContentType;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.impl.conn.PoolingHttpClientConnectionManager;
import org.apache.log4j.Logger;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.testng.ITest;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Optional;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import java.io.File;
import java.io.IOException;


/**
 * This Testng class is used to test the api's response as in curl hit manner
 * 
 */
/**
 * 
 * @author prachi.nagpal
 *
 */
public class APIResponseTest implements ITest {

    private static final Logger logger = Logger.getLogger(APIResponseTest.class);
    public static final String API_ID = "api";
    protected String testCaseName = "";
    private String filePath;
    private int sheetNum;
    protected ApiHelper apiHelper;
    private CloseableHttpClient httpClient;

    /**
	 * This function is used to set up the environment before class  
	 * @param env: it is the environment being passed through runtime (dev, staging, prod)
	 */
    @BeforeSuite
    @Parameters("env")
    public void beforeSuite(@Optional("staging") String env) {
        logger.info("Inside beforeSuite");
        apiHelper = new ApiHelper();
        Config.loadConfigFile(env, API_ID);
    }

	@BeforeClass
	public void setUp() {

        filePath = Config.getInstance(API_ID).getConfig("api.excel.filename");

        String sheetNumStr = Config.getInstance(API_ID).getConfig("api.excel.sheetNum");
        sheetNum = sheetNumStr != null ? Integer.parseInt(sheetNumStr) : 0;

        logger.info("File Path:"+filePath);
        logger.info("Sheet Num:"+sheetNum);
        PoolingHttpClientConnectionManager cm = new PoolingHttpClientConnectionManager();
        cm.setMaxTotal(100);

        httpClient = HttpClients.custom()
                .setConnectionManager(cm)
                .build();
	}

	@BeforeMethod(alwaysRun=true)
	 public void testData(Object[] testData) {
        logger.info("Before mehtod");
        String testCase = (String)testData[0];
        logger.info(testCase);
        this.testCaseName = testCase.replace("/",".");
        logger.info(testCaseName);
	}
	
	@Test(dataProvider = "excelLoader")
	public void checkApiResponse(String apiName, String baseUrl, String endPoint, String methodType, String headerJson, String pathParamJson, String queryParamJson, String requestBody, String responseCode, String responseParamJson) {
        RequestDTO requestDTO = new RequestDTO(baseUrl);
        requestDTO.setApiName(apiName);
        requestDTO.setEndPoint(endPoint);
        requestDTO.setMethodType(methodType);
        requestDTO.setHeaderJson(headerJson);
        requestDTO.setPathParamJson(pathParamJson);
        requestDTO.setQueryParamJson(queryParamJson);
        requestDTO.setRequestBody(requestBody);
        requestDTO.setExpectedResponseCode(StringUtils.isNotBlank(responseCode) ? Integer.parseInt(responseCode) : 200);
        requestDTO.setExpectedResponseJson(responseParamJson);
        
        logger.info("Trying Request:"+requestDTO);
        
        try {
            final HttpRequestBase httpRequest = apiHelper.prepareHTTPRequest(requestDTO);
            Assert.assertNotNull(httpRequest);
            final HttpResponse httpResponse = httpClient.execute(httpRequest);
            Assert.assertNotNull(httpResponse);
            String responseBody = apiHelper.getResponseBody(httpResponse);
            logger.info("Response:"+responseBody);
            Assert.assertEquals(requestDTO.getExpectedResponseCode(), httpResponse.getStatusLine().getStatusCode());
            Assert.assertNotNull(responseBody);
            validateActualResponseWithExpected(requestDTO.getExpectedResponseJson(), responseBody, httpRequest.getFirstHeader(ApiHelper.CONTENT_TYPE));

        } catch (Exception e) {
            logger.error("Test Case failed:"+getTestName(), e);
            throw new RuntimeException(e);
        }
	}

    private void validateActualResponseWithExpected(String expectedResponseJson, String actualResponse, Header contentTypeHeader) throws ParseException {
       if(StringUtils.isNotBlank(expectedResponseJson) && contentTypeHeader != null && StringUtils.isNotBlank(contentTypeHeader.getValue())) {
           final String contentTypeValue = contentTypeHeader.getValue();
           if(contentTypeValue.contains(ContentType.APPLICATION_JSON.getMimeType())) {
               JSONParser parser = new JSONParser();
               final JSONObject expectedResponseMap = (JSONObject) parser.parse(expectedResponseJson);
               final JSONObject actualResponseMap = (JSONObject) parser.parse(actualResponse);
               for (Object k : expectedResponseMap.keySet()) {
                   if(actualResponseMap.containsKey(k)) {
                       Assert.assertEquals(expectedResponseMap.get(k), actualResponseMap.get(k));
                   }
               }
           }
           else if(contentTypeValue.contains(ContentType.TEXT_PLAIN.getMimeType()) || contentTypeValue.contains(ContentType.WILDCARD.getMimeType())) {
               Assert.assertTrue(expectedResponseJson.contains(actualResponse));
           }
           else {
               throw new UnsupportedOperationException("Response Body Type("+contentTypeValue+") not supported");
           }
       }
    }

    @AfterSuite
	public void afterSuite() {
		if(httpClient != null) {
			try {
				httpClient.close();
			} catch (IOException e) {
				throw new RuntimeException(e);
			}
		}
	}

	
	@DataProvider(name = "excelLoader", parallel = true)
	public Object[][] createData1() throws Exception {
		Object[][] retObjArr = apiHelper.getTableArray(filePath, sheetNum);
		return (retObjArr);
	}
	
	public String getTestName() {
		return this.testCaseName;
	}
}