package com.groupon.web.webPerformance.objectRepository;

import java.util.List;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;

public class HomePageLocator {
	
	@FindBy(how = How.CSS, using = ".app_name a")
	public List<WebElement> appNameLinks;
	
	@FindBy(how = How.ID, using = "app_search_field")
	public WebElement appSearchTextBox;
	
	@FindBy(how = How.ID, using = "secondary_navigation")
	public WebElement headerOnAppPage;
	
	@FindBy(how = How.ID, using = "app_search_results")
	public WebElement appSearchResultBox;

	@FindBy(how = How.ID, using = "tab_content")
	public WebElement memoryTabContent;

	@FindBy(how = How.XPATH, using = ".//*[@id='app_search_results']/li")
	public List<WebElement> appSearchResultLists;

	@FindBy(how = How.CSS, using = "#current_agent .app_dropdown_trigger")
	public WebElement currentAppHeadingOnDropDown;

    @FindBy(how = How.CSS, using = "#current_agent .application_dropdown")
    public WebElement appSearchDropDown;

	@FindBy(how = How.XPATH, using = ".//*[@id='primary_navigation']//a")
	public List<WebElement> leftTabs;
	
	@FindBy(how = How.ID, using = "instances")
	public WebElement jvmTable;
	
	@FindBy(how = How.CSS, using = ".instance_jvm h3")
	public WebElement serverNameText;
	
	@FindBy(how = How.CSS, using = ".number_block.small p.unit")
	public List<WebElement> unitNames;
	
	@FindBy(how = How.CSS, using = ".number_block.small p.unit")
	public WebElement values;
	
	@FindBy(how = How.XPATH, using = ".//*[starts-with(@id,'drilldown_content')]")
	public WebElement rightContent;
	
	@FindBy(how = How.CSS, using = "#time_window_nav h4")
	public WebElement timePickerTab;
	
	@FindBy(how = How.CSS, using = "#time_window_nav .time_picker_container")
	public WebElement timePickerContainer;
	
	@FindBy(how = How.ID, using = "custom_date")
	public WebElement customDateTab;
	
	@FindBy(how = How.ID, using = "custom_date_picker")
	public WebElement customDatePicker;
	
	@FindBy(how = How.CSS, using = "#time_window_ranges a")
	public List<WebElement> timeWindowRanges;
	
	@FindBy(how = How.CSS, using = "input.ui-timepicker-input")
	public WebElement endingTimePickerInput;
	
	@FindBy(how = How.ID, using = "set_the_time")
	public WebElement goButton;

    @FindBy(how = How.XPATH, using = ".//*[starts-with(@id,'instances_tabs')]//a[text()='Threads']")
    public WebElement threadsTabLink;

    @FindBy(how = How.CSS, using = "[id^='instances_tabs'] [class^=instance_threads]")
    public WebElement threadsTab;

    @FindBy(how = How.CSS, using = "#tab_content [id^='tab_content_instance_threads']")
	public WebElement threadsTabContent;

    @FindBy(how = How.CSS, using = "#tab_content [id^='tab_content_instance_threads'] .highcharts[id^='instance_threads_chart_'][id$='_1']")
	public WebElement threadCountGraph;

    @FindBy(how = How.CSS, using = "#tab_content [id^='tab_content_instance_threads'] .highcharts[id^='instance_threads_chart_'][id$='_1'] .highcharts-tracker")
    public WebElement threadCountLineTracker;

    @FindBy(how = How.CSS, using = "#tab_content [id^='tab_content_instance_threads'] .highcharts[id^='instance_threads_chart_'][id$='_1'] .highcharts-tooltip")
    public WebElement threadCountToolTip;

    @FindBy(how = How.CSS, using = "#tab_content [id^='tab_content_instance_threads'] .highcharts[id^='instance_threads_chart_'][id$='_1'] .highcharts-markers")
    public WebElement threadCountLineMarker;

    @FindBy(how = How.CSS, using = "#instances a")
    public List<WebElement> ipList;
}
