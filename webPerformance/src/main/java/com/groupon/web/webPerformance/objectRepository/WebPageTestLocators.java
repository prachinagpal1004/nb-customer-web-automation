package com.groupon.web.webPerformance.objectRepository;

import java.util.List;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;

public class WebPageTestLocators {
	

	@FindBy(how = How.ID, using = "url")
	public WebElement urlBox;
	
	@FindBy(how = How.ID, using = "location")
	public WebElement locationSelectBox;
	
	@FindBy(how = How.ID, using = "browser")
	public WebElement browserSelectBox;
	
	@FindBy(how = How.ID, using = "connection")
	public WebElement connectionSelectBox;
	
	@FindBy(how = How.ID, using = "videoCheck")
	public WebElement videoCheckbox;
	
	@FindBy(how = How.ID, using = "advanced_settings-container")
	public WebElement advancedSettingContainer;
	
	@FindBy(how = How.ID, using = "advanced_settings")
	public WebElement advancedSettingsLink;
	
	@FindBy(how = How.CSS, using = "#test_subbox-container .ui-tabs-nav li a")
	public List<WebElement> authTab;
	
	@FindBy(how = How.ID, using = "username")
	public WebElement authTabUsername;
	
	@FindBy(how = How.ID, using = "password")
	public WebElement authTabPassword;
	
	@FindBy(how = How.CSS, using = ".start_test")
	public WebElement startTestButton;
	
	@FindBy(how = How.ID, using = "statusImg")
	public WebElement waitingInQueueImage;
	
	@FindBy(how = How.CSS, using = "#statusText")
	public WebElement waitingInQueueStatusText;
	
	@FindBy(how = How.CSS, using = "#result h3")
	public WebElement heading;
	
	@FindBy(how = How.ID, using = "tableResults")
	public WebElement tabularResults;
	
	@FindBy(how = How.CSS, using = "h2 .medium")
	public WebElement performanceTestRunningForURL;
	
	@FindBy(how = How.ID, using = "LoadTime")
	public WebElement loadTime;
	
	@FindBy(how = How.ID, using = "TTFB")
	public WebElement firstByte;
	
	@FindBy(how = How.ID, using = "StartRender")
	public WebElement startRender;
	
	@FindBy(how = How.ID, using = "SpeedIndex")
	public WebElement speedIndex;
	
	@FindBy(how = How.ID, using = "TimeToInteractive")
	public WebElement interactiveTime;
	
	@FindBy(how = How.ID, using = "DocComplete")
	public WebElement docCompleteTime;
	
	@FindBy(how = How.ID, using = "RequestsDoc")
	public WebElement requestsDoc;
	
	@FindBy(how = How.ID, using = "BytesInDoc")
	public WebElement bytesInDoc;
	
	@FindBy(how = How.ID, using = "FullyLoaded")
	public WebElement fullyLoaded;
	
	@FindBy(how = How.ID, using = "Requests")
	public WebElement requests;
	
	@FindBy(how = How.ID, using = "BytesIn")
	public WebElement bytesIn;
	
	@FindBy(how = How.CSS, using = ".logo a")
	public WebElement logo;
	
	
	
}
