package com.groupon.web.webPerformance.objectRepository;

import java.util.List;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;

public class GooglePageSpeedLocators {
	

	@FindBy(how = How.CSS, using = ".score.value")
	public List<WebElement> scoreValue;
	
	@FindBy(how = How.CSS, using = ".screenshot.desktop")
	public WebElement screenshot;
	
	@FindBy(how = How.CSS, using = ".screenshot.mobile")
	public WebElement mobileScreenshot;
	
	
	
}
