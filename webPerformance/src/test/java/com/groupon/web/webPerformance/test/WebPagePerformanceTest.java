package com.groupon.web.webPerformance.test;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Optional;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import com.groupon.web.common.config.Config;
import com.groupon.web.common.test.BaseTest;
import com.groupon.web.common.utils.GoogleSheetUtils;
import com.groupon.web.webPerformance.data.URLsData;
import com.groupon.web.webPerformance.page.WebPageTest;

/**
 * 
 * @author prachi.nagpal
 *
 */
@Test(groups = {"webPagePerformanceTest"})
public class WebPagePerformanceTest extends BaseTest {

	private WebPageTest resultsPage;
	private List<String> pageSpeedValues = new ArrayList<String>();
	List<String> listOfAllScores = new ArrayList<String>();
	List<String> allValues = new ArrayList<String>();

	@BeforeMethod(alwaysRun=true)
	public void init() {
		resultsPage = new WebPageTest();
	}


	@BeforeClass
	public void addHeaders(){
		pageSpeedValues.add("URL" + "," + "GooglePageSpeedScore");
	}

	/**
	 * WebPageTest
	 */

	@Test(priority = 1, dataProviderClass = URLsData.class, dataProvider = "webPageSpeedURL")
	public void testWebPageTest(String endPoint, String location, String browser, String network){
		resultsPage.openBaseUrl();
		String url = Config.getInstance().getConfig("web.performance.webpagetest.url") + endPoint;
		allValues.addAll(resultsPage.enterURLAndGetResults(url, location, browser, network));
//		pageSpeedValues.add(url + "," + loadTime);
//		listOfAllScores.add(loadTime);
	}


	@SuppressWarnings("unchecked")
	@AfterClass
	@Parameters({"domain", "duration"})
	public void writeToFile(
			@Optional("web") String domain,
			@Optional("daily") String duration) throws ParseException{
		try {
//			File file = new File(System.getProperty("user.dir")+File.separator+"target"+File.separator+"WebPageTestTime.csv");
//			// if file doesn't exists, then create it
//			if (file.exists()) {
//				file.delete();
//			}
//			file.createNewFile();
//
//			FileWriter fw = new FileWriter(file.getAbsoluteFile());
//			BufferedWriter bw = new BufferedWriter(fw);
//
//			for(String l : pageSpeedValues){
//				bw.write(l);
//				bw.newLine();
//			}
//			bw.close();


			String spreadsheetId = Config.getInstance().getConfig("web.performance.webpagetest.output.sheetId");
			String sheetName = Config.getInstance().getConfig("web.performance.webpagetest.output.sheetName."+domain+"."+duration);
			String range = sheetName + "!A3";
			
			List<String> colValues = new ArrayList<String>();
			colValues.add(new SimpleDateFormat("MM/dd HH:mm").format(new Date()));
			colValues.addAll(allValues);
			List<Object> finalList = new ArrayList<Object>();
			finalList.addAll(colValues);
			Map<String, Object> response = GoogleSheetUtils.appendValues(spreadsheetId, range, Arrays.asList(finalList));
			System.out.println(response);

		} catch (IOException e) {
			e.printStackTrace();
		}

	}
}
