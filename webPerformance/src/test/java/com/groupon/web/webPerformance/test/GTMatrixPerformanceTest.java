package com.groupon.web.webPerformance.test;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.apache.http.HttpResponse;
import org.apache.http.auth.AuthScope;
import org.apache.http.auth.UsernamePasswordCredentials;
import org.apache.http.client.CredentialsProvider;
import org.apache.http.client.methods.HttpRequestBase;
import org.apache.http.impl.client.BasicCredentialsProvider;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.impl.conn.PoolingHttpClientConnectionManager;
import org.apache.log4j.Logger;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Optional;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.groupon.web.api.dto.RequestDTO;
import com.groupon.web.api.helper.ApiHelper;
import com.groupon.web.common.config.Config;
import com.groupon.web.common.test.BaseTest;
import com.groupon.web.common.utils.GoogleSheetUtils;
import com.groupon.web.webPerformance.data.URLsData;

import junit.framework.Assert;

/**
 * 
 * @author prachi.nagpal
 *
 */
@Test(groups = {"gtMatrixPerformanceTest"})
public class GTMatrixPerformanceTest extends BaseTest {

	private static final Logger logger = Logger.getLogger(GTMatrixPerformanceTest.class);
	protected ApiHelper apiHelper;
	private CloseableHttpClient httpClient;
	private String baseUrl;
	private String loginUser;
	private String loginPass;
	private String ySlow;
	private List<String> reportUrls = new ArrayList<String>();
	List<String> list = new ArrayList<String>();
	List<String> yslowScoresList = new ArrayList<String>();

	@BeforeClass
	public void setup() {

		apiHelper = new ApiHelper();
		baseUrl = Config.getInstance().getConfig("web.performance.gtmatrix.baseUrl");
		loginUser = Config.getInstance().getConfig("web.performance.gtmatrix.login.user");
		loginPass = Config.getInstance().getConfig("web.performance.gtmatrix.login.pwd");

		logger.info("Base Url:"+baseUrl);

		PoolingHttpClientConnectionManager cm = new PoolingHttpClientConnectionManager();
		cm.setMaxTotal(100);

		CredentialsProvider provider = new BasicCredentialsProvider();
		UsernamePasswordCredentials credentials = new UsernamePasswordCredentials(loginUser, loginPass);
		provider.setCredentials(AuthScope.ANY, credentials);

		httpClient = HttpClients.custom()
				.setConnectionManager(cm)
				.setDefaultCredentialsProvider(provider)
				.build();

		List<String> headers = new ArrayList<String>();
		headers.add("URL");
		headers.add("PAGESPEED_SCORE");
		headers.add("YSLOW_SCORE");
		headers.add("PAGE_LOAD_TIME(s)");
		headers.add("PAGE_SIZE(KB)");
		headers.add("REQUESTS");
		headers.add("REPORT_PDF_FULL_URL");
		reportUrls.add(StringUtils.join(headers, ","));

	}

	//	@BeforeMethod(alwaysRun=true)
	//	public void testData(Object[] testData) {
	//		logger.info("Before method");
	//		String testCase = (String)testData[0];
	//		logger.info(testCase);
	//		this.testCaseName = testCase;
	//	}

	/**
	 * Test Page Speed for GTMatrix
	 */
	@Test(priority = 1, dataProviderClass = URLsData.class, dataProvider = "gtMetrixURL")
	public void testGTmetrix(String url, String browserId, String locationId, String networkId, String cookies) {

		RequestDTO requestDTO = new RequestDTO(baseUrl);
		//		requestDTO.setApiName(apiName);
		requestDTO.setMethodType("POST");
		requestDTO.setExpectedResponseCode(200);
		requestDTO.setHeaderJson("{\"Content-Type\":\"application/x-www-form-urlencoded\"}");
		Map<String, String> formParams = new HashMap<String, String>();
		formParams.put("url", url);
		formParams.put("browser", browserId);
		formParams.put("location", locationId);
		formParams.put("x-metrix-throttle", networkId);
		formParams.put("login-user", loginUser);
		formParams.put("login-pass", loginPass);
		if(StringUtils.isNotBlank(cookies)) {
			formParams.put("x-metrix-cookies", cookies);
		}
		requestDTO.setFormParams(formParams);

		logger.info("Trying Request:"+requestDTO);

		try {
			final HttpRequestBase httpRequest = apiHelper.prepareHTTPRequest(requestDTO);
			Assert.assertNotNull(httpRequest);
			final HttpResponse httpResponse = httpClient.execute(httpRequest);
			Assert.assertNotNull(httpResponse);
			String responseBody = apiHelper.getResponseBody(httpResponse);
			logger.info("Response:" + responseBody);
			Assert.assertEquals(requestDTO.getExpectedResponseCode(), httpResponse.getStatusLine().getStatusCode());
			Assert.assertNotNull(responseBody);
			String poll_state_url = new JsonParser().parse(responseBody).getAsJsonObject().get("poll_state_url").getAsString();
			logger.info("poll_state_url:"+poll_state_url);

			Thread.sleep(90000);

			final HttpResponse pollStateResponse = httpClient.execute(apiHelper.prepareHTTPRequest(new RequestDTO(poll_state_url)));
			Assert.assertNotNull(pollStateResponse);
			String pollStateResponseBody = apiHelper.getResponseBody(pollStateResponse);
			logger.info("PollStateResponseBody:" + pollStateResponseBody);
			Assert.assertEquals(200, httpResponse.getStatusLine().getStatusCode());
			Assert.assertNotNull(pollStateResponseBody);
			JsonObject parentJsonObject = new JsonParser().parse(pollStateResponseBody).getAsJsonObject();
			String reportPdfFullUrl = parentJsonObject.get("resources").getAsJsonObject().get("report_pdf_full").getAsString();
			JsonObject resultMetrics = parentJsonObject.get("results").getAsJsonObject();
			logger.info("reportPdfFullUrl:"+reportPdfFullUrl);
			logger.info("resultMetrics:"+resultMetrics.toString());
			reportPdfFullUrl = appendUserCredentialsToUrl(reportPdfFullUrl, loginUser, loginPass);

			String pageSpeed = resultMetrics.get("pagespeed_score").getAsString();
			ySlow = resultMetrics.get("yslow_score").getAsString();
			String pageLoadTimeMilis = resultMetrics.get("page_load_time").getAsString();
			final Double pageLoadTimeSeconds = Double.valueOf(pageLoadTimeMilis)*0.001;
			String pageSizeBytes = resultMetrics.get("page_bytes").getAsString();
			final Double pageSizeKB = Double.valueOf(pageSizeBytes)/1024;
			String requests = resultMetrics.get("page_elements").getAsString();


			list.add(url);
			list.add(pageSpeed);
			list.add(ySlow);
			list.add(pageLoadTimeSeconds.toString());
			list.add(pageSizeKB.toString());
			list.add(requests);
			list.add(reportPdfFullUrl);
			reportUrls.add(StringUtils.join(list, ","));
			yslowScoresList.add(ySlow);

		} catch (Exception e) {
			throw new RuntimeException(e);
		}

	}

	private String appendUserCredentialsToUrl(String reportPdfFullUrl, String loginUser, String loginPass) {
		return reportPdfFullUrl.replace("https://","https://"+loginUser+":"+loginPass+"@");
	}


	@AfterClass
	@Parameters({"domain", "duration"})
	public void afterClass(
			@Optional("web") String domain,
			@Optional("daily") String duration) {
		if(httpClient != null) {
			try {
				httpClient.close();
			} catch (IOException e) {
				throw new RuntimeException(e);
			}
		}

		try {
//			writeGTMetrixResultsToFile();
			writeGTMetrixResultsToGoogleSheet(domain, duration);

		} catch (IOException e) {
			logger.error("Error in writing to excel",e);
		}
	}

	@SuppressWarnings("unchecked")
	private void writeGTMetrixResultsToGoogleSheet(String domain, String duration) throws IOException {
		logger.info("Writing GT Metrix Results to Google Sheet");

		String spreadsheetId = Config.getInstance().getConfig("web.performance.gtmatrix.output.sheetId");
		String sheetName = Config.getInstance().getConfig("web.performance.gtmatrix.output.sheetName."+domain+"."+duration);
		String range = sheetName + "!A3";

		List<String> colValues = new ArrayList<String>();
		colValues.add(new SimpleDateFormat("MM/dd").format(new Date()));
		colValues.addAll(yslowScoresList);
		List<Object> finalList = new ArrayList<Object>();
		finalList.addAll(colValues);

		Map<String, Object> response = GoogleSheetUtils.appendValues(spreadsheetId, range, Arrays.asList(finalList));
		
		logger.info("Completed writing GT Metrix Results to Google Sheet, Data:"+colValues+", Response:"+response);
	}

//	private void writeGTMetrixResultsToFile() throws IOException {
//		logger.info("Writing GT Metrix Results to File");
//		File file = new File(System.getProperty("user.dir")+File.separator+"target"+File.separator+"GTmetrix.csv");
//		// if file doesn't exists, then create it
//		if (file.exists()) {
//			file.delete();
//		}
//		file.createNewFile();
//
//		FileWriter fw = new FileWriter(file.getAbsoluteFile());
//		BufferedWriter bw = new BufferedWriter(fw);
//
//		for(String l : reportUrls){
//			bw.write(l);
//			bw.newLine();
//		}
//		bw.close();
//		logger.info("Completed Writing gt metrix results to File: + "+reportUrls);
//	}

}
