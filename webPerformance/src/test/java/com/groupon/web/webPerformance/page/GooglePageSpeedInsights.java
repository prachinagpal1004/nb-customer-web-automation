package com.groupon.web.webPerformance.page;

import com.groupon.web.common.config.Config;
import com.groupon.web.common.page.BasePage;
import com.groupon.web.webPerformance.objectRepository.GooglePageSpeedLocators;

import org.apache.log4j.Logger;
import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;

/**
 * 
 * @author prachi.nagpal
 *
 */
public class GooglePageSpeedInsights extends BasePage{

	private GooglePageSpeedLocators object;
	private static final Logger logger = Logger.getLogger(GooglePageSpeedInsights.class);

	public GooglePageSpeedInsights() {
		object = PageFactory.initElements(getWebDriver(), GooglePageSpeedLocators.class);
	}

	/**
	 * Traverse to Results Page
	 * @return
	 */

	public String scoreValue(){

		waitVar.until(ExpectedConditions.visibilityOf(object.screenshot));
		Assert.assertTrue(object.scoreValue.get(1).isDisplayed());
		String scoreValue = object.scoreValue.get(1).getText();
		logger.info("Score :" + scoreValue);
	
		return scoreValue;

	}
	public String mobileScoreValue(){
		
		waitVar.until(ExpectedConditions.visibilityOf(object.mobileScreenshot));
		Assert.assertTrue(object.scoreValue.get(0).isDisplayed());
		String scoreValue = object.scoreValue.get(0).getText();
		logger.info("Score :" + scoreValue);
		
		return scoreValue;
		
	}

    public void openUrl(String url) {
        String baseUrl = Config.getInstance().getConfig("web.performance.googlepagespeed.baseUrl");
        getWebDriver().get(baseUrl + "?url="+url+"&tab=desktop");
    }
    
    public void openMobileUrl(String url) {
    	String baseUrl = Config.getInstance().getConfig("web.performance.googlepagespeed.baseUrl");
    	getWebDriver().get(baseUrl + "?url="+url+"&tab=mobile");
    }
}
