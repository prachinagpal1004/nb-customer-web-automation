package com.groupon.web.webPerformance.test;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.groupon.web.api.dto.RequestDTO;
import com.groupon.web.api.helper.ApiHelper;
import com.groupon.web.common.config.Config;
import com.groupon.web.common.test.BaseTest;
import com.groupon.web.webPerformance.data.URLsData;
import junit.framework.Assert;
import org.apache.commons.lang3.StringUtils;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpRequestBase;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.impl.conn.PoolingHttpClientConnectionManager;
import org.apache.log4j.Logger;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 
 * @author prachi.nagpal
 *
 */
public class WebPageAPITest extends BaseTest {

	private static final Logger logger = Logger.getLogger(WebPageAPITest.class);
	protected ApiHelper apiHelper;
	private CloseableHttpClient httpClient;
	private String baseUrl;
	private String loginApiKey;
	private List<String> reportUrls = new ArrayList<String>();

	@BeforeClass
	public void setup() {

		apiHelper = new ApiHelper();
		baseUrl = Config.getInstance().getConfig("web.performance.webpagetest.apibaseUrl");
		loginApiKey = Config.getInstance().getConfig("web.performance.webpagetest.login.apikey");

		logger.info("Base Url:"+baseUrl);

		PoolingHttpClientConnectionManager cm = new PoolingHttpClientConnectionManager();
		cm.setMaxTotal(100);

		/*CredentialsProvider provider = new BasicCredentialsProvider();
		UsernamePasswordCredentials credentials = new UsernamePasswordCredentials(loginUser, loginPass);
		provider.setCredentials(AuthScope.ANY, credentials);*/

		httpClient = HttpClients.custom()
				.setConnectionManager(cm)
//				.setDefaultCredentialsProvider(provider)
				.build();
		
		List<String> headers = new ArrayList<String>();
		headers.add("URL");
		headers.add("LOAD TIME");
		headers.add("FIRST BYTE");
		headers.add("START RENDER");
		headers.add("SPEED INDEX");
		headers.add("DOM ELEMENTS");
		headers.add("REQUESTS");
		headers.add("REPORT_FULL_URL");
		reportUrls.add(StringUtils.join(headers, ","));

	}

//	@BeforeMethod(alwaysRun=true)
//	public void testData(Object[] testData) {
//		logger.info("Before method");
//		String testCase = (String)testData[0];
//		logger.info(testCase);
//		this.testCaseName = testCase;
//	}

	/**
	 * Test Page Speed for WebPageTest
	 */
	@Test(priority = 1, dataProviderClass = URLsData.class, dataProvider = "webPageTestURL")
	public void testWebPageTestAPI(String url, String locationId) {

		RequestDTO requestDTO = new RequestDTO(baseUrl);
//		requestDTO.setApiName(apiName);
		requestDTO.setMethodType("GET");
		requestDTO.setExpectedResponseCode(200);
		//requestDTO.setHeaderJson("{\"Content-Type\":\"application/x-www-form-urlencoded\"}");
		Map<String, String> queryParams = new HashMap<String, String>();
		queryParams.put("url", url);
		queryParams.put("location", locationId);
		queryParams.put("f", "json");
		queryParams.put("fvonly", "1");
		queryParams.put("k", loginApiKey);
        Gson gson = new GsonBuilder().create();
		requestDTO.setQueryParamJson(gson.toJson(queryParams));

		logger.info("Trying Request:"+requestDTO);

		try {
			final HttpRequestBase httpRequest = apiHelper.prepareHTTPRequest(requestDTO);
			Assert.assertNotNull(httpRequest);
			final HttpResponse httpResponse = httpClient.execute(httpRequest);
			Assert.assertNotNull(httpResponse);
			String responseBody = apiHelper.getResponseBody(httpResponse);
			logger.info("Response:" + responseBody);
			Assert.assertEquals(requestDTO.getExpectedResponseCode(), httpResponse.getStatusLine().getStatusCode());
			Assert.assertNotNull(responseBody);
            JsonParser jsonParser = new JsonParser();
            String jsonUrl = jsonParser.parse(responseBody).getAsJsonObject().get("data").getAsJsonObject().get("jsonUrl").getAsString();
            String testId = jsonParser.parse(responseBody).getAsJsonObject().get("data").getAsJsonObject().get("testId").getAsString();
			logger.info("TestId:"+testId+", JsonUrl:"+jsonUrl);

            boolean complete = false;
            boolean error = false;
            String successResponse = null;
            int tries = 1;

            while(!complete && tries <=5) {
                sleep();
                final HttpResponse pollStateResponse = httpClient.execute(apiHelper.prepareHTTPRequest(new RequestDTO(jsonUrl)));
                Assert.assertNotNull(pollStateResponse);
                String pollStateResponseBody = apiHelper.getResponseBody(pollStateResponse);
                logger.info("Checking Status of Test:" + pollStateResponseBody);
                Assert.assertEquals(200, httpResponse.getStatusLine().getStatusCode());
                Assert.assertNotNull(pollStateResponseBody);
                int testStatus = jsonParser.parse(pollStateResponseBody).getAsJsonObject().get("statusCode").getAsInt();
                if(testStatus == 200) {
                    logger.info("Test is complete for RequestUrl:"+url);
                    complete = true;
                    successResponse = pollStateResponseBody;
                }
                else if ((testStatus/100) == 4) {
                    logger.info("Test failed for RequestUrl:"+url);
                    complete = true;
                    error = true;
                    successResponse = pollStateResponseBody;
                }
                else {
                    tries++;
                    logger.info("Test not yet complete for RequestUrl:"+url+", Retry after 40 seconds...Num of Retry:"+tries);
                }
            }

            if(error) {
                Assert.fail("Test failed for RequestUrl:"+url+", Reason:"+jsonParser.parse(successResponse).getAsJsonObject().get("statusText").getAsString());
            }
            else {
                if(!complete) {
                    //Cancel Test
                    httpClient.execute(apiHelper.prepareHTTPRequest(new RequestDTO("http://www.webpagetest.org/cancelTest.php?test="+testId+"&k="+loginApiKey)));
                    Assert.fail("Test not complete after 5 retries for RequestUrl:"+url);
                }
                else {
                    JsonObject dataJsonObj = jsonParser.parse(successResponse).getAsJsonObject().get("data").getAsJsonObject();
                    String reportPdfFullUrl = dataJsonObj.get("summary").getAsString();
                    JsonObject resultMetrics = dataJsonObj.get("average").getAsJsonObject().get("firstView").getAsJsonObject();
                    logger.info("reportPdfFullUrl:"+reportPdfFullUrl);
                    logger.info("resultMetrics:"+resultMetrics.toString());

                    Double pageLoadTimeSeconds = resultMetrics.get("loadTime").getAsDouble()/1000;
                    Double firstByteTimeSeconds = resultMetrics.get("TTFB").getAsDouble()/1000;
                    Double startRenderTimeSeconds = resultMetrics.get("render").getAsDouble()/1000;
                    Long speedIndex = resultMetrics.get("SpeedIndex").getAsLong();
                    Long domElements = resultMetrics.get("domElements").getAsLong();
                    Integer requests = resultMetrics.get("requestsFull").getAsInt();

                    List<String> list = new ArrayList<String>();
                    list.add(url);
                    list.add(pageLoadTimeSeconds.toString());
                    list.add(firstByteTimeSeconds.toString());
                    list.add(startRenderTimeSeconds.toString());
                    list.add(speedIndex.toString());
                    list.add(domElements.toString());
                    list.add(requests.toString());
                    list.add(reportPdfFullUrl);
                    reportUrls.add(StringUtils.join(list, ","));
                }
            }
		} catch (Exception e) {
			throw new RuntimeException(e);
		}

	}

    private void sleep() throws InterruptedException {
    	logger.info("Sleeping for 40 seconds");
        Thread.sleep(40000);
        logger.info("Resume from sleep");
        
    }

	@AfterClass
	public void afterSuite() {
		if(httpClient != null) {
			try {
				httpClient.close();
			} catch (IOException e) {
				throw new RuntimeException(e);
			}
		}

		try {
			File file = new File(System.getProperty("user.dir")+File.separator+"target"+File.separator+"WebPageTestAPI.csv");
			// if file doesn't exists, then create it
			if (file.exists()) {
				file.delete();
			}
			file.createNewFile();

			FileWriter fw = new FileWriter(file.getAbsoluteFile());
			BufferedWriter bw = new BufferedWriter(fw);

			for(String l : reportUrls){
				bw.write(l);
				bw.newLine();
			}
			bw.close();
		} catch (IOException e) {
			logger.error("Error in writing to excel",e);
		}
	}

}
