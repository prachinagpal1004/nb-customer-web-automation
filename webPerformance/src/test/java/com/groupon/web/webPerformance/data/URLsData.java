package com.groupon.web.webPerformance.data;

import com.groupon.web.common.config.Config;
import com.groupon.web.common.utils.DataProviderUtil;
import org.apache.log4j.Logger;
import org.testng.annotations.DataProvider;

/**
 * 
 * @author prachi.nagpal
 *
 */
public class URLsData {

    private static final Logger LOGGER = Logger.getLogger(URLsData.class);

	@DataProvider(name = "webPageSpeedURL")
	public static Object[][] provideWebPageTestData(){
        LOGGER.info("Load webPageSpeedURL Data");
		return DataProviderUtil.provideData(Config.getInstance().getConfig("testData.file"), "webPageTest","url");
	}

	@DataProvider(name = "gtMetrixURL")
	public static Object[][] provideGTMetrixData(){
		LOGGER.info("Load gtMetrixURL Data");
		return DataProviderUtil.provideData(Config.getInstance().getConfig("testData.file"), "gtMetrix","url");
	}
	
	@DataProvider(name = "googlePageSpeedURL")
	public static Object[][] provideGooglePageSpeedURL(){
		LOGGER.info("Load googlePageSpeedURL Data");
		return DataProviderUtil.provideData(Config.getInstance().getConfig("testData.file"), "googlePageSpeed","url");
	}
	
	@DataProvider(name = "webPageTestURL")
	public static Object[][] providWebPageTestURL(){
		LOGGER.info("Load webPageAPItest URL");
		return DataProviderUtil.provideData(Config.getInstance().getConfig("testData.file"), "webPageTestAPI","url");
	}
}
