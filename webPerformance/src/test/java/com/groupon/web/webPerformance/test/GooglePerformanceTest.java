package com.groupon.web.webPerformance.test;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Optional;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import com.groupon.web.common.config.Config;
import com.groupon.web.common.test.BaseTest;
import com.groupon.web.common.utils.GoogleSheetUtils;
import com.groupon.web.webPerformance.data.URLsData;
import com.groupon.web.webPerformance.page.GooglePageSpeedInsights;

/**
 * 
 * @author prachi.nagpal
 *
 */
@Test(groups = {"googlePerformanceTest"})
public class GooglePerformanceTest extends BaseTest {

	private GooglePageSpeedInsights resultsPage;
	private List<String> pageSpeedValues = new ArrayList<String>();
	List<String> listOfAllScores = new ArrayList<String>();
	private String domain;

	@BeforeMethod(alwaysRun=true)
	public void init() {
		resultsPage = new GooglePageSpeedInsights();
	}


	@BeforeClass
	@Parameters({"domain"})
	public void addHeaders(@Optional("web") String domain) {
		pageSpeedValues.add("URL" + "," + "GooglePageSpeedScore");
		this.domain = domain;
		System.out.println("******** Web Performance Domain ******  :"+domain);
	}

	/**
	 * Google Page Speed Performance
	 */
	@Test(priority = 1, dataProviderClass = URLsData.class, dataProvider = "googlePageSpeedURL")
	public void testGooglePageSpeed(String url) throws InterruptedException {
		if("mobile".equalsIgnoreCase(this.domain)) {
			resultsPage.openMobileUrl(url);
		}
		else {
			resultsPage.openUrl(url);
		}
		String scoreValue = "mobile".equalsIgnoreCase(this.domain) ? resultsPage.mobileScoreValue() : resultsPage.scoreValue();
		pageSpeedValues.add(url + "," + scoreValue);
		listOfAllScores.add(scoreValue.substring(0, scoreValue.indexOf('/')));
	}


	@SuppressWarnings("unchecked")
	@AfterClass
	@Parameters({"duration"})
	public void writeToFile(
			@Optional("daily") String duration) throws ParseException{
		try {
//			File file = new File(System.getProperty("user.dir")+File.separator+"target"+File.separator+"GooglePageSpeedScore.csv");
//			// if file doesn't exists, then create it
//			if (file.exists()) {
//				file.delete();
//			}
//			file.createNewFile();
//
//			FileWriter fw = new FileWriter(file.getAbsoluteFile());
//			BufferedWriter bw = new BufferedWriter(fw);
//
//			for(String l : pageSpeedValues){
//				bw.write(l);
//				bw.newLine();
//			}
//			bw.close();

			String spreadsheetId = Config.getInstance().getConfig("web.performance.googlepagespeed.output.sheetId");
			String sheetName = Config.getInstance().getConfig("web.performance.googlepagespeed.output.sheetName."+this.domain+"."+duration);
			String range = sheetName + "!A3";
			
			List<String> colValues = new ArrayList<String>();
			colValues.add(new SimpleDateFormat("MM/dd").format(new Date()));
			colValues.addAll(listOfAllScores);
			List<Object> finalList = new ArrayList<Object>();
			finalList.addAll(colValues);
			Map<String, Object> response = GoogleSheetUtils.appendValues(spreadsheetId, range, Arrays.asList(finalList));

			System.out.println(response);

		} catch (IOException e) {
			e.printStackTrace();
		}

	}
}
