package com.groupon.web.webPerformance.page;

import com.groupon.web.common.config.Config;
import com.groupon.web.common.page.BasePage;
import com.groupon.web.common.utils.TakeSnapshotUtil;
import com.groupon.web.common.utils.WaitUtility;
import com.groupon.web.webPerformance.objectRepository.GooglePageSpeedLocators;
import com.groupon.web.webPerformance.objectRepository.WebPageTestLocators;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;

/**
 * 
 * @author prachi.nagpal
 *
 */
public class WebPageTest extends BasePage {

	private WebPageTestLocators object;
	private static final Logger logger = Logger.getLogger(WebPageTest.class);
	List<String> list = new ArrayList<String>();

	public WebPageTest() {
		object = PageFactory.initElements(getWebDriver(), WebPageTestLocators.class);
	}

	/**
	 * Traverse to Results Page
	 * @return
	 */

	public List<String> enterURLAndGetResults(String url, String testLocation, String browser, String connection){

		waitVar.until(ExpectedConditions.elementToBeClickable(object.urlBox));
		Assert.assertTrue(object.urlBox.isDisplayed());
		Assert.assertTrue(object.urlBox.isEnabled());

		object.urlBox.clear();
		object.urlBox.sendKeys(url);

		Select location = new Select(object.locationSelectBox);
		location.selectByValue(testLocation);

		Select browserBox = new Select(object.browserSelectBox);
		browserBox.selectByValue(browser);

		waitVar.until(ExpectedConditions.elementToBeClickable(object.advancedSettingsLink));
		object.advancedSettingsLink.click();
		waitVar.until(ExpectedConditions.visibilityOf(object.advancedSettingContainer));
		waitVar.until(ExpectedConditions.invisibilityOfElementLocated(By.id("settings_summary_label")));

		Select connectionBox = new Select(object.connectionSelectBox);
		connectionBox.selectByValue(connection);

		object.videoCheckbox.click();

		if(baseUrl.contains("v3")){
			object.authTab.get(3).click();
			waitVar.until(ExpectedConditions.elementToBeClickable(object.authTabUsername));
			object.authTabUsername.sendKeys("nearbuy");
			object.authTabPassword.sendKeys("Nearbuy@2016");
		}


		object.startTestButton.submit();
		
//		WaitUtility.waitForLoad(getWebDriver());

//		waitVar.until(ExpectedConditions.visibilityOf(object.waitingInQueueImage));
//		waitVar.until(ExpectedConditions.visibilityOf(object.heading));
//		Assert.assertTrue(object.heading.isDisplayed());

		//waitVar.until(ExpectedConditions.textToBePresentInElement(object.waitingInQueueStatusText, "Waiting at the front of the queue…"));


		try {
			Thread.sleep(70000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		waitVar.until(ExpectedConditions.presenceOfElementLocated(By.id("average")));
		waitVar.until(ExpectedConditions.visibilityOfElementLocated(By.id("average")));
		waitVar.until(ExpectedConditions.visibilityOf(object.tabularResults));
		Assert.assertTrue(object.performanceTestRunningForURL.isDisplayed());

		logger.info("Showing Results for : " + object.performanceTestRunningForURL.getText());

		String firstViewLoadTimeText = object.loadTime.getText().split("\n")[0];
		logger.info("First View LoadTime : " + firstViewLoadTimeText);

		String firstByteText = object.firstByte.getText().split("\n")[0];
		logger.info("First Byte : " + firstByteText);

		String startRenderText = object.startRender.getText().split("\n")[0];
		logger.info("Start Render : " + startRenderText);

		String speedIndexText = object.speedIndex.getText().split("\n")[0];
		logger.info("speedIndex : " + speedIndexText);
		String interactiveTimeText="";
		if(WaitUtility.isElementPresent(getWebDriver(), object.interactiveTime)){
			interactiveTimeText = object.interactiveTime.getText().split("\n")[0];
		}
		logger.info("interactiveTimeText : " + interactiveTimeText);

		String docCompleteTimeText = object.docCompleteTime.getText().split("\n")[0];
		logger.info("docCompleteTime : " + docCompleteTimeText);

		String requestsDocText = object.requestsDoc.getText().split("\n")[0];
		logger.info("requestsDoc : " + requestsDocText);

		String bytesInDocText = object.bytesInDoc.getText().split("\n")[0];
		logger.info("bytesInDoc : " + bytesInDocText);

		String fullyLoadedText = object.fullyLoaded.getText().split("\n")[0];
		logger.info("fullyLoaded : " + fullyLoadedText);

		String requests = object.requests.getText().split("\n")[0];
		logger.info("requests : " + requests);

		String bytesIn = object.bytesIn.getText().split("\n")[0];
		logger.info("bytesIn : " + bytesIn);


		list.add(firstViewLoadTimeText);
		list.add(firstByteText);
		list.add(startRenderText);
		list.add(speedIndexText);
		list.add(interactiveTimeText);
		list.add(docCompleteTimeText);
		list.add(requestsDocText);
		list.add(bytesInDocText);
		list.add(fullyLoadedText);
		list.add(requests);
		list.add(bytesIn);


		//		try {
		//			new TakeSnapshotUtil().fullPageScreenshot(getWebDriver());
		//		} catch (Exception e) {
		//			// TODO Auto-generated catch block
		//			e.printStackTrace();
		//		}

		object.logo.click();

		return list;


	}

	public void openBaseUrl() {
		String url = Config.getInstance().getConfig("web.performance.webpagetest.baseUrl");
		getWebDriver().manage().deleteAllCookies();
		getWebDriver().get(url);
	}
}
