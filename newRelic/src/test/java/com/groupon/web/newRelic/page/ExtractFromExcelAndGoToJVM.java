package com.groupon.web.newRelic.page;

import com.groupon.web.common.config.Config;
import com.groupon.web.common.exception.NotAuthorizedException;
import com.groupon.web.common.page.BasePage;
import com.groupon.web.common.utils.ExcelUtil;
import com.groupon.web.common.utils.WaitUtility;
import com.groupon.web.newRelic.objectRepository.HomePageLocator;
import com.steadystate.css.parser.Locatable;

import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.testng.Assert;

import java.io.IOException;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 *
 * @author prachi.nagpal
 *
 */
public class ExtractFromExcelAndGoToJVM extends BasePage{

	private static List<List<String>> jvmStats = new ArrayList<List<String>>();
	private HomePageLocator object;
	private static final Logger logger = Logger.getLogger(ExtractFromExcelAndGoToJVM.class);

	public ExtractFromExcelAndGoToJVM() {
		object = PageFactory.initElements(getWebDriver(), HomePageLocator.class);
	}

	/**
	 * Click On First App Name and to go to next page
	 * @return
	 */

	public ExtractFromExcelAndGoToJVM clickOnAppNameAndGoToDetailPage(){

		waitVar.until(ExpectedConditions.visibilityOfAllElements(object.appNameLinks));
		object.appNameLinks.get(5).click();
		//        for(WebElement link : object.appNameLinks){
		//            if(link.getText().contains(appToBeSelected)){
		//                logger.info("Link clicked : " + link.getText());
		//                link.click();
		//                waitVar.until(ExpectedConditions.elementToBeClickable(object.appSearchDropDown));
		//                logger.info("Clicked on First App");
		//                break;
		//            }
		//        }

		return this;
	}

	/**
	 * Select the Time for the date
	 * @param timeRangeToBeSelected
	 * @param endingTime
	 * @return
	 */
	public ExtractFromExcelAndGoToJVM selectCalenderDateAndTime(String timeRangeToBeSelected, String endingTime){

		waitVar.until(ExpectedConditions.elementToBeClickable(object.timePickerTab));
		object.timePickerTab.click();
		waitVar.until(ExpectedConditions.visibilityOf(object.timePickerContainer));
		object.customDateTab.click();
		waitVar.until(ExpectedConditions.visibilityOf(object.customDatePicker));

		for(WebElement range : object.timeWindowRanges){
			if(range.getAttribute("data-open-duration").equals(timeRangeToBeSelected)){
				range.click();
			}
		}

		object.endingTimePickerInput.clear();
		object.endingTimePickerInput.sendKeys(endingTime);
		object.goButton.click();
		waitVar.until(ExpectedConditions.invisibilityOfElementLocated(By.cssSelector(".time_picker_container")));

		return this;
	}




	/**
	 * Click to open search drop down and enter search keyword
	 * @param searchAppName
	 * @return
	 * @throws NotAuthorizedException 
	 */
	public ExtractFromExcelAndGoToJVM clickOnSearchDropDownAndEnterSearchKeyword(String searchAppName) throws NotAuthorizedException{
		waitVar.until(ExpectedConditions.elementToBeClickable(object.currentAppHeadingOnDropDown));
		object.currentAppHeadingOnDropDown.click();
		WaitUtility.waitForAttributeContainsGivenValue(object.currentAppHeadingOnDropDown,"class","open", getWebDriver(), waitVar);
		waitVar.until(ExpectedConditions.visibilityOf(object.appSearchDropDown));
		waitVar.until(ExpectedConditions.elementToBeClickable(object.appSearchTextBox));
		Assert.assertTrue(object.appSearchDropDown.isDisplayed());
		Assert.assertTrue(object.appSearchTextBox.isDisplayed());
		Assert.assertTrue(object.appSearchTextBox.isEnabled());
		object.appSearchTextBox.clear();
		object.appSearchTextBox.sendKeys(searchAppName.toLowerCase());
		try {
			Thread.sleep(1000);
		} catch (InterruptedException e) {
			throw new RuntimeException(e);
		}
		try {
			waitVar.until(ExpectedConditions.visibilityOf(object.appSearchResultBox));
		}
		catch(Exception e) {
			logger.error("*****No search result found for Application Name*****:"+searchAppName, e);
			throw new NotAuthorizedException("No search result found for Application Name:"+searchAppName);
		}

		WebElement result = getWebDriver().findElement(By.cssSelector("#app_search_results li[style*='list-item'][data-sort-by='"+searchAppName.toLowerCase()+"'] a"));
		Assert.assertTrue(result.isDisplayed());
		logger.info("Result Link Text:"+result.getText());
		result.click();
		//		waitVar.until(ExpectedConditions.textToBePresentInElement(object.currentAppHeadingOnDropDown, searchAppName));
		WaitUtility.waitForLoad(getWebDriver());
		waitVar.until(ExpectedConditions.visibilityOfElementLocated(By.id("time_window_nav")));
		logger.info("Page for Application : " + searchAppName +  " is opened");
		return this;
	}

	/**
	 * Click on JVM tab
	 * @param tabToBeOpened
	 * @return
	 */
	public ExtractFromExcelAndGoToJVM clickOnGivenMonitoringTab(String tabToBeOpened){

		for(WebElement tab : object.leftTabs){
			if(tab.getText().equals(tabToBeOpened)){
				tab.click();
				waitVar.until(ExpectedConditions.visibilityOf(object.jvmTable));
				waitVar.until(ExpectedConditions.visibilityOf(object.rightContent));
				break;
			}
		}

		return this;
	}

	/**
	 * Extract all the values for each units
	 * @return
	 */
	public ExtractFromExcelAndGoToJVM extractAllJvmMonitoringValuesFromPage(String serviceName) {
		List<String> headers = new ArrayList<String>(Arrays.asList(Config.getInstance().getConfig("stats.headers").split(",")));
		for(WebElement ip : object.ipList){
			ip.click();
			waitVar.until(ExpectedConditions.invisibilityOfElementLocated(By.id("drilldown_help")));
			waitVar.until(ExpectedConditions.visibilityOf(object.serverNameText));
			waitVar.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(".//*[starts-with(@id,'div_wrap_instance_memory_chart')]")));
			logger.info("Running for IP : " + object.serverNameText.getText());
			String classValue = ip.getAttribute("class");
			String[] split = classValue.split(" ");
			String instanceId = null;
			for (String value : split) {
				if(value.startsWith("instance_")) {
					instanceId = value.substring(value.indexOf("_") + 1);
					break;
				}
			}
			logger.info("InstanceId:"+instanceId);

			List<String> values = new ArrayList<String>(Arrays.asList(serviceName));
			WebElement parent = getWebDriver().findElement(By.xpath(".//*[starts-with(@id,'drilldown_content')]"));
			for (String header : headers) {
				logger.info("Header : " + header);
				final String locatorKey = header.trim().replaceAll(" ", ".");
				logger.info("Locator Key : " + locatorKey);
				String locator = Config.getInstance().getConfig(locatorKey);
				Assert.assertNotNull(locator);
				logger.info("Locator Value : " + locator);
				WebElement element = parent.findElement(By.xpath(locator));
				String value = element.getText();
				String newValue = value;
				if(value.contains(" ")){
					newValue = value.substring(0, value.indexOf(" "));
				}
				else if(value.contains("(")){
					newValue= value.substring(0, value.indexOf("("));
				}
				logger.info("Value : " + newValue);
				values.add(newValue);
			}
			values.add(getThreadCountValue(instanceId));
			jvmStats.add(values);
			try {
				Thread.sleep(2000);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
		return this;
	}

	private String getThreadCountValue(String instanceId) {
		try {
			Thread.sleep(1000);
		} catch (InterruptedException e1) {
			e1.printStackTrace();
		}
		
		waitVar.until(ExpectedConditions.visibilityOfElementLocated(By.id("drilldown_content_"+instanceId)));
		WebElement drillDownContent = getWebDriver().findElement(By.id("drilldown_content_"+instanceId));
		By threadsTabLocator = By.cssSelector("#instances_tabs_"+instanceId+" .instance_threads-"+instanceId);
		WebElement threadsTab = drillDownContent.findElement(threadsTabLocator);
		waitVar.until(ExpectedConditions.visibilityOf(threadsTab));
		threadsTab.findElement(By.cssSelector("a")).click();

		WaitUtility.waitForAttributeContainsGivenValue(threadsTabLocator, "class", "current", getWebDriver(), waitVar);
		waitVar.until(ExpectedConditions.visibilityOfElementLocated(By.id("tab_content_instance_threads-"+instanceId)));
		waitVar.until(ExpectedConditions.visibilityOfElementLocated(By.id("instance_threads_chart_"+instanceId+"_1")));
		waitVar.until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector("#instance_threads_chart_"+instanceId+"_1 .highcharts-tracker")));
		logger.info("Threads Tab is opened");
		logger.info("Trying Path Points");
		WebElement threadsChart = getWebDriver().findElement(By.id("instance_threads_chart_"+instanceId+"_1"));
		for (WebElement webElement : threadsChart.findElement(By.className("highcharts-markers")).findElements(By.tagName("path"))) {
			logger.info("Marker Path:"+webElement.getLocation());
			Actions actions = new Actions(getWebDriver());
			actions.moveToElement(webElement).build().perform();
			try {
				Thread.sleep(500);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
			if(threadsChart.findElement(By.className("highcharts-tooltip")).isDisplayed()) {
				break;
			}
		}
		waitVar.until(ExpectedConditions.visibilityOf(threadsChart.findElement(By.className("highcharts-tooltip"))));
		String avgThreadCount = threadsChart.findElement(By.className("highcharts-tooltip")).findElement(By.xpath("//*[contains(text(),'Avg:')]")).getText();
		logger.info("Avg Thread Count:" + avgThreadCount);
		return avgThreadCount.replace("Avg:", "").trim();
	}

	public void writeValues() throws IOException, URISyntaxException {
		final String fileName = Config.getInstance().getConfig("stats.output.fileName");
		final String sheetName = Config.getInstance().getConfig("stats.output.sheetName");
		Assert.assertNotNull(fileName);
		Assert.assertNotNull(sheetName);
		ExcelUtil.createExcelFile(fileName, sheetName, true);
		List<List<String>> rows = new ArrayList<List<String>>();
		rows.add(prepareHeaders());
		rows.addAll(jvmStats);
		ExcelUtil.setColValues(fileName, sheetName, rows, 0);
	}

	private List<String> prepareHeaders() {
		List<String> headers = new ArrayList<String>(Arrays.asList("Service Name"));
		headers.addAll(Arrays.asList(Config.getInstance().getConfig("stats.headers").split(",")));
		headers.add("Thread Count");
		return headers;
	}
	
	// Not to be used now
	public ExtractFromExcelAndGoToJVM clickOnAllAppsAndClickForIPs(){
		
		waitVar.until(ExpectedConditions.visibilityOfAllElements(object.appNameLinks));
		for(WebElement eachApp : object.appNameLinks){
			
			Assert.assertTrue(eachApp.isDisplayed());
			Assert.assertTrue(eachApp.isEnabled());
			logger.info("Running for : " + eachApp.getText());
			eachApp.click();
			waitVar.until(ExpectedConditions.visibilityOf(object.headerOnAppPage));
			
			
		}
		
		return this;
	}

}
