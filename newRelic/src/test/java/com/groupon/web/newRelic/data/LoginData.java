package com.groupon.web.newRelic.data;

import org.testng.annotations.DataProvider;

import com.groupon.web.common.config.Config;
import com.groupon.web.common.utils.DataProviderUtil;

/**
 * 
 * @author prachi.nagpal
 *
 */
public class LoginData {

	@DataProvider(name = "newRelicCredentials")
	public static Object[][] provideCredentials(){
		return DataProviderUtil.provideData(Config.getInstance().getConfig("testData.file"), "newRelic","newRelicCredentials");
	}
}
