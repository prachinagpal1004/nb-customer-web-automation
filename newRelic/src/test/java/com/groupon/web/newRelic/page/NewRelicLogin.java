package com.groupon.web.newRelic.page;

import com.groupon.web.common.page.BasePage;
import com.groupon.web.newRelic.objectRepository.LoginLocators;

import org.apache.log4j.Logger;
import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;

/**
 * 
 * @author prachi.nagpal
 *
 */
public class NewRelicLogin extends BasePage{

	private LoginLocators object;
	private static final Logger logger = Logger.getLogger(NewRelicLogin.class);

	public NewRelicLogin() {
		object = PageFactory.initElements(getWebDriver(), LoginLocators.class);
	}

	/**
	 * Enter New Relic Credentials
	 * @return
	 */

	public NewRelicLogin loginNewRelic(String username, String password){

		waitVar.until(ExpectedConditions.elementToBeClickable(object.signInButton));
		object.emailIdTextbox.sendKeys(username);
		object.passwordTextbox.sendKeys(password);
		object.signInButton.click();
		waitVar.until(ExpectedConditions.invisibilityOfElementLocated(By.id("login_submit")));
		waitVar.until(ExpectedConditions.urlContains("accounts"));
		Assert.assertTrue(getWebDriver().getTitle().contains("All applications"));
		logger.info("User in New Relic is logged in");

		return this;

	}

}
