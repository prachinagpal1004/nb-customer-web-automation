package com.groupon.web.newRelic.data;

import com.groupon.web.common.config.Config;
import com.groupon.web.common.utils.DataProviderUtil;
import org.testng.annotations.DataProvider;

/**
 * 
 * @author prachi.nagpal
 *
 */
public class ReportData {

	@DataProvider(name = "serviceNames")
	public static Object[][] provideCredentials(){
		return DataProviderUtil.provideData(Config.getInstance().getConfig("testData.file"), "newRelic","appService");
	}
}
