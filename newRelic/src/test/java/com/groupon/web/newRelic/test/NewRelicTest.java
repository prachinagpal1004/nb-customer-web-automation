package com.groupon.web.newRelic.test;

import com.groupon.web.common.exception.NotAuthorizedException;
import com.groupon.web.common.test.BaseTest;
import com.groupon.web.newRelic.data.LoginData;
import com.groupon.web.newRelic.data.ReportData;
import com.groupon.web.newRelic.page.ExtractFromExcelAndGoToJVM;
import com.groupon.web.newRelic.page.NewRelicLogin;

import org.testng.Assert;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.io.IOException;
import java.net.URISyntaxException;

/**
 * 
 * @author prachi.nagpal
 *
 */
public class NewRelicTest extends BaseTest{

	private NewRelicLogin login;
	private ExtractFromExcelAndGoToJVM extract;

	@BeforeMethod(alwaysRun=true)
	public void init() {
		login = new NewRelicLogin();
		extract = new ExtractFromExcelAndGoToJVM();
	}
	/**
	 * Login New Relic Test
	 */

	@Test(priority = 1  , dataProvider="newRelicCredentials" , dataProviderClass = LoginData.class)
	public void testLoginNewRelic(String username, String password){
		login.loginNewRelic(username, password);
	}

	@Test(priority = 2)
	public void testHomePage(){
		extract.clickOnAppNameAndGoToDetailPage();
	}

	@Test(priority = 3)
	public void testTimePicker(){
		extract.selectCalenderDateAndTime("last_12_hours", "10:00 PM");
	}

    @Test(priority = 4)
	public void testJVMTab(){
        extract.clickOnGivenMonitoringTab("JVMs");
    }

	@Test(priority = 5, dataProviderClass = ReportData.class, dataProvider = "serviceNames")
	public void testExractValues(String serviceName) {
		try {
			extract.clickOnSearchDropDownAndEnterSearchKeyword(serviceName);
		} catch (NotAuthorizedException e) {
			Assert.fail(e.getMessage());
			return;
		}
		extract.extractAllJvmMonitoringValuesFromPage(serviceName);
	}

    @Test(priority = 6)
    public void testWriteValuesToExcel() throws IOException, URISyntaxException {
        extract.writeValues();
    }

}
