package com.groupon.web.newRelic.objectRepository;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;

public class LoginLocators {
	

	@FindBy(how = How.ID, using = "login_email")
	public WebElement emailIdTextbox;
	
	@FindBy(how = How.ID, using = "login_password")
	public WebElement passwordTextbox;
	
	@FindBy(how = How.ID, using = "login_submit")
	public WebElement signInButton;


	
	
}
