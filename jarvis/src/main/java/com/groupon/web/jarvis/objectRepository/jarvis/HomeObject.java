package com.groupon.web.jarvis.objectRepository.jarvis;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import com.groupon.web.common.objectRepository.BaseObject;

public class HomeObject extends BaseObject {

	public HomeObject(WebDriver driver) {
		super(driver);
		// TODO Auto-generated constructor stub
	}

	@FindBy(css=".login .content .form-title")
	public WebElement title;
	
	@FindBy(css=".login .content .form-actions .btn")
	public WebElement googleSignInButton;
	
}
