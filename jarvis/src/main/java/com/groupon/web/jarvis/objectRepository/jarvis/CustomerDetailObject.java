package com.groupon.web.jarvis.objectRepository.jarvis;

import java.util.List;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import com.groupon.web.common.objectRepository.BaseObject;

public class CustomerDetailObject extends BaseObject {

	public CustomerDetailObject(WebDriver driver) {
		super(driver);
		// TODO Auto-generated constructor stub
	}

	@FindBy(css=".block-heading h3")
	public List<WebElement> allDealsHeadings;

	@FindBy(css="button[ng-click='openDeal(order.dealUrl)']")
	public List<WebElement> dealPreviewButton;
	
	@FindBy(css="button[ng-click='order.showAll()']")
	public List<WebElement> viewDetailsLink;
	
	@FindBy(css="p[ng-bind='order.status']")
	public List<WebElement> orderStatus;

	

}
