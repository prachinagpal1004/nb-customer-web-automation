package com.groupon.web.jarvis.objectRepository.jarvis;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import com.groupon.web.common.objectRepository.BaseObject;

public class CustomerSearchObject extends BaseObject {

	public CustomerSearchObject(WebDriver driver) {
		super(driver);
		// TODO Auto-generated constructor stub
	}

	@FindBy(css=".navbar-default .navbar-brand h1")
	public WebElement cutomerSupportHeading;

	@FindBy(css=".block-heading h3")
	public WebElement customerSearchHeading;
	
	@FindBy(css=".navbar-default .navbar-nav>.active>a")
	public WebElement homeTabText;

	@FindBy(xpath=".//button[@class='dropdown-toggle']//span")
	public WebElement usernameVisibleAfterLogin;
	
	@FindBy(css=".form-group .btn")
	public WebElement searchButton;
	
	@FindBy(css="#bs-example-navbar-collapse-1")
	public WebElement menuBars;
	

}
