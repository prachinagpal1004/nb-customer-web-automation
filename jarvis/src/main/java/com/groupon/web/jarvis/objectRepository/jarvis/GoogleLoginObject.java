package com.groupon.web.jarvis.objectRepository.jarvis;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import com.groupon.web.common.objectRepository.BaseObject;

public class GoogleLoginObject extends BaseObject {

	public GoogleLoginObject(WebDriver driver) {
		super(driver);
		// TODO Auto-generated constructor stub
	}

	@FindBy(id="Email")
	public WebElement emailIdTextBox;
	
	@FindBy(id="next")
	public WebElement nextButton;
	
	@FindBy(id="Passwd")
	public WebElement passwordTextBox;
	
	@FindBy(id="signIn")
	public WebElement signInButton;
	
	@FindBy(css="#grant_heading")
	public WebElement grantHeading;
	
	@FindBy(id="submit_approve_access")
	public WebElement allowButton;

}
