package com.groupon.web.jarvis.page.jarvis;

import com.groupon.web.common.page.BasePage;
import com.groupon.web.jarvis.objectRepository.jarvis.HomeObject;
import org.apache.log4j.Logger;
import org.junit.Assert;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;

/**
 * 
 * @author prachi.nagpal
 *
 */
public class Home extends BasePage {
	
	private HomeObject object;
	private static final Logger logger = Logger.getLogger(Home.class);
	
	public Home() {
		object = PageFactory.initElements(getWebDriver(), HomeObject.class);
	}

	/**
	 * Click on Google Login Button
	 * @return
	 */
	public Home clickOnSignInWithGoogleButton(){
		
		waitVar.until(ExpectedConditions.titleIs("Centralized Login"));
		Assert.assertTrue(object.title.getText().contains("NearBuyTools Login"));
		logger.info("Title of the button : "+ object.title.getText());
		object.googleSignInButton.click();
		waitVar.until(ExpectedConditions.titleContains("Sign in - Google Accounts"));
		
		return this;
	}
	
}
