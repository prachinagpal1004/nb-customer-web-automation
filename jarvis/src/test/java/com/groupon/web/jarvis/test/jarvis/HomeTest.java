package com.groupon.web.jarvis.test.jarvis;

import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Factory;
import org.testng.annotations.Test;

import com.groupon.web.jarvis.data.jarvis.CustomerSearchData;
import com.groupon.web.jarvis.data.jarvis.GoogleLoginData;
import com.groupon.web.jarvis.page.jarvis.CustomerSearch;
import com.groupon.web.jarvis.page.jarvis.GoogleLogin;
import com.groupon.web.jarvis.page.jarvis.Home;
import com.groupon.web.common.test.BaseTest;
/**
 * 
 * @author prachi.nagpal
 *
 */
public class HomeTest extends BaseTest{

	private Home home;
	private GoogleLogin login;
	private CustomerSearch search;

	@BeforeMethod(alwaysRun=true)
	public void init() {
		home = new Home();
		login = new GoogleLogin();
		search = new CustomerSearch();
	}
	/**
	 * Test Home banners & widgets
	 */

	@Test(priority = 1 )
	public void testclickOnGoogleButton(){
		home.clickOnSignInWithGoogleButton();
	}

	@Test(priority = 2 , dataProvider="googleCredentials" , dataProviderClass = GoogleLoginData.class)
	public void testEnterCredentials(String username, String password){
		login.enterGoogleCredentials(username, password).clickOnAllowAccessButton();
	}

	@Test(priority = 3 , dataProvider="searchParameters" , dataProviderClass = CustomerSearchData.class)
	public void testSearch(String locator, String value){
		search.enterSearchParameterAndClickOnSearchForMultipleValues(locator, value);
	}



}
