package com.groupon.web.jarvis.data.jarvis;

import org.testng.annotations.DataProvider;

/**
 * 
 * @author prachi.nagpal
 *
 */
public class CustomerDetailData {

	@DataProvider(name = "personalInfo")
	public static Object[][] provideMenuName(){
		return new Object[][] {

				{"emailAddress"},
				{"phoneNumber"},
				{"userId"},
		};
	}
}
