package com.groupon.web.jarvis.page.jarvis;

import com.groupon.web.common.page.BasePage;
import com.groupon.web.jarvis.objectRepository.jarvis.GoogleLoginObject;
import org.apache.log4j.Logger;
import org.junit.Assert;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;

/**
 * 
 * @author prachi.nagpal
 *
 */
public class GoogleLogin extends BasePage{

	private GoogleLoginObject object;
	private static final Logger logger = Logger.getLogger(GoogleLogin.class);

	public GoogleLogin() {
		object = PageFactory.initElements(getWebDriver(), GoogleLoginObject.class);
	}

	/**
	 * Enter Google Credentials
	 * @return
	 */

	public GoogleLogin enterGoogleCredentials(String username, String password){

		waitVar.until(ExpectedConditions.elementToBeClickable(object.nextButton));
		object.emailIdTextBox.sendKeys(username);
		object.nextButton.click();
		waitVar.until(ExpectedConditions.elementToBeClickable(object.signInButton));
		object.passwordTextBox.sendKeys(password);
		object.signInButton.click();
		waitVar.until(ExpectedConditions.titleIs("Request for Permission"));

		return this;

	}

	/**
	 * Click on ALlow Access Button of Google
	 * @return
	 */
	public GoogleLogin clickOnAllowAccessButton(){

		waitVar.until(ExpectedConditions.elementToBeClickable(object.allowButton));
		Assert.assertTrue(object.grantHeading.getText().contains("would like to:"));
		object.allowButton.click();

		return this;

	}

}
