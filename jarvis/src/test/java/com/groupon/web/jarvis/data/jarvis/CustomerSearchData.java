package com.groupon.web.jarvis.data.jarvis;

import org.testng.annotations.DataProvider;

/**
 * 
 * @author prachi.nagpal
 *
 */
public class CustomerSearchData {

	@DataProvider(name = "searchParameters")
	public static Object[][] provideMenuName(){
		return new Object[][] {

				{"phoneNumber","9971798831"},
				{"emailAddress","rajni.singh@nearbuy.com"},
				{"userId","10000000013"},
				{"name","Rajni Singh"}
		};
	}
	
	@DataProvider(name = "singleSearchParameter")
	public static Object[][] provideSingleSearchParameter(){
		return new Object[][] {

				{"name","Rajni Singh"}
		};
	}
}
