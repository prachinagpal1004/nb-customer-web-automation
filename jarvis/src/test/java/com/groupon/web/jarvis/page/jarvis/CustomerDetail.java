package com.groupon.web.jarvis.page.jarvis;

import com.groupon.web.common.page.BasePage;
import com.groupon.web.jarvis.objectRepository.jarvis.CustomerDetailObject;
import org.apache.log4j.Logger;
import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;

/**
 * 
 * @author prachi.nagpal
 *
 */
public class CustomerDetail extends BasePage{

	private CustomerDetailObject object;
	private static final Logger logger = Logger.getLogger(CustomerDetail.class);

	public CustomerDetail() {
		object = PageFactory.initElements(getWebDriver(), CustomerDetailObject.class);
	}

	/**
	 * Verify Customer's Personal Details
	 * @return
	 */
	public CustomerDetail verifyCustomerPersonalDetails(String fieldName){

		String cssPersonalDetails = "p[ng-bind='user."+ fieldName +"']";

		Assert.assertTrue(getWebDriver().findElement(By.cssSelector(cssPersonalDetails)).isDisplayed());
		logger.info(fieldName + " value on the order detail page " + getWebDriver().findElement(By.cssSelector(cssPersonalDetails)).getText());
		logger.info("Customer's Personal Info has been validated");

		return this;
	}

	/**
	 * Verify Orders Details
	 * @return
	 */
	public CustomerDetail verifyOrderDetails(){

		for(WebElement each : object.allDealsHeadings){

			Assert.assertTrue(each.isDisplayed());
			logger.info(each.getText());

		}
		logger.info("No. of deals present in the Page : " + object.allDealsHeadings.size());

		for(WebElement each : object.dealPreviewButton){

			Assert.assertTrue(each.isDisplayed());
			Assert.assertTrue(each.isEnabled());
			logger.info(each.getText());

		}
		logger.info("No. of deal buttons : " + object.dealPreviewButton.size());

		for(WebElement each : object.viewDetailsLink){

			Assert.assertTrue(each.isDisplayed());
			Assert.assertTrue(each.isEnabled());
			logger.info(each.getText());

		}
		logger.info("No. of view details link : " + object.viewDetailsLink.size());
		
		for(WebElement each : object.orderStatus){

			Assert.assertTrue(each.isDisplayed());
			logger.info(each.getText());

		}
		logger.info("No. of Order Status : " + object.orderStatus.size());


		return this;
	}


}
