package com.groupon.web.jarvis.test.jarvis;

import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Factory;
import org.testng.annotations.Test;

import com.groupon.web.jarvis.data.jarvis.CustomerDetailData;
import com.groupon.web.jarvis.data.jarvis.CustomerSearchData;
import com.groupon.web.jarvis.data.jarvis.GoogleLoginData;
import com.groupon.web.jarvis.page.jarvis.CustomerDetail;
import com.groupon.web.jarvis.page.jarvis.CustomerSearch;
import com.groupon.web.jarvis.page.jarvis.GoogleLogin;
import com.groupon.web.jarvis.page.jarvis.Home;
import com.groupon.web.common.test.BaseTest;
/**
 * 
 * @author prachi.nagpal
 *
 */
public class OrderDetailPageTest extends BaseTest{

	private Home home;
	private GoogleLogin login;
	private CustomerSearch search;
	private CustomerDetail detail;

	
	
	@BeforeMethod(alwaysRun=true)
	public void init() {
		home = new Home();
		login = new GoogleLogin();
		search = new CustomerSearch();
		detail = new CustomerDetail();
	}
	/**
	 * Test Customer Detail Page
	 */

	@Test(priority = 1 )
	public void testclickOnGoogleButton(){
		home.clickOnSignInWithGoogleButton();
	}

	@Test(priority = 2 , dataProvider="googleCredentials" , dataProviderClass = GoogleLoginData.class)
	public void testEnterCredentials(String username, String password){
		login.enterGoogleCredentials(username, password).clickOnAllowAccessButton();
	}

	@Test(priority = 3 , dataProvider="singleSearchParameter" , dataProviderClass = CustomerSearchData.class)
	public void testSearch(String fieldName, String value){
		search.enterSearchParameterForOneField(fieldName, value);
	}
	
	@Test(priority = 4 , dataProvider="personalInfo" , dataProviderClass = CustomerDetailData.class)
	public void testPersonalDetails(String fieldName){
		detail.verifyCustomerPersonalDetails(fieldName);
	}

	@Test(priority = 5)
	public void testCustomerOrderDetails(){
		detail.verifyOrderDetails();
	}
}
