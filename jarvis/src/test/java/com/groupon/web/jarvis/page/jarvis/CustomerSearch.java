package com.groupon.web.jarvis.page.jarvis;

import org.apache.log4j.Logger;
import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.groupon.web.jarvis.objectRepository.jarvis.CustomerSearchObject;
import com.groupon.web.common.page.BasePage;

/**
 * 
 * @author prachi.nagpal
 *
 */
public class CustomerSearch extends BasePage{

	private CustomerSearchObject object;
	private static final Logger logger = Logger.getLogger(CustomerSearch.class);

	public CustomerSearch() {
		object = PageFactory.initElements(getWebDriver(), CustomerSearchObject.class);
	}

	/**
	 * Enter Search Parameter and click on Search for multiple data sets
	 * @return
	 * 
	 */
	public CustomerSearch enterSearchParameterAndClickOnSearchForMultipleValues(String textBoxLocator, String searchValue){

		enterSearchParameterAndClickOnSearchForMultipleValues(textBoxLocator, searchValue);
		object.homeTabText.click();
		waitVar.until(ExpectedConditions.elementToBeClickable(object.searchButton));

		return this;

	}

	/**
	 * Enter Search Parameter and click on Search for one data set
	 * @param textBoxLocator
	 * @param searchValue
	 * @return
	 */
	public CustomerSearch enterSearchParameterForOneField(String textBoxLocator, String searchValue){

		waitVar.until(ExpectedConditions.elementToBeClickable(object.searchButton));
		Assert.assertTrue(object.cutomerSupportHeading.getText().equals("Customer Support"));
		Assert.assertTrue(object.customerSearchHeading.getText().equals("Customer Search"));
		Assert.assertTrue(object.homeTabText.isDisplayed());
		Assert.assertTrue(object.homeTabText.isEnabled());
		logger.info("Username visible after Login : " + object.usernameVisibleAfterLogin.getText());
		logger.info("Assertions for Customer Search Home Page done. User is on Customer Support Page!");

		String locator = ".block-inner form[name='search-form'] input[type='text'][ng-model='customer." + textBoxLocator + "']";

		getWebDriver().findElement(By.cssSelector(locator)).sendKeys(searchValue);
		object.searchButton.click();
		waitVar.until(ExpectedConditions.invisibilityOfElementLocated(By.cssSelector(".form-group .btn")));

		waitVar.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(".//*[contains(@class,'order-history')]")));
		System.out.println("Name after clicking on Search Button : " + getWebDriver().findElement(By.cssSelector(".block-heading h3")).getText());

		return this;
	}

}
