package com.groupon.web.nearbuy.utils;

import java.util.Arrays;
import java.util.List;

import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpRequestBase;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.log4j.Logger;

import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.groupon.web.api.dto.RequestDTO;
import com.groupon.web.api.helper.ApiHelper;
import com.groupon.web.common.config.Config;
import com.groupon.web.nearbuy.dto.DealDetailDTO;

/**
 * @author prachi.nagpal
 */
public class ApiDataUtils {

	private static final Logger logger = Logger.getLogger(ApiDataUtils.class);
	
    public static DealDetailDTO getDealDetailByDealId(String dealId) {

        //Initialize APIhelper & HttpClient
        ApiHelper apiHelper = new ApiHelper();
        CloseableHttpClient httpClient = HttpClients.createDefault();

        String baseUrl = Config.getInstance().getConfig("web.api.baseUrl"); //Base url for Web API

        //Prepare RequestDTO Object
        RequestDTO requestDTO = new RequestDTO(baseUrl);
        requestDTO.setApiName("DealDetail");
        requestDTO.setEndPoint("deals/{dealId}");
        requestDTO.setPathParamJson("{\"dealId\":\""+dealId+"\"}");
        requestDTO.setMethodType(HttpGet.METHOD_NAME);
        requestDTO.setExpectedResponseCode(200);

        try {
            //create HTTPrequest object using requestDTO
            final HttpRequestBase httpRequest = apiHelper.prepareHTTPRequest(requestDTO);
            if(httpRequest == null) {
                throw new RuntimeException("Error in creating HttpRequest for DealId:"+dealId);
            }

            //execute httpRequest & fetch httpResponse
            final HttpResponse httpResponse = httpClient.execute(httpRequest);
            if(httpResponse == null) {
                throw new RuntimeException("HttpReponse received is empty for DealId:"+dealId);
            }

            //Verify status code with the expected one
            int actualStatusCode = httpResponse.getStatusLine().getStatusCode();
            if(requestDTO.getExpectedResponseCode() != actualStatusCode) {
                throw new RuntimeException("HttpResponse Status Code is incorrect for dealId:"+dealId+", Expected:"+requestDTO.getExpectedResponseCode()+" Actual:"+actualStatusCode);
            }

            //get response body from httpResponse
            String responseBody = apiHelper.getResponseBody(httpResponse);
            if(responseBody == null) {
                throw new RuntimeException("Response Body is empty for DealId:"+dealId);
            }

            //Parse Response Body & fetch Deal Detail json object
            JsonObject body = new JsonParser().parse(responseBody).getAsJsonObject();
            JsonObject dealDetail = body.get("result").getAsJsonObject().get("deal").getAsJsonObject();

            //Fetch specific properties of deal
            String title = dealDetail.get("title").getAsString();
            Integer soldCount = dealDetail.get("soldCount").getAsInt();
            //TODO: fetch other properties of deal
            DealDetailDTO detailDTO = new DealDetailDTO();
            detailDTO.setTitle(title);
            detailDTO.setSoldCount(soldCount.toString());

            return detailDTO;

        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }
}
