package com.groupon.web.nearbuy.locator;

import java.util.List;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;

public class ThankYouLocator {
	
	@FindBy(how = How.CSS, using = ".msg__heading")
	public WebElement congratulationsHeading;

	@FindBy(how = How.CSS, using = ".msg__heading b")
	public WebElement userName;
	
	@FindBy(how = How.CSS, using = ".order-table td b")
	public List<WebElement> boldText;
	
	@FindBy(how = How.CSS, using = ".row.payment-page .row img")
	public WebElement dealImage;
	
	@FindBy(how = How.CSS, using = ".order-table td a")
	public WebElement viewDealLink;
	
	@FindBy(how = How.CSS, using = ".order-total .font-size_10px")
	public WebElement inclusiveOfTaxes;
	
	/**
	 * Data Layer
	 */
	
	@FindBy(how = How.ID, using = "datainformation")
	public WebElement dlDealId;
	
	@FindBy(how = How.ID, using = "datainformation")
	public WebElement dlDealPrice;
	
	@FindBy(how = How.ID, using = "datainformation")
	public WebElement dlDealTitle;
	
	@FindBy(how = How.ID, using = "datainformation")
	public WebElement dlFinalAmountPaid;
	
	@FindBy(how = How.ID, using = "datainformation")
	public WebElement dlGrossBookingValue;
	
	@FindBy(how = How.ID, using = "datainformation")
	public WebElement dlNumberOfVouchers;
	
	@FindBy(how = How.ID, using = "datainformation")
	public WebElement dlOrderId;
	
	@FindBy(how = How.ID, using = "datainformation")
	public WebElement dlPermalink;
	
	@FindBy(how = How.ID, using = "datainformation")
	public WebElement dlSku;
	
	@FindBy(how = How.ID, using = "datainformation")
	public WebElement dlVertical;
	
	
}
