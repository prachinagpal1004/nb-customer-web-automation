package com.groupon.web.nearbuy.locator;

import java.util.List;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;

public class BuyingInBulkLocator {
	
	@FindBy(how = How.NAME, using = "name")
	public WebElement nameTextBox;

	@FindBy(how = How.NAME, using = "email")
	public WebElement emailTextBox;
	
	@FindBy(how = How.NAME, using = "quantity")
	public WebElement quantityTextBox;
	
	@FindBy(how = How.NAME, using = "organization")
	public WebElement organizationTextBox;
	
	@FindBy(how = How.NAME, using = "phone")
	public WebElement phoneTextBox;
	
	@FindBy(how = How.NAME, using = "submit")
	public WebElement submitButton;
	
	@FindBy(how = How.CSS, using = "section h4")
	public WebElement formHeading;
	
	@FindBy(how = How.CSS, using = "section h5 a")
	public WebElement dealHeading;
	
	@FindBy(how = How.CSS, using = "h5.text-center")
	public WebElement thanksMessage;
	
	@FindBy(how = How.CSS, using = "a.btn_orange")
	public List<WebElement> continueShoppingAndBackToDealButtons;
	
	
}
