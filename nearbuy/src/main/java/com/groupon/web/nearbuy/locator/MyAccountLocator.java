package com.groupon.web.nearbuy.locator;

import java.util.List;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;

public class MyAccountLocator {
	

	@FindBy(how = How.CSS, using = ".myaccount_dropdown .dropdown_menu")
	public WebElement myAccountDropdown;

	@FindBy(how = How.XPATH, using = "//a[text()='My Account']")
	public WebElement loggedInUser;
	
	@FindBy(how = How.CSS, using = ".myaccount_dropdown a")
	public List<WebElement> dropDownList;

	@FindBy(how = How.CSS, using = ".user-heading h1")
	public WebElement pageHeading;
	
	@FindBy(how = How.CSS, using = ".utility-btn span")
	public WebElement loginLink;
	
	@FindBy(how = How.CSS, using = ".top_deals.container")
	public WebElement carousel;
	
	@FindBy(how = How.CSS, using = ".hero_image")
	public WebElement mainBackgroundImage;
	
	@FindBy(how = How.CSS, using = ".secondary-nav")
	public WebElement mainPageHeader;
	
	/**
	 * Tabs 
	 */
	@FindBy(how = How.CSS, using = ".tab-title a")
	public List<WebElement> tabsLink;
	
	@FindBy(how = How.CSS, using = ".tab-title a span")
	public List<WebElement> tabsText;
	
	/**
	 * My Orders
	 */
	
	
	@FindBy(how = How.CSS, using = ".tabs-content h6")
	public WebElement ordersStatus;
	
	@FindBy(how = How.CSS, using = ".tabs-content")
	public WebElement allOrderSections;
	
	@FindBy(how = How.CSS, using = "#orders")
	public WebElement numberOfOrdersPresent;
	
	@FindBy(how = How.CSS, using = ".my_orders .txt-right .btn")
	public List<WebElement> listOfPDFlinks;
	
	/**
	 * My Credits
	 */
	
	@FindBy(how = How.CSS, using = ".user-credits")
	public WebElement creditsSection;
	
	@FindBy(how = How.CSS, using = ".user-heading h2")
	public WebElement currentCredits;

	/**
	 * My Profile
	 */
	
	@FindBy(how = How.CSS, using = "profile .form-group")
	public WebElement profileSection;
	
	@FindBy(how = How.ID, using = "label")
	public List<WebElement> profileLabels;
	
	@FindBy(how = How.CSS, using = ".form-group--inner .form-control[type='text']")
	public WebElement nameTextbox;
	
	@FindBy(how = How.CSS, using = ".form-group--inner .form-control[type='email']")
	public WebElement emailTextbox;
	
	@FindBy(how = How.CSS, using = "#emailLabel+p")
	public WebElement emailText;
	
	@FindBy(how = How.CSS, using = "#mobileLabel+p")
	public WebElement mobileText;
	
	@FindBy(how = How.CSS, using = ".form-control[type='number']")
	public WebElement mobileTextbox;
	
	@FindBy(how = How.CSS, using = ".form-group .row button")
	public WebElement myProfileSaveChangesButton;
	
	@FindBy(how = How.CSS, using = ".txt-right.txt-primary.icon-s")
	public WebElement successMessageDiv;
	
	/**
	 * My Email Subscriptions
	 */
	
	@FindBy(how = How.CSS, using = "news-letter .user-heading")
	public WebElement subscriptionSection;
	
	@FindBy(how = How.CSS, using = ".user-heading h2")
	public WebElement mySubscriptionSubHeading;
	
	@FindBy(how = How.CSS, using = "news-letter .loaders")
	public WebElement subscribeButton;
	
	@FindBy(how = How.CSS, using = "input[value='Save']")
	public WebElement saveButton;
	
	@FindBy(how = How.CSS, using = ".txt-right.txt-primary.icon-s")
	public WebElement updateSuccessMessage;
	
	@FindBy(how = How.ID, using = "unsubscribeDiv")
	public WebElement unsubscribeDiv;
	
	@FindBy(how = How.CSS, using = "news-letter .row a")
	public WebElement unsubscribeLink;
	
	@FindBy(how = How.CSS, using = "unsubscribe-modal .wrapper.show")
	public WebElement unsubscribePopUp;
	
	@FindBy(how = How.CSS, using = "unsubscribe-modal .btn--secondary")
	public WebElement unsubscribeNoOption;
	
	@FindBy(how = How.CSS, using = "unsubscribe-modal .btn--primary")
	public WebElement unsubscribeYesOption;
	
	@FindBy(how = How.CSS, using = ".user-newsletter")
	public WebElement newsLetterSection;
	
	@FindBy(how = How.CSS, using = ".user-newsletter__location")
	public WebElement allCitiesBox;
	
	@FindBy(how = How.CSS, using = ".ms-choice")
	public WebElement currentCityButton;
	
	@FindBy(how = How.CSS, using = ".nb-checkbox__bg")
	public List<WebElement> locationCheckboxes;
	
	@FindBy(how = How.CSS, using = ".nb-checkbox input:checked+.nb-checkbox__bg+span")
	public List<WebElement> allCheckedLocationsLabel;
	
	@FindBy(how = How.CSS, using = ".nb-checkbox input:checked+.nb-checkbox__bg")
	public List<WebElement> allCheckedLocations;
	
	@FindBy(how = How.CSS, using = ".nb-checkbox span")
	public List<WebElement> allLocationLabels;
	
	@FindBy(how = How.XPATH, using = ".//*[@id='email']/preceding::label[1]")
	public WebElement disabledEmailLabel;
	
	@FindBy(how = How.ID, using = "email")
	public WebElement disabledEmailTextbox;
	
	@FindBy(how = How.XPATH, using = ".//*[contains(@class,'user-newsletter__location')]/preceding::label[1]")
	public WebElement cityNewsletterLabel;
	
	/**
	 * Refer A Friend
	 */
	
	@FindBy(how = How.CSS, using = ".referral p")
	public WebElement noReferralMessage;
	
	@FindBy(how = How.CSS, using = ".continue_shopping")
	public WebElement continueShoppingButton;
	
	@FindBy(how = How.CSS, using = ".user-profile .content h3")
	public WebElement referAfriendHeading;
	
	@FindBy(how = How.CSS, using = ".nb-finally-arrangement")
	public WebElement referAfriendSubHeading;
	
	@FindBy(how = How.CSS, using = ".share-referral")
	public WebElement shareReferralCodeDiv;
	
	@FindBy(how = How.CSS, using = ".share-referral .share-referral_text")
	public WebElement shareReferralText;
	
	@FindBy(how = How.CSS, using = ".share-referral .share-referral_code")
	public WebElement shareReferralCode;
	
	@FindBy(how = How.CSS, using = ".credits-value")
	public WebElement referralAmount;
	
	@FindBy(how = How.CSS, using = "ul.social_refer_icons a")
	public List<WebElement> shareReferral;
	
	@FindBy(how = How.CSS, using = ".how-it-work h5")
	public WebElement howItWorksHeading;
	
	@FindBy(how = How.CSS, using = ".how-it-work p")
	public WebElement howItWorksDetails;
	
	@FindBy(how = How.XPATH, using = ".//*[@class='green_text']")
	public WebElement TnClink;
	
	@FindBy(how = How.ID, using = "gta_tc")
	public WebElement TnCpopup;
	
	@FindBy(how = How.CSS, using = ".reveal-modal.reveal-modal_deals .close-reveal-modal")
	public WebElement closeButton;
	
	@FindBy(how = How.CSS, using = ".reveal-modal_deals li")
	public WebElement popUpText;
	
	@FindBy(how = How.CSS, using = ".reveal-modal_deals b")
	public WebElement popUpBoldText;
	
	/**
	 * Twitter
	 */
	
	@FindBy(how = How.ID, using = "username_or_email")
	public WebElement twitterUsername;
	
	@FindBy(how = How.ID, using = "password")
	public WebElement twitterPassword;
	
	@FindBy(how = How.ID, using = "status")
	public WebElement statusText;
	
	@FindBy(how = How.XPATH, using = ".//*[@id='char-count']/following-sibling::input")
	public WebElement loginAndTweetButton;
	
}
