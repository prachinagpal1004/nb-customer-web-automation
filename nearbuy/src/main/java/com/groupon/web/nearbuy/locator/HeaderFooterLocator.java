package com.groupon.web.nearbuy.locator;

import java.util.List;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;

public class HeaderFooterLocator {

	@FindBy(how = How.CSS, using = ".has_subdropdown a")
	public List<WebElement> menuLink;

	@FindBy(how = How.CSS, using = ".header__primary .brand_logo")
	public WebElement logo;
	
	@FindBy(how = How.CSS, using = ".header__secondary .secondary_links .utility-btn")
	public List<WebElement> utilityButtons;
	
	@FindBy(how = How.XPATH, using = ".//div[contains(@class,'how-it-works-show')]")
	public WebElement howItWorksPanel;
	
	@FindBy(how = How.CSS, using = ".how-it-works-show .btn-close")
	public WebElement howItWorksPanelCloseButton;
	
	@FindBy(how = How.CSS, using = ".header__primary")
	public WebElement headerPrimary;
	
	@FindBy(how = How.CSS, using = ".hiw .how-it-works img")
	public List<WebElement> howItWorksImages;
	
	@FindBy(how = How.CSS, using = ".how-it-works-show .h5")
	public List<WebElement> howItWorksSubHeadings;
	
	@FindBy(how = How.CSS, using = ".how-it-works-show p")
	public List<WebElement> howItWorksSubTitles;

	@FindBy(how = How.CSS, using = ".secondary-nav")
	public WebElement secondaryNavBar;

	@FindBy(how = How.CSS, using = ".search-bar__input")
	public WebElement searchBox;	
	
	@FindBy(how = How.CSS, using = ".hero-search-button button")
	public WebElement searchButton;	
	
	@FindBy(how = How.CSS, using = ".hero-banner--local-search input[type=search]")
	public WebElement largeSearchBoxInSearchPage;	
	
	@FindBy(how = How.CSS, using = ".auto-suggest-modal.modal-shown")
	public WebElement searchPage;	

	@FindBy(how = How.CSS, using = ".searchbox__suggestion")
	public WebElement autoSuggestionBox;	
	
	@FindBy(how = How.CSS, using = ".hero-banner__suggestion.searchbox--suggest")
	public WebElement autoSuggestionBoxPresence;	

	@FindBy(how = How.XPATH, using = ".//*[@class='searchbox__suggestion']/li/a/h6")
	public List<WebElement> autoSuggestionList;	

	@FindBy(how = How.CSS, using = ".result_title")
	public WebElement resultTitle;	
	
	@FindBy(how = How.CSS, using = ".orders__total+button")
	public WebElement bookNowButton;	
	
	@FindBy(how = How.CSS, using = ".breadcrumbs")
	public WebElement breadcrumbs;	
	
	@FindBy(how = How.CSS, using = ".result_content ")
	public WebElement searchResultPageRightSide;	
	
	@FindBy(how = How.CSS, using = ".col-m-4.col-l-3")
	public WebElement searchResultPageLeftSide;	
	
	@FindBy(how = How.CSS, using = "local_category_no_result_page")
	public WebElement localNoResultPage;	
	
	@FindBy(how = How.CSS, using = ".no-search-result__text h4")
	public WebElement localNoResultHeading;	
	
	@FindBy(how = How.CSS, using = ".no-search-result__text p")
	public WebElement localNoResultSubTitle;	
	
	@FindBy(how = How.CSS, using = ".no-search-result__img img")
	public WebElement localNoResultImageIcon;	
	
	@FindBy(how = How.CSS, using = ".font-weight_semibold")
	public WebElement searchedKeywordInHeading;	
	
	@FindBy(how = How.CSS, using = ".btn_cream")
	public WebElement viewAllOffersButton;	

	@FindBy(how = How.CSS, using = ".result_title .category_name")
	public WebElement categoryName;	

	@FindBy(how = How.CSS, using = ".city-selector p")
	public WebElement selectLocationText;	
	
	@FindBy(how = How.CSS, using = ".auto-suggest-modal .btn-location")
	public WebElement changeLocationText;	
	
	@FindBy(how = How.CSS, using = ".city-selector .search-tags")
	public List<WebElement> textInMainCitiesPopUp;	

	@FindBy(how = How.CSS, using = ".travel_link .color-red")
	public WebElement discoverBestHotelsTextInMainCitiesPopUp;	
	
	@FindBy(how = How.CSS, using = ".travel_link span")
	public WebElement lookingForHotelsTextMainCitiesPopUp;	
	
	@FindBy(how = How.CSS, using = ".travel_link a")
	public WebElement travelLinkInCitiesPopUp;	
	
	@FindBy(how = How.CSS, using = ".city-selector span")
	public WebElement locationDropDownBox;	
	
	@FindBy(how = How.CSS, using = ".auto-suggest-modal .current-location")
	public WebElement locationDropDownBoxInSearchBox;	
	
	@FindBy(how = How.CSS, using = ".city-selector .modal-show .search")
	public WebElement locationModal;
	
	@FindBy(how = How.CSS, using = ".header__secondary .city-selector .city-name")
	public WebElement citySelected;	
	
	@FindBy(how = How.CSS, using = ".auto-suggest-modal .current-location")
	public WebElement currentLocation;	
	
	@FindBy(how = How.CSS, using = ".top-bar+.search_box .search-location button")
	public WebElement locationDropDown;	
	
	@FindBy(how = How.CSS, using = ".search-location .location-name span:last-child")
	public WebElement locationDropDownText;	
	
	@FindBy(how = How.CSS, using = ".city-selector .btn-close")
	public WebElement crossIconButtonOnCitySelector;	
	
	@FindBy(how = How.CSS, using = ".city-selector .btn-close .icon-close")
	public WebElement crossIconImageOnCitySelector;	
	
	@FindBy(how = How.CSS, using = ".city-selector .btn-close p")
	public WebElement crossIconTextOnCitySelector;	
	
	@FindBy(how = How.CSS, using = ".city-selector .searchbox__input input")
	public WebElement citySearchBox;	
	
	@FindBy(how = How.CSS, using = "city-selector-typeahead form > *:first-child")
	public WebElement autosuggestionLoaderIncitySearchBox;	
	
	@FindBy(how = How.CSS, using = ".city-selector .searchbox .searchbox--suggest .searchbox__suggestion")
	public WebElement autosuggestionBoxInCitySearch;	
	
	@FindBy(how = How.CSS, using = ".city-selector .searchbox .searchbox--suggest .searchbox__suggestion li")
	public List<WebElement> listOfCitiesInAutosuggestion;	
	
	@FindBy(how = How.CSS, using = ".icon-location")
	public WebElement locationIconInCitySearchBox;	
	
	@FindBy(how = How.CSS, using = ".city-selector .search__footer img")
	public WebElement nearbuyLogoInCitySelector;	
	
	@FindBy(how = How.CSS, using = ".city-selector .search__footer p")
	public WebElement nearbuyTagLineInCitySelector;	

	@FindBy(how = How.CSS, using = ".city-selector br ~ p")
	public List<WebElement> listOfTextOnCitySelector;	
	
	@FindBy(how = How.CSS, using = ".city-selector .search-tags")
	public List<WebElement> topCitiesList;	

	@FindBy(how = How.XPATH, using = ".//*[contains(@class,'has_subdropdown')]/a")
	public List<WebElement> mainMenuList;	

	@FindBy(how = How.CSS, using = ".nav_menu .has_subdropdown .sub_dropdown")
	public List<WebElement> subMenuList;	

	@FindBy(how = How.CSS, using = ".top-bar .name label")
	public WebElement categoryLabelName;	


	/**
	 * Footer
	 */
	@FindBy(how = How.CSS, using = ".footer-top")
	public WebElement footerSection;

	@FindBy(how = How.CSS, using = ".footer-top p")
	public WebElement footerParagraphs;

	@FindBy(how = How.CSS, using = ".footer-top ul p")
	public WebElement followUsHeading;

	@FindBy(how = How.CSS, using = ".footer-bottom__categories .list-block p")
	public List<WebElement> companyAndHelpHeading;

	@FindBy(how = How.CSS, using = "[class^='footer'] a")
	public List<WebElement> footerAllLinks;

	@FindBy(how = How.CSS, using = ".social-links a")
	public List<WebElement> socialMediaLinks;

	@FindBy(how = How.CSS, using = "ul.footer_logo a")
	public WebElement blogLink;

	@FindBy(how = How.CSS, using = ".bg_black li a")
	public List<WebElement> companyLinks;

	@FindBy(how = How.XPATH, using = ".//*[contains(@class,'app_store')]//a")
	public List<WebElement> downloadAppLinks;

	/**
	 * Company Links Content
	 */

	@FindBy(how = How.CSS, using = ".contact_us .top_tabs a")
	public List<WebElement> topTabsLinks;

	@FindBy(how = How.CSS, using = ".contact_us .tabs-content .content")
	public WebElement tabsContent;

	@FindBy(how = How.CSS, using = ".contact_us .three_verticals .tab-title a")
	public List<WebElement> policyTabs;

	@FindBy(how = How.CSS, using = ".tabs-content>.content.active")
	public WebElement policyVerticalContent;

	@FindBy(how = How.CSS, using = ".contact_us .vertical_content .faq_heading")
	public List<WebElement> faqsLinks;

	/**
	 * Universal Fine Print
	 */

	@FindBy(how = How.CSS, using = ".contact_us h3")
	public WebElement universalFinePrintHeading;
	
	@FindBy(how = How.CSS, using = ".contact_us li")
	public WebElement universalListPoints;
	
	@FindBy(how = How.CSS, using = ".btn_orange")
	public WebElement okButtonOnUniversalPrint;
	
	/**
	 * Search Data Layer
	 */
	
	@FindBy(how = How.ID, using = "city")
	public WebElement dlCity;
	
	@FindBy(how = How.ID, using = "dealId0")
	public WebElement dldeal1;
	
	@FindBy(how = How.ID, using = "dealId1")
	public WebElement dldeal2;
	
	@FindBy(how = How.ID, using = "dealId2")
	public WebElement dldeal3;
	
	@FindBy(how = How.ID, using = "dealTitle0")
	public WebElement dldealTitle1;
	
	@FindBy(how = How.ID, using = "dealTitle1")
	public WebElement dldealTitle2;
	
	@FindBy(how = How.ID, using = "dealTitle2")
	public WebElement dldealTitle3;
	
	
	/**
	 * Mobile locators
	 */

	@FindBy(how = How.CSS, using = ".nb-search-marker i.fa-search")
	public WebElement mobileSearchIcon;
	
	/**
	 * New SearchBox 
	 */
	@FindBy(how = How.CSS, using = ".search-bar__input")
	public WebElement locationBoxInSearchBox;
	
	/**
	 * No Search Result Page
	 */
	@FindBy(how = How.CSS, using = ".deal_row")
	public List<WebElement> trendingDealAndRecentlyViewDealDiv;

	@FindBy(how = How.CSS, using = ".browse_deals")
	public List<WebElement> dealsInDiv;
	
	@FindBy(how = How.CSS, using = "dealcard a")
	public List<WebElement> dealCards;
	
	/**
	 * List Your Business
	 */
	
	@FindBy(how = How.XPATH, using = ".//*[@class='utility-btn'][@href='/list-your-business']")
	public WebElement listYourBusinessLink;
	
	@FindBy(how = How.ID, using = "partnerWithUsCategory")
	public WebElement listYourBusinessFormDiv;
	
	@FindBy(how = How.CSS, using = "#partnerWithUsCategory button")
	public WebElement nextButtonInListYourForm;
	
	@FindBy(how = How.CSS, using = "#partnerWithUsBusinessInfo .btn-active-right")
	public WebElement nextButtonInSecondScreenOfListYourForm;
	
	@FindBy(how = How.CSS, using = "#partnerWithUsBusinessInfo .btn-active-left")
	public WebElement backButtonInListYourForm;
	
	@FindBy(how = How.NAME, using = "businessName")
	public WebElement businessNameInListYourForm;
	
	@FindBy(how = How.CSS, using = "#partnerWithUsContactInfo button[type='submit']")
	public WebElement submitButtonInListForm;
	
	@FindBy(how = How.CSS, using = ".form-group input[name='name']")
	public WebElement nameInListForm;
	
	@FindBy(how = How.CSS, using = ".form-group input[name='phoneNumber']")
	public WebElement phoneNumberInListForm;
	
	@FindBy(how = How.CSS, using = ".form-group input[name='emailId']")
	public WebElement emailIdInListForm;
	
	@FindBy(how = How.CSS, using = ".form-group input[name='city']")
	public WebElement cityInListForm;
	
	@FindBy(how = How.NAME, using = "localBusinessAddress")
	public WebElement localAddressInListYourForm;
	
	@FindBy(how = How.CSS, using = ".thanks-check")
	public WebElement thanksImage;
	
	@FindBy(how = How.CSS, using = ".thanks-heading")
	public WebElement thanksHeading;
	
	@FindBy(how = How.CSS, using = ".thanks-msg")
	public WebElement thanksMessage;
	
	@FindBy(how = How.NAME, using = "workedForNearbuy")
	public List<WebElement> workedForInListYourForm;
	
	@FindBy(how = How.NAME, using = "numberOfLocations")
	public List<WebElement> noOfLocationsInListYourForm;
	
	@FindBy(how = How.CSS, using = ".category-list li input")
	public List<WebElement> categoryOptions;
	
	
	
}
