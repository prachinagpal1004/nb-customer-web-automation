package com.groupon.web.nearbuy.locator;

import java.util.List;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;

public class PayULocators {
	
	@FindBy(how = How.ID, using = "payu_logo")
	public WebElement payUlogo;

	@FindBy(how = How.ID, using = "totalAmount")
	public WebElement totalAmount;
	
	@FindBy(how = How.ID, using = "txndiv")
	public WebElement transactionIdText;
	
	@FindBy(how = How.CSS, using = "#txndiv span")
	public WebElement transactionIdNumber;
	
	@FindBy(how = How.ID, using = "credit-card")
	public WebElement ccCheckBox;
	
	@FindBy(how = How.NAME, using = "cardNumber")
	public WebElement ccNo;
	
	@FindBy(how = How.ID, using = "cname_on_card")
	public WebElement nameOnCard;
	
	@FindBy(how = How.ID, using = "ccvv_number")
	public WebElement cvvNo;
	
	@FindBy(how = How.ID, using = "cexpiry_date_month")
	public WebElement expiryMonth;
	
	@FindBy(how = How.ID, using = "cexpiry_date_year")
	public WebElement expiryYear;
	
	@FindBy(how = How.XPATH, using = ".//card-payment[@type='debit']//button")
	public WebElement debitCardPayNowButton;
	
	@FindBy(how = How.XPATH, using = ".//card-payment[@type='credit']//button")
	public WebElement creditCardPayNowButton;
	
	@FindBy(how = How.CSS, using = "#payment .payment-buttons span")
	public WebElement goBackToText;

	@FindBy(how = How.LINK_TEXT, using = "Nearbuy.com")
	public WebElement nearbuyUrl;
	
	@FindBy(how = How.XPATH, using = ".//*[@id='TriggerBlock']/li/a")
	public List<WebElement> paymentTabs;
	
	@FindBy(how = How.ID, using = "netbanking_select")
	public WebElement netbankingDropDown;
	
	@FindBy(how = How.CSS, using = "#PayuPaisaWallet .button")
	public WebElement payUmoneyButton;
	
	@FindBy(how = How.ID, using = "wallet_email_input")
	public WebElement walletEmailIdTextbox;
	
	@FindBy(how = How.ID, using = "wallet_mobile_input")
	public WebElement walletMobileTextbox;
	
	
	/**
	 * Debit Card
	 */
	
	@FindBy(how = How.CSS, using = ".debit-card-input")
	public WebElement debitCardNumber;
	
	@FindBy(how = How.XPATH, using = ".//card-payment[@type='debit']//input[@formcontrolname='name']")
	public WebElement debitCardName;
	
	@FindBy(how = How.XPATH, using = ".//card-payment[@type='debit']//input[@formcontrolname='cvv']")
	public WebElement debitCardCVV;
	
	@FindBy(how = How.XPATH, using = ".//card-payment[@type='debit']//input[@formcontrolname='expiry']")
	public WebElement debitCardExpiryDate;
	
	/**
	 * Credit card
	 */
	@FindBy(how = How.CSS, using = ".credit-card-input")
	public WebElement creditCardNumber;
	
	@FindBy(how = How.XPATH, using = ".//card-payment[@type='credit']//input[@formcontrolname='name']")
	public WebElement creditCardName;
	
	@FindBy(how = How.XPATH, using = ".//card-payment[@type='credit']//input[@formcontrolname='cvv']")
	public WebElement creditCardCVV;
	
	@FindBy(how = How.XPATH, using = ".//card-payment[@type='credit']//input[@formcontrolname='expiry']")
	public WebElement creditCardExpiryDate;
	
	@FindBy(how = How.CSS, using = ".payment-proceed.loaders__block-level")
	public WebElement waitLoader;

	/**
	 * Netbanking
	 */
	
	@FindBy(how = How.CSS, using = "net-banking")
	public List<WebElement> netBankingDiv;
	
	@FindBy(how = How.CSS, using = ".select-bank")
	public WebElement allBanksDropDown;
	
	@FindBy(how = How.CSS, using = ".bank-btn.active")
	public WebElement selectedPopularBank;
	
	@FindBy(how = How.CSS, using = "net-banking button")
	public WebElement netbankingPayNowButton;
	
	
	
	

	
	
}
