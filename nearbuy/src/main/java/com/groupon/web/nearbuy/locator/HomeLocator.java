package com.groupon.web.nearbuy.locator;

import java.util.List;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;

public class HomeLocator {

	@FindBy(how = How.XPATH, using = "//*[@id='carousel1']/ul/li")
	public List<WebElement> upperCrousel;
	
	@FindBy(how = How.CSS, using = ".breadcrumbs>*.current")
	public WebElement currentBreadcrumb;

	@FindBy(how = How.CSS, using = ".top_deals .detail h4")
	public WebElement topDealsHeading;

	@FindBy(how = How.CSS, using = ".deal_caption p")
	public List<WebElement> dealCaption;

	@FindBy(how = How.CSS, using = ".category_slider ul li a")
	public List<WebElement> categorySlider;

	@FindBy(how = How.CSS, using = ".category_slider ul li a img")
	public List<WebElement> categorySliderImage;	

	@FindBy(how = How.CSS, using = ".section-heading a.category-name")
	public List<WebElement> allBlocksHeading;	

	@FindBy(how = How.CSS, using = ".deal_cart a")
	public List<WebElement> allBlocksAchorTags;	

	@FindBy(how = How.CSS, using = ".ad_banner1 .alert-box a")
	public List<WebElement> adsSection;

	@FindBy(how = How.CSS, using = ".ad_banner1 .alert-box .close")
	public List<WebElement> adsCloseButtons;

	/**
	 * Migrate PopUp on Home Page
	 */

	@FindBy(how = How.CSS, using = "#drift-box")
	public WebElement driftBox;

	@FindBy(how = How.CSS, using = "#drift-box p")
	public WebElement driftBoxText;

	@FindBy(how = How.CSS, using = "#drift-box img")
	public WebElement driftBoxNearbuyImage;

	@FindBy(how = How.CSS, using = "#drift-box a")
	public List<WebElement> driftBoxYesNoText;

	/**
	 * Data Layer
	 */

	@FindBy(how = How.ID, using = "city")
	public WebElement dlCity;

	@FindBy(how = How.ID, using = "dealTitle0")
	public WebElement dlDealTitle1;

	@FindBy(how = How.ID, using = "dealTitle1")
	public WebElement dlDealTitle2;

	@FindBy(how = How.ID, using = "dealTitle2")
	public WebElement dlDealTitle3;

	@FindBy(how = How.ID, using = "dealId0")
	public WebElement dlDeal1;
	
	@FindBy(how = How.ID, using = "dealId1")
	public WebElement dlDeal2;
	
	@FindBy(how = How.ID, using = "dealId2")
	public WebElement dlDeal3;
	
	@FindBy(how = How.ID, using = "searchForm")
	public WebElement dlVertical;
	

	/**
	 * New Local Home Page
	 */
	@FindBy(how = How.CSS, using = ".h2")
	public WebElement heading;

	@FindBy(how = How.CSS, using = ".hero")
	public WebElement backgroundImage;

	@FindBy(how = How.CSS, using = ".hero__search")
	public WebElement newSearchBoxDiv;
	
	@FindBy(how = How.CSS, using = ".search-location.icon")
	public WebElement searchLocationIcon;
	
	@FindBy(how = How.CSS, using = ".search-location a")
	public WebElement searchLocationName;
	
	@FindBy(how = How.CSS, using = ".search-bar__input")
	public WebElement searchBox;
	
	@FindBy(how = How.CSS, using = ".hero-search-button")
	public WebElement searchButton;
	
	@FindBy(how = How.CSS, using = ".deals h3 a.see-all")
	public List<WebElement> seeAllLinks;
	
	@FindBy(how = How.CSS, using = ".hero__categories li img")
	public List<WebElement> iconsImages;
	
	@FindBy(how = How.CSS, using = ".hero__categories li a")
	public List<WebElement> iconsURLs;
	
	@FindBy(how = How.CSS, using = ".hero__categories li a p")
	public List<WebElement> iconsLabels;
	
	@FindBy(how = How.CSS, using = ".txt-capitalize")
	public WebElement visibleCityText;
	
	@FindBy(how = How.CSS, using = ".modal_wrapper .modal")
	public WebElement cityLocationModal;
	
	@FindBy(how = How.CSS, using = ".txt-right .icon-sort")
	public WebElement travelRowRightLink;
	
	@FindBy(how = How.CSS, using = ".location_footer_left a")
	public WebElement travelRowLeftLink;
	
	@FindBy(how = How.CSS, using = ".hero-banner__location-list")
	public WebElement locationSelectedInSearchBox;
	
	@FindBy(how = How.CSS, using = "brand_logo")
	public WebElement nearbuyLogo;
	
	@FindBy(how = How.CSS, using = ".secondary-nav__close")
	public WebElement moreIcon;
	
	@FindBy(how = How.CSS, using = ".secondary-nav__close h2")
	public WebElement moreIconText;
	
	@FindBy(how = How.ID, using = "local_index_page")
	public WebElement localHomePage;
	
	@FindBy(how = How.CSS, using = ".secondary-nav__wrapper")
	public WebElement secondaryNavWrapper;
	
	@FindBy(how = How.CSS, using = ".secondary-nav__item")
	public List<WebElement> categoryIcons;
	
	@FindBy(how = How.XPATH, using = "//div[contains(@class,'secondary-nav__item')]/..")
	public List<WebElement> categoryIconsHref;
	
	@FindBy(how = How.CSS, using = ".secondary-nav__item p")
	public List<WebElement> categoryIconsText;
	
	@FindBy(how = How.CSS, using = ".secondary-nav__icon")
	public List<WebElement> categoryIconsImage;
	
	@FindBy(how = How.CSS, using = ".section-heading .category-name")
	public List<WebElement> sectionHeadings;
	
	@FindBy(how = How.CSS, using = ".card-slide-wrapper")
	public List<WebElement> slideSections;
	
	@FindBy(how = How.CSS, using = "a.btn_cream")
	public WebElement viewAllOffersButton;
	
	@FindBy(how = How.CSS, using = ".more-offers")
	public WebElement freeOffersDiv;
	
	@FindBy(how = How.CSS, using = ".raffles .more-offers .card__inner")
	public List<WebElement> rafflesCards;
	
	@FindBy(how = How.CSS, using = ".card--horizontal .card__inner .card__image img")
	public List<WebElement> rafflesImages;
	
	@FindBy(how = How.CSS, using = ".card--horizontal .card__inner .card__option")
	public List<WebElement> rafflesSubTitles;
	
	@FindBy(how = How.CSS, using = ".raffles .price p")
	public List<WebElement> rafflesPrice;
	
	@FindBy(how = How.CSS, using = ".raffles .nav-pane .prev")
	public WebElement rafflesPrevArrow;
	
	@FindBy(how = How.CSS, using = ".raffles .nav-pane .next")
	public WebElement rafflesNextArrow;
	
	@FindBy(how = How.CSS, using = ".location-cards .nav-pane .prev")
	public WebElement mostSearchedFromPrevArrow;
	
	@FindBy(how = How.CSS, using = ".location-cards .nav-pane .next")
	public WebElement mostSearchedFromNextArrow;
	
	@FindBy(how = How.CSS, using = ".card--horizontal .card__inner .card__title")
	public List<WebElement> rafflesTitles;
	
	@FindBy(how = How.CSS, using = ".location-cards .card--secondary .card__image .card__title")
	public List<WebElement> travelLocationCardTitles;
	
	@FindBy(how = How.CSS, using = ".section-heading .show-all-deal")
	public WebElement seeAllLinkForToRatedInYourCity;
	
	@FindBy(how = How.CSS, using = ".collection-cards .nav-pane .prev")
	public WebElement RHRNprevArrowButton;
	
	@FindBy(how = How.CSS, using = ".collection-cards .nav-pane .next")
	public WebElement RHRNnextArrowButton;
	
	@FindBy(how = How.CSS, using = ".dealcard .nav-pane .prev")
	public List<WebElement> prevArrowButtonForNavPanels;
	
	@FindBy(how = How.CSS, using = ".dealcard .nav-pane .next")
	public List<WebElement> nextArrowButtonForNavPanels;
	
	@FindBy(how = How.CSS, using = ".card--secondary .card__image img")
	public List<WebElement> RHRNimages;
	
	@FindBy(how = How.CSS, using = ".card__title.font-weight-bold")
	public List<WebElement> collectionTitle;
	
	@FindBy(how = How.CSS, using = ".texticon-group__icon .icon-deal")
	public List<WebElement> collectionIcons;
	
	@FindBy(how = How.CSS, using = ".card__description .texticon-group__content .line-height-default")
	public List<WebElement> numberOfOffersInCollectionCards;
	
	@FindBy(how = How.CSS, using = ".collection-cards .card--secondary .card__description .list-inline li:last-child p")
	public List<WebElement> priceRangeInCollectionCards;
	
	@FindBy(how = How.CSS, using = ".collection-cards .card--secondary .card__inner")
	public List<WebElement> RHRNcollectionCards;
	
	@FindBy(how = How.CSS, using = ".travel-discover")
	public WebElement travelSearchDiv;
	
	@FindBy(how = How.CSS, using = ".travel-discover__heading")
	public WebElement travelSearchHeading;
	
	@FindBy(how = How.CSS, using = ".travel-discover__content")
	public WebElement travelSearchRightContent;
	
	@FindBy(how = How.CSS, using = ".travel-discover__content .btn--primary")
	public WebElement findHotelsButton;
	
	@FindBy(how = How.CSS, using = ".travel-discover__content")
	public List<WebElement> travelSearchBoxes;
	
	@FindBy(how = How.CSS, using = ".travel-discover__content .icon-calender")
	public List<WebElement> travelCalenderIcon;
}
