package com.groupon.web.nearbuy.locator;

import java.util.List;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;

public class AuthenticationLocator {

	@FindBy(how = How.CSS, using = "a.utility-btn[href*='login']")
	public WebElement loginLink;
	
	@FindBy(how = How.CSS, using = ".header-msg")
	public List<WebElement> headerMessages;
	
	@FindBy(how = How.CSS, using = ".login-field__input")
	public List<WebElement> loginInputFields;
	
	
	
	@FindBy(how = How.CSS, using = ".hero-banner__title")
	public WebElement mainHeading;
	
	@FindBy(how = How.CSS, using = "#login-page img")
	public WebElement imageOnLoginPage;
	
	@FindBy(how = How.CSS, using = ".top_deals.container")
	public WebElement carousel;

	@FindBy(how = How.XPATH, using = ".//input[contains(@class,'login-field__input')][@formcontrolname='emailOrphone']")
	public WebElement usernameInput;
	
	@FindBy(how = How.XPATH, using = ".//input[contains(@class,'login-field__input')][@formcontrolname='password']")
	public WebElement passwordInput;
	
	@FindBy(how = How.XPATH, using = ".//input[contains(@class,'login-field__input')][@formcontrolname='otp']")
	public WebElement otpInput;
	
	@FindBy(how = How.CSS, using = ".login-popup input[formcontrolname='emailOrphone']")
	public WebElement forgotPasswordUsernameInput;

	@FindBy(how = How.CSS, using = ".sign-up-poup .btn--primary")
	public WebElement continueButtonForSignUp;
	
	@FindBy(how = How.CSS, using = ".login-popup button")
	public List<WebElement> signInSubmitButton;
	
	@FindBy(how = How.CSS, using = ".login-modal .modal")
	public WebElement loginModalPopUp;
	
	@FindBy(how = How.CSS, using = ".login-tabs li")
	public List<WebElement> loginSignUpTabs;
	
	@FindBy(how = How.CSS, using = ".modal__header button")
	public WebElement loginCloseButton;
	
	@FindBy(how = How.CSS, using = ".margin-top-m span")
	public WebElement notifyOffersText;
	
	@FindBy(how = How.CSS, using = ".nb-checkbox input")
	public WebElement notifyOffersCheckbox;
	
	@FindBy(how = How.CSS, using = ".login-field__link")
	public WebElement recieveOtpOnCallLink;
	
	@FindBy(how = How.CSS, using = ".login-popup .txt-tertiary")
	public WebElement textOnForgotPasswordOtpScreen;
	
	@FindBy(how = How.CSS, using = "forgot-password-form .btngroup-inline a")
	public WebElement backLinkOnForgotPassSetOtpScreen;
	
	@FindBy(how = How.CSS, using = ".login-popup .txt-tertiary span")
	public WebElement numberTextOnForgotPasswordOtpScreen;
	
	@FindBy(how = How.CSS, using = ".sign-up-poup .txt-tertiary")
	public WebElement textOnOtpScreen;
	
	@FindBy(how = How.CSS, using = ".sign-up-poup .txt-tertiary span")
	public WebElement numberTextOnOtpScreen;
	
	@FindBy(how = How.CSS, using = ".sign-up-poup .btngroup-inline a")
	public WebElement backLinkOnSignUpScreen;

	@FindBy(how = How.CSS, using = "span.error-msg")
	public List<WebElement> error;
	
	@FindBy(how = How.CSS, using = ".sign-up-poup span.error-msg")
	public List<WebElement> signUpError;
	
	@FindBy(how = How.CSS, using = ".utility-btn span")
	public WebElement loginSignUpLink;

	@FindBy(how = How.CSS, using = ".sign-up-poup a")
	public WebElement alreadyUserLink;

	@FindBy(how = How.XPATH, using = "//a[text()='Create an account']")
	public WebElement createAnAccountLink;
	
	@FindBy(how = How.CSS, using = ".signin-card h4")
	public WebElement heading;
	
	@FindBy(how = How.CSS, using = ".login-popup .txt-tertiary")
	public WebElement forgotPasswordText;
	
	@FindBy(how = How.CSS, using = ".signup")
	public WebElement signUpForm;
	
	@FindBy(how = How.CSS, using = "#presignUp img")
	public WebElement imageOnSignUp;
	
	@FindBy(how = How.CSS, using = ".secondary-links.flt-right a")
	public WebElement forgotYourPasswordLink;

	@FindBy(how = How.CSS, using = ".btngroup-inline .btn--primary")
	public WebElement forgotYourPasswordProceedButton;
	
	@FindBy(how = How.CSS, using = "p.error-msg")
	public WebElement forgotYourPasswordCustomerNotExistingError;
	
	/**
	 * Migrate To Nearbuy
	 */
	
	@FindBy(how = How.CSS, using = ".sign-in p")
	public WebElement migrateSubHeading;
	
	@FindBy(how = How.CSS, using = "#submit-button")
	public WebElement initiateMigrationButton;
	
	
}
