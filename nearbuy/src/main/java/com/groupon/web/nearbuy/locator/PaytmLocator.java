package com.groupon.web.nearbuy.locator;

import java.util.List;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;

public class PaytmLocator {
	
	@FindBy(how = How.ID, using = "login-modal")
	public WebElement loginPopup;
	
	@FindBy(how = How.ID, using = "login-iframe")
	public WebElement loginIframe;
	
	@FindBy(how = How.XPATH, using = ".//button[text()='Secure Sign In']")
	public WebElement secureSignInButton;

	@FindBy(how = How.XPATH, using = "//*[@id='loginForm']/div/div[2]/input")
	public WebElement username;

	@FindBy(how = How.XPATH, using = "//*[@id='loginForm']/div/input[3]")
	public WebElement password;
	
	@FindBy(how = How.ID, using = "logout-iframe")
	public WebElement logoutIframe;
	
	@FindBy(how = How.CSS , using = ".cancel")
	public WebElement cancelLink;
	
	@FindBy(how = How.XPATH, using = ".//input[@value='Pay now']")
	public WebElement payNowButton;

	/**
	 * Wallet 
	 */
	
	@FindBy(how = How.CSS, using = ".wallet-btn")
	public List<WebElement> walletOptions;
	
	@FindBy(how = How.CSS, using = "wallet-section button")
	public WebElement walletPayNowButton;
	
	@FindBy(how = How.CSS, using = "wallet-section .show")
	public WebElement walletBlock;

	
}
