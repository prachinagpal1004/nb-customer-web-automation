package com.groupon.web.nearbuy.locator;

import java.util.List;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;

public class OrderSummaryLocator {
	
	@FindBy(how = How.CSS, using = ".card-head")
	public List<WebElement> orderSummaryHeading;
	
	@FindBy(how = How.CSS, using = ".venue__tickets")
	public List<WebElement> offerCards;

	@FindBy(how = How.CSS, using = ".venue__image")
	public WebElement cardImage;
	
	@FindBy(how = How.CSS, using = ".offer .card__image img")
	public WebElement travelCardImage;
	
	@FindBy(how = How.CSS, using = ".card__inner-content b")
	public List<WebElement> travelCheckInCheckOutDates;
	
	@FindBy(how = How.CSS, using = ".offer-content__title")
	public WebElement travelOptionTitle;
	
	@FindBy(how = How.CSS, using = ".offer-content__actual-price")
	public WebElement travelOptionMrpPrice;
	
	@FindBy(how = How.CSS, using = ".offer-content__actual-price + span")
	public WebElement travelOptionNearbuyPrice;
	
	@FindBy(how = How.CSS, using = ".col-m-7 p.offer-content__qty")
	public WebElement travelOptionNumberOfRoomsAndNights;
	
	@FindBy(how = How.CSS, using = ".col-m-5 span.used-credit-text")
	public WebElement travelOptionPriceAfterRoomsAndNightsCalculation;
	
	@FindBy(how = How.CSS, using = ".offer-content__qty.txt-brand-secondary")
	public WebElement payableNowLabelText;
	
	@FindBy(how = How.CSS, using = ".col-l-6.col-m-5 span")
	public WebElement payableNowAmount;
	
	@FindBy(how = How.CSS, using = ".col-m-7.col-l-8 p")
	public List<WebElement> totalPackageCostLabelTextAndInclOfTaxesLabelText;
	
	@FindBy(how = How.CSS, using = ".col-m-5.col-l-4 p")
	public WebElement totalPackageCost;
	
	@FindBy(how = How.CSS, using = "travel-order-summary-card .txt-center")
	public WebElement travelYourTotalSavingsText;
	
	@FindBy(how = How.CSS, using = ".opener.show")
	public WebElement offerListOpener;
	
	@FindBy(how = How.CSS, using = ".block__inner p.txt-tertiary")
	public WebElement offerSubTotalLabel;
	
	@FindBy(how = How.CSS, using = ".card__title")
	public WebElement dealCardTitle;
	
	@FindBy(how = How.CSS, using = ".offer-card")
	public WebElement offersDescription;
	
	@FindBy(how = How.XPATH, using = ".//p[@class='txt-secondary']")
	public WebElement quanityInSubtotalDiv;
	
	@FindBy(how = How.CSS, using = ".txt-decoration-underline")
	public WebElement promoLink;
	
	@FindBy(how = How.CSS, using = "apply-coupon-modal .modal_wrapper.modal-show")
	public WebElement promoPoUp;
	
	@FindBy(how = How.CSS, using = ".modal__body.txt-center.show")
	public WebElement congratsMessagePromoPopUp;
	
	@FindBy(how = How.CSS, using = ".form-group promo-card .promo")
	public List<WebElement> listOfPromosAvailable;
	
	@FindBy(how = How.CSS, using = ".col-m-6 .txt-right p")
	public WebElement subTotalPrice;
	
	@FindBy(how = How.CSS, using = "#applyCodeContainer label")
	public WebElement promoHeading;
	
	@FindBy(how = How.CSS, using = ".search-container input")
	public WebElement promoTextBox;
	
	@FindBy(how = How.CSS, using = ".search-container button")
	public WebElement applyPromoButton;
	
	@FindBy(how = How.CSS, using = ".coupon-error .error-msg")
	public WebElement promoErrorMsg;
	
	@FindBy(how = How.CSS, using = "#removeCodeContainer")
	public WebElement removeVoucherContainer;
	
	@FindBy(how = How.CSS, using = ".show .offer-content__qty.txt-secondary")
	public WebElement promoAppliedSuccessMessage;
	
	@FindBy(how = How.CSS, using = ".VoucherCode")
	public WebElement promoCodeApplied;
	
	@FindBy(how = How.CSS, using = ".show .offer-content__qty+a")
	public WebElement removePromoCodeLink;
	
	@FindBy(how = How.CSS, using = ".col-l-4 .col-m-4 .txt-right .font-size-xs")
	public WebElement promoDiscountAmount;
	
	@FindBy(how = How.CSS, using = ".payment-proceed")
	public WebElement paymentGatewaysDiv;
	
	@FindBy(how = How.CSS, using = ".payment-proceed__navigation li")
	public List<WebElement> paymentOptions;
	
	@FindBy(how = How.CSS, using = "apply-coupon-modal button.close")
	public WebElement promoPopUpCloseIcon;
	
	@FindBy(how = How.CSS, using = ".card__inner-content .nb-checkbox input")
	public WebElement creditCheckbox;
	
	@FindBy(how = How.CSS, using = ".col-l-6.col-m-8 .txt-brand-secondary")
	public WebElement availableCreditsAmountOnRight;
	
	@FindBy(how = How.CSS, using = ".col-l-6.col-m-8 .txt-primary")
	public WebElement useMyCreditsText;
	
	@FindBy(how = How.CSS, using = ".col-l-6.col-m-7 span")
	public WebElement creditsUsedText;
	
	@FindBy(how = How.CSS, using = ".col-l-6.col-m-5 .txt-right span")
	public WebElement usedCreditsAmount;
	
	@FindBy(how = How.CSS, using = ".col-m-8 .h5")
	public WebElement finalPrice;
	
	@FindBy(how = How.CSS, using = ".col-m-7 p")
	public List<WebElement> priceListOfOptionsSelectedByUsers;
	
	@FindBy(how = How.CSS, using = ".order-total .text-right strong")
	public WebElement orderTotalText;
	
	@FindBy(how = How.CSS, using = "button[class='nb-btn btn_orange']")
	public WebElement proceedToPaymentButton;
	
	@FindBy(how = How.CSS, using = ".saved-card .card-cvv-input")
	public WebElement cvvTextBox;
	
	@FindBy(how = How.CSS, using = "saved-card button")
	public WebElement savedCardPayNowButton;
	
	@FindBy(how = How.CSS, using = ".btn--primary")
	public WebElement proceedButton;
	
	@FindBy(how = How.CSS, using = ".select_payment_mode_heading")
	public WebElement selectPaymentModeHeading;
	
//	@FindBy(how = How.ID, using = "paymentModePayU")
//	public WebElement payuMode;
//	
//	@FindBy(how = How.CSS, using = "#paymentModePayU + span + span + span")
//	public WebElement payuModeLabel;
//	
//	@FindBy(how = How.ID, using = "paymentModePayTM")
//	public WebElement paytmMode;
//	
//	@FindBy(how = How.CSS, using = "#paymentModePayTM + span + span + span img")
//	public WebElement paytmModeLabel;
//	
//	@FindBy(how = How.ID, using = "paymentModeMobikwik")
//	public WebElement mobikwikMode;
//	
//	@FindBy(how = How.CSS, using = "#paymentModeMobikwik + span + span + span img")
//	public WebElement mobikwikModeLabel;
	
	@FindBy(how = How.CSS, using = ".payment-proceed__navigation li.has-offer")
	public List<WebElement> paymentModesHavingOffers;
	
	@FindBy(how = How.CSS, using = ".payment-proceed__navigation li.has-offer.active")
	public WebElement currentPaymentModeHaveOffers;
	
	@FindBy(how = How.CSS, using = ".payment-discount p")
	public WebElement availOffersHeading;
	
	@FindBy(how = How.CSS, using = ".payment-discount__offer")
	public List<WebElement> paymentOffersDiv;
	
	@FindBy(how = How.CSS, using = ".list-style-disc li>p")
	public WebElement offerDetailsBulletPoints;
	
	@FindBy(how = How.CSS, using = ".payment-error-banner")
	public WebElement transactionFailedSection;
	
	@FindBy(how = How.CSS, using = ".payment-error-banner h5")
	public WebElement transactionFailedHeading;
	
	@FindBy(how = How.CSS, using = ".payment-error-banner ul li")
	public WebElement transactionFailedMessage;
	
	@FindBy(how = How.CSS, using = ".payment-error-banner .error-btn")
	public WebElement transactionFailedTryAgainButton;
	
	/**
	 * Delivery Address in case of Products
	 */
	
	@FindBy(how = How.CSS, using = "#paymentSummary h3")
	public WebElement deliveryAddressHeading;
	
	@FindBy(how = How.CSS, using = ".deliverAddress")
	public WebElement oldDeliveryAddress;
	
	@FindBy(how = How.CSS, using = "#shipAddressId1")
	public WebElement oldDeliveryAddressCheckbox;
	
	@FindBy(how = How.CSS, using = "#shipAddressId1 + label")
	public WebElement oldDeliveryAddressText;
	
	@FindBy(how = How.ID, using = "paymentAddress")
	public WebElement paymentAddressSection;
	
	@FindBy(how = How.ID, using = "addAddressBtn")
	public WebElement addNewAddressLink;
	
	@FindBy(how = How.ID, using = "Pincode")
	public WebElement pincodeTextbox;
	
	@FindBy(how = How.ID, using = "City")
	public WebElement cityTextbox;
	
	@FindBy(how = How.ID, using = "locationSuggestion")
	public WebElement locationSelectionbox;
	
	@FindBy(how = How.ID, using = "line1")
	public WebElement addressLine1Textbox;
	
	@FindBy(how = How.ID, using = "line2")
	public WebElement addressLine2Textbox;
	
	/**
	 * Data Layer
	 */
	
	@FindBy(how = How.ID, using = "page")
	public WebElement dlCustomerId;
	
	@FindBy(how = How.ID, using = "dealType")
	public WebElement dlDealType;
	
	@FindBy(how = How.ID, using = "totalPrice")
	public WebElement dlTotalPrice;
	
	@FindBy(how = How.ID, using = "dealTitle0")
	public WebElement dlDealTitle;
	
	@FindBy(how = How.NAME, using = "offers[0].offerId")
	public WebElement dlDealId;
	
	@FindBy(how = How.NAME, using = "offers[0].price")
	public WebElement dlOfferPrice;
	
	@FindBy(how = How.NAME, using = "offers[0].quantity")
	public WebElement dlNumberOfVouchers;
	
}
