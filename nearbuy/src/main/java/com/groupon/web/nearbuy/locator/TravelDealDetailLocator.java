package com.groupon.web.nearbuy.locator;

import java.util.List;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;

public class TravelDealDetailLocator {
	
	@FindBy(how = How.CSS, using = ".option .option__choice button")
	public List<WebElement> bookThisOptionAllButtons;
	
	@FindBy(how = How.CSS, using = ".option__choice .error-msg")
	public WebElement errorMessage;
	
	@FindBy(how = How.CSS, using = "booking-modal .modal-show")
	public WebElement bookingFormModal;
	
	@FindBy(how = How.XPATH, using = ".//input[@placeholder='Check-in date ']")
	public WebElement checkInDateBox;
	
	@FindBy(how = How.XPATH, using = ".//input[@placeholder='Check-out date ']")
	public WebElement checkOutDateBox;
	
	@FindBy(how = How.ID, using = "ui-datepicker-div")
	public WebElement calenderDiv;
	
	@FindBy(how = How.CSS, using = "select")
	public WebElement numberOfRoomsDropDown;
	
	@FindBy(how = How.CSS, using = ".clear-dates")
	public WebElement clearDatesLink;
	
	@FindBy(how = How.CSS, using = ".add-on__heading+.margin-top-l button")
	public WebElement proceedToPayButtonInTravelForm;
	
	@FindBy(how = How.CSS, using = ".rating-icon.tripadvisor")
	public WebElement tripAdvisorLogo;
	
	@FindBy(how = How.CSS, using = ".ui-datepicker-calendar a")
	public List<WebElement> allAvailableDates;
	
	@FindBy(how = How.CSS, using = ".deal-detail-travel .option__desc")
	public List<WebElement> allOptionsDescription;
	
	@FindBy(how = How.CSS, using = ".deal-detail-travel .option__desc a")
	public List<WebElement> allMoreLinks;
	
	@FindBy(how = How.CSS, using = ".inclusion-modal.modal-show .option__offer p")
	public List<WebElement> inclusionsBulletPoints;
	
	@FindBy(how = How.CSS, using = ".inclusion-modal.modal-show .h5")
	public WebElement inclusionHeading;
	
	@FindBy(how = How.CSS, using = ".inclusion-modal.modal-show .icon-close")
	public WebElement inclusionsPopUpCloseButton;
	
	@FindBy(how = How.CSS, using = ".deal-detail-travel .option__right")
	public List<WebElement> optionsRightSide;
	
	@FindBy(how = How.CSS, using = ".calender-container.modal-show")
	public WebElement ratesAndAvailaibilityPopUp;
	
	@FindBy(how = How.CSS, using = ".calendar__inner-data")
	public WebElement calenderInnerData;
	
	@FindBy(how = How.XPATH, using = ".//*[contains(@class, 'cal-arrow--left')]/..")
	public WebElement calenderLeftArrow;
	
	@FindBy(how = How.XPATH, using = ".//*[contains(@class, 'cal-arrow--right')]/..")
	public WebElement calenderRightArrow;
	
	@FindBy(how = How.CSS, using = ".location-map img")
	public WebElement mapsImage;
	
	@FindBy(how = How.CSS, using = ".calendar__body")
	public List<WebElement> calenderBody;
	
	@FindBy(how = How.CSS, using = ".col-m-4 .h6")
	public List<WebElement> rightHandHeadings;
	
	@FindBy(how = How.CSS, using = ".col-m-5 p.font-size-xs")
	public List<WebElement> checkIncheckoutLabels;
	
	@FindBy(how = How.CSS, using = ".col-m-7 p.txt-primary")
	public List<WebElement> checkIncheckoutTimings;

	@FindBy(how = How.CSS, using = ".location-popup")
	public WebElement locationPopUp;

	@FindBy(how = How.CSS, using = ".location-address-list ul li")
	public List<WebElement> mapsLocationAddress;
	
	@FindBy(how = How.CSS, using = ".margin-bottom-xl .h6")
	public List<WebElement> headings;
	
	@FindBy(how = How.CSS, using = ".list-style-disc li")
	public List<WebElement> listBulletPoints;

	@FindBy(how = How.CSS, using = ".location-popup .close")
	public WebElement locationPopUpCloseButton;
	
	@FindBy(how = How.CSS, using = ".facalities .h6")
	public WebElement facilitiesHeading;
	
	@FindBy(how = How.CSS, using = ".facalities .col-l-4.col-m-4")
	public List<WebElement> listOfFacilities;
	
	@FindBy(how = How.CSS, using = ".facalities a")
	public WebElement moreOrLessLinkInFacilities;
	
	@FindBy(how = How.CSS, using = ".deal-detail-travel .facalities__content")
	public WebElement facilitiesContentDiv;
	
}
