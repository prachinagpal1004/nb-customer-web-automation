package com.groupon.web.nearbuy.locator;

import java.util.List;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;

public class DealCatalogLocator {
	
	@FindBy(how = How.CSS, using = "[class^='btn'].block")
	public WebElement viewDealButton;
	
	@FindBy(how = How.CSS, using = ".featured_deal")
	public WebElement featuredDealSection;

	@FindBy(how = How.CSS, using = ".browse_deals")
	public WebElement moreDealsSection;
	
	@FindBy(how = How.CSS, using = ".btn_cream")
	public WebElement viewAllDealsLink;
	
	@FindBy(how = How.CSS, using = ".deal_cart")
	public List<WebElement> dealBox;
	
	@FindBy(how = How.CSS, using = ".back-to-top")
	public WebElement scrollToTopButton;
	
	/**
	 * Breadcrumbs
	 */
	@FindBy(how = How.CSS, using = ".breadcrumbs a")
	public List<WebElement> breadcrumbsList;
	
	@FindBy(how = How.XPATH, using = ".//*[@class='breadcrumbs__list']/a[last()]")
	public WebElement lastBreadcrumb;
	
	/**
	 * Filters
	 */
	
	@FindBy(how = How.CSS, using = ".categories")
	public WebElement categorySection;
	
	@FindBy(how = How.CSS, using = ".filter_box .filter_title")
	public List<WebElement> categoryAndPriceHeadings;
	
	@FindBy(how = How.CSS, using = ".filter_menu .count")
	public List<WebElement> categoryAndPriceCounts;
	
	@FindBy(how = How.CSS, using = ".filter_menu .cat_name")
	public List<WebElement> categoryNames;
	
	@FindBy(how = How.ID, using = "ul-price-filter")
	public WebElement priceFilterSection;
	
	@FindBy(how = How.CSS, using = "#ul-price-filter .checker span")
	public List<WebElement> priceFilterCheckboxes;
	
	@FindBy(how = How.CSS, using = "#ul-price-filter label")
	public List<WebElement> priceFilterLabels;
	
	@FindBy(how = How.CSS, using = ".custom_price label")
	public WebElement enterPriceRangeLabel;
	
	@FindBy(how = How.ID, using = "custom_min_price")
	public WebElement minPriceRangeTextbox;
	
	@FindBy(how = How.ID, using = "custom_max_price")
	public WebElement maxPriceRangeTextbox;
	
	@FindBy(how = How.ID, using = "ccustom_apply_price")
	public WebElement applyPriceRangeLink;
	
	@FindBy(how = How.CSS, using = ".filter_box.location")
	public WebElement locationSection;
	
	@FindBy(how = How.CSS, using = ".filter-location .view-more")
	public WebElement viewMoreLink;
	
	@FindBy(how = How.CSS, using = "#locationContainer")
	public WebElement viewMoreContainerModal;
	
	@FindBy(how = How.CSS, using = ".modal__header p")
	public WebElement viewMoreModalHeading;
	
	@FindBy(how = How.CSS, using = "#locationContainer .form-control")
	public WebElement viewMoreSearchBox;
	
	@FindBy(how = How.CSS, using = "more-locations-popup .close")
	public WebElement viewMorePopUpCLoseButton;
	
	@FindBy(how = How.CSS, using = "#locationContainer .menu-header")
	public List<WebElement> viewMorePopupMainLocationHeadings;
	
	@FindBy(how = How.CSS, using = ".sub-locations span.count")
	public List<WebElement> viewMorePopUpDealCountInSublocation;
	
	@FindBy(how = How.CSS, using = ".sub-locations span")
	public List<WebElement> viewMorePopUpSubLocationText;
	
	@FindBy(how = How.ID, using = "location_modal")
	public WebElement locationPopup;
	
	@FindBy(how = How.XPATH, using = "//button[.='Clear']")
	public WebElement viewMorePopUpClearButton;
	
	@FindBy(how = How.XPATH, using = "//button[.='Apply']")
	public WebElement viewMorePopUpApplyButton;
	
	@FindBy(how = How.CSS, using = "#location_modal h6")
	public List<WebElement> locationHeading;
	
	@FindBy(how = How.ID, using = "resetLocs")
	public WebElement resetButton;
	
	@FindBy(how = How.ID, using = "applyMoreLocation")
	public WebElement applyButton;
	
	@FindBy(how = How.CSS, using = ".close-reveal-modal")
	public WebElement closeButton;
	
	@FindBy(how = How.CSS, using = "#moreLocContainer .count")
	public List<WebElement> countOfDealsInPopup;
	
	@FindBy(how = How.CSS, using = "#moreLocContainer label")
	public List<WebElement> labelOfDealsInPopup;
	
	@FindBy(how = How.CSS, using = "#moreLocContainer .checker")
	public List<WebElement> checkboxForDealsInPopup;
	
	@FindBy(how = How.CSS, using = "div.table")
	public WebElement leftSmallImageWidget;
	
	@FindBy(how = How.CSS, using = "div.table h3")
	public List<WebElement> categories;
	
	@FindBy(how = How.CSS, using = ".cat_name")
	public WebElement headingOnLeftSmallImageWidget;
	
	/**
	 * Data Layer
	 */
	
	@FindBy(how = How.ID, using = "category")
	public WebElement dlCategory;
	
	@FindBy(how = How.ID, using = "city")
	public WebElement dlCity;
	
	@FindBy(how = How.ID, using = "dealTitle0")
	public WebElement dlDealTitle1;
	
	@FindBy(how = How.ID, using = "dealTitle1")
	public WebElement dlDealTitle2;
	
	@FindBy(how = How.ID, using = "dealTitle2")
	public WebElement dlDealTitle3;
	
	@FindBy(how = How.ID, using = "dealId0")
	public WebElement dlDeal1;
	
	@FindBy(how = How.ID, using = "dealId1")
	public WebElement dlDeal2;
	
	@FindBy(how = How.ID, using = "dealId2")
	public WebElement dlDeal3;
	
	@FindBy(how = How.ID, using = "searchForm")
	public WebElement dlVertical;
	
	@FindBy(how = How.ID, using = "searchKey")
	public WebElement dlSubcategory;
	
	/**
	 * New Listing Page
	 */
	
	@FindBy(how = How.CSS, using = "div[class^=deal-detail-]")
	public WebElement dealDetailPage;
	
	@FindBy(how = How.CSS, using = ".card__inner a")
	public List<WebElement> dealCards;
	
	@FindBy(how = How.CSS, using = ".card__rating")
	public List<WebElement> ratingsOnDealCards;
	
	@FindBy(how = How.CSS, using = "img.deal.loading")
	public List<WebElement> dealImagesOnDealCards;
	
	@FindBy(how = How.CSS, using = ".card__title")
	public List<WebElement> dealTitlesOnDealCards;
	
	@FindBy(how = How.CSS, using = ".card__location")
	public List<WebElement> dealLocationsOnDealCards;
	
	@FindBy(how = How.CSS, using = ".card__description")
	public List<WebElement> dealDescriptionsOnDealCards;
	
	@FindBy(how = How.CSS, using = ".discount_price")
	public List<WebElement> dealDiscountPricesOnDealCards;
	
	@FindBy(how = How.CSS, using = ".actual_price")
	public List<WebElement> dealActualPricesOnDealCards;
	
	@FindBy(how = How.CSS, using = ".bought")
	public List<WebElement> dealBoughtCountersOnDealCards;
	
	@FindBy(how = How.CSS, using = ".filter-categories")
	public WebElement categoryFilterFullDiv;
	
	@FindBy(how = How.CSS, using = ".list-block.menu")
	public List<WebElement> categoryFilterTree;
	
	@FindBy(how = How.CSS, using = ".category-heading")
	public WebElement categoryHeading;
	
	@FindBy(how = How.CSS, using = ".city-name span:first-child")
	public WebElement categoryTextInPageHeading;
	
	@FindBy(how = How.CSS, using = ".city-name span:last-child")
	public WebElement locationTextInPageHeading;
	
	@FindBy(how = How.CSS, using = ".city-name h1")
	public WebElement offersInTextInPageHeading;
	
	/**
	 * Filters
	 */
	
	@FindBy(how = How.CSS, using = ".filter-location .h6")
	public WebElement locationFilterMainHeading;
	
	@FindBy(how = How.CSS, using = ".filter-location input[type='search']")
	public WebElement locationFilterSearchbox;
	
	@FindBy(how = How.CSS, using = "#ul-location-filter")
	public List<WebElement> locationFilterHeadings;
	
	@FindBy(how = How.CSS, using = "#ul-location-filter .nb-checkbox")
	public List<WebElement> subLocationFilters;
	
	@FindBy(how = How.CSS, using = ".nb-checkbox__bg")
	public List<WebElement> allLocationCheckboxes;
	
	@FindBy(how = How.CSS, using = ".filter-location .count")
	public List<WebElement> locationFilterCounts;
	
	@FindBy(how = How.CSS, using = ".padding-left-xsg")
	public List<WebElement> locationFilterName;
	
	@FindBy(how = How.CSS, using = ".sublocation-wrapper li")
	public List<WebElement> subLocationWrapper;
	
	@FindBy(how = How.CSS, using = ".padding-left-xs")
	public List<WebElement> subLocationListAfterUserSearched;
	
	@FindBy(how = How.CSS, using = ".nb-checkbox input:checked+.nb-checkbox__bg")
	public WebElement checkedBoxInRedColor;
	
	@FindBy(how = How.CSS, using = ".card__location h3")
	public List<WebElement> locationInEachCard;
	
	/**
	 * Sort
	 */
	@FindBy(how = How.CSS, using = ".sorter")
	public WebElement sortDiv;
	
	@FindBy(how = How.CSS, using = ".sorter button")
	public List<WebElement> sortOptions;
	
	@FindBy(how = How.CSS, using = ".card .actual-price")
	public List<WebElement> nearbuyPrices;
	
	@FindBy(how = How.CSS, using = ".sort-buttons button.active")
	public WebElement activeSortOption;

}
