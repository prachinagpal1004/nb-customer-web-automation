package com.groupon.web.nearbuy.utils;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.List;

import org.apache.log4j.Logger;
import org.junit.Assert;
import org.openqa.selenium.WebElement;

/**
 * @author prachi.nagpal
 */
public class ImageServiceUtils {

	private static final Logger logger = Logger.getLogger(ImageServiceUtils.class);

	public static void verifyImageURL(WebElement imageURLxpath) throws IOException{

		String imageName =	imageURLxpath.getAttribute("alt");
		String imageURLtext = imageURLxpath.getAttribute("src");
		System.out.println(imageName);
		Assert.assertTrue("Image URL is NOT starting with img1.nbstatic",imageURLtext.startsWith("//img1.nbstatic.in/"));  
		URL imageURL = new URL(imageURLtext);
		HttpURLConnection connection = (HttpURLConnection)imageURL.openConnection();
		connection.setRequestMethod("GET");
		connection.connect();
		int code = connection.getResponseCode();
		if(code!=200){

			System.out.println("Breaking Image URL : " + imageURLtext);
			System.out.println("Breaking Image Name : " + imageName);
		}
	}
}
