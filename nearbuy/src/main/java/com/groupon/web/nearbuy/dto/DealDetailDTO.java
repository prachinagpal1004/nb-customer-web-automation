package com.groupon.web.nearbuy.dto;

public class DealDetailDTO {

	private String title;
	private String soldCount;

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getSoldCount() {
		return soldCount;
	}

	public void setSoldCount(String soldCount) {
		this.soldCount = soldCount;
	}

}
