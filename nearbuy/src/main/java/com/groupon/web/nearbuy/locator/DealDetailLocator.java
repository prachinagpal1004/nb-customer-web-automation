package com.groupon.web.nearbuy.locator;

import java.util.List;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;

public class DealDetailLocator {

	@FindBy(how = How.CSS, using = ".common-offer-card .offer-card__title")
	public WebElement dealHeading;
	
	@FindBy(how = How.CSS, using = ".offer-card__img")
	public WebElement dealImageDiv;

	@FindBy(how = How.CSS, using = ".orders__buy button")
	public WebElement buyNowButton;
	
	@FindBy(how = How.CSS, using = ".orders__total+button")
	public WebElement emptyOrderBuyNowButton;
	
	@FindBy(how = How.CSS, using = ".o-sold-out a")
	public WebElement continueShoppingButton;
	
	@FindBy(how = How.CSS, using = ".login-modal")
	public WebElement loginModal;

	@FindBy(how = How.CSS, using = ".nb-freedom .select_offer-list ul.offers-list")
	public WebElement offersBlockDiv;
	//
	// @FindBy(how = How.CSS, using = ".select_option_head h3")
	// public WebElement chooseOfferHeading;

	@FindBy(how = How.CSS, using = ".active.quantity_plus")
	public WebElement activeQuantityPlusIcons;

	// @FindBy(how = How.XPATH, using =
	// ".//*[@id='quantity']/preceding-sibling::a")
	// public List<WebElement> quantityMinusIcons;
	//
	// @FindBy(how = How.ID, using = "quantity")
	// public WebElement quantity;
	//
	// @FindBy(how = How.CSS, using = ".offer_discount .actual_price")
	// public List<WebElement> actualPrice;
	//
	// @FindBy(how = How.CSS, using = ".offer_discount .green_text")
	// public List<WebElement> priceAfterDiscount;
	//
	// @FindBy(how = How.CSS, using = ".offer_discount p .left")
	// public List<WebElement> percentageDiscounts;
	//
	// @FindBy(how = How.CSS, using = ".offer_discount .font-size12")
	// public List<WebElement> selectQtyAndTotalText;
	//
	// @FindBy(how = How.CSS, using = ".offer_location")
	// public List<WebElement> offerLabels;
	//
	// @FindBy(how = How.CSS, using = ".offer_location + h6")
	// public List<WebElement> offerHeading;
	//
	// @FindBy(how = How.ID, using = "total")
	// public List<WebElement> totalPrice;

	@FindBy(how = How.ID, using = "mCSB_1_dragger_vertical")
	public WebElement verticalDraggerOnOffers;

	@FindBy(how = How.CSS, using = ".bi_bulk .btn_green")
	public WebElement buyInBulkButton;

	@FindBy(how = How.CSS, using = ".text-center b")
	public WebElement bulkCountForVouchers;

	@FindBy(how = How.CSS, using = "#slider .slides li")
	public List<WebElement> mainImageSlider;

	@FindBy(how = How.CSS, using = "flex-next")
	public WebElement sliderNextButton;

	@FindBy(how = How.CSS, using = "#slider a")
	public List<WebElement> sliderNextPrevIcons;

	// @FindBy(how = How.CSS, using = "#carousel .slides li")
	// public List<WebElement> carouselSlider;

	// @FindBy(how = How.CSS, using = "#carousel a")
	// public List<WebElement> carouselNextPrevIcons;

	@FindBy(how = How.CSS, using = ".more_deal_details h6")
	public List<WebElement> moreDealHeadings;

	@FindBy(how = How.CSS, using = ".more_deal_details a")
	public List<WebElement> moreDealBlocks;

	@FindBy(how = How.CSS, using = ".deal_cart .product_image p")
	public List<WebElement> moreDealBlocksTextOnHover;

	// @FindBy(how = How.CSS, using = ".tags")
	// public List<WebElement> tags;

	@FindBy(how = How.CSS, using = ".share-deal label")
	public WebElement shareThisDealLabel;

	@FindBy(how = How.CSS, using = ".share-deal")
	public WebElement shareThisDealSection;
	
	@FindBy(how = How.CSS, using = ".payment-offers")
	public WebElement bankOfferSection;
	
	@FindBy(how = How.CSS, using = ".payment-offers__content p")
	public WebElement bankOfferSectionHeading;
	
	@FindBy(how = How.CSS, using = ".bank-offer-modal .close")
	public WebElement bankOfferPopUpCloseButton;
	
	@FindBy(how = How.CSS, using = ".payment-offers__cards li")
	public List<WebElement> bankOffersList;
	
	@FindBy(how = How.CSS, using = ".bank-offer-modal.modal-show")
	public WebElement bankOffersPopUp;
	
	@FindBy(how = How.CSS, using = ".bank-offer-modal .offer-banner__slider-element")
	public List<WebElement> bankOffersListInPopUp;

	@FindBy(how = How.CSS, using = ".share-deal .social_media a")
	public List<WebElement> shareThisDealIcons;
	
	@FindBy(how = How.CSS, using = ".block.block--secondary")
	public List<WebElement> blockSectionOnDealDetail;
	
	@FindBy(how = How.CSS, using = ".custom-list-detail li p")
	public List<WebElement> howToUseOfferListPoints;
	
	@FindBy(how = How.XPATH, using = ".//*[contains(@class,'custom-list-detail')]/../p")
	public WebElement howToUseOfferHeading;
	
	@FindBy(how = How.XPATH, using = ".//*[@class='list-style-disc']/../p")
	public WebElement thingsToRememberHeading;
	
	@FindBy(how = How.XPATH, using = ".//*[@class='list-style-disc']/li")
	public List<WebElement> thingsToRememberPoints;

	// @FindBy(how = How.ID, using = "map-canvas")
	// public WebElement maps;

	@FindBy(how = How.XPATH, using = ".//*[@class='map-cont']//..//..//..//p")
	public WebElement mapsLocationHeading;

	@FindBy(how = How.CSS, using = ".map-cont")
	public WebElement mapsLocationContainer;
	
	@FindBy(how = How.CSS, using = ".all-promo-wrapper")
	public WebElement promoContainer;
	
	@FindBy(how = How.CSS, using = ".promo-offer-modal.modal-show")
	public WebElement promoPopUp;
	
	@FindBy(how = How.CSS, using = ".promo-offer-modal .close")
	public WebElement promoPopUpCloseButton;
	
	@FindBy(how = How.CSS, using = ".promo-offer-modal .h6")
	public WebElement promoPopUpHeading;
	
	@FindBy(how = How.CSS, using = ".offers-card")
	public List<WebElement> promoPopUpOffersCards;
	
	@FindBy(how = How.CSS, using = ".promo-slider-wrapper .all-promo-wrapper .promo-wrapper")
	public List<WebElement> allPromoList;
	
	@FindBy(how = How.CSS, using = ".location-popup")
	public WebElement locationPopUp;
	
	@FindBy(how = How.CSS, using = ".location-address-list+button")
	public WebElement viewAllLocationsButotn;
	
	@FindBy(how = How.CSS, using = ".location-popup .close")
	public WebElement locationPopUpCloseButton;

	@FindBy(how = How.CSS, using = ".location-address-list ul li")
	public List<WebElement> mapsLocationAddress;

	@FindBy(how = How.CSS, using = ".loaders")
	public WebElement dealDetailsSection;

	@FindBy(how = How.CSS, using = ".option__title")
	public List<WebElement> dealDetailOptionTitles;

	@FindBy(how = How.CSS, using = ".hardrock .address")
	public WebElement addressSection;

	@FindBy(how = How.CSS, using = ".deal_details .view_grp_location")
	public WebElement viewLocationList;

	@FindBy(how = How.CSS, using = ".deal_details .view_grp_menu")
	public WebElement viewServicesOrGrouponMenu;

	@FindBy(how = How.CSS, using = "#menu_modal")
	public WebElement viewMenuPopUpDialogBox;

	@FindBy(how = How.CSS, using = "#body li")
	public List<WebElement> foodMenuOrServicesListInPopUp;

	@FindBy(how = How.CSS, using = "#body font")
	public List<WebElement> locationListInPopUp;

	@FindBy(how = How.CSS, using = ".close-reveal-modal")
	public WebElement closeButtonOnMenuPopUp;

	/**
	 * For Products
	 */

	@FindBy(how = How.CSS, using = "#product-deal-detail-page .no-hand-icon")
	public WebElement selectOptionText;

	@FindBy(how = How.CSS, using = "#mCSB_1_container")
	public WebElement offerSection;

	@FindBy(how = How.CSS, using = ".choose_an_offer .select_option_body div.radio span")
	public WebElement radioButtonCheckbox;

	@FindBy(how = How.CSS, using = "#mCSB_1_container span input")
	public List<WebElement> optionsCheckbox;

	@FindBy(how = How.CSS, using = "#mCSB_1_container li label")
	public List<WebElement> optionsLabelText;

	@FindBy(how = How.CSS, using = "#product-deal-detail-page .select_option_body .no-hand-icon")
	public List<WebElement> sizeOrColorLabel;

	@FindBy(how = How.CSS, using = "#mCSB_1_container li")
	public List<WebElement> offersList;

	@FindBy(how = How.ID, using = "resetOption")
	public WebElement resetLink;

	@FindBy(how = How.ID, using = "actual_price")
	public WebElement productActualPrice;

	@FindBy(how = How.ID, using = "discount_price")
	public WebElement productDiscountPrice;

	@FindBy(how = How.ID, using = "total")
	public WebElement productTotal;

	@FindBy(how = How.ID, using = "discount")
	public WebElement productDiscount;

	@FindBy(how = How.ID, using = "productOfferQantitySelectionError")
	public WebElement qtyErrorMessage;

	@FindBy(how = How.CSS, using = ".select_color li")
	public List<WebElement> numberOfOptions;

	/**
	 * For Deal Content
	 */

	@FindBy(how = How.CSS, using = ".nb-freedom .deal_details b")
	public List<WebElement> dealContentHeadings;

	@FindBy(how = How.CSS, using = ".nb-freedom .deal_details li")
	public List<WebElement> dealContentLists;

	@FindBy(how = How.CSS, using = ".deal_details p")
	public List<WebElement> dealContentParagraphs;

	@FindBy(how = How.CSS, using = ".nb-freedom .view_all")
	public List<WebElement> viewMoreLink;

	@FindBy(how = How.CSS, using = ".nb-freedom .deal_details a.btn_green")
	public List<WebElement> tnCandMenuForTheDealButton;

	@FindBy(how = How.ID, using = "nearbuy-terms-conditions")
	public WebElement tNcPopUp;

	@FindBy(how = How.ID, using = "nearbuy_menu")
	public WebElement menuPopUp;

	@FindBy(how = How.CSS, using = "#nearbuy-terms-conditions b")
	public WebElement tNcPopUpHeading;

	@FindBy(how = How.CSS, using = "#nearbuy_menu b")
	public WebElement menuPopUpHeading;

	@FindBy(how = How.CSS, using = "#body p strong")
	public List<WebElement> menuPopUpBoldText;

	@FindBy(how = How.CSS, using = "#nearbuy-terms-conditions strong")
	public List<WebElement> tNcPopUpBoldText;

	@FindBy(how = How.CSS, using = "#nearbuy-terms-conditions li")
	public List<WebElement> tNcPopUpListPoints;

	@FindBy(how = How.CSS, using = "#body li strong")
	public List<WebElement> menuPopUpBoldPoints;

	@FindBy(how = How.CSS, using = "#body li")
	public List<WebElement> menuPopUpListPoints;

	@FindBy(how = How.CSS, using = "#nearbuy_menu .close-reveal-modal")
	public WebElement menuCloseButton;

	@FindBy(how = How.CSS, using = "#nearbuy-terms-conditions .close-reveal-modal")
	public WebElement tNcCloseButton;

	/**
	 * Data Layer
	 */

	@FindBy(how = How.ID, using = "title")
	public WebElement dlTitle;

	@FindBy(how = How.ID, using = "city")
	public WebElement dlCity;

	@FindBy(how = How.ID, using = "dealId")
	public WebElement dlDealId;

	@FindBy(how = How.ID, using = "price")
	public WebElement dlPrice;

	@FindBy(how = How.ID, using = "soldQuantity")
	public WebElement dlSoldQuantity;

	@FindBy(how = How.ID, using = "permalink")
	public WebElement dlPermalink;

	@FindBy(how = How.NAME, using = "vertical")
	public WebElement dlVertical;

	/**
	 * Offer Side
	 */

	@FindBy(how = How.CSS, using = ".nb-freedom .select_offer-heading p")
	public WebElement offerHeading;

	@FindBy(how = How.CSS, using = ".nb-select-qty-text")
	public List<WebElement> selectQtyText;

	@FindBy(how = How.CSS, using = ".nb-freedom .select_offer .actual_price")
	public List<WebElement> strikeThroughPrice;

	@FindBy(how = How.CSS, using = ".quantity__inc")
	public List<WebElement> optionPlusSigns;

	@FindBy(how = How.CSS, using = ".orders__total .col-m-8 p")
	public WebElement totalPriceText;

	@FindBy(how = How.CSS, using = ".orders__total .col-m-4 p")
	public WebElement totalAmount;
	
	@FindBy(how = How.CSS, using = ".orders__total span")
	public WebElement inclusiveOfAllText;

	@FindBy(how = How.CSS, using = ".quantity__count")
	public List<WebElement> quantityCount;

	@FindBy(how = How.CSS, using = ".option__title")
	public List<WebElement> optionTitle;
	
	@FindBy(how = How.CSS, using = ".orders__total--empty")
	public WebElement emptyOrderCart;

	@FindBy(how = How.CSS, using = ".discounted-price")
	public List<WebElement> nearbuyPrice;

	// @FindBy(how = How.CSS, using = "a.inactive.quantity_minus")
	@FindBy(how = How.CSS, using = ".quantity__dec")
	public List<WebElement> optionMinusSigns;

	@FindBy(how = How.CSS, using = ".option .block__inner")
	public List<WebElement> optionPanelList;
	
	@FindBy(how = How.CSS, using = ".offer__status")
	public WebElement offerStatusDiv;
	
	@FindBy(how = How.CSS, using = ".offer__status p")
	public WebElement offerStatusHeading;
	
	@FindBy(how = How.CSS, using = ".offer__status b")
	public WebElement offerStatusSoldOutText;

	@FindBy(how = How.CSS, using = ".option .txt-brand-primary")
	public List<WebElement> soldOutOffer;

	/**
	 * Travel
	 */

	@FindBy(how = How.CSS, using = ".offer_sold_out")
	public WebElement ioei;

	/**
	 * New Deal Detail Page
	 */

	@FindBy(how = How.CSS, using = ".selected_offer-buy .grey_txt b")
	public WebElement buyInBulkBoldMessage;

	@FindBy(how = How.CSS, using = ".promise-content")
	public WebElement nearbuyPromiseContent;
	
	@FindBy(how = How.XPATH, using = ".//*[@class='promise-content']//..//img")
	public WebElement nearbuyPromiseImage;
	
	@FindBy(how = How.CSS, using = ".promise-content+.tooltip-popover-wrapper .icon-help")
	public WebElement nearbuyPromiseTooltip;

	@FindBy(how = How.CSS, using = ".promise-content+.tooltip-popover-wrapper .tooltip-popover.show")
	public WebElement nearbuyPromisePopUp;
	
	@FindBy(how = How.CSS, using = ".promise-tag b")
	public List<WebElement> nearbuyPromiseTagLinesInPopUp;
	
	@FindBy(how = How.XPATH, using = ".//*[@class='texticon-group__content']//..//img")
	public List<WebElement> nearbuyPromiseImageInPopUp;

	@FindBy(how = How.CSS, using = ".promise-content+.tooltip-popover-wrapper .icon-close")
	public WebElement nearbuyPromiseCloseIcon;

	@FindBy(how = How.CSS, using = ".btn--tags")
	public List<WebElement> nearbuyTags;

	@FindBy(how = How.ID, using = "local-deal-detail-page")
	public WebElement localDealDetailSection;
	
	@FindBy(how = How.CSS, using = ".quantity__inc")
	public List<WebElement> plusSign;
	
	@FindBy(how = How.XPATH, using = ".//*[@class='quantity__dec']//..//..//..//..//..//h2")
	public List<WebElement> nameOfOptionsUserHasSelected;
	
	@FindBy(how = How.XPATH, using = ".//*[@class='quantity__dec']/following-sibling::span")
	public List<WebElement> quantityOfOptionsUserHasSelected;
	
	@FindBy(how = How.XPATH, using = ".//*[@class='quantity__dec']//..//..//*[contains(@class,'discounted-price')]")
	public List<WebElement> nearbuyPriceOfOptionsUserHasSelected;
	
	@FindBy(how = How.XPATH, using = ".//*[@class='quantity__dec']//..//..//*[contains(@class,'actual-price')]")
	public List<WebElement> actualPriceOfOptionsUserHasSelected;
	
	@FindBy(how = How.CSS, using = ".orders__list p")
	public List<WebElement> nameOfOffersInCart;
	
	@FindBy(how = How.CSS, using = ".orders__savings p")
	public WebElement orderSavingText;
	
	@FindBy(how = How.CSS, using = ".orders__savings b")
	public WebElement orderSavingPrice;
	
	@FindBy(how = How.CSS, using = ".per-order-quant")
	public List<WebElement> quantityOfOptionsInCart;
	
	@FindBy(how = How.CSS, using = ".per-order-price")
	public List<WebElement> priceOfOptionsInCart;

}
