package com.groupon.web.nearbuy.data;

import java.io.IOException;
import java.util.List;

import org.testng.annotations.DataProvider;

import com.groupon.web.common.utils.GoogleSheetUtils;

/**
 * 
 * @author prachi.nagpal
 *
 */
public class  SeoData {

	@DataProvider(name = "fetchSeoInputData")
	public static Object[][] fetchSeoInputData(){
		String spreadsheetId = "1mACzxSkG_xJi3Ej4v-YaQIEvmSEUTTM6NnOL2h4cd0Q";
		String range = "Sheet1!A2:F";
		try {
			List<List<Object>> values = GoogleSheetUtils.getValues(spreadsheetId, range);
			Object[][] data = new Object[values.size()][];
			for (int i = 0; i < values.size(); i++) {
				List<Object> list = values.get(i);
				data[i] = list.toArray(new Object[list.size()]);
			}
			return data;
		} catch (IOException e) {
			e.printStackTrace();
			throw new RuntimeException(e);
		}
		
//		return new Object[][] {
//
//				{"Around You","Food & Drink","","","Best <Category> Offers in <City Name> | nearbuy", "Find the most awesome <Category> Offers trending in <City Name> right now! Compare, select &amp; ENJOY the best <Category> offers near you with 100% Guarantee."},
//				{"Around You","Food & Drink","Breakfast","","Best <Sub-Category> Offers in <City Name> | nearbuy", "Find the best trending <Sub-Category> Offers in <City Name> today! Make your mornings truly special–enjoy your <Sub-Category> @ jaw dropping prices...with the nearbuy Promise!"},
//				{"Around You","Food & Drink","Lunch","","Best <Sub-Category> Offers in <City Name> | nearbuy", "Find the best trending <Sub-Category> Offers in <City Name> today! Make your afternoons awesome–munch your <Sub-Category> @ jaw dropping prices...backed by the nearbuy Promise!"},
//				{"Hotels","Rajasthan","","","<Category> Tour Packages: Top Trending Hotels, Trips, Holidays...| nearbuy","See today's TOP TRENDING <Category> Tour Packages: Stay for 2 in AC Room + Breakfast, etc. Get 20-80% OFF @Neemrana Fort-Palace, Rajputana, FabHotel...with nearbuy's 100% Guarantee!"}
//		};
	}
}
