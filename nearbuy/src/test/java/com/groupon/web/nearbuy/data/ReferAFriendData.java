package com.groupon.web.nearbuy.data;

import org.testng.annotations.DataProvider;
import com.groupon.web.common.config.Config;
import com.groupon.web.common.utils.DataProviderUtil;

/**
 * 
 * @author prachi.nagpal
 *
 */
public class ReferAFriendData {

	@DataProvider(name = "twitterCredentials")
	public static Object[][] provideCredentials(){
		return DataProviderUtil.provideData(Config.getInstance().getConfig("testData.file"), "nearbuy","twitterCredentials");
	}
	
}
