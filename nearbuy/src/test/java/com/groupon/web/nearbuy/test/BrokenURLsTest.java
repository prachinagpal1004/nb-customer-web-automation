package com.groupon.web.nearbuy.test;

import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import com.groupon.web.common.test.BaseTest;
import com.groupon.web.common.utils.TestListener;
import com.groupon.web.nearbuy.data.AuthenticationData;
import com.groupon.web.nearbuy.page.AuthenticationPage;
import com.groupon.web.nearbuy.page.DealCatalogPage;
import com.groupon.web.nearbuy.page.DealDetailPage;
import com.groupon.web.nearbuy.page.HomePage;
import com.groupon.web.nearbuy.page.OrderSummaryPage;
/**
 * 
 * @author Prachi Nagpal
 *
 */
import com.relevantcodes.extentreports.LogStatus;
@Test(groups = {"sanity","broken"})
public class BrokenURLsTest extends BaseTest {

	private HomePage homePage;
	private DealCatalogPage listing;
	private DealDetailPage detail;
	private AuthenticationPage authenticationPage;
	private OrderSummaryPage order;

	@BeforeMethod(alwaysRun=true)
	public void init() {
		homePage = new HomePage();
		listing = new DealCatalogPage();
		detail = new DealDetailPage();
		authenticationPage = new AuthenticationPage();
		order = new OrderSummaryPage();
	}

	@Test(priority = 1)
	public void testAllBrokenURLsInThePage() {
		
		homePage.printAllBrokenLinksForCurrentPage();
		listing.openURL("https://www.nearbuy.com/offers/mumbai/food-and-drink");
		listing.printAllBrokenLinksForCurrentPage();
		listing.openURL("https://www.nearbuy.com/offer/mumbai/kurla-west/KFC-6032/6032?list=Search%20Page");
		listing.printAllBrokenLinksForCurrentPage();
//		detail.clickOnPlusToIncreaseOptionQuantity(1, 4);
//		detail.clickOnBuyNowButton();
//		listing.printAllBrokenLinksForCurrentPage();
//		order.waitForOrderSummaryToLoad();
//		listing.printAllBrokenLinksForCurrentPage();
	}

//	@Test(priority = 2 , dataProvider="successNumberLogin" ,dataProviderClass = AuthenticationData.class )
//	public void testUserDoesSuccessNumberLogin(String username, String password, String message, String bool) throws Throwable {
//
//		TestListener.setLog(LogStatus.INFO, "Enter Credentials for Successful Number login");
//		authenticationPage.verifyLoginAndErrorMessages(username, password, message, Boolean.valueOf(bool));
//		Thread.sleep(4000);
//		listing.printAllBrokenLinksForCurrentPage();
//		Thread.sleep(4000);
//	}
}
