package com.groupon.web.nearbuy.data;

import org.testng.annotations.DataProvider;
import com.groupon.web.common.config.Config;
import com.groupon.web.common.utils.DataProviderUtil;

/**
 * 
 * @author prachi.nagpal
 *
 */
public class OrderSummaryData {

	@DataProvider(name = "deliveryAddressDetails")
	public static Object[][] provideAddress(){
		return DataProviderUtil.provideData(Config.getInstance().getConfig("testData.file"), "nearbuy","deliveryAddressDetails");
	}
}
