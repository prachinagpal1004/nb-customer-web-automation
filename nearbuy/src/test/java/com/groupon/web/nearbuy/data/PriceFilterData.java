package com.groupon.web.nearbuy.data;

import org.testng.annotations.DataProvider;

import com.groupon.web.common.config.Config;
import com.groupon.web.common.utils.DataProviderUtil;

/**
 * 
 * @author prachi.nagpal
 *
 */
public class PriceFilterData {

	@DataProvider(name = "minMaxPriceRange")
	public static Object[][] providePriceRange(){
		return DataProviderUtil.provideData(Config.getInstance().getConfig("testData.file"), "nearbuy","minMaxPriceRange");
	}
	
}
