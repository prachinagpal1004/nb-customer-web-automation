package com.groupon.web.nearbuy.data;

import org.testng.annotations.DataProvider;

import com.groupon.web.common.config.Config;
import com.groupon.web.common.utils.DataProviderUtil;

/**
 * 
 * @author prachi.nagpal
 *
 */
public class PromoData {

	@DataProvider(name = "enterValidPromo")
	public static Object[][] provideValidPromo(){
		return DataProviderUtil.provideData(Config.getInstance().getConfig("testData.file"), "nearbuy","enterValidPromo");
	}
	@DataProvider(name = "enterInvalidPromo")
	public static Object[][] provideInvalidPromo(){
		return DataProviderUtil.provideData(Config.getInstance().getConfig("testData.file"), "nearbuy","enterInvalidPromo");
	}
	@DataProvider(name = "selfReferralCode")
	public static Object[][] provideselfReferral(){
		return DataProviderUtil.provideData(Config.getInstance().getConfig("testData.file"), "nearbuy","selfReferralCode");
	}
	@DataProvider(name = "notFirstTransactionReferralCode")
	public static Object[][] providenotFirstTransactionReferral(){
		return DataProviderUtil.provideData(Config.getInstance().getConfig("testData.file"), "nearbuy","notFirstTransactionReferralCode");
	}
}
