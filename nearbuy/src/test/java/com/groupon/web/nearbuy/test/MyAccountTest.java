package com.groupon.web.nearbuy.test;

import com.groupon.web.common.test.BaseTest;
import com.groupon.web.common.utils.PageUtils;
import com.groupon.web.common.utils.TestListener;
import com.groupon.web.nearbuy.data.AuthenticationData;
import com.groupon.web.nearbuy.data.ReferAFriendData;
import com.groupon.web.nearbuy.page.AuthenticationPage;
import com.groupon.web.nearbuy.page.MyAccountPage;
import com.relevantcodes.extentreports.LogStatus;

import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
/**
 * 
 * @author Prachi Nagpal
 *
 */
@Test(groups = {"sanity", "staging", "myAccount"})
public class MyAccountTest extends BaseTest {

	private AuthenticationPage authenticationPage;
	private MyAccountPage myAccountPage;

	@BeforeMethod(alwaysRun=true)
	public void init() {
		authenticationPage = new AuthenticationPage();
		myAccountPage = new MyAccountPage();
	}


	//	/**
	//	 * Test Click on Login Link
	//	 */
	//	@Test(priority = 1 )
	//	public void user_clicks_On_loginLink() throws Throwable {
	//		authenticationPage.clickOnLoginLink();
	//	}

	/**
	 * Test Success Login
	 */
	@Test(priority = 2 , dataProvider="successNumberLogin" ,dataProviderClass = AuthenticationData.class )
	public void user_clicks_SuccessLogin(String username, String password, String message, String bool) throws Throwable {
		TestListener.setLog(LogStatus.INFO, "Enter Credentials for Successful Login");
		//		String authPopupEnabled = Config.getInstance().getConfig("auth.popup.enabled");
		//		if(authPopupEnabled != null && Boolean.valueOf(authPopupEnabled)) {
		//			(new Thread(new BrowserAuthenticationWindow())).start();
		//		}
		authenticationPage.clickOnLoginLink().verifyLoginAndErrorMessages(username, password, message, Boolean.valueOf(bool));

	}



	/**
	 * Test Credits
	 */
	@Test(priority = 3)
	public void testCredits() throws Throwable {
		TestListener.setLog(LogStatus.INFO, "Click on 'Credits' Link from My Account Drop Down Box");
		myAccountPage.clickOnLink("My Credits");
		TestListener.setLog(LogStatus.INFO, "Verify Credits Section");
		myAccountPage.myCredits();
		//		TestListener.setLog(LogStatus.INFO, "Verify Credits Section DataLayer");
		//		myAccountPage.verifyDataLayerMyCredits();
	}

	/**
	 * Test Profile
	 */
	@Test(priority = 4)
	public void testProfile() throws Throwable {
		TestListener.setLog(LogStatus.INFO, "Click on 'My Profile' Link from My Account Drop Down Box");
		myAccountPage.clickOnLink("My Profile");
		TestListener.setLog(LogStatus.INFO, "Verify Profile Section. Edit Name & Save");
		myAccountPage.myProfile(PageUtils.getRandomName());
		//		TestListener.setLog(LogStatus.INFO, "Verify Profile Section DataLayer");
		//		myAccountPage.verifyDataLayerMyProfile();
	}

	/**
	 * Test Newsletter Subscription
	 */
	@Test(priority = 5)
	public void testNewsletter() throws Throwable {
		TestListener.setLog(LogStatus.INFO, "Click on 'My Newsletters' Link from My Account Drop Down Box");
		myAccountPage.clickOnLink("My Newsletters");
		TestListener.setLog(LogStatus.INFO, "Verify Newsletters Section. Edit City & Subscribe");
		myAccountPage.myNewsletters();
		//TestListener.setLog(LogStatus.INFO, "Verify Newsletter Section DataLayer");
		//myAccountPage.verifyDataLayerMyNewsletters();
	}



	/**
	 * Test Promos
	 */
	@Test(priority = 6)
	public void testPromos() throws Throwable {
		TestListener.setLog(LogStatus.INFO, "Click on 'My Promos' Link from My Account Drop Down Box");
		myAccountPage.clickOnLink("My Promos");
		TestListener.setLog(LogStatus.INFO, "Verify My Promos Section");
		myAccountPage.myPromos();
	}

	/**
	 * Test Referrals
	 */
	@Test(priority = 7, dataProvider="twitterCredentials" ,dataProviderClass = ReferAFriendData.class)
	public void testReferrals(String user, String pass) throws Throwable {
		TestListener.setLog(LogStatus.INFO, "Click on 'Refer a friend' Link from My Account Drop Down Box");
		myAccountPage.clickOnLink("Refer a friend");
		TestListener.setLog(LogStatus.INFO, "Verify Refer a friend Section");
		myAccountPage.referAfriend(user,pass);
	}

	/**
	 * Test LogOut
	 */
	@Test(priority = 8)
	public void testLogout() throws Throwable {
		TestListener.setLog(LogStatus.INFO, "Click on Sign Out Link & Verify");
		myAccountPage.clickOnLink("Sign Out");
	}


}
