package com.groupon.web.nearbuy.test;

import com.groupon.web.common.test.BaseTest;
import com.groupon.web.common.utils.TestListener;
import com.groupon.web.nearbuy.page.HeaderFooterPage;
import com.relevantcodes.extentreports.LogStatus;

import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
/**
 * 
 * @author Prachi Nagpal
 *
 */
@Test(groups = {"sanity", "staging", "headerFooter"})
public class HeaderAndFooterTest extends BaseTest {

	private HeaderFooterPage headerFooterPage;

	@BeforeMethod(alwaysRun=true)
	public void init() {
		headerFooterPage = new HeaderFooterPage();
	}

	@Test(priority = 1)
	public void testHowItWorks() throws Throwable {
		TestListener.setLog(LogStatus.INFO, "Verify How It Works");
		headerFooterPage.verifyHowItWorks();
	}
	@Test(priority = 2)
	public void testHeaderFooterAndLogo() {
		TestListener.setLog(LogStatus.INFO, "Verify Footer");
		headerFooterPage.footer();
		TestListener.setLog(LogStatus.INFO, "Verify Logo");
		headerFooterPage.verifyLogo();
	}
	
	@Test(priority = 3)
	public void testStickySearchBox() throws Throwable {
		TestListener.setLog(LogStatus.INFO, "Verify Search Box is sticky on Header");
		headerFooterPage.verifyPrimaryHeaderIsSticky();
	}

	@Test(priority = 4)
	public void testEachSocialLinkAndVerifyInNewTab() throws Throwable {
		TestListener.setLog(LogStatus.INFO, "Verify All Social Links in Footer");
		headerFooterPage.clickOnSocialLinksAndVerifyTabsOpened();
	}

	@Test(priority = 5)
	public void testCompanyAndAppLinks() throws Throwable {
		TestListener.setLog(LogStatus.INFO, "Click & Verify Company Links opened New Tabs");
		headerFooterPage.clickOnCompanyLinksAndVerify();
	}

	@Test(priority = 6)
	public void testCompanyLinksContent() throws Throwable {
		TestListener.setLog(LogStatus.INFO, "Click & Verify About Us, Policy , Universal FIne Print");
		headerFooterPage.verifyCompanyLinksContent();
	}
}

