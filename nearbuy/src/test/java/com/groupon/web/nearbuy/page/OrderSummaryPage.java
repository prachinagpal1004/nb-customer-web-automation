package com.groupon.web.nearbuy.page;

import com.groupon.web.common.page.BasePage;
import com.groupon.web.common.utils.WaitUtility;
import com.groupon.web.nearbuy.locator.OrderSummaryLocator;
import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.testng.Assert;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class OrderSummaryPage extends BasePage {

	private static final Logger logger = Logger.getLogger(OrderSummaryPage.class);
	public OrderSummaryLocator locator;

	public OrderSummaryPage() {
		locator = PageFactory.initElements(getWebDriver(), OrderSummaryLocator.class);
	}

	/**
	 * Verify Order Summary Details
	 * @return
	 */
	public OrderSummaryPage verifyOrderSummaryDetails(){

		waitVar.until(ExpectedConditions.urlContains("order/placeorder"));
		waitVar.until(ExpectedConditions.visibilityOf(locator.cardImage));
		Assert.assertTrue(locator.cardImage.isDisplayed());
		//Assert.assertTrue(locator.dealCardTitle.isDisplayed());

		for(WebElement heading : locator.orderSummaryHeading){
			Assert.assertTrue(heading.isDisplayed());
			logger.info("Heading : " + heading.getText());
		}

		for(WebElement card : locator.offerCards){
			Assert.assertTrue(card.isDisplayed());

			if(locator.offerCards.size()>2){
				Assert.assertTrue(locator.offerListOpener.isDisplayed());
				Assert.assertTrue(locator.offerListOpener.isEnabled());
				locator.offerListOpener.click();
				waitVar.until(ExpectedConditions.visibilityOfAllElementsLocatedBy(By.cssSelector(".venue.collapsible")));
			}

			WebElement offerTitles = card.findElement(By.cssSelector("p"));
			Assert.assertTrue(offerTitles.isDisplayed());
			logger.info(offerTitles.getText());
		}


		Assert.assertTrue(locator.offerSubTotalLabel.isDisplayed());
		logger.info(locator.offerSubTotalLabel.getText());

		Assert.assertTrue(locator.subTotalPrice.isDisplayed());
		String subTotalPriceAmount = locator.subTotalPrice.getText();
		if(subTotalPriceAmount.contains(",")){
			subTotalPriceAmount=subTotalPriceAmount.replace(",", "");
		}
		logger.info("SubTotal Price : " + Double.valueOf(subTotalPriceAmount.trim()));

		return this;
	}

	/**
	 * Verify Promo Code Section and Send Promocode
	 * @return
	 */
	public OrderSummaryPage enterPromoCode(String promo, Boolean validPromo, Boolean userWantsToRemovePromo){


		waitVar.until(ExpectedConditions.elementToBeClickable(locator.promoLink));
		Assert.assertTrue(locator.promoLink.isDisplayed());
		Assert.assertTrue(locator.promoLink.isEnabled());
		locator.promoLink.click();
		waitVar.until(ExpectedConditions.visibilityOf(locator.promoPoUp));
		waitVar.until(ExpectedConditions.elementToBeClickable(locator.applyPromoButton));
		logger.info("PromoLink is clicked.");

		Assert.assertTrue(locator.promoTextBox.isDisplayed());
		Assert.assertTrue(locator.promoTextBox.isEnabled());

		Assert.assertTrue(locator.applyPromoButton.isDisplayed());
		Assert.assertTrue(locator.applyPromoButton.isEnabled());
		Assert.assertTrue(locator.promoTextBox.getAttribute("placeholder").equals("Enter coupon code"));

		locator.applyPromoButton.click();
		waitVar.until(ExpectedConditions.visibilityOf(locator.promoErrorMsg));
		Assert.assertTrue(locator.promoErrorMsg.getText().equals("Please Enter a coupon code"));

		locator.promoTextBox.clear();
		locator.promoTextBox.sendKeys(promo);
		logger.info("Running for promocode : " + promo);
		locator.applyPromoButton.click();

		try {
			Thread.sleep(2000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		// If promo is applied successfully
		if(WaitUtility.isElementPresent(getWebDriver(), locator.promoErrorMsg)){

			System.out.println("Probably promo code is invalid");
			applyPromoFromListAvailable();
		}
		else {

			waitVar.until(ExpectedConditions.elementToBeClickable(locator.removePromoCodeLink));
			logger.info("Promocode is applied successfully : " + promo);

			Assert.assertTrue(locator.promoDiscountAmount.isDisplayed());
			logger.info("Discount Amount after Promocode is applied : " + locator.promoDiscountAmount.getText());

			Assert.assertTrue(locator.promoAppliedSuccessMessage.isDisplayed());
			Assert.assertTrue(locator.promoAppliedSuccessMessage.getText().equals("Code successfully applied"));
			logger.info("Promo Applied Success Message: " + locator.promoAppliedSuccessMessage.getText());

			locator.removePromoCodeLink.click();
			waitVar.until(ExpectedConditions.visibilityOf(locator.promoLink));

			locator.promoLink.click();
			waitVar.until(ExpectedConditions.visibilityOf(locator.promoPoUp));

			applyPromoFromListAvailable();
		}

		//			Assert.assertTrue(locator.promoCodeApplied.isDisplayed());
		//			logger.info("Promo Code Used : " + locator.promoCodeApplied.getText());

		if(userWantsToRemovePromo){
			removePromo();
		}
		else {
			logger.info("User wants to apply this promo");
		}


//		// If invalid Promo
//		if(!validPromo && locator.promoErrorMsg.isDisplayed()){
//
//			waitVar.until(ExpectedConditions.visibilityOf(locator.promoErrorMsg));
//			waitVar.until(ExpectedConditions.visibilityOf(locator.applyPromoButton));
//			logger.info("Promocode is invalid : " + promo);
//
//			Assert.assertTrue(locator.promoErrorMsg.isDisplayed());
//			logger.info("Invalid Promo Message : " + locator.promoErrorMsg.getText());
//			Assert.assertTrue(locator.promoErrorMsg.getText().equals("Oops! Invalid coupon code."));
//			//	closeInvaildPromoMessage();
//		}


		return this;
	}

	public void applyPromoFromListAvailable() {

		if(locator.listOfPromosAvailable.size()>0){
			for(WebElement each : locator.listOfPromosAvailable){
				Assert.assertTrue(each.findElement(By.cssSelector("h3")).isDisplayed());
				Assert.assertTrue(each.findElement(By.cssSelector(".promo__title p")).isDisplayed());
				Assert.assertTrue(each.findElement(By.cssSelector(".promo__code__text")).isDisplayed());
				Assert.assertTrue(each.findElement(By.cssSelector(".promo__code__copy button")).isDisplayed());
				Assert.assertTrue(each.findElement(By.cssSelector(".promo__code__copy button")).isEnabled());
				Assert.assertTrue(each.findElement(By.cssSelector(".block__footer")).isDisplayed());
				Assert.assertTrue(each.findElement(By.cssSelector(".promo__more button")).isDisplayed());
				Assert.assertTrue(each.findElement(By.cssSelector(".promo__more button")).isEnabled());
				each.findElement(By.cssSelector(".promo__more button")).click();
				waitVar.until(ExpectedConditions.visibilityOf(each.findElement(By.cssSelector(".block__footer.show-more"))));
				each.findElement(By.cssSelector(".promo__more button")).click();
				waitVar.until(ExpectedConditions.visibilityOf(each.findElement(By.cssSelector(".block__footer.show-less"))));

			}
			locator.listOfPromosAvailable.get(0).findElement(By.cssSelector(".promo__code__copy button")).click();

			try {
				Thread.sleep(3000);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

			if(WaitUtility.isElementPresent(getWebDriver(), locator.congratsMessagePromoPopUp)){
				waitVar.until(ExpectedConditions.elementToBeClickable(locator.congratsMessagePromoPopUp.findElement(By.cssSelector("button"))));
				Assert.assertTrue(locator.congratsMessagePromoPopUp.findElement(By.cssSelector("h3")).isDisplayed());
				Assert.assertTrue(locator.congratsMessagePromoPopUp.findElement(By.cssSelector("button")).isDisplayed());
				Assert.assertTrue(locator.congratsMessagePromoPopUp.findElement(By.cssSelector(".txt-tertiary")).isDisplayed());
				Assert.assertTrue(locator.congratsMessagePromoPopUp.findElement(By.cssSelector("button")).isEnabled());
				Assert.assertTrue(locator.congratsMessagePromoPopUp.findElement(By.cssSelector("button")).getText().toLowerCase().trim().equals("continue"));

				locator.congratsMessagePromoPopUp.findElement(By.cssSelector("button")).click();
				System.out.println("Continue Button is clciked");

			}

			waitVar.until(ExpectedConditions.visibilityOf(locator.removePromoCodeLink));
			waitVar.until(ExpectedConditions.elementToBeClickable(locator.removePromoCodeLink));
			Assert.assertTrue(locator.promoAppliedSuccessMessage.isDisplayed());
			System.out.println("Promo code applied by choosing from the list");
		}

		else if(locator.listOfPromosAvailable.size()==0){
			logger.info("No Promos available in the website right now");
			locator.promoPopUpCloseIcon.click();
			waitVar.until(ExpectedConditions.invisibilityOfElementLocated(By.cssSelector("apply-coupon-modal .modal_wrapper.modal-show")));
		}
	}

	/**
	 * Remove Promocode and its post verifications
	 * @return
	 */
	public OrderSummaryPage removePromo(){

		locator.removePromoCodeLink.click();
		waitVar.until(ExpectedConditions.visibilityOf(locator.promoLink));
		waitVar.until(ExpectedConditions.elementToBeClickable(locator.promoLink));
		//Assert.assertFalse(locator.promoDiscountAmount.isDisplayed());
		logger.info("Promoocde removed");

		return this;
	}


	/**
	 * Close Invalid Promo Message
	 * @return
	 */
	//	public OrderSummaryPage closeInvaildPromoMessage(){
	//
	//		Assert.assertTrue(locator.closeIconOfInvalidPromoMessage.isEnabled());
	//		locator.closeIconOfInvalidPromoMessage.click();
	//		waitVar.until(ExpectedConditions.invisibilityOfElementLocated(By.cssSelector(".incorrect-promo-code .fa")));
	//		logger.info("Promocode Invalid Message closed");
	//
	//		return this;
	//	}

	/**
	 * Use my credits Section
	 */
	public OrderSummaryPage selectDeselectCredits(Boolean userDontWantToUseCredits){

		if(WaitUtility.isElementPresentByLocator(getWebDriver(), By.cssSelector(".card__inner-content .nb-checkbox input"))){

			logger.info("Credits Section is present");
			Assert.assertTrue(locator.useMyCreditsText.isDisplayed());
			logger.info("Credits Text : " + locator.useMyCreditsText.getText());

			waitVar.until(ExpectedConditions.elementToBeClickable(locator.creditCheckbox));
			Assert.assertTrue(locator.creditCheckbox.isDisplayed());
			Assert.assertTrue(locator.creditCheckbox.isEnabled());
			//Assert.assertTrue(locator.creditCheckbox.getAttribute("checked").equals("checked"));
			logger.info("Credits available : " + locator.availableCreditsAmountOnRight.getText());
			logger.info("Credits Checkbox is preselected");

			Assert.assertTrue(locator.creditsUsedText.isDisplayed());
			Assert.assertEquals(locator.creditsUsedText.getText(), "Credits used");
			Assert.assertTrue(locator.usedCreditsAmount.isDisplayed());
			logger.info("Credits in use : " + locator.usedCreditsAmount.getText());

			//Assert.assertEquals(locator.availableCreditsText.getText(), locator.usedCredits.getText());
			logger.info("Checkbox is pre selected for credits");

			// If user wants to deselect credits
			if(userDontWantToUseCredits){
				Assert.assertNotNull(userDontWantToUseCredits);
				locator.creditCheckbox.click();
				waitVar.until(ExpectedConditions.invisibilityOfElementLocated(By.cssSelector(".col-l-6.col-m-7 span")));
				logger.info("Checkbox is unclicked not to use credits");
				Assert.assertFalse(locator.creditsUsedText.isDisplayed());
			}

			else {

				logger.info("User wants to use credits available");
			}
			//
			//			// If user wants to select credits
			//			else if(userWantsToDeselectCredits){
			//				Assert.assertNotNull(userWantsToDeselectCredits);
			//				locator.creditCheckbox.click();
			//				waitVar.until(ExpectedConditions.visibilityOfElementLocated(By.id("usedCreditDiv")));
			//				logger.info("Checkbox is selected again to use credits");
			//				Assert.assertTrue(locator.usedCreditsDiv.isDisplayed());
			//			}

		}

		else if(!WaitUtility.isElementPresentByLocator(getWebDriver(), By.cssSelector(".col-l-6 .col-m-8")) && WaitUtility.isElementPresentByLocator(getWebDriver(), By.cssSelector(".card__inner-content .nb-checkbox input"))){

			logger.info("Credits are not used but present");
		}

		return this;
	}

	/**
	 * Verify total price based on credits selection or not
	 * @return
	 */
	public OrderSummaryPage verifyTotalPriceBasedOnCreditsSelection(){

		Assert.assertTrue(locator.orderTotalText.isDisplayed());
		logger.info("Text : " + locator.orderTotalText.getText());

		Assert.assertTrue(locator.finalPrice.isDisplayed());
		logger.info("Final Price : " + locator.finalPrice.getText());

		return this;
	}

	/**
	 * Click on Proceed To Payment Button
	 * @return
	 */
	public OrderSummaryPage enterCVVinSavedCards(String cvvNo){

		Assert.assertTrue(locator.cvvTextBox.isDisplayed());
		Assert.assertTrue(locator.cvvTextBox.isEnabled());
		locator.cvvTextBox.sendKeys(cvvNo);
		return this;
	}
	public OrderSummaryPage clickOnSavedCardPayNowButton(){
		
		Assert.assertTrue(locator.savedCardPayNowButton.isDisplayed());
		Assert.assertTrue(locator.savedCardPayNowButton.isEnabled());
		locator.savedCardPayNowButton.click();
		try {
			Thread.sleep(3000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return this;
	}

	/**
	 * Click on Proceed Button
	 * @return
	 */
	public OrderSummaryPage clickOnProceedButton(){

		locator.proceedButton.click();
		return this;
	}

	/**
	 * Select Payment Mode
	 * @param paymentModeValue
	 * @return
	 */
	public OrderSummaryPage selectModeOfPayment(String paymentModeValue){

		waitVar.until(ExpectedConditions.visibilityOf(locator.paymentGatewaysDiv));
		Assert.assertTrue(locator.paymentGatewaysDiv.isDisplayed(), "Payment gateways Div is not visible");
		for(WebElement option : locator.paymentOptions){
			if(option.getText().contains(paymentModeValue)){
				Assert.assertTrue(option.isDisplayed(), "Payment option is displayed");
				Assert.assertTrue(option.isEnabled(), "Payment option is enabled");
				option.click();
				WaitUtility.waitForAttributeContainsGivenValue(option, "class", "active", getWebDriver(), waitVar);
				logger.info(paymentModeValue + " : payment mode has been selected");
			}
		}
		return this;
	}

	/**
	 * Avail Offers During Payment 
	 * @return
	 */
	public OrderSummaryPage availOffersDuringPayment(){

		System.out.println("NUMBER OF PAYMENT MODES HAVING OFFER " + locator.paymentModesHavingOffers.size());

		if(WaitUtility.isElementPresent(getWebDriver(), locator.currentPaymentModeHaveOffers) || WaitUtility.isElementPresent(getWebDriver(), locator.availOffersHeading)){

			logger.info("Active payment Mode has offers present for users!");
			Assert.assertTrue(locator.availOffersHeading.isDisplayed());
			logger.info("Avail Offer Heading : " + locator.availOffersHeading.getText());
			for(WebElement each : locator.paymentOffersDiv){
				logger.info("Running for :" + each.findElement(By.cssSelector("p.txt-primary")));
				Assert.assertTrue(each.isDisplayed());
				Assert.assertTrue(each.findElement(By.cssSelector("img")).isDisplayed());
				Assert.assertTrue(each.findElement(By.cssSelector("a")).isDisplayed());
				Assert.assertTrue(each.findElement(By.cssSelector("a")).isEnabled());
				logger.info("Image Path : " + each.findElement(By.cssSelector("img")).getText());
				Assert.assertTrue(each.findElement(By.cssSelector("p.txt-primary")).isDisplayed());
				logger.info("Text On Offer Box : " + each.findElement(By.cssSelector("p.txt-primary")).getText());
				each.findElement(By.cssSelector("a")).click();
				waitVar.until(ExpectedConditions.visibilityOf(each.findElement(By.cssSelector(".tooltip-popover-wrapper .tooltip-popover--left"))));
				Assert.assertTrue(each.findElement(By.cssSelector(".tooltip__content li")).isDisplayed());
				logger.info("Text Of points : " + locator.offerDetailsBulletPoints.getText());
				Assert.assertTrue(each.findElement(By.cssSelector(".tooltip__content button")).isDisplayed());
				Assert.assertTrue(each.findElement(By.cssSelector(".tooltip__content button")).isEnabled());
				each.findElement(By.cssSelector(".tooltip__content button")).click();
				waitVar.until(ExpectedConditions.invisibilityOfElementLocated(By.cssSelector(".payment-discount__offer .tooltip-popover.show")));
				logger.info("Offer Info Box has been closed");
			}
		}
		else
			logger.info("No Offers Present at the moment");
		return this;
	}

	/**
	 * Verify failed Transaction Message
	 * @return
	 */
	public OrderSummaryPage verifyFailedTransactionMessage(){

		WaitUtility.waitForURLtoChange(getWebDriver(), waitVar);
		WaitUtility.waitForLoad(getWebDriver());
		waitVar.until(ExpectedConditions.visibilityOf(locator.transactionFailedSection));

		Assert.assertTrue(locator.transactionFailedSection.isDisplayed());
		Assert.assertTrue(locator.transactionFailedTryAgainButton.isDisplayed());

		Assert.assertTrue(locator.transactionFailedHeading.isDisplayed());
		logger.info("Failed Heading : " + locator.transactionFailedHeading.getText());

		Assert.assertTrue(locator.transactionFailedMessage.isDisplayed());
		logger.info("Failed Message : " + locator.transactionFailedMessage.getText());

		Assert.assertTrue(locator.transactionFailedTryAgainButton.isDisplayed());
		Assert.assertTrue(locator.transactionFailedTryAgainButton.isDisplayed());
		locator.transactionFailedTryAgainButton.click();
		waitVar.until(ExpectedConditions.invisibilityOfElementLocated(By.cssSelector(".payment-error-banner .error-btn")));


		return this;
	}

	//	public List<WebElement> getOrderTableHeadings() {
	//		return locator.orderTableHeadings;
	//	}
	//
	//	public List<WebElement> getOrderTableDetails() {
	//		return locator.orderTableDetails;
	//	}
	//
	//	public List<WebElement> getNumberOfRows() {
	//		return locator.numberOfRows;
	//	}

	/**
	 * Price Verifications
	 * @return
	 */
	public OrderSummaryPage priceVerifications(){

		Double expectedFinalPrice ;
		Double creditsUsed;
		Double promoDiscountAmount;
		Double quantity = Double.valueOf((locator.quanityInSubtotalDiv.getText().split(" ")[1].trim()));

		String subTotalAmount = locator.subTotalPrice.getText().trim();
		String finalPriceAmount = locator.finalPrice.getText().trim();

		if(subTotalAmount.contains(",") && finalPriceAmount.contains(",")){
			subTotalAmount = subTotalAmount.replace(",", "");
			finalPriceAmount = finalPriceAmount.replace(",", "");
		}
		Double actualSubTotal = Double.valueOf(subTotalAmount);
		Double actualFinalPrice = Double.valueOf(finalPriceAmount); 

		logger.info("Quantity of the deal  : " + quantity);
		logger.info("Subtotal  : " + actualSubTotal);

		Double expectedSubTotal = 0.0;
		for(WebElement eachPrice : locator.priceListOfOptionsSelectedByUsers){
			String eachNearbuyPrice = eachPrice.getText().trim();
			if(eachNearbuyPrice.contains(",")){
				eachNearbuyPrice = eachNearbuyPrice.replace(",", "");
			}
			if(WaitUtility.isElementPresentByLocator(getWebDriver(), By.cssSelector(".col-m-7 p span"))==false){
			logger.info("After SPlitting space in NB price: " + eachNearbuyPrice.split(" ")[0]);
			expectedSubTotal = expectedSubTotal + Double.valueOf(eachNearbuyPrice.split(" ")[0]);
			}
			else if(WaitUtility.isElementPresent(getWebDriver(), eachPrice.findElement(By.cssSelector("span")))){
				logger.info("After SPlitting space in NB price : " + eachNearbuyPrice.split(" ")[1]);
				expectedSubTotal = expectedSubTotal + Double.valueOf(eachNearbuyPrice.split(" ")[1]);
			}
		}
		Assert.assertEquals(expectedSubTotal,actualSubTotal);


		// If Only Credits
		//		if(locator.applyPromoButton.isDisplayed() && locator.creditsUsedText.isDisplayed()){
		if(locator.promoLink.isDisplayed() && WaitUtility.isElementPresent(getWebDriver(), locator.creditsUsedText)){

			logger.info("Credits are present and used. No Promo applied");
			String creditsUsedAmount = locator.usedCreditsAmount.getText().trim();
			if(creditsUsedAmount.contains(",")){
				creditsUsedAmount = creditsUsedAmount.replace(",", "");
			}
			creditsUsed  = Double.valueOf(creditsUsedAmount);
			expectedFinalPrice = expectedSubTotal - creditsUsed;
			logger.info("Credits user want to use : " + creditsUsed);

			Assert.assertEquals(expectedFinalPrice, actualFinalPrice);
		}

		// If No Promo & No Credits also
		else if(locator.promoLink.isDisplayed() &&  ! WaitUtility.isElementPresent(getWebDriver(), locator.creditsUsedText)){

			logger.info("No Credits. No Promo applied");
			Assert.assertEquals(expectedSubTotal, actualFinalPrice);

		}

		// If both Promo & Credits
		else if(locator.removePromoCodeLink.isDisplayed() && WaitUtility.isElementPresent(getWebDriver(), locator.promoDiscountAmount) && WaitUtility.isElementPresent(getWebDriver(), locator.usedCreditsAmount)){

			logger.info("Promo applied. Credits apllied");
			promoDiscountAmount = Double.valueOf(locator.promoDiscountAmount.getText().trim());
			creditsUsed  = Double.valueOf(locator.usedCreditsAmount.getText().trim());
			expectedFinalPrice = expectedSubTotal - promoDiscountAmount - creditsUsed;

			Assert.assertEquals(expectedFinalPrice, actualFinalPrice);

		}

		// If only Promo
		else if (locator.removePromoCodeLink.isDisplayed() && WaitUtility.isElementPresent(getWebDriver(), locator.promoDiscountAmount)){

			promoDiscountAmount = Double.valueOf(locator.promoDiscountAmount.getText());
			expectedFinalPrice = expectedSubTotal - promoDiscountAmount;

			Assert.assertEquals(expectedFinalPrice, actualFinalPrice);
		}

		return this;
	}

	/**
	 * Verify Delivery Address in Product's Order Summary Page
	 * @return
	 */
	public OrderSummaryPage deliveryAddress(String pincode, String locality, String addressLine1, String addressLine2 ){

		Assert.assertTrue(locator.deliveryAddressHeading.isDisplayed());
		Assert.assertTrue(locator.oldDeliveryAddressCheckbox.isDisplayed());
		Assert.assertTrue(locator.oldDeliveryAddressCheckbox.getAttribute("checked").toString().equals("checked"));
		Assert.assertTrue(locator.oldDeliveryAddressText.isDisplayed());
		logger.info("Delivery Address Text : " + locator.oldDeliveryAddressText.getText());

		locator.addNewAddressLink.click();

		waitVar.until(ExpectedConditions.visibilityOf(locator.paymentAddressSection));

		Assert.assertTrue(locator.pincodeTextbox.isEnabled());
		locator.pincodeTextbox.sendKeys(pincode);

		Assert.assertTrue(locator.cityTextbox.getAttribute("readonly").toString().equals("readonly"));

		Select localitySelectList = new Select(locator.locationSelectionbox);
		localitySelectList.selectByValue(locality);

		locator.addressLine1Textbox.sendKeys(addressLine1);
		locator.addressLine1Textbox.sendKeys(addressLine2);

		return this;
	}

	/**
	 * Verify Data Layer
	 * @return
	 */
	public OrderSummaryPage verifyDataLayerOrderSummary() {
		//prepare map by extracting values from html
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("customerId", locator.dlCustomerId.getAttribute("userid"));
		map.put("dealId", locator.offersDescription.getAttribute("dealid"));
		map.put("dealTitle", locator.dlDealTitle.getText());
		map.put("dealType", locator.dlDealType.getAttribute("value"));
		map.put("numberOfVouchers", locator.dlNumberOfVouchers.getAttribute("value"));
		map.put("offerPrice", locator.dlOfferPrice.getAttribute("value"));
		map.put("totalPrice", locator.dlTotalPrice.getAttribute("value"));
		map.put("pageType", "Payment");
		map.put("sitetype", "d");

		verifyDataLayer(map,"3");
		return this;
	}

	/**
	 * Get Title of Order Summary Page
	 * @param currentCity 
	 * @return
	 */
	public OrderSummaryPage getTitleOfOrderSummaryPage(String currentCity){
		waitVar.until(ExpectedConditions.elementToBeClickable(locator.proceedButton));
		String currentTitle = getWebDriver().getTitle();
		Assert.assertEquals("Restaurants, Spa, Salon, Gym, Movies, Activities, Travel offers in " + currentCity + " | nearbuy", currentTitle);
		return this;
	}

	public OrderSummaryPage waitForOrderSummaryToLoad(){

		WaitUtility.waitForURLtoChange(getWebDriver(), waitVar);
		return this;
	}

	public OrderSummaryPage verifyTravelOrderSummaryDetails(){

		waitVar.until(ExpectedConditions.visibilityOf(locator.travelCardImage));
		for(WebElement each : locator.travelCheckInCheckOutDates){
			Assert.assertTrue(each.isDisplayed());
		}
		
		logger.info("Check-in Date : " + locator.travelCheckInCheckOutDates.get(0).getText());
		logger.info("Check-out Date : " + locator.travelCheckInCheckOutDates.get(1).getText());
		Assert.assertTrue(locator.travelOptionTitle.isDisplayed());
		Assert.assertTrue(locator.travelOptionNearbuyPrice.isDisplayed());
		Assert.assertTrue(locator.travelOptionTitle.isDisplayed());
		Assert.assertTrue(locator.travelOptionNumberOfRoomsAndNights.isDisplayed());
		Assert.assertTrue(locator.travelOptionPriceAfterRoomsAndNightsCalculation.isDisplayed());
		Assert.assertTrue(locator.payableNowLabelText.isDisplayed());
		Assert.assertTrue(locator.totalPackageCostLabelTextAndInclOfTaxesLabelText.get(0).getText().equals("Total package cost"));
		Assert.assertTrue(locator.totalPackageCostLabelTextAndInclOfTaxesLabelText.get(1).getText().equals("Inclusive of all taxes"));
		Assert.assertTrue(locator.totalPackageCost.isDisplayed());
		logger.info("Travel Option Title : " + locator.travelOptionTitle.getText());
		logger.info("Travel Option Nearbuy price : " + locator.travelOptionNearbuyPrice.getText());
		logger.info("Travel Option Number Of Rooms & Nights : " + locator.travelOptionNumberOfRoomsAndNights.getText());
		logger.info("Travel Option Price after calculation Of Rooms & Nights : " + locator.travelOptionPriceAfterRoomsAndNightsCalculation.getText());
		logger.info("Travel Package Cost : " + locator.totalPackageCost.getText());
		
		if(WaitUtility.isElementPresent(getWebDriver(), locator.travelOptionMrpPrice)){
			Assert.assertTrue(locator.travelOptionMrpPrice.isDisplayed());
			logger.info("Travel Option MRP Price : " + locator.travelOptionMrpPrice.getText());
			Assert.assertTrue(locator.travelYourTotalSavingsText.isDisplayed());
			logger.info("Travel Option Savings : " + locator.travelYourTotalSavingsText.getText());
		}
		

		return this;
	}

	public OrderSummaryPage travelPriceVerifications(){

		return this;
	}


}
