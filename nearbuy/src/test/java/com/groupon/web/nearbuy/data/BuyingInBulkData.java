package com.groupon.web.nearbuy.data;

import org.testng.annotations.DataProvider;

/**
 * 
 * @author prachi.nagpal
 *
 */
public class BuyingInBulkData {

	@DataProvider(name = "enterDetailsInForm")
	public static Object[][] provideDetailsInForm(){
		return new Object[][] {

				{"Test","test@nearbuy.com","50","Nearbuy","9999999999"},
		
		};
	}
	
	@DataProvider(name = "buttonToBeClickedOnThankYouPage")
	public static Object[][] provideButtonToBeClicked(){
		return new Object[][] {
			
			{"Continue Shopping"},
			{"Back to Deal"},
			
		};
	}
}
