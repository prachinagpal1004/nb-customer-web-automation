package com.groupon.web.nearbuy.test;
//package com.groupon.web.nearbuy.test;
//
//import com.groupon.web.common.test.BaseTest;
//import com.groupon.web.common.utils.TestListener;
//import com.groupon.web.nearbuy.data.AuthenticationData;
//import com.groupon.web.nearbuy.data.SearchData;
//import com.groupon.web.nearbuy.page.AuthenticationPage;
//import com.groupon.web.nearbuy.page.HeaderFooterPage;
//import com.groupon.web.nearbuy.page.HomePage;
//import com.relevantcodes.extentreports.LogStatus;
//
//import org.testng.annotations.BeforeMethod;
//import org.testng.annotations.Test;
//
//public class MigrateToNearbuy extends BaseTest {
//
//	private HomePage homePage;
//	private AuthenticationPage login;
//
//    @BeforeMethod(alwaysRun=true)
//    public void init() {
//		homePage = new HomePage();
//		login = new AuthenticationPage();
//	}
//
//	@Test(priority = 1)
//	public void testMigrateBoxOnHomePage(){
//		TestListener.setLog(LogStatus.INFO, "Verify Migrate Pop up on Home page");
//		homePage.migratePopUp();
//	}
//	
//	@Test(priority = 2)
//	public void testMigrateOnLoginScreen(){
//		TestListener.setLog(LogStatus.INFO, "Click on Login Link");
//		login.clickOnLoginLink();
//	}
//	
//	@Test(priority = 3)
//	public void testClickOnMigrateButton(){
//		TestListener.setLog(LogStatus.INFO, "Click & Veirfy Initaite Migration Button");
//		login.clickOnInitiateMigrationButtonAndVerify();
//	}
//	
//	@Test(priority = 4)
//	public void testInitiateMigrateScreen(){
//		TestListener.setLog(LogStatus.INFO, "Verify Migrate Screen");
//		login.migrateToGrouponScreen();
//	}
//}
//
