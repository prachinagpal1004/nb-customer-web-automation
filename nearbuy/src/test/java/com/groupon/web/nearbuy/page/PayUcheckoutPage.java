package com.groupon.web.nearbuy.page;

import com.groupon.web.common.page.BasePage;
import com.groupon.web.common.utils.WaitUtility;
import com.groupon.web.nearbuy.locator.PayULocators;
import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.testng.Assert;

public class PayUcheckoutPage extends BasePage {

	private static final Logger logger = Logger.getLogger(PayUcheckoutPage.class);
	public PayULocators locator;

	public PayUcheckoutPage() {
		locator = PageFactory.initElements(getWebDriver(), PayULocators.class);
	}

	/**
	 * Verify Amount
	 */
	public PayUcheckoutPage verifyPayUAmountAndTransaction(){

		waitVar.until(ExpectedConditions.urlContains("payu.in"));
		//waitVar.until(ExpectedConditions.urlContains("payu.in/response"));
		//		waitVar.until(ExpectedConditions.visibilityOf(locator.payUlogo));
		//		Assert.assertTrue(locator.payUlogo.isDisplayed());
		//		Assert.assertTrue(locator.totalAmount.isDisplayed());
		//		logger.info("Total Amount : " + locator.totalAmount.getText());
		//
		//		Assert.assertTrue(locator.transactionIdText.isDisplayed());
		//		Assert.assertTrue(locator.transactionIdNumber.isDisplayed());
		//		logger.info("Transaction Text : " + locator.transactionIdText.getText());
		//		logger.info("Transaction ID : " + locator.transactionIdNumber.getText());

		return this;
	}

	/**
	 * Enter Credit Card Details
	 */
	public PayUcheckoutPage enterCreditCardCredentialsAndClickOnPayNow(String cardNo, String name, String cvv, String month, String year){

		waitVar.until(ExpectedConditions.elementToBeClickable(locator.debitCardPayNowButton));
		//		Assert.assertTrue(locator.ccCheckBox.isDisplayed());
		//
		//		Assert.assertTrue(locator.ccNo.isEnabled());
		//		String[] parts = cardNo.split(" ");
		//		for (String part : parts) {
		//			locator.ccNo.sendKeys(part);
		//		}
		//
		//		try {
		//			Thread.sleep(2000);
		//		} catch (InterruptedException e) {
		//			// TODO Auto-generated catch block
		//			e.printStackTrace();
		//		}
		//		//waitVar.until(ExpectedConditions.presenceOfElementLocated(By.xpath("#ccard_number.identified.valid")));
		//
		//		locator.ccNo.sendKeys(Keys.TAB);
		//
		//		Assert.assertTrue(locator.nameOnCard.isEnabled());
		//		locator.nameOnCard.sendKeys(name);
		//		
		//		//	waitVar.until(ExpectedConditions.presenceOfElementLocated(By.xpath("#cname_on_card.valid")));
		//
		//		locator.ccNo.sendKeys(Keys.TAB);

		Assert.assertTrue(locator.cvvNo.isEnabled());
		locator.cvvNo.sendKeys(cvv);
		//	locator.ccNo.sendKeys(Keys.TAB);
		//waitVar.until(ExpectedConditions.presenceOfElementLocated(By.cssSelector("#ccvv_number.valid")));

		locator.cvvNo.sendKeys(Keys.TAB);
		//
		//		Assert.assertTrue(locator.expiryMonth.isEnabled());
		//		//locator.expiryMonth.click();
		//		Select monthValue = new Select(locator.expiryMonth);
		//		monthValue.selectByValue(month);
		//	
		//
		//		locator.ccNo.sendKeys(Keys.TAB);
		//
		//		Assert.assertTrue(locator.expiryYear.isEnabled());
		//		Select yearValue = new Select(locator.expiryYear);
		//		//locator.expiryYear.click();
		//		yearValue.selectByValue(year);
		//		

		locator.debitCardPayNowButton.submit();

		//		waitVar.until(ExpectedConditions.urlContains("payu.in/paytxn"));
		//		
		//		waitVar.until(ExpectedConditions.urlContains("/payment-vendor/success"));

		return this;
	}

	/**
	 * Click on Nearbuy Url & go back to website
	 */
	public PayUcheckoutPage goBackToNearbuy(){

		waitVar.until(ExpectedConditions.elementToBeClickable(locator.nearbuyUrl));
		//		Assert.assertTrue(locator.goBackToText.isDisplayed());
		//		logger.info("Go back text : " + locator.goBackToText.getText());

		Assert.assertTrue(locator.nearbuyUrl.isDisplayed());
		Assert.assertTrue(locator.nearbuyUrl.isEnabled());
		logger.info("Nearbuy url text : " + locator.nearbuyUrl.getText());

		locator.nearbuyUrl.click();
		waitVar.until(ExpectedConditions.urlContains("order/placeorder"));

		return this;
	}

	/**
	 * NetBanking
	 * @return
	 */
	public PayUcheckoutPage clickOnNetbankingAndGoToBankWebsite(){

		clickOnPaymentTab("netbanking");
		waitVar.until(ExpectedConditions.elementToBeClickable(locator.netbankingPayNowButton));
		Assert.assertTrue(locator.netbankingPayNowButton.isEnabled());

		Select drop = new Select(locator.netbankingDropDown);
		drop.selectByValue("BOIB");
		locator.netbankingPayNowButton.submit();
		waitVar.until(ExpectedConditions.urlContains("payu.in"));
		Assert.assertTrue(getWebDriver().getCurrentUrl().contains("payu.in"));
		//		waitVar.until(ExpectedConditions.urlContains("bankofindia"));

		return this;
	}

	public PayUcheckoutPage clickOnPayUtabAndGoToPayUwebsite(String mobileno){

		clickOnPaymentTab("wallet");
		waitVar.until(ExpectedConditions.elementToBeClickable(locator.payUmoneyButton));
		Assert.assertTrue(locator.walletEmailIdTextbox.getAttribute("class").equals("valid"));
		logger.info("Email Id already written on wallet textbox: " + locator.walletEmailIdTextbox.getAttribute("value"));
		locator.walletMobileTextbox.sendKeys(mobileno);
		locator.payUmoneyButton.click();
		waitVar.until(ExpectedConditions.urlContains("payumoney"));
		return this;

	}

	public PayUcheckoutPage clickOnPaymentTab(String tabname){

		for(WebElement tab : locator.paymentTabs){

			waitVar.until(ExpectedConditions.elementToBeClickable(locator.debitCardPayNowButton));
			if(tab.getAttribute("href").contains(tabname)){
				tab.click();
				waitVar.until(ExpectedConditions.presenceOfElementLocated(By.xpath(".//*[@id='TriggerBlock']/li[contains(@class,'ui-state-active')]")));
				break;
			}


		}

		return this;
	}

	public PayUcheckoutPage enterDebitCardDetails(String dbCardNo, String name, String cvv, String date){

		waitVar.until(ExpectedConditions.elementToBeClickable(locator.debitCardNumber));
		//String[] parts = dbCardNo.split(" ");
		//for (String part : parts) {
			locator.debitCardNumber.sendKeys(dbCardNo);
		//}
		locator.debitCardName.sendKeys(name);
//		String[] parts2 = date.split("/");
//		for (String part : parts2) {
			locator.debitCardExpiryDate.sendKeys(date);
	//	}
		locator.debitCardCVV.sendKeys(cvv);
		Assert.assertTrue(locator.debitCardPayNowButton.isEnabled());
		locator.debitCardPayNowButton.click();
		try {
			Thread.sleep(2000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	//	waitVar.until(ExpectedConditions.visibilityOf(locator.waitLoader));

		return this;
	}
	public PayUcheckoutPage enterCreditCardDetailsAndClickOnPayNowButton(String cCardNo, String name, String cvv, String date){

		waitVar.until(ExpectedConditions.elementToBeClickable(locator.creditCardNumber));
		locator.creditCardNumber.sendKeys(cCardNo);
		locator.creditCardName.sendKeys(name);
		locator.creditCardExpiryDate.sendKeys(date);
		locator.creditCardCVV.sendKeys(cvv);
		Assert.assertTrue(locator.creditCardPayNowButton.isEnabled());
		locator.creditCardPayNowButton.click();
	//	waitVar.until(ExpectedConditions.visibilityOf(locator.waitLoader));
		//waitVar.until(ExpectedConditions.invisibilityOfElementLocated(By.cssSelector(".payment-proceed.loaders__block-level")));

		return this;
	}

	public PayUcheckoutPage clickAndVerifyPopularBanksInDropDown(String popularBankToBeClicked){
		Select banksDropDown = new Select(locator.allBanksDropDown);

		for(WebElement bank : locator.netBankingDiv){
			WebElement eachBankRadioButton = bank.findElement(By.cssSelector("input"));
			WebElement bankImage = bank.findElement(By.cssSelector("img"));
		//	Assert.assertTrue(eachBankRadioButton.isDisplayed());
			Assert.assertTrue(eachBankRadioButton.isEnabled());
			if(eachBankRadioButton.isEnabled() && eachBankRadioButton.getAttribute("class").contains("ng-untouched ng-pristine")){
				if(bankImage.getAttribute("alt").equals(popularBankToBeClicked)){
				eachBankRadioButton.click();
//				try {
//					Thread.sleep(1000);
//				} catch (InterruptedException e) {
//					// TODO Auto-generated catch block
//					e.printStackTrace();
//				}
				//Assert.assertTrue(eachBankRadioButton.getAttribute("class").contains("ng-valid ng-dirty ng-touched"));
				Assert.assertFalse(banksDropDown.getFirstSelectedOption().getText().equals("Select Bank"));
				Assert.assertTrue(banksDropDown.getFirstSelectedOption().isDisplayed());
				break;
				}
			}
		}
		return this;
	}

	public PayUcheckoutPage clickBankDropDownSelectBankAndVerify(){
		Select banksDropDown = new Select(locator.allBanksDropDown);

		banksDropDown.selectByValue("UCOB");
		Assert.assertTrue(banksDropDown.getFirstSelectedOption().isDisplayed());
		locator.netbankingPayNowButton.click();
		WaitUtility.waitForURLtoChange(getWebDriver(), waitVar);
		try {
			Thread.sleep(3000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		Assert.assertFalse(getCurrentUrl().contains("nearbuy"));
		return this;
	}
}
