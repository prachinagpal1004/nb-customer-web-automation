	package com.groupon.web.nearbuy.test;

import com.groupon.web.common.test.BaseTest;
import com.groupon.web.common.utils.TestListener;
import com.groupon.web.nearbuy.data.MenuData;
import com.groupon.web.nearbuy.page.DealCatalogPage;
import com.groupon.web.nearbuy.page.HeaderFooterPage;
import com.relevantcodes.extentreports.LogStatus;

import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
/**
 * 
 * @author Prachi Nagpal
 *
 */
@Test(groups = {"sanity", "listYourBusiness"})
public class ListYourBusiness extends BaseTest {

	private HeaderFooterPage headerFooterPage;

	@BeforeMethod(alwaysRun=true)
	public void init() {
		headerFooterPage = new HeaderFooterPage();
	}

	@Test(priority = 1)
	public void testListYourBusiness() {
		TestListener.setLog(LogStatus.INFO, "Test List Your Business Form");
		headerFooterPage.listYourBusiness();
	}
}
