package com.groupon.web.nearbuy.test;

import com.groupon.web.common.test.BaseTest;
import com.groupon.web.common.utils.TestListener;
import com.groupon.web.nearbuy.data.SearchData;
import com.groupon.web.nearbuy.page.DealCatalogPage;
import com.groupon.web.nearbuy.page.HeaderFooterPage;
import com.groupon.web.nearbuy.page.HomePage;
import com.relevantcodes.extentreports.LogStatus;

import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
/**
 * 
 * @author Prachi Nagpal
 *
 */
@Test(groups = {"sanity", "staging"})
public class SearchTest extends BaseTest {

	private HeaderFooterPage headerFooterPage;
	private DealCatalogPage catalog;
	private HomePage homePage;

    @BeforeMethod(alwaysRun=true)
    public void init() {
		headerFooterPage = new HeaderFooterPage();
		homePage = new HomePage();
		catalog = new DealCatalogPage();
	}
    
//	@Test(priority = 1)
//	public void testHomePage() throws Throwable {
//		homePage.selectCityFromLocationPopUp("Delhi/NCR");
//		
//	}

	@Test(priority = 2 , dataProvider="enterSearchKeywrod" , dataProviderClass = SearchData.class )
	public void testLocalSearchResultPage(String searchKeyword, Boolean value) throws Throwable {
		TestListener.setLog(LogStatus.INFO, "Enter Search Keyword");
		headerFooterPage.searchBox(searchKeyword, value ).verifyLocalResultPage();
		//.verifySearchPageDataLayer();
		
	}
	@Test(priority = 3 , dataProvider="searchMore" , dataProviderClass = SearchData.class )
	public void testLocalSearchCategoryResultPage(String searchKeyword, Boolean value) throws Throwable {
		TestListener.setLog(LogStatus.INFO, "Enter Search Keyword");
		headerFooterPage.searchBox(searchKeyword, value).verifyAutocomplete("2");
		//catalog.verifyDataLayerCatalog();
		
	}
	@Test(priority = 4 , dataProvider="enterNoResultSearchKeywrod" , dataProviderClass = SearchData.class )
	public void testLocalNoSearchResultPage(String searchKeyword, Boolean value) throws Throwable {
		TestListener.setLog(LogStatus.INFO, "Enter Search Keyword");
		headerFooterPage.searchBox(searchKeyword, value).verifyAutocomplete("2");
		
	}

}

