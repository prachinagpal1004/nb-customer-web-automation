	package com.groupon.web.nearbuy.test;

import com.groupon.web.common.test.BaseTest;
import com.groupon.web.common.utils.TestListener;
import com.groupon.web.nearbuy.data.MenuData;
import com.groupon.web.nearbuy.page.DealCatalogPage;
import com.groupon.web.nearbuy.page.HeaderFooterPage;
import com.groupon.web.nearbuy.page.HomePage;
import com.relevantcodes.extentreports.LogStatus;

import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
/**
 * 
 * @author Prachi Nagpal
 *
 */
@Test(groups = {"sanity", "staging","storefrontLocalAndTravel"})
public class StorefrontLocalAndTravelTest extends BaseTest {

	private HomePage home;
	private HeaderFooterPage menu;

	@BeforeMethod(alwaysRun=true)
	public void init() {
		home = new HomePage();
	}

	/**
	 * Navigate to particular Menu
	 * @param menu
	 * @param submenu
	 * @throws Throwable
	 */
	@Test(priority = 1)
	public void testLocalStorefront() throws Throwable {
		TestListener.setLog(LogStatus.INFO, "Test Local Storefront");
		home.verifyLocalStorefront();
	}
	
	@Test(priority = 2)
	public void testTravelStorefront() throws Throwable {
		TestListener.setLog(LogStatus.INFO, "Test Travel Storefront");
		menu.navigateToMenu("Hotels", "", "");
		home.verifyTravelStorefront();
	}

}
