package com.groupon.web.nearbuy.page;

import com.groupon.web.common.base.DriverFactory;
import com.groupon.web.common.page.BasePage;
import com.groupon.web.common.utils.PageUtils;
import com.groupon.web.common.utils.WaitUtility;
import com.groupon.web.nearbuy.locator.HeaderFooterLocator;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.By.ByCssSelector;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Action;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;

public class HeaderFooterPage extends BasePage {

	public HeaderFooterLocator locator;
	private String currentLocationSelected;
	private static final Logger logger = Logger.getLogger(HeaderFooterPage.class);

	public HeaderFooterPage() {
		locator = PageFactory.initElements(getWebDriver(), HeaderFooterLocator.class);
	}

	public String getCurrentLocationSelected() {
		currentLocationSelected= locator.citySelected.getText();
		return currentLocationSelected;
	}

	/**
	 * Click on Sub Menu
	 * @param menu
	 * @param submenu
	 */
	//	public HeaderFooterPage navigateToMenu(String menu, String submenu) {
	//
	//		waitVar.until(ExpectedConditions.elementToBeClickable(By.cssSelector(".main_menu")));
	//		WebElement mainMenuElement = getWebDriver().findElement(By.xpath(".//div[contains(@class,'local-sub-category')]//a[text()='" + menu + "']"));
	//
	//		if(menu.equals(submenu)){
	//			System.out.println("User wants to click on main menu");
	//			mainMenuElement.click();
	//			logger.info("User clicked on Main Menu : " + menu);
	//			WaitUtility.waitForURLtoChange(getWebDriver(), waitVar);
	//			WaitUtility.waitForLoad(getWebDriver());
	//			try {
	//				Thread.sleep(2000);
	//			} catch (InterruptedException e) {
	//				e.printStackTrace();
	//			}
	//			new Actions(getWebDriver()).moveToElement(getWebDriver().findElement(By.cssSelector(".wrapper"))).build().perform();
	//			if(WaitUtility.isAlertPresent(getWebDriver())){
	//				System.out.println(getWebDriver().switchTo().alert().getText());
	//				getWebDriver().switchTo().alert().accept();
	//			}
	//			waitVar.until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector(".city-name h1")));
	//		}
	//		else {
	//			System.out.println("User wants to click on submenu");
	//			new Actions(getWebDriver()).moveToElement(mainMenuElement).build().perform();
	//			By submenuCssSelector = By.cssSelector(".sub-category-head a[href$='" + submenu + "']");
	//			waitVar.until(ExpectedConditions.elementToBeClickable(submenuCssSelector));
	//			Assert.assertNotNull(submenuCssSelector);
	//			getWebDriver().findElement(submenuCssSelector).click();
	//			logger.info("User clicked on Sub Menu : " + submenu);
	//			WaitUtility.waitForURLtoChange(getWebDriver(), waitVar);
	//			WaitUtility.waitForLoad(getWebDriver());
	//
	//			waitVar.until(ExpectedConditions.visibilityOfAllElementsLocatedBy(By.cssSelector(".city-name h1")));
	//			//waitVar.until(ExpectedConditions.invisibilityOfElementLocated(By.xpath("//a[text()='"+submenu+"']")));
	//			System.out.println("Clicked on sub menu");
	//			WaitUtility.waitForLoad(getWebDriver());
	//		}
	//		return this;
	//	}

	/**
	 * Click on Menu
	 * @param mainMenu
	 * @param category
	 * @param subCategory
	 */
	public HeaderFooterPage navigateToMenu(String mainMenu, String category, String subCategory) {

		String mainMenuXpath = ".//a[contains(@class,'main_menu')][contains(text(),'"+mainMenu+"')]";
		waitVar.until(ExpectedConditions.elementToBeClickable(By.cssSelector(".main_menu")));
		WebElement mainMenuElement = getWebDriver().findElement(By.xpath(mainMenuXpath));

		if(StringUtils.isNotBlank(category)) {
			try {
				Thread.sleep(2000);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}

			logger.info("User clicking on Category : " + category);
			WebElement menu = getWebDriver().findElement(By.xpath(mainMenuXpath+"/.."));
			waitVar.until(ExpectedConditions.elementToBeClickable(menu));
			new Actions(getWebDriver()).moveToElement(menu).build().perform();

			WebElement catMenu = menu;
			System.out.println(catMenu);
			WaitUtility.waitForAttributeContainsGivenValue(catMenu, "class", "header-hover", getWebDriver(), waitVar);
			waitVar.until(ExpectedConditions.visibilityOf(catMenu.findElement(By.cssSelector("div.sub_menu"))));
			String catXpath;
			if("Around You".equalsIgnoreCase(mainMenu)){
				catXpath = ".//p[contains(@class,'sub-category-head')]/a[contains(text(),'"+category+"')]";
			}
			else {
				catXpath = ".//div[contains(@class,'travel-sub-category')]//a[contains(text(),'"+category+"')]";
			}

			if(StringUtils.isNotBlank(subCategory)) {
				catXpath += "/../..//a[contains(text(),'"+subCategory+"')]";
			}
			System.out.println(catXpath);
			waitVar.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(catXpath)));
			catMenu.findElement(By.xpath(catXpath)).click();
			WaitUtility.waitForURLtoChange(getWebDriver(), waitVar);
			WaitUtility.waitForLoad(getWebDriver());
			try {
				Thread.sleep(1000);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
			waitVar.until(ExpectedConditions.visibilityOfAllElementsLocatedBy(By.cssSelector("div.city-name span")));
			waitVar.until(ExpectedConditions.visibilityOfAllElementsLocatedBy(By.xpath(".//div[contains(@class,'wrapper-content')]/*[1]")));
			//			if(subCategory == null){
			//			waitVar.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(".//div[contains(@class,'city-name')]//span[text()='"+category+"']")));
			//		}
			//			else{
			//				waitVar.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(".//div[contains(@class,'city-name')]//span[text()='"+subCategory+"']")));
			//			}
		}

		else if(StringUtils.isBlank(category) && StringUtils.isBlank(subCategory)){

			mainMenuElement.click();

		}
		return this;
	}

	public HeaderFooterPage clickOnSubSubCategory(String subCat, String subCat2) {
		System.out.println("Sub Sub Category:"+subCat2);
		WaitUtility.waitForAttributeContainsGivenValue(By.xpath(".//span[@class='ellipsis'][text()='"+subCat+"']/../.."), "class", "open", getWebDriver(), waitVar);
		System.out.println("Found Sub Category Menu Listing with class open");
		waitVar.until(ExpectedConditions.visibilityOfAllElementsLocatedBy(By.cssSelector("ul.sub-menu-options.active>li")));
		System.out.println("Found Sub Category Menu Options with class active");
		WebElement subSubCategoryLink = getWebDriver().findElement(By.xpath(".//ul[contains(@class,'sub-menu-options')][contains(@class,'active')]//span[text()='"+subCat2+" ']"));
		System.out.println("Found Sub Sub Category Menu:"+subSubCategoryLink);
		waitVar.until(ExpectedConditions.elementToBeClickable(subSubCategoryLink));
		subSubCategoryLink.click();
		WaitUtility.waitForURLtoChange(getWebDriver(), waitVar);
		WaitUtility.waitForLoad(getWebDriver());
		waitVar.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(".//div[contains(@class,'city-name')]//span[text()='"+subCat2+"']")));
		return this;
	}

	/**
	 * Test Footer Section
	 * @return
	 */
	public HeaderFooterPage footer(){

		waitVar.until(ExpectedConditions.visibilityOfAllElements(locator.footerAllLinks));
		Assert.assertTrue(locator.footerSection.isDisplayed());
		Assert.assertTrue(locator.footerParagraphs.isDisplayed());
		Assert.assertTrue(locator.followUsHeading.isDisplayed());
		logger.info("Blog Section Heading : " + locator.followUsHeading.getText());

		for(WebElement heading : locator.companyAndHelpHeading){
			Assert.assertTrue(heading.isDisplayed());
			logger.info("Links Section Heading : " + heading.getText());
		}

		for(WebElement link : locator.footerAllLinks){

			Assert.assertTrue(link.isDisplayed());
			Assert.assertTrue(link.isEnabled());
			Assert.assertNotNull(link.getAttribute("href"));
			logger.info("Link : " + link.getAttribute("href"));
			logger.info("Link : " + link.getText());

		}
		return this;
	}

	/**
	 * Send Search Key to search Box
	 * @param searchKey
	 * @return
	 */
	public HeaderFooterPage searchBox(String searchKey, Boolean searchKeyCorrect){

		if(BasePage.platform.equalsIgnoreCase(DriverFactory.MOBILE)){

			waitVar.until(ExpectedConditions.elementToBeClickable(locator.mobileSearchIcon));
			locator.mobileSearchIcon.click();

		}

		waitVar.until(ExpectedConditions.visibilityOf(locator.searchBox));

		Assert.assertTrue(locator.searchBox.isDisplayed());
		Assert.assertTrue(locator.searchBox.isEnabled());
		//Assert.assertFalse(locator.autoSuggestionBox.isDisplayed());

		if(locator.searchBox.findElement(By.cssSelector("p")).getText().isEmpty()==false){

			Assert.assertTrue(locator.searchBox.findElement(By.cssSelector("p")).getText().contains("Search restaurants, spa, events"));
			locator.searchBox.click();
			waitVar.until(ExpectedConditions.visibilityOf(locator.searchPage));
			Assert.assertTrue(locator.largeSearchBoxInSearchPage.isDisplayed());
			Assert.assertTrue(locator.largeSearchBoxInSearchPage.isEnabled());
			locator.largeSearchBoxInSearchPage.clear();
			locator.largeSearchBoxInSearchPage.sendKeys(searchKey);
			waitVar.until(ExpectedConditions.invisibilityOfElementLocated(By.cssSelector(".loader")));
			logger.info("Search Key has been send");
			if(searchKeyCorrect){
				logger.info("Correct Search Key has been send");
				waitVar.until(ExpectedConditions.visibilityOf(locator.autoSuggestionBoxPresence));
				waitVar.until(ExpectedConditions.visibilityOf(locator.autoSuggestionBox));
				Assert.assertTrue("AutoSuggestion Box is not opened", locator.autoSuggestionBox.isDisplayed());
			}
			else if(!searchKeyCorrect){
				logger.info("Incorrect Search Key has been send");
				Assert.assertFalse("AutoSuggestion Box is opened", locator.autoSuggestionBox.isDisplayed());
				
			}
		}



		//		else if(locator.searchBox.getAttribute("autocomplete").equals("off")){
		//
		//			locator.searchBox.clear();
		//			locator.searchBox.sendKeys(searchKey);
		//			locator.searchBox.submit();
		//			waitVar.until(ExpectedConditions.visibilityOf(locator.breadcrumbs));

		//		}


		return this;
	}



	public HeaderFooterPage verifyLocalResultPage(){

		Assert.assertTrue("Search button is NOT displayed", locator.searchButton.isDisplayed());
		Assert.assertTrue("Search button is NOT enabled", locator.searchButton.isEnabled());
		locator.searchButton.click();
		waitVar.until(ExpectedConditions.visibilityOf(locator.resultTitle));
		Assert.assertTrue("Result Title not coming", locator.resultTitle.getText().contains("Results for "));
		Assert.assertTrue("Results Not coming after Searching", locator.searchResultPageRightSide.isDisplayed());
		Assert.assertTrue("Filters Not coming after Searching", locator.searchResultPageLeftSide.isDisplayed());

		return this;
	}

	/**
	 * Test AutoSuggestion Box & Select from the List
	 * @param selectSuggestion
	 * @return
	 */
	public HeaderFooterPage verifyAutocomplete(String selectSuggestion){

		try {
			Thread.sleep(1000);
		} catch (InterruptedException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}

		if(locator.autoSuggestionBox.isDisplayed() && locator.autoSuggestionBoxPresence.isDisplayed()){

			waitVar.until(ExpectedConditions.visibilityOf(locator.autoSuggestionBox));
			//Assert.assertTrue(locator.searchBox.getAttribute("class").contains("ui-autocomplete-loading"));
			Assert.assertTrue(locator.autoSuggestionBox.isDisplayed());
			logger.info("AutoSuggestion box is present");
			logger.info("No. of Suggestions : " + locator.autoSuggestionList.size());

			for(WebElement list : locator.autoSuggestionList){

				logger.info("Suggestion : "+ list.getText());
			}

			String xpath = ".//*[@class='searchbox__suggestion']/li/a/h6";
			List<WebElement> selectedSuggestion = getWebDriver().findElements(By.xpath(xpath));
			String textUserSelectedFromList = selectedSuggestion.get(2).getText();
			logger.info("From all the autosuggestions , User Selected : " + textUserSelectedFromList);
			String xpath2 = ".//*[@class='searchbox__suggestion']/li/a/p";
			List<WebElement> autoSuggestionSource = getWebDriver().findElements(By.xpath(xpath2));
			String autoSuggestionSourceText = autoSuggestionSource.get(2).getText();
			selectedSuggestion.get(2).click();
			if(autoSuggestionSourceText.equals("category")){
				waitVar.until(ExpectedConditions.visibilityOf(locator.searchResultPageRightSide));
			}
			else if(autoSuggestionSourceText.equals("outlet")){
				waitVar.until(ExpectedConditions.elementToBeClickable(locator.bookNowButton));
			}

//			String valueInSearchBox =	locator.searchBox.getAttribute("value");
//			logger.info("Value in the search box after selection is : " + valueInSearchBox);
//			Assert.assertTrue(textUserSelectedFromList.contains(valueInSearchBox));

			//			logger.info("Title displayed on the heading : " + locator.categoryName.getText());
			//			Assert.assertTrue(locator.categoryName.getText().contains(textUserSelectedFromList));
			//Assert.assertTrue(getWebDriver().getTitle().contains(textUserSelectedFromList));

			//verifySearchPageDataLayer();

			try {
				Thread.sleep(2000);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}


		} 

		else if((locator.autoSuggestionBox.isDisplayed())==false && WaitUtility.isElementPresent(getWebDriver(), locator.autoSuggestionBoxPresence)==false){

			//waitVar.until(ExpectedConditions.visibilityOf(locator.localNoResultHeading));
			//logger.info("Result Title : " + locator.resultTitle.getText());
			//logger.info("Category Name : " + locator.categoryName.getText());
			//logger.info("Value in search box : " + locator.searchBox.getAttribute("value"));
			//	Assert.assertTrue(locator.categoryName.getText().contains(locator.searchBox.getAttribute("value")));

			//	waitVar.until(ExpectedConditions.visibilityOf(locator.localNoResultPage));
			try {
				Thread.sleep(2000);
			} catch (InterruptedException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			locator.searchButton.click();
			logger.info("Running for no search results page");
			waitVar.until(ExpectedConditions.visibilityOf(locator.localNoResultImageIcon));
			waitVar.until(ExpectedConditions.visibilityOf(locator.localNoResultHeading));
			Assert.assertTrue(locator.localNoResultImageIcon.isDisplayed());
			Assert.assertTrue(locator.localNoResultHeading.isDisplayed());
			Assert.assertTrue(locator.localNoResultSubTitle.isDisplayed());

			Assert.assertTrue(locator.localNoResultHeading.getText().contains(" Sorry !! No search results found for "));
			Assert.assertTrue(locator.localNoResultSubTitle.getText().equals("Please try out some other interesting offers."));

			for(WebElement e : locator.trendingDealAndRecentlyViewDealDiv){
				Assert.assertTrue(e.isDisplayed());
			}
			for(WebElement e : locator.dealsInDiv){
				Assert.assertTrue(e.isDisplayed());
			}
			for(WebElement e : locator.dealCards){
				Assert.assertTrue(e.isDisplayed());
				Assert.assertTrue(e.isEnabled());
			}

			//verifyNoSearchPageDataLayer();

			try {
				Thread.sleep(2000);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}

		return this;
	}

	/**
	 * Verify Change Your Location 
	 * @return
	 */
	public HeaderFooterPage changeYourLocation(String cityToBeSelected){

		waitVar.until(ExpectedConditions.visibilityOf(locator.selectLocationText));
		waitVar.until(ExpectedConditions.elementToBeClickable(locator.locationDropDownBox));
		Assert.assertTrue(locator.selectLocationText.isDisplayed());
		Assert.assertTrue(locator.selectLocationText.getText().contains("Select Location"));
		Assert.assertTrue(locator.citySelected.isDisplayed());
		logger.info("Location Preselected : " + locator.citySelected.getText());
		locator.citySelected.click();
		verifyCitySelectorModal();
		getWebDriver().findElement(By.cssSelector("body")).sendKeys(Keys.ESCAPE);
		waitVar.until(ExpectedConditions.visibilityOf(locator.selectLocationText));
		waitVar.until(ExpectedConditions.elementToBeClickable(locator.locationDropDownBox));
		locator.citySelected.click();
		waitVar.until(ExpectedConditions.visibilityOf(locator.locationModal));

		selectCityFromAutoSuggest("Vi");
		locator.citySelected.click();
		waitVar.until(ExpectedConditions.visibilityOf(locator.locationModal));

		for(WebElement cityToBeClicked : locator.topCitiesList){

			if(cityToBeClicked.getText().equals(cityToBeSelected)){
				cityToBeClicked.click();
				waitVar.until(ExpectedConditions.elementToBeClickable(locator.locationBoxInSearchBox));
				//	WaitUtility.waitForAttributeContainsGivenValue(By.xpath(".//a[@data-dropdown='divisions']"), "aria-expanded", "false", getWebDriver(), waitVar);
				break;
			}
		}

		logger.info("New City Changed : " + locator.citySelected.getText());
		Assert.assertTrue(locator.citySelected.getText().toLowerCase().equals(cityToBeSelected.toLowerCase()));
		logger.info("New Location Selected : " + locator.citySelected.getText());

		return this;
	}

	private void verifyCitySelectorModal() {
		waitVar.until(ExpectedConditions.visibilityOf(locator.locationModal));
		waitVar.until(ExpectedConditions.visibilityOfAllElements(locator.topCitiesList));
		waitVar.until(ExpectedConditions.elementToBeClickable(locator.citySearchBox));

		logger.info("No. of Top cities : " + locator.topCitiesList.size());
		Assert.assertTrue("Cross Icon on City Selector is NOT visible",locator.crossIconImageOnCitySelector.isDisplayed());
		Assert.assertTrue("Cross Icon on City Selector is NOT clickable",locator.crossIconButtonOnCitySelector.isEnabled());
		Assert.assertTrue("Cross Icon Text NOT visible",locator.crossIconTextOnCitySelector.isDisplayed());
		Assert.assertTrue("Cross Icon Text NOT as expected which is : ESC",locator.crossIconTextOnCitySelector.getText().equals("ESC"));
		Assert.assertTrue("City Search Box is NOT displayed",locator.citySearchBox.isDisplayed());
		Assert.assertTrue("City Search Box is NOT enabled",locator.citySearchBox.isEnabled());
		Assert.assertTrue("Placeholder in City Search Box is NOT visible",locator.citySearchBox.getAttribute("placeholder").equals("Enter your city name"));
		Assert.assertTrue("Location Icon in City Search Box is NOT visible",locator.locationIconInCitySearchBox.isDisplayed());
		Assert.assertTrue("Nearbuy Logo in City Search Box is NOT visible",locator.nearbuyLogoInCitySelector.isDisplayed());
		Assert.assertTrue("Nearbuy TagLine in City Search Box is NOT visible",locator.nearbuyTagLineInCitySelector.isDisplayed());


		for(WebElement p : locator.listOfTextOnCitySelector){
			Assert.assertTrue(p.isDisplayed());
		}

		for(WebElement text : locator.textInMainCitiesPopUp){
			Assert.assertTrue(text.isDisplayed());
			logger.info("Text of Top Cities : " + text.getText());
		}

	}


	private void selectCityFromAutoSuggest(String cityName){

		waitVar.until(ExpectedConditions.elementToBeClickable(locator.citySearchBox));
		try {
			Thread.sleep(1000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		locator.citySearchBox.sendKeys(cityName);
		//waitVar.until(ExpectedConditions.visibilityOf(locator.autosuggestionLoaderIncitySearchBox));
		//waitVar.until(ExpectedConditions.invisibilityOfElementLocated(By.cssSelector("city-selector-typeahead form > *:first-child")));
		//Assert.assertFalse(locator.autosuggestionLoaderIncitySearchBox.isDisplayed());
		waitVar.until(ExpectedConditions.visibilityOf(locator.autosuggestionBoxInCitySearch));

		for(WebElement city : locator.listOfCitiesInAutosuggestion){

			Assert.assertTrue("City is not NOT displayed", city.isDisplayed());
			Assert.assertTrue("City is not NOT enabled", city.isEnabled());
		}
		WebElement cityElementUserWantsToClick = locator.listOfCitiesInAutosuggestion.get(2);
		String cityUserWantsToClick = cityElementUserWantsToClick.getText();
		cityElementUserWantsToClick.click();

		waitVar.until(ExpectedConditions.visibilityOf(locator.selectLocationText));
		waitVar.until(ExpectedConditions.elementToBeClickable(locator.locationDropDownBox));

		logger.info("New City User Selected : " + cityUserWantsToClick);
		Assert.assertTrue(locator.citySelected.getText().toLowerCase().equals(cityUserWantsToClick.toLowerCase()));
		logger.info("New Location Selected : " + locator.citySelected.getText());

	}

	public HeaderFooterPage verifyCitySelectorFromSearchBox(String cityToBeSelected){

		waitVar.until(ExpectedConditions.visibilityOf(locator.changeLocationText));
		waitVar.until(ExpectedConditions.elementToBeClickable(locator.locationDropDownBoxInSearchBox));
		Assert.assertTrue(locator.changeLocationText.isDisplayed());
		Assert.assertTrue(locator.changeLocationText.getText().contains("Change Location:"));
		waitVar.until(ExpectedConditions.elementToBeClickable(locator.currentLocation));
		Assert.assertTrue(locator.currentLocation.isDisplayed());
		Assert.assertTrue(locator.currentLocation.isEnabled());
		logger.info("Location Preselected : " + locator.currentLocation.getText());
		try {
			Thread.sleep(1000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		locator.currentLocation.click();
		waitVar.until(ExpectedConditions.visibilityOf(locator.locationModal));
		verifyCitySelectorModal();

		selectCityFromAutoSuggest("Me");
		try {
			Thread.sleep(1000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		locator.currentLocation.click();
		waitVar.until(ExpectedConditions.visibilityOf(locator.locationModal));

		for(WebElement cityToBeClicked : locator.topCitiesList){

			if(cityToBeClicked.getText().equals(cityToBeSelected)){
				cityToBeClicked.click();
				waitVar.until(ExpectedConditions.elementToBeClickable(locator.locationBoxInSearchBox));
				//	WaitUtility.waitForAttributeContainsGivenValue(By.xpath(".//a[@data-dropdown='divisions']"), "aria-expanded", "false", getWebDriver(), waitVar);
				break;
			}
		}

		waitVar.until(ExpectedConditions.elementToBeClickable(locator.currentLocation));
		logger.info("New City Changed : " + locator.currentLocation.getText());
		Assert.assertTrue(locator.currentLocation.getText().toLowerCase().equals(cityToBeSelected.toLowerCase()));
		logger.info("New Location Selected : " + locator.currentLocation.getText());

		getWebDriver().findElement(By.cssSelector("body")).sendKeys(Keys.ESCAPE);
		waitVar.until(ExpectedConditions.visibilityOf(locator.selectLocationText));

		return this;
	}



	/**
	 * Verify Logo 
	 * @return
	 */
	public HeaderFooterPage verifyLogo(){

		waitVar.until(ExpectedConditions.elementToBeClickable(locator.logo));
		Assert.assertTrue(locator.logo.isDisplayed());
		Assert.assertTrue(locator.logo.isEnabled());
		locator.logo.click();
		WaitUtility.waitForLoad(getWebDriver());
		logger.info("Logo is Verified");

		return this;
	}

	/**
	 * Click and Verify New Tab opened for Social Links
	 * @return
	 */
	public HeaderFooterPage clickOnSocialLinksAndVerifyTabsOpened(){

		waitVar.until(ExpectedConditions.visibilityOfAllElements(locator.footerAllLinks));
		String	oldTab = getWebDriver().getWindowHandle();
		logger.info("Old tab : " + oldTab);

		for(WebElement link : locator.socialMediaLinks){

			String linkText = link.getAttribute("title").toLowerCase();
			logger.info("Text in href of the link : " + linkText);
			Assert.assertTrue(link.isDisplayed());
			Assert.assertTrue(link.isEnabled());

			// Store the current window handle
			String winHandleBefore = getWebDriver().getWindowHandle();

			logger.info("Parent window : " + winHandleBefore);

			waitVar.until(ExpectedConditions.visibilityOf(link));
			waitVar.until(ExpectedConditions.elementToBeClickable(link));
			// Perform the click operation that opens new window
			link.click();

			logger.info(link.getText() + " : Link is clicked");

			try {
				Thread.sleep(2000);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

			// Switch to new window opened
			for(String winHandle : getWebDriver().getWindowHandles()){
				getWebDriver().switchTo().window(winHandle);
			}

			waitVar.until(ExpectedConditions.urlContains(linkText.toLowerCase()));
			logger.info("New window URL : " + getWebDriver().getCurrentUrl());
			Assert.assertTrue(getWebDriver().getCurrentUrl().contains(linkText.toLowerCase()));
			logger.info("Asserted the new link opened & the href text");

			logger.info(getWebDriver().getCurrentUrl());

			// Close the new window, if that window no more required
			getWebDriver().close();

			getWebDriver().switchTo().window(winHandleBefore);
			logger.info(getWebDriver().getCurrentUrl());
			// Switch back to original browser (first window)
		}
		return this;
	}

	/**
	 * Hover all Main Menus and Verify Submenus and images
	 * @return
	 */
	public HeaderFooterPage hoverMainMenuAndVerifyAllSubmenuLinksAndImages(){

		for(WebElement mainMenu : locator.mainMenuList){

			Assert.assertTrue(mainMenu.isDisplayed());
			Assert.assertTrue(mainMenu.isEnabled());
			logger.info("Main Menu text : " + mainMenu.getText());
			new Actions(getWebDriver()).moveToElement(mainMenu).build().perform();
			//waitVar.until(ExpectedConditions.presenceOfElementLocated(locator))

			for(WebElement subMenu : locator.subMenuList){

				WebElement subMenuLink = subMenu.findElement(By.cssSelector("a"));
				Assert.assertTrue(subMenuLink.isDisplayed());
				Assert.assertTrue(subMenuLink.isEnabled());
				logger.info("Sub Menu Text : " + subMenuLink.getText());

				WebElement subMenuCount = subMenu.findElement(By.cssSelector("span"));
				Assert.assertTrue(subMenuCount.isDisplayed());
				Assert.assertTrue(subMenuCount.isEnabled());
				logger.info("Sub Menu Count Text : " + subMenuCount.getText());

				WebElement subMenuImage = subMenu.findElement(By.cssSelector("img"));
				Assert.assertTrue(subMenuImage.isDisplayed());
				Assert.assertTrue(subMenuImage.isEnabled());
				logger.info("Sub Menu Image is present");

			}
		}

		return this;
	}

	/**
	 * Click And Verify Blog Link
	 * @return
	 */
	public HeaderFooterPage clickAndVerifyBlog(){

		String blogLink = locator.blogLink.getAttribute("href");

		// Store the current window handle
		String winHandleBefore = getWebDriver().getWindowHandle();

		// Perform the click operation that opens new window
		locator.blogLink.click();

		// Switch to new window opened
		for(String winHandle : getWebDriver().getWindowHandles()){
			getWebDriver().switchTo().window(winHandle);
		}

		WaitUtility.waitForLoad(getWebDriver());
		waitVar.until(ExpectedConditions.urlContains(blogLink));
		logger.info("New window URL : " + getWebDriver().getCurrentUrl());
		Assert.assertTrue(getWebDriver().getCurrentUrl().contains(blogLink));

		// Close the new window, if that window no more required
		getWebDriver().close();

		// Switch back to original browser (first window)
		getWebDriver().switchTo().window(winHandleBefore);

		return this;
	}

	/**
	 * Click and Verify Company Links & App Links
	 * @return
	 */
	public HeaderFooterPage clickOnCompanyLinksAndVerify(){

		for(WebElement link : locator.companyLinks){

			Assert.assertTrue(link.isDisplayed());
			Assert.assertTrue(link.isEnabled());

			String linkHref = link.getAttribute("href");

			// Store the current window handle
			String winHandleBefore = getWebDriver().getWindowHandle();


			// Perform the click operation that opens new window
			link.click();

			// Switch to new window opened
			for(String winHandle : getWebDriver().getWindowHandles()){
				getWebDriver().switchTo().window(winHandle);
			}

			if(getCurrentUrl().contains("policy") || getCurrentUrl().contains("aboutus") || getCurrentUrl().contains("contactus")){
				waitVar.until(ExpectedConditions.urlContains(linkHref));
				logger.info("New window URL : " + getWebDriver().getCurrentUrl());
				Assert.assertTrue(getWebDriver().getCurrentUrl().contains(linkHref));
			}
			else if(getCurrentUrl().contains("dynamic") ){
				waitVar.until(ExpectedConditions.urlContains("http://dynamic.nearbuy.com/universal_fine_print/"));
				Assert.assertTrue(locator.universalFinePrintHeading.isDisplayed());
				logger.info("Heading : " + locator.universalFinePrintHeading.getText());
			}

			// Close the new window, if that window no more required
			getWebDriver().close();

			// Switch back to original browser (first window)
			getWebDriver().switchTo().window(winHandleBefore);

		}

		return this;
	}

	/**
	 * Click & Verify Download App Link
	 * @return
	 */
	public HeaderFooterPage clickOnDownloadAppLinks(){

		for(WebElement link : locator.downloadAppLinks){

			Assert.assertTrue(link.isDisplayed());
			Assert.assertTrue(link.isEnabled());

			// Store the current window handle
			String winHandleBefore = getWebDriver().getWindowHandle();

			// Perform the click operation that opens new window
			link.click();

			// Switch to new window opened
			for(String winHandle : getWebDriver().getWindowHandles()){
				getWebDriver().switchTo().window(winHandle);
			}

			WaitUtility.waitForLoad(getWebDriver());
			logger.info("New window URL : " + getWebDriver().getCurrentUrl());

			// Close the new window, if that window no more required
			getWebDriver().close();

			// Switch back to original browser (first window)
			getWebDriver().switchTo().window(winHandleBefore);

		}

		return this;
	}

	/**
	 * Verify COmpany page Content & Links 
	 * @return
	 */
	public HeaderFooterPage verifyCompanyLinksContent(){

		// Store the current window handle
		String winHandleBefore = getWebDriver().getWindowHandle();

		getWebDriver().findElement(By.linkText("About Us")).click();

		// Switch to new window opened
		for(String winHandle : getWebDriver().getWindowHandles()){
			getWebDriver().switchTo().window(winHandle);
			logger.info("Switched to About Us window");
		}

		WaitUtility.waitForLoad(getWebDriver());
		logger.info("New window URL : " + getWebDriver().getCurrentUrl());

		for (int i=0 ; i<=locator.topTabsLinks.size()-1; i++){

			List<WebElement> menus = locator.topTabsLinks;
			WebElement menu = menus.get(i);

			logger.info("Running for : " + menu.getText());

			if(menu.getText().contains("Policy")){
				Assert.assertTrue(menu.isDisplayed());
				Assert.assertTrue(menu.isEnabled());
				menu.click();
				//logger.info("In Tab : " + menu.getAttribute("href"));
				waitVar.until(ExpectedConditions.visibilityOf(locator.tabsContent));

				Assert.assertTrue(locator.tabsContent.isDisplayed());
				PageUtils.verifyAllLinksInPage(getWebDriver());

				for(WebElement policy : locator.policyTabs){

					Assert.assertTrue(policy.isDisplayed());
					Assert.assertTrue(policy.isEnabled());
					logger.info("Policy Link Name : " + policy.getText());

					policy.click();
					waitVar.until(ExpectedConditions.visibilityOf(locator.policyVerticalContent));
					Assert.assertTrue(locator.policyVerticalContent.isDisplayed());
					Assert.assertTrue(locator.policyVerticalContent.getAttribute("class").contains("active"));
					PageUtils.verifyAllLinksInPage(getWebDriver());
				}
			}

			else if(menu.getText().contains("FAQs")){
				menu.click();
				waitVar.until(ExpectedConditions.visibilityOf(locator.tabsContent));
				//logger.info("In Tab : " + menu.getAttribute("href"));

				Assert.assertTrue(locator.tabsContent.isDisplayed());
				Assert.assertTrue(locator.policyVerticalContent.isDisplayed());
				PageUtils.verifyAllLinksInPage(getWebDriver());

				for(WebElement faq : locator.faqsLinks){

					Assert.assertTrue(faq.isDisplayed());
					Assert.assertTrue(faq.isEnabled());
					logger.info(faq.getText());
				}

			}

			else if(menu.getText().contains("Promise")){
				logger.info("Running for : " + menu.getText());
				menu.click();
				waitVar.until(ExpectedConditions.visibilityOf(locator.tabsContent));
				//logger.info("In Tab : " + menu.getAttribute("href"));
				Assert.assertTrue(locator.tabsContent.isDisplayed());
			}
			else if(menu.getText().contains("Universal Fine Print")){
				logger.info("Running for : " + menu.getText());
				menu.click();
				waitVar.until(ExpectedConditions.visibilityOf(locator.universalFinePrintHeading));
				Assert.assertTrue(locator.universalFinePrintHeading.isDisplayed());
				logger.info("Heading : " + locator.universalFinePrintHeading.getText());
				Assert.assertTrue(locator.universalListPoints.isDisplayed());
				Assert.assertTrue(locator.okButtonOnUniversalPrint.isDisplayed());
				Assert.assertTrue(locator.okButtonOnUniversalPrint.isEnabled());
				locator.okButtonOnUniversalPrint.click();
				waitVar.until(ExpectedConditions.visibilityOf(locator.secondaryNavBar));
				Assert.assertTrue(locator.secondaryNavBar.isDisplayed());

			}
		}
		// Close the new window, if that window no more required
		getWebDriver().close();

		// Switch back to original browser (first window)
		getWebDriver().switchTo().window(winHandleBefore);

		return this;
	}


	/**
	 * Verify How It Works Link
	 * @return
	 */
	public HeaderFooterPage verifyHowItWorks(){

		clickOnSpecificUtilityButton("How it Works");

		waitVar.until(ExpectedConditions.visibilityOf(locator.howItWorksPanel));
		logger.info("How it works is opened");
		for(WebElement image : locator.howItWorksImages){
			Assert.assertTrue(image.isDisplayed());
		}
		for(WebElement subHeading : locator.howItWorksSubHeadings){
			Assert.assertTrue(subHeading.isDisplayed());
			logger.info(subHeading.getText());
		}
		for(WebElement subTitle : locator.howItWorksSubTitles){
			Assert.assertTrue(subTitle.isDisplayed());
			logger.info(subTitle.getText());
		}

		Assert.assertTrue("Cross Button on How it works is NOT displayed",locator.howItWorksPanelCloseButton.isDisplayed());
		Assert.assertTrue("Cross Button on How it works is NOT enabled",locator.howItWorksPanelCloseButton.isEnabled());
		locator.howItWorksPanelCloseButton.click();
		waitVar.until(ExpectedConditions.invisibilityOfElementLocated(By.cssSelector(".how-it-works-show .btn-close")));

		clickOnSpecificUtilityButton("How it Works");

		waitVar.until(ExpectedConditions.visibilityOf(locator.howItWorksPanel));
		getWebDriver().findElement(By.cssSelector("body")).sendKeys(Keys.ESCAPE);
		waitVar.until(ExpectedConditions.invisibilityOfElementLocated(By.cssSelector(".how-it-works-show .btn-close")));

		logger.info("How it works is closed");

		return this;
	}

	private void clickOnSpecificUtilityButton(String utilityButtonText) {

		for(WebElement utilityButton : locator.utilityButtons){

			if(utilityButton.getText().equals(utilityButtonText)){

				Assert.assertTrue(utilityButton.isDisplayed());
				Assert.assertTrue(utilityButton.isEnabled());
				Assert.assertTrue(utilityButton.getText().equals(utilityButtonText));
				utilityButton.click();
				break;

			}
		}
	}

	public HeaderFooterPage verifyPrimaryHeaderIsSticky(){

		waitVar.until(ExpectedConditions.visibilityOf(locator.headerPrimary));
		Assert.assertTrue(locator.headerPrimary.isDisplayed());
		for(int i=0; i<=4; i++){
			getWebDriver().findElement(By.cssSelector("body")).sendKeys(Keys.PAGE_DOWN);
		}
		Assert.assertTrue(locator.headerPrimary.isDisplayed());
		Assert.assertTrue(locator.headerPrimary.getCssValue("position").equals("relative"));

		return this;
	}


	/**
	 * Get Title of the home page and assert
	 * @return
	 */
	public HeaderFooterPage getTitleOfTheHomePage(){
		waitVar.until(ExpectedConditions.elementToBeClickable(locator.logo));
		this.currentLocationSelected = locator.locationBoxInSearchBox.getText();
		String currentTitle = getWebDriver().getTitle();
		Assert.assertEquals("Restaurants, Spa, Salon, Gym, Movies, Activities, Travel offers in " + currentLocationSelected + " | nearbuy", currentTitle);
		logger.info("Asserted the title of home page");
		return this;
	}
	/**
	 * Get Title of the catalog page and assert
	 * @return
	 */
	public HeaderFooterPage getTitleOfTheCatalogPage(){
		waitVar.until(ExpectedConditions.elementToBeClickable(locator.locationDropDown));
		String currentLocationSelected = locator.locationDropDownText.getText();
		String currentTitle = getWebDriver().getTitle();
		Assert.assertEquals("Restaurants, Spa, Salon, Gym, Movies, Activities, Travel offers in " + currentLocationSelected + " | nearbuy", currentTitle);
		logger.info("Asserted the title of catalog page");
		return this;
	}


	public HeaderFooterPage verifyNoSearchPageDataLayer(){

		//prepare map by extracting values from html
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("city", locator.dlCity.getAttribute("value"));
		map.put("pageType", "Local Search No Result");
		map.put("searchkey", locator.searchBox.getAttribute("value"));
		map.put("sitetype", "d");
		map.put("vertical", "local");

		verifyDataLayer(map,"3");
		return this;
	}


	public HeaderFooterPage verifySearchPageDataLayer(){

		//prepare map by extracting values from html
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("city", locator.dlCity.getAttribute("value"));
		map.put("deal-1", locator.dldeal1.getAttribute("value"));
		map.put("deal-2", locator.dldeal2.getAttribute("value"));
		map.put("deal-3", locator.dldeal3.getAttribute("value"));
		map.put("dealTitle1", locator.dldealTitle1.getAttribute("value"));
		map.put("dealTitle2", locator.dldealTitle2.getAttribute("value"));
		map.put("dealTitle3", locator.dldealTitle3.getAttribute("value"));
		map.put("pageType", "Local Search Result");
		map.put("searchkey", locator.searchBox.getAttribute("value"));
		map.put("sitetype", "d");
		map.put("vertical", "local");

		verifyDataLayer(map,"3");
		return this;
	}


	public WebElement getMenuNavigationBar() {
		return locator.secondaryNavBar;
	}

	public boolean isMenuNavigationBarDisplayed() {
		return locator.secondaryNavBar.isDisplayed();
	}

	public String getCurrentCity() {
		return locator.locationDropDown.getText();
	}

	public HeaderFooterPage clickOnSearchBox(){

		waitVar.until(ExpectedConditions.elementToBeClickable(locator.searchBox));
		locator.searchBox.click();
		waitVar.until(ExpectedConditions.visibilityOf(locator.searchPage));


		return this;
	}

	public HeaderFooterPage listYourBusiness(){

		waitVar.until(ExpectedConditions.visibilityOf(locator.listYourBusinessLink));
		waitVar.until(ExpectedConditions.elementToBeClickable(locator.listYourBusinessLink));
		locator.listYourBusinessLink.click();
		WaitUtility.waitForURLtoChange(getWebDriver(), waitVar);
		waitVar.until(ExpectedConditions.urlContains("/list-your-business"));
		Assert.assertTrue(locator.listYourBusinessFormDiv.isDisplayed());
		Assert.assertTrue(locator.nextButtonInListYourForm.isDisplayed());

		locator.categoryOptions.get(2).click();

		waitVar.until(ExpectedConditions.elementToBeClickable(locator.nextButtonInListYourForm));
		Assert.assertTrue(locator.nextButtonInListYourForm.isEnabled());

		locator.nextButtonInListYourForm.click();
		waitVar.until(ExpectedConditions.elementToBeClickable(locator.backButtonInListYourForm));


		for(WebElement no : locator.noOfLocationsInListYourForm){
			if(no.getAttribute("value").equals("1-5 Physical Location")){
				no.click();
				break;
			}
		}

		locator.localAddressInListYourForm.sendKeys("xyz");
		for(WebElement yes : locator.workedForInListYourForm){
			if(yes.getAttribute("value").equals("Yes")){
				yes.click();
				break;
			}
		}
		locator.nextButtonInSecondScreenOfListYourForm.click();
		waitVar.until(ExpectedConditions.presenceOfElementLocated(By.cssSelector("#businessInfoRequiredFields .error-msg")));
		locator.businessNameInListYourForm.sendKeys("abc");
		locator.nextButtonInSecondScreenOfListYourForm.click();
		waitVar.until(ExpectedConditions.elementToBeClickable(locator.submitButtonInListForm));

		locator.nameInListForm.sendKeys("ABC");
		locator.phoneNumberInListForm.sendKeys("9999999999");
		locator.emailIdInListForm.sendKeys("abc@xyz.com");
		locator.cityInListForm.sendKeys("Gurgaon");
		locator.submitButtonInListForm.submit();

		waitVar.until(ExpectedConditions.urlContains("/partner-with-us/submit"));

		Assert.assertTrue(locator.thanksImage.isDisplayed());
		Assert.assertTrue(locator.thanksHeading.isDisplayed());
		Assert.assertTrue(locator.thanksMessage.isDisplayed());
		Assert.assertTrue(locator.thanksHeading.getText().equals("Thanks for writing in!"));
		Assert.assertTrue(locator.thanksMessage.getText().equals("You can expect a call from our consultant/team within 2 business days to take this forward."));



		return this;
	}

}