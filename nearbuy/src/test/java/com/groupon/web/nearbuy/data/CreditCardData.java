package com.groupon.web.nearbuy.data;

import org.testng.annotations.DataProvider;

import com.groupon.web.common.config.Config;
import com.groupon.web.common.utils.DataProviderUtil;

/**
 * 
 * @author prachi.nagpal
 *
 */
public class CreditCardData {

	@DataProvider(name = "enterCreditCardDetails")
	public static Object[][] provideCreditCardDetails(){
		return DataProviderUtil.provideData(Config.getInstance().getConfig("testData.file"), "nearbuy","enterCreditCardDetails");

	}
	@DataProvider(name = "enterWrongCreditCardDetails")
	public static Object[][] provideWrongCreditCardDetails(){
		return DataProviderUtil.provideData(Config.getInstance().getConfig("testData.file"), "nearbuy","enterWrongCreditCardDetails");
		
	}
}
