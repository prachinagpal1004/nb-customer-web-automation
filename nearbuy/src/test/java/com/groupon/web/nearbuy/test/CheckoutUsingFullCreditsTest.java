package com.groupon.web.nearbuy.test;

import com.groupon.web.common.test.BaseTest;
import com.groupon.web.common.utils.TestListener;
import com.groupon.web.nearbuy.data.AuthenticationData;
import com.groupon.web.nearbuy.data.CreditsData;
import com.groupon.web.nearbuy.data.IncDecQtyData;
import com.groupon.web.nearbuy.data.MenuData;
import com.groupon.web.nearbuy.data.PaymentModeData;
import com.groupon.web.nearbuy.page.AuthenticationPage;
import com.groupon.web.nearbuy.page.DealCatalogPage;
import com.groupon.web.nearbuy.page.DealDetailPage;
import com.groupon.web.nearbuy.page.HeaderFooterPage;
import com.groupon.web.nearbuy.page.OrderSummaryPage;
import com.groupon.web.nearbuy.page.ThankYouPage;
import com.relevantcodes.extentreports.LogStatus;

import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
/**
 * 
 * @author Prachi Nagpal
 *
 */
@Test(groups = {"sanity", "staging"})
public class CheckoutUsingFullCreditsTest extends BaseTest {

	private AuthenticationPage authenticationPage;
	private HeaderFooterPage headerFooterPage;
	private DealDetailPage dealDetailPage;
	private DealCatalogPage dealCatalogPage;
	private OrderSummaryPage orderSummaryPage;
	private ThankYouPage thankYouPage;

    @BeforeMethod(alwaysRun=true)
    public void init() {
		authenticationPage = new AuthenticationPage();
		headerFooterPage = new HeaderFooterPage();
		dealDetailPage = new DealDetailPage();
		dealCatalogPage = new DealCatalogPage();
		orderSummaryPage = new OrderSummaryPage();
		thankYouPage = new ThankYouPage();
	}


	/**
	 * Traverse to Deal Catalog Page and click on featured Deal
	 * @param menu
	 * @param submenu
	 */
	@Test(priority = 1, dataProvider="foodAndDrink" , dataProviderClass = MenuData.class )
	public void testLandingOnDealDetailPage(String mainMenu, String cat, String subCat) {
		TestListener.setLog(LogStatus.INFO, "Navigate to Deal Listing Page from Menu");
		headerFooterPage.navigateToMenu(mainMenu, cat, subCat);
		TestListener.setLog(LogStatus.INFO, "Click on Featured Deal");
		dealCatalogPage.clickOnSpecifiedDealCard(2);
	}

	/**
	 * Increase the quantity
	 * @param inc
	 * @param dec
	 */
	@Test(priority = 2)
	public void testOfferSection() {
		TestListener.setLog(LogStatus.INFO, "Verify Deal Details & OfferSection");
		dealDetailPage.dealDetailHeading().dealDetailOptionTitles().verifyOfferSectionAndClickOnOption(1, 1);
	}

	/**
	 * Click on Buy Now Button of Deal
	 */
	@Test(priority = 3)
	public void testClickOnBuyNowButton(){
		TestListener.setLog(LogStatus.INFO, "Click on Buy Now Button");
		dealDetailPage.clickOnBuyNowButton();
	}

	/**
	 * Do Success Login
	 */
	@Test(priority = 4 , dataProvider="successNumberLogin" , dataProviderClass = AuthenticationData.class )
	public void testSuccessLogin(String username, String password, String message, String bool) throws Throwable {
		TestListener.setLog(LogStatus.INFO, "Enter Credentials for Successful Login");
		authenticationPage.verifyLoginAndErrorMessages(username, password, message, Boolean.valueOf(bool));
	}

	/**
	 * Verify Order Details
	 */
	@Test(priority = 5 )
	public void testOrderDetails() {
		TestListener.setLog(LogStatus.INFO, "Verify Order Details & Price");
		orderSummaryPage.verifyOrderSummaryDetails();
	}

	/**
	 * Select the credits
	 * @param bool
	 */
	@Test(priority = 6, dataProvider="userWantsToUseCredits" , dataProviderClass = CreditsData.class )
	public void testCredits(String bool) {
		TestListener.setLog(LogStatus.INFO, "User Wants to use all credits");
		orderSummaryPage.selectDeselectCredits(Boolean.valueOf(bool)).priceVerifications();
	}

	@Test(priority = 8)
	public void testProceedToPayment() {
		TestListener.setLog(LogStatus.INFO, "Click on 'Proceed To Payment' Button");
		orderSummaryPage.clickOnProceedButton();
	}

	@Test(priority = 9)
	public void testThankYouPage() {
		TestListener.setLog(LogStatus.INFO, "Verify Thank You Page Messages");
		thankYouPage.verifyCongratsMessage();
	}

}
