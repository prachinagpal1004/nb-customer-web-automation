package com.groupon.web.nearbuy.test;

import com.groupon.web.common.test.BaseTest;
import com.groupon.web.common.utils.TestListener;
import com.groupon.web.nearbuy.data.MenuData;
import com.groupon.web.nearbuy.page.DealCatalogPage;
import com.groupon.web.nearbuy.page.HeaderFooterPage;
import com.relevantcodes.extentreports.LogStatus;

import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
/**
 * 
 * @author Prachi Nagpal
 *
 */
@Test(groups = {"sanity", "localListingDealCards"})
public class ListingDealCardsTest extends BaseTest {

	private HeaderFooterPage headerFooterPage;
	private DealCatalogPage dealCatalogPage;

    @BeforeMethod(alwaysRun=true)
    public void init() {
		headerFooterPage = new HeaderFooterPage();
		dealCatalogPage = new DealCatalogPage();
	}

	/**
	 * Navigate to particular Menu
	 * @param menu
	 * @param submenu
	 * @throws Throwable
	 */
	@Test(priority = 1 , dataProvider="foodAndDrink" , dataProviderClass = MenuData.class )
	public void test_Home_Pages(String menu, String submenu, String subSubMenu) throws Throwable {
		TestListener.setLog(LogStatus.INFO, "Navigate to Deal Listing Page from Menu");
		headerFooterPage.navigateToMenu(menu, submenu, subSubMenu);
	}

	/**
	 *  Test Scroll Down Link
	 */
	@Test(priority = 2 )
	public void testViewAllDealsLink() throws Throwable {
		TestListener.setLog(LogStatus.INFO, "Scroll Down to Verify all deals");
		dealCatalogPage.scrollDownToSeeAllDeals();
	}

	/**
	 *  Test Deal Details for each deal
	 */
	@Test(priority = 3 )
	public void testDealDetaislForEachDeal() throws Throwable {
		TestListener.setLog(LogStatus.INFO, "Verify Deal Details for Each Deal Card");
		dealCatalogPage.dealDetailsForEachDeal();
	}

	/**
	 *  Test Scroll Up Button 
	 */
	@Test(priority = 4 )
	public void testScrollUpButton() throws Throwable {
		TestListener.setLog(LogStatus.INFO, "Click on Scroll Up Button to verify Featured Deal");
		dealCatalogPage.clickOnBackToTopButton();
	}
	/**
	 *  Test Rating & Fav
	 */
	@Test(priority = 5 )
	public void testRatingAndFavIcon() throws Throwable {
		TestListener.setLog(LogStatus.INFO, "Verify Rating & Fav Icon");
		dealCatalogPage.verifyRatingAndFavIcon();
	}

	/**
	 *  Click on specific deal
	 */
	@Test(priority = 6 )
	public void user_clicks_on_featuredDeal() throws Throwable {
		TestListener.setLog(LogStatus.INFO, "Click on Featured Deal");
		dealCatalogPage.clickOnSpecifiedDealCard(3);
	}	
}
