package com.groupon.web.nearbuy.page;

import com.groupon.web.common.page.BasePage;
import com.groupon.web.common.utils.PageUtils;
import com.groupon.web.common.utils.WaitUtility;
import com.groupon.web.nearbuy.locator.PaytmLocator;

import java.util.List;

import org.apache.log4j.Logger;
import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
/**
 * 
 * @author Prachi Nagpal
 *
 */
public class PaytmWalletPage extends BasePage {

	private static final Logger logger = Logger.getLogger(PaytmWalletPage.class);
	public PaytmLocator locator;

	public PaytmWalletPage() {
		locator = PageFactory.initElements(getWebDriver(), PaytmLocator.class);
	}

	/**
	 * Enter Paytm Credentials
	 */
	public PaytmWalletPage enterPaytmCredentials(String username , String password){

		waitVar.until(ExpectedConditions.urlContains("paytm"));
		waitVar.until(ExpectedConditions.visibilityOf(locator.loginPopup));
		waitVar.until(ExpectedConditions.visibilityOf(locator.loginIframe));
		getWebDriver().switchTo().activeElement();
		PageUtils.switchToFrame(getWebDriver(), locator.loginIframe);
		logger.info("Swicthed to Login Frame");
		waitVar.until(ExpectedConditions.visibilityOf(locator.secureSignInButton));
		waitVar.until(ExpectedConditions.elementToBeClickable(locator.secureSignInButton));
		Assert.assertTrue(locator.username.isEnabled());
		locator.username.clear();
		locator.username.sendKeys(username);
		Assert.assertTrue(locator.password.isEnabled());
		locator.password.sendKeys(password);
		locator.secureSignInButton.submit();
		logger.info("Paytm Credentials entered");
		getWebDriver().switchTo().defaultContent();
		waitVar.until(ExpectedConditions.urlContains("oauth=true"));

		return this;
	}

	/**
	 * Click on Pay Now Button
	 * @return
	 */
	public PaytmWalletPage clickOnPayNowButton(){

		waitVar.until(ExpectedConditions.elementToBeClickable(locator.payNowButton));
		locator.payNowButton.submit();
		logger.info("Pay Now Button has been clicked");
		waitVar.until(ExpectedConditions.urlContains("/payment-vendor/payment-status"));
		return this;
		
	}
	/**
	 * Click on cancel Link
	 * @return
	 */
	public PaytmWalletPage clickOnCancelLink(){
		
		waitVar.until(ExpectedConditions.elementToBeClickable(locator.cancelLink));
		locator.cancelLink.click();
		waitVar.until(ExpectedConditions.urlContains("isTransectionError=true"));
		
		return this;
	}
	
	public PaytmWalletPage selectWalletsAndVisitRetailer(){
		
		waitVar.until(ExpectedConditions.visibilityOf(locator.walletBlock));
		//waitVar.until(ExpectedConditions.visibilityOfAllElements(locator.walletOptions));
		
		//for(WebElement wallet : locator.walletOptions){
			for(int index=0; index<locator.walletOptions.size(); index++ ){
			List<WebElement> wallet = locator.walletOptions; 
				wallet.get(index).click();
				
			Assert.assertTrue(wallet.get(index).isDisplayed());
			Assert.assertTrue(wallet.get(index).isEnabled());
			wallet.get(index).findElement(By.cssSelector("input")).click();
			String walletClicked = wallet.get(index).findElement(By.cssSelector("img")).getAttribute("src");
			locator.walletPayNowButton.click();
			WaitUtility.waitForURLtoChange(getWebDriver(), waitVar);
			try {
				Thread.sleep(3000);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			if(walletClicked.contains("payu")){
				Assert.assertTrue(getCurrentUrl().contains("payumoney.com"));
			}
			if(walletClicked.contains("paytm")){
				Assert.assertTrue(getCurrentUrl().contains("paytm.in"));
			}
			if(walletClicked.contains("mobikwik")){
				Assert.assertTrue(getCurrentUrl().contains("mobikwik.com"));
			}
			if(walletClicked.contains("freecharge")){
				Assert.assertTrue(getCurrentUrl().contains("freecharge.in"));
			}
			if(walletClicked.contains("airtel")){
				Assert.assertTrue(getCurrentUrl().contains("airtelmoney.in"));
			}
			getWebDriver().navigate().back();
			logger.info("Navigated back to nearbuy website");
			WaitUtility.waitForURLtoChange(getWebDriver(), waitVar);
			try {
				Thread.sleep(3000);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			waitVar.until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector(".card-heading")));
			
			if(WaitUtility.isElementPresentByLocator(getWebDriver(), By.id("avlCreditChbox"))){
				getWebDriver().findElement(By.id("avlCreditChbox")).click();
			}
			new OrderSummaryPage().selectModeOfPayment("Wallets");
			try {
				Thread.sleep(2000);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
		}
		
		return this;
	}
}