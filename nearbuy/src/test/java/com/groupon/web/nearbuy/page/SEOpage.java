package com.groupon.web.nearbuy.page;

import java.util.List;

import org.apache.commons.lang3.StringEscapeUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import com.groupon.web.common.page.BasePage;
import com.groupon.web.nearbuy.test.SEOTest.SEOResult;

public class SEOpage extends BasePage {

	private static final Logger logger = Logger.getLogger(SEOpage.class);
	private String currentLocationSelected;

	public SEOpage() {
	}
	
	public String getCurrentLocationSelected() {
		currentLocationSelected = getWebDriver().findElement(By.cssSelector(".city-name")).getText();
		return currentLocationSelected;
	}
	public String getCategorySelected() {
		List<WebElement> breadcrumbs = getWebDriver().findElements(By.cssSelector(".breadcrumbs__txt"));
		String category = breadcrumbs.get(breadcrumbs.size()-1).getText();
		return category;
	}
	
	/**
	 * Get Title of Page
	 * @param expectedTitle 
	 * @param seoResult 
	 * @return
	 */
	public SEOpage assertTitleOfThePage(String expectedTitle, SEOResult seoResult){
		String actualTitle = getWebDriver().getTitle();
		boolean titleMatch = StringUtils.equalsIgnoreCase(expectedTitle, actualTitle);
		logger.info("Page Title is matching with the expected one ? " + titleMatch);
		seoResult.expectedTitle = expectedTitle;
		seoResult.actualTitle = actualTitle;
		seoResult.titleMatch = titleMatch ? "Y" : "N";
		return this;
	}
	
	/**
	 * Get Meta Description of Page
	 * @param expectedTitle 
	 * @param seoResult 
	 * @return
	 */
	public SEOpage assertMetaOfThePage(String expectedMeta, SEOResult seoResult){
		
		getWebDriver().navigate().refresh();
		
		String pageSource = getWebDriver().getPageSource();
		pageSource = pageSource.replaceAll("\\n", "");
		int i1 = pageSource.indexOf("\" name=\"description\"");
		String s1 = pageSource.substring(0, i1);
		String metaPrefix = "<meta content=\"";
		int i2 = s1.lastIndexOf(metaPrefix);
		String actualMeta = s1.substring(i2 + metaPrefix.length());
		actualMeta = StringEscapeUtils.unescapeHtml4(actualMeta);
		expectedMeta = StringEscapeUtils.unescapeHtml4(expectedMeta);
		logger.info("Actual Meta Description:"+actualMeta);
		boolean metDescMatch = StringUtils.equalsIgnoreCase(expectedMeta, actualMeta);
		logger.info("Meta Description is matching with the expected one ? " + metDescMatch);
		seoResult.expectedMetaDesc = expectedMeta;
		seoResult.actualMetaDesc = actualMeta;
		seoResult.metaDescMatch = metDescMatch ? "Y" : "N";
		return this;

		
		
		
//		String pageSource = getWebDriver().getPageSource();
//		logger.info(pageSource.substring(0, 10000));
//		String metaPrefix = "<meta name=\"description\" content=\"";
//		logger.info(pageSource.indexOf(metaPrefix));
//		String actualMeta = pageSource.substring(pageSource.indexOf(metaPrefix)+metaPrefix.length());
//		actualMeta = actualMeta.substring(0, actualMeta.indexOf("\">"));
//		try {
//			Thread.sleep(2000);
//		} catch (InterruptedException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
//		Assert.assertEquals(expectedMeta.toUpperCase(), actualMeta.toUpperCase());
//		return this;
	}
	

}
