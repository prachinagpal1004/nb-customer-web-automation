package com.groupon.web.nearbuy.test;

import com.groupon.web.common.test.BaseTest;
import com.groupon.web.nearbuy.data.MenuData;
import com.groupon.web.nearbuy.page.HeaderFooterPage;
import com.groupon.web.nearbuy.page.HomePage;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Factory;
import org.testng.annotations.Test;

public class LocalHomeTest extends BaseTest {

	private HeaderFooterPage headerFooterPage;
	private HomePage homePage;
	private String menuName;

    @BeforeMethod(alwaysRun=true)
    public void init() {
		headerFooterPage = new HeaderFooterPage();
		homePage = new HomePage();
	}

    
//	@Factory(dataProvider="enterMenuAndSubmenu" , dataProviderClass = MenuData.class)
//	public LocalHomeTest(String menuName, String submenuName) {
//		this.menuName = menuName;
//	}

//	@Test(priority = 1 )
//	public void testMenuNavigation() throws Throwable {
//		headerFooterPage.navigateToMenu(menuName, menuName);
//	}	

	@Test(priority = 2 )
	public void testBackgroundImageAndHeadings() throws Throwable {
		homePage.verifyIconsClickabilityAndImagePresence();
		homePage.verify_blocks_headings_and_anchorTags();
	}

//	@Test(priority = 3 )
//	public void testnearbuyPromiseSection() throws Throwable {
//		homePage.nearbuyPromise();
//	}

	@Test(priority = 4 )
	public void testViewAllDealsLink() throws Throwable {
		homePage.viewAllOffersButton();
	}
}
