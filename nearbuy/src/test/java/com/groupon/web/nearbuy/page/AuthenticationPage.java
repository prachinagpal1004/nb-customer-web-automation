package com.groupon.web.nearbuy.page;

import com.groupon.web.common.page.BasePage;
import com.groupon.web.common.utils.PageUtils;
import com.groupon.web.common.utils.WaitUtility;
import com.groupon.web.nearbuy.locator.AuthenticationLocator;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class AuthenticationPage extends BasePage {

	private static final Logger logger = Logger.getLogger(AuthenticationPage.class);
	private AuthenticationLocator locator;
	MyAccountPage myAccountPage = new MyAccountPage();

	public AuthenticationPage() {
		locator = PageFactory.initElements(getWebDriver(), AuthenticationLocator.class);
	}

	/**
	 * User clicks on Login Link
	 * @throws Throwable 
	 */
	public AuthenticationPage clickOnLoginLink(){
		waitVar.until(ExpectedConditions.elementToBeClickable(By.cssSelector("a.utility-btn[href*='login']")));
		locator.loginLink.click();
		waitVar.until(ExpectedConditions.elementToBeClickable(locator.signInSubmitButton.get(0)));

		//getWebDriver().get(baseUrl+"/login");
		//		if(baseUrl.contains("iwanto")){
		//		getWebDriver().get("https://kibana:Krypton90!@www.iwanto.in/login");
		//		}
		//		else if(baseUrl.contains("test-apache")){
		//			getWebDriver().get("https://kibana:Krypton90!@test-apache.nearbuytools.com/login");
		//		}
		return this;
	}

	public AuthenticationPage verifyLoginAndSignUpPopUp(){

		waitVar.until(ExpectedConditions.visibilityOf(locator.loginModalPopUp));
		for(WebElement tabs : locator.loginSignUpTabs){

			Assert.assertTrue(tabs.isDisplayed());
			Assert.assertTrue(tabs.isEnabled());
			if(tabs.getText().equals("Log in")){
				Assert.assertTrue(tabs.getAttribute("class").equals("active"));
				Assert.assertTrue(locator.forgotYourPasswordLink.isDisplayed());
			}
			if(tabs.getText().equals("Sign up")){
				tabs.click();
				waitVar.until(ExpectedConditions.visibilityOf(locator.notifyOffersText));
				Assert.assertTrue(tabs.getAttribute("class").equals("active"));
			}
			locator.loginSignUpTabs.get(0).click();
			waitVar.until(ExpectedConditions.elementToBeClickable(locator.forgotYourPasswordLink));
		}
		return this;
	}

	public AuthenticationPage clickOnSignUpLinkAfterClosingLoginPopUp(){

		if(WaitUtility.isElementPresent(getWebDriver(), locator.loginCloseButton)){
			locator.loginCloseButton.click();
			waitVar.until(ExpectedConditions.invisibilityOfElementLocated(By.cssSelector(".login-modal .modal")));
		}
		locator.loginSignUpLink.click();
		waitVar.until(ExpectedConditions.visibilityOf(locator.loginModalPopUp));
		locator.loginSignUpTabs.get(1).click();
		Assert.assertTrue(locator.loginSignUpTabs.get(1).getAttribute("class").equals("active"));
		logger.info("SignUp PopUp Box is opened");

		return this;
	}

	public AuthenticationPage clickOnLoginTabFromSignUpTab(){

		locator.loginSignUpTabs.get(0).click();
		waitVar.until(ExpectedConditions.elementToBeClickable(locator.forgotYourPasswordLink));
		Assert.assertTrue(locator.loginSignUpTabs.get(0).getAttribute("class").equals("active"));
		logger.info("Switched to Login tab");
		return this;
	}

	/**
	 * Login on Nearbuy
	 * @param username
	 * @param password
	 * @throws Throwable 
	 */
	public AuthenticationPage enterUserCredentialsAndClickOnSubmit(String username, String password) throws Throwable{

		//	waitVar.until(ExpectedConditions.titleIs("nearbuy | Login")); // To Uncomment afterwards
		//Assert.assertTrue(locator.imageOnLoginPage.isDisplayed());
		System.out.println("entering credentials");
		waitVar.until(ExpectedConditions.elementToBeClickable(locator.forgotYourPasswordLink));
		if(WaitUtility.isElementPresent(getWebDriver(), locator.heading)){
			Assert.assertTrue(locator.heading.getText().equals("Login to nearbuy"));
		}
		//	Assert.assertTrue(locator.createAnAccountLink.isEnabled());
		Assert.assertTrue(locator.forgotYourPasswordLink.isDisplayed());
		Assert.assertTrue(locator.forgotYourPasswordLink.isEnabled());
		if(username != null)	{
			locator.usernameInput.clear();
			locator.usernameInput.sendKeys(username);
		}
		if(password != null){
			locator.passwordInput.clear();
			locator.passwordInput.sendKeys(password);
		}
		if(getCurrentUrl().contains("/login"))
			locator.signInSubmitButton.get(1).click();

		else{
			locator.signInSubmitButton.get(0).click();
			logger.info("Login button is clicked");
		}

		return this;

	}

	/**
	 * Verify login
	 * @param message
	 * @throws Throwable
	 */
	public AuthenticationPage verifyLoginAndErrorMessages(String username, String password,String message, Boolean userWantsTologout) throws Throwable {

		//		if(!(getWebDriver().getCurrentUrl().contains("login"))){
		//			clickOnLoginLink();
		//			logger.info("Login Link is clicked");
		//		}
		enterUserCredentialsAndClickOnSubmit(username, password);
		if(StringUtils.isAnyBlank(message)){ 
			if(getCurrentUrl().contains("/order/placeorder")){

				waitVar.until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector(".order-summary")));
			}

			else if(getCurrentUrl().contains("/order/placeorder")==false){

				waitVar.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//a[text()='My Account']")));
				Assert.assertTrue(myAccountPage.getLoggedInUserText().contains("My Account"));
				logger.info("Logged in Successfully in Nearbuy Website");
			}

			if(userWantsTologout){
				myAccountPage = new MyAccountPage();
				myAccountPage.clickOnLink("Sign Out");
				waitVar.until(ExpectedConditions.elementToBeClickable(locator.loginLink));
				Assert.assertTrue(locator.loginLink.isEnabled());
				waitVar.until(ExpectedConditions.visibilityOf(locator.mainHeading));
				Assert.assertTrue(locator.mainHeading.getText().equals("Experience the world around you"));

			}
			else {
				logger.info("User wants to remain logged in");
			}
		}
		else if(getWebDriver().getCurrentUrl().contains("login/?request")){
			logger.info("User came on login page by clicking Book Now Button");
			logger.info("User has come on Order Summary Page from Deal Detail Page");
			enterUserCredentialsAndClickOnSubmit(username, password);
			if(StringUtils.isBlank(message)){
				waitVar.until(ExpectedConditions.visibilityOfAllElements(locator.headerMessages));
			}
		}

		else{
			List<String> errorMessages = Arrays.asList(message.toLowerCase().split(";"));
			for(WebElement msg : locator.error){
				Assert.assertTrue(errorMessages.contains(msg.getText().toLowerCase()));
			}
		}
		return this;
	}

	public AuthenticationPage verifyLoginAndErrorMessagesInLoginPopUp(String username, String password,String message, Boolean userWantsTologout) throws Throwable {

		//TODO: Remove Boolean argument. not required, remove sign out code form here

		//		if(!(getWebDriver().getCurrentUrl().contains("login?requestUri"))){
		//			clickOnLoginLink();
		//			logger.info("Login Link is clicked");
		enterUserCredentialsAndClickOnSubmit(username, password);
		if(StringUtils.isBlank(message)){ 
			waitVar.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//a[text()='My Account']")));
			MyAccountPage myAccountPage = new MyAccountPage();
			Assert.assertTrue(myAccountPage.getLoggedInUserText().contains("My Account"));
			logger.info("Logged in Successfully in Nearbuy Website");

			if(userWantsTologout){
				myAccountPage = new MyAccountPage();
				myAccountPage.clickOnLink("Sign Out");
				waitVar.until(ExpectedConditions.elementToBeClickable(locator.loginLink));
				Assert.assertTrue(locator.loginLink.isEnabled());
				waitVar.until(ExpectedConditions.textToBePresentInElement(getLoginLink(), "Login / Sign Up"));
				Assert.assertTrue(getLoginLink().getText().equals("Login / Sign Up"));

			}
			else {
				logger.info("User wants to remain logged in");
			}
		}
		//}
		else if(getWebDriver().getCurrentUrl().contains("login?requestUri")){
			logger.info("User came on login page by clicking Book Now Button");
			logger.info("User has come on Order Summary Page from Deal Detail Page");
			enterUserCredentialsAndClickOnSubmit(username, password);
			if(StringUtils.isBlank(message)){
				waitVar.until(ExpectedConditions.visibilityOfAllElements(locator.headerMessages));
			}
		}
		else{
			List<String> errorMessages = Arrays.asList(message.toLowerCase().split(";"));
			for(WebElement msg : locator.error){
				Assert.assertTrue(errorMessages.contains(msg.getText().toLowerCase()));
			}
		}
		return this;
	}


	/**
	 * SignUp
	 */
	public AuthenticationPage enterDetailsForSignUp(String errorMessage){

		/**
		 * Blank Values Validation
		 */
		waitVar.until(ExpectedConditions.elementToBeClickable(locator.continueButtonForSignUp));
		locator.continueButtonForSignUp.click();
		waitVar.until(ExpectedConditions.visibilityOfAllElements(locator.signUpError));
		List<String> errorMessages = Arrays.asList(errorMessage.toLowerCase().trim().split(";"));
		List<String> actualMsgs = new ArrayList<String>();
		for(WebElement msg : locator.signUpError){
			String trim = msg.getText().toLowerCase().trim();
			System.out.println("TRIM: "+trim);
			System.out.println(errorMessages);
			//	Assert.assertTrue("Error Messages are not matching expected messages",errorMessages.contains(trim));
		}


		/**
		 * Fill values & verify
		 */

		String randomMobileNumber = PageUtils.getRandomMobileNumber();
		for(WebElement input : locator.loginInputFields){

			if(input.getAttribute("formcontrolname").equals("name")){

				input.sendKeys(PageUtils.getRandomName());
			}
			if(input.getAttribute("formcontrolname").equals("email")){

				input.sendKeys(PageUtils.getRandomUserEmail());
			}
			if(input.getAttribute("formcontrolname").equals("mobile")){

				input.sendKeys(randomMobileNumber);
			}
			if(input.getAttribute("formcontrolname").equals("password")){

				input.sendKeys("Nearbuy04");
			}
		}

		/**
		 * Notify Offers Checkbox
		 */
		Assert.assertTrue(locator.notifyOffersText.isDisplayed());
		Assert.assertTrue(locator.notifyOffersText.getText().contains("Notify me about awesome offers"));
		Assert.assertTrue(locator.notifyOffersCheckbox.isEnabled());

		locator.continueButtonForSignUp.click();
		waitVar.until(ExpectedConditions.elementToBeClickable(locator.otpInput));
		Assert.assertTrue(locator.textOnOtpScreen.isDisplayed());
		Assert.assertTrue(locator.textOnOtpScreen.getText().contains("Enter the One-Time Password (OTP) that was sent to "));
		Assert.assertTrue(locator.numberTextOnOtpScreen.isDisplayed());
		Assert.assertTrue(locator.numberTextOnOtpScreen.getText().contains(randomMobileNumber));
		Assert.assertTrue(locator.recieveOtpOnCallLink.isDisplayed());
		Assert.assertTrue(locator.recieveOtpOnCallLink.isEnabled());
		Assert.assertTrue(locator.backLinkOnSignUpScreen.isDisplayed());
		Assert.assertTrue(locator.backLinkOnSignUpScreen.isEnabled());
		Assert.assertTrue(locator.recieveOtpOnCallLink.getText().contains("Resend OTP?"));

		/**
		 * Click for Blank Value
		 */
		locator.forgotYourPasswordProceedButton.click();
		waitVar.until(ExpectedConditions.visibilityOfAllElements(locator.error));
		for(WebElement msg : locator.error){
			Assert.assertTrue(msg.getText().contains("This field is required."));
		}

		locator.recieveOtpOnCallLink.click();

		WaitUtility.waitForTextToAppear(getWebDriver(), "Receive OTP on call", locator.recieveOtpOnCallLink);
		Assert.assertTrue(locator.recieveOtpOnCallLink.getText().contains("Receive OTP on call"));

		WaitUtility.waitForTextToAppear(getWebDriver(), "Call 080-66010101", locator.recieveOtpOnCallLink);
		Assert.assertTrue(locator.recieveOtpOnCallLink.getText().contains("Call 080-66010101"));

		return this;
	}

	public AuthenticationPage clickOnSignUpLink() {
		waitVar.until(ExpectedConditions.elementToBeClickable(locator.loginSignUpLink));
		Assert.assertTrue(locator.loginSignUpLink.isDisplayed());
		locator.loginSignUpLink.click();
		waitVar.until(ExpectedConditions.urlContains("sign-up"));
		//Assert.assertTrue(locator.imageOnSignUp.isDisplayed());
		Assert.assertTrue(locator.signUpForm.isDisplayed());
		//Assert.assertTrue(locator.alreadyUserLink.isEnabled());
		return this;
	}

	/**
	 * Already a registered user link
	 */
	public AuthenticationPage alreadyAUserLink(){

		Assert.assertTrue("Already a member link is not present", locator.alreadyUserLink.isDisplayed());
		Assert.assertTrue("Already a member link is not enabled", locator.alreadyUserLink.isEnabled());
		locator.alreadyUserLink.click();
		System.out.println("Already a user link is clicked");
		waitVar.until(ExpectedConditions.visibilityOfAllElementsLocatedBy(By.cssSelector(".login-popup .btn--primary")));
		Assert.assertTrue(locator.loginSignUpTabs.get(0).getAttribute("class").equals("active"));
		locator.loginSignUpTabs.get(1).click();
		waitVar.until(ExpectedConditions.visibilityOfAllElementsLocatedBy(By.cssSelector(".sign-up-poup .btn--primary")));
		Assert.assertTrue(locator.loginSignUpTabs.get(1).getAttribute("class").equals("active"));
		System.out.println("Signup link is clicked");
		return this;

	}

	/**
	 * Forgot Password Link
	 */
	public AuthenticationPage forgotPasswordLink(String emailOrmobile, String message){

		logger.info("Verify Forgot Password Screens");
		waitVar.until(ExpectedConditions.elementToBeClickable(locator.forgotYourPasswordLink));
		Assert.assertTrue(locator.forgotYourPasswordLink.isDisplayed());
		Assert.assertTrue(locator.forgotYourPasswordLink.isEnabled());
		locator.forgotYourPasswordLink.click();
		waitVar.until(ExpectedConditions.elementToBeClickable(locator.forgotYourPasswordProceedButton));
		Assert.assertTrue(locator.backLinkOnForgotPassSetOtpScreen.isEnabled());
		if(getCurrentUrl().contains("forgot-password")){
			waitVar.until(ExpectedConditions.urlContains("forgot-password"));
			Assert.assertTrue(locator.heading.getText().contains("Forgot password ?"));
		}
		Assert.assertTrue(locator.forgotPasswordText.getText().contains("A One-Time Password (OTP) that was sent to your registered Email ID / Mobile Number"));
		waitVar.until(ExpectedConditions.elementToBeClickable(locator.forgotPasswordUsernameInput));

		/**
		 * Blank Input
		 */
		locator.forgotYourPasswordProceedButton.click();
		waitVar.until(ExpectedConditions.visibilityOfAllElements(locator.error));
		for(WebElement msg : locator.error){
			Assert.assertTrue(msg.getText().contains("Please enter valid email address or mobile number."));
		}
		/**
		 * Customer does not exist.
		 */
		locator.forgotPasswordUsernameInput.sendKeys("9358118208");
		locator.forgotYourPasswordProceedButton.click();
		waitVar.until(ExpectedConditions.visibilityOf(locator.forgotYourPasswordCustomerNotExistingError));
		Assert.assertTrue(locator.forgotYourPasswordCustomerNotExistingError.getText().contains("Sorry, something went wrong."));

		/**
		 * Existing Customer
		 */
		locator.forgotPasswordUsernameInput.clear();
		locator.forgotPasswordUsernameInput.sendKeys(emailOrmobile);
		locator.forgotYourPasswordProceedButton.click();
		waitVar.until(ExpectedConditions.elementToBeClickable(locator.otpInput));
		waitVar.until(ExpectedConditions.elementToBeClickable(locator.forgotYourPasswordProceedButton));
		Assert.assertTrue(locator.forgotYourPasswordProceedButton.isEnabled());
		Assert.assertTrue(locator.forgotYourPasswordProceedButton.getText().toLowerCase().contains("set password"));
		Assert.assertTrue(locator.otpInput.isEnabled());
		Assert.assertTrue(locator.backLinkOnForgotPassSetOtpScreen.isEnabled());
		Assert.assertTrue(locator.passwordInput.isEnabled());
		Assert.assertTrue(locator.textOnForgotPasswordOtpScreen.isDisplayed());
		Assert.assertTrue(locator.textOnForgotPasswordOtpScreen.getText().contains("Enter the One-Time Password (OTP) that was sent to "));
		Assert.assertTrue(locator.numberTextOnForgotPasswordOtpScreen.isDisplayed());
		Assert.assertTrue(locator.numberTextOnForgotPasswordOtpScreen.getText().contains(emailOrmobile));
		Assert.assertTrue(locator.recieveOtpOnCallLink.isDisplayed());
		Assert.assertTrue(locator.recieveOtpOnCallLink.isEnabled());
		Assert.assertTrue(locator.recieveOtpOnCallLink.getText().contains("Resend OTP?"));

		/**
		 * Click on Set Password for Blank Values
		 */
		locator.forgotYourPasswordProceedButton.click();
		waitVar.until(ExpectedConditions.visibilityOfAllElements(locator.error));
		List<String> errorMessages = Arrays.asList(message.toLowerCase().trim().split(";"));
		List<String> actualMsgs = new ArrayList<String>();
		for(WebElement msg : locator.error) {
			String text = msg.getText().trim().toLowerCase();
			if(text.contains("\n")) {
				logger.warn("mult line text.. split on new line char");
				actualMsgs.addAll(Arrays.asList(text.split("\n")));
			}
			else {
				actualMsgs.add(text);
			}
		}
		Assert.assertTrue(errorMessages.containsAll(actualMsgs));

		locator.recieveOtpOnCallLink.click();

		WaitUtility.waitForTextToAppear(getWebDriver(), "Receive OTP on Call?", locator.recieveOtpOnCallLink);
		Assert.assertTrue(locator.recieveOtpOnCallLink.getText().contains("Receive OTP on Call?"));

		WaitUtility.waitForTextToAppear(getWebDriver(), "Call 080-66010101", locator.recieveOtpOnCallLink);
		Assert.assertTrue(locator.recieveOtpOnCallLink.getText().contains("Call 080-66010101"));

		return this;

	}

	public WebElement getLoginLink() {
		return locator.loginLink;
	}

	public AuthenticationPage navigateToHome() {
		getWebDriver().findElement(By.cssSelector(".brand_logo")).click();
		return this;
	}
	public AuthenticationPage navigateToLoginPage() {

		getWebDriver().navigate().to(baseUrl+"/login");
		WaitUtility.waitForURLtoChange(getWebDriver(), waitVar);
		waitVar.until(ExpectedConditions.urlContains("login"));
		waitVar.until(ExpectedConditions.elementToBeClickable(locator.signInSubmitButton.get(1)));
		Assert.assertTrue(locator.signInSubmitButton.get(1).isEnabled());
		return this;
	}

	/**
	 * Migrate to Nearbuy on Login Screen
	 * @return
	 */
	public AuthenticationPage clickOnInitiateMigrationButtonAndVerify(){

		Assert.assertTrue(locator.heading.getText().contains("Existing Groupon India user?"));

		Assert.assertTrue(locator.migrateSubHeading.isDisplayed());
		logger.info("SubHeading : " + locator.migrateSubHeading.getText());

		if(locator.forgotYourPasswordProceedButton.getText().equals("Migrate to nearbuy")){
			Assert.assertTrue(locator.forgotYourPasswordProceedButton.isDisplayed());
			Assert.assertTrue(locator.forgotYourPasswordProceedButton.isEnabled());
			locator.forgotYourPasswordProceedButton.click();
			waitVar.until(ExpectedConditions.urlContains("gpmerge"));
		}

		return this;
	}

	/**
	 * Initiate Migrate Screen
	 * @param username
	 * @param password
	 * @return
	 */
	public AuthenticationPage migrateToGrouponScreen(){

		Assert.assertTrue(locator.heading.isDisplayed());
		Assert.assertTrue(locator.migrateSubHeading.isDisplayed());
		Assert.assertTrue(locator.usernameInput.isDisplayed());
		Assert.assertTrue(locator.usernameInput.isEnabled());
		Assert.assertTrue(locator.passwordInput.isDisplayed());
		Assert.assertTrue(locator.passwordInput.isEnabled());
		Assert.assertTrue(locator.forgotYourPasswordLink.isEnabled());
		logger.info(locator.migrateSubHeading.getText());

		locator.forgotYourPasswordLink.click();
		waitVar.until(ExpectedConditions.urlContains("pw_reminder_form"));
		getWebDriver().navigate().back();
		waitVar.until(ExpectedConditions.elementToBeClickable(locator.initiateMigrationButton));

		return this;
	}

	/**
	 * Get Title of Login page and assert
	 * @return
	 */
	public AuthenticationPage getTitleOfTheLoginPage(){

		String currentTitle = getWebDriver().getTitle();
		Assert.assertEquals("nearbuy | Login", currentTitle);
		logger.info("Asserted the title of home page");
		return this;
	}

}
