package com.groupon.web.nearbuy.test;

import com.groupon.web.common.test.BaseTest;
import com.groupon.web.common.utils.TestListener;
import com.groupon.web.nearbuy.data.AuthenticationData;
import com.groupon.web.nearbuy.data.CreditCardData;
import com.groupon.web.nearbuy.data.CreditsData;
import com.groupon.web.nearbuy.data.MenuData;
import com.groupon.web.nearbuy.data.PaymentModeData;
import com.groupon.web.nearbuy.data.PromoData;
import com.groupon.web.nearbuy.page.AuthenticationPage;
import com.groupon.web.nearbuy.page.DealCatalogPage;
import com.groupon.web.nearbuy.page.DealDetailPage;
import com.groupon.web.nearbuy.page.HeaderFooterPage;
import com.groupon.web.nearbuy.page.OrderSummaryPage;
import com.groupon.web.nearbuy.page.PayUcheckoutPage;
import com.relevantcodes.extentreports.LogStatus;

import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
/**
 * 
 * @author Prachi Nagpal
 *
 */
@Test(groups = {"sanity", "staging","failedTransaction"})
public class FailedTransactionTest extends BaseTest {

	private AuthenticationPage authenticationPage;
	private HeaderFooterPage headerFooterPage;
	private DealDetailPage dealDetailPage;
	private DealCatalogPage dealCatalogPage;
	private OrderSummaryPage orderSummaryPage;
	private PayUcheckoutPage payUcheckoutPage;

    @BeforeMethod(alwaysRun=true)
    public void init() {

		authenticationPage = new AuthenticationPage();
		headerFooterPage = new HeaderFooterPage();
		dealDetailPage = new DealDetailPage();
		dealCatalogPage = new DealCatalogPage();
		orderSummaryPage = new OrderSummaryPage();
		payUcheckoutPage = new PayUcheckoutPage();
	}

	/**
	 * Traverse to Deal Catalog Page and click on featured Deal
	 * @param menu
	 * @param submenu
	 */
	@Test(priority = 1, dataProvider="activities" , dataProviderClass = MenuData.class )
	public void testLandingOnDealDetailPage(String mainMenu, String cat, String subCat) {
		TestListener.setLog(LogStatus.INFO, "Navigate to Deal Listing Page from Menu");
		headerFooterPage.navigateToMenu(mainMenu, cat, subCat);
		TestListener.setLog(LogStatus.INFO, "Click on Featured Deal");
		dealCatalogPage.clickOnSpecifiedDealCard(3);
	}

	/**
	 * Increase the quantity
	 * @param inc
	 * @param dec
	 */
	@Test(priority = 2)
	public void testOfferSection() {
		TestListener.setLog(LogStatus.INFO, "Verify Deal Details & OfferSection");
		dealDetailPage.dealDetailHeading().dealDetailOptionTitles().verifyOfferSectionAndClickOnOption(0, 1);
	}

	/**
	 * Click on Buy Now Button of Deal
	 */
	@Test(priority = 3)
	public void testClickOnBuyNowButton(){
		TestListener.setLog(LogStatus.INFO, "Click on Buy Now Button");
		dealDetailPage.clickOnBuyNowButton();
	}

	/**
	 * Do Success Login
	 */
	@Test(priority = 4 , dataProvider="successNumberLogin" , dataProviderClass = AuthenticationData.class )
	public void testSuccessLogin(String username, String password, String message, String bool) throws Throwable {
		TestListener.setLog(LogStatus.INFO, "Enter Credentials for Successful Login");
		authenticationPage.verifyLoginAndErrorMessages(username, password, message, Boolean.valueOf(bool));
	}

	/**
	 * Verify Order Details
	 */
	@Test(priority = 6 )
	public void testOrderDetails() {
		TestListener.setLog(LogStatus.INFO, "Verify Order Details & Price");
		orderSummaryPage.verifyOrderSummaryDetails();
	}

	/**
	 * Select the credits
	 * @param bool
	 */
	@Test(priority = 7, dataProvider="userDontWantToUseCredits" , dataProviderClass = CreditsData.class )
	public void testCreditsOnOrderSummaryPage(String bool) {
		TestListener.setLog(LogStatus.INFO, "Deselect the credits checkbox");
		orderSummaryPage.selectDeselectCredits(Boolean.valueOf(bool)).priceVerifications();
	}

	/**
	 * Enter Valid Promocode
	 * @param promo
	 * @param validPromo
	 */
	@Test(priority = 8, dataProvider="enterValidPromo" , dataProviderClass = PromoData.class )
	public void testPromoValid(String promo, String validPromo, String removePromo) {
		TestListener.setLog(LogStatus.INFO, "Apply Valid Promocode");
		orderSummaryPage.enterPromoCode(promo, Boolean.valueOf(validPromo), Boolean.valueOf(removePromo)).priceVerifications();
	}
	

	@Test(priority = 9,  dataProvider="selectCreditCard" , dataProviderClass = PaymentModeData.class )
	public void testSelectingPaymentModes(String mode) {
		TestListener.setLog(LogStatus.INFO, "Select PayU Mode of Transaction");
		orderSummaryPage.selectModeOfPayment(mode);
	}
	
	
	@Test(priority = 10,  dataProvider="enterWrongCreditCardDetails" , dataProviderClass = CreditCardData.class )
	public void testDebitCardDetails(String cardNo, String name, String cvv, String date) {
		TestListener.setLog(LogStatus.INFO, "Enter Wrong Credit Card Details Credentials");
		payUcheckoutPage.enterCreditCardDetailsAndClickOnPayNowButton(cardNo, name, cvv, date);
	}

//	@Test(priority = 11)
//	public void testProceedToPayment() {
//		TestListener.setLog(LogStatus.INFO, "Click on 'Proceed To Payment' Button");
//		orderSummaryPage.clickOnProceedToPaymentButton("cc-paynow-btn");
//	}
//
//	@Test(priority = 12)
//	public void testGoBackToNearbuyLink() {
//		TestListener.setLog(LogStatus.INFO, "Click on 'Go Back To Nearbuy' Link");
//		payUcheckoutPage.goBackToNearbuy();
//	}

	@Test(priority = 13)
	public void testFailedTransactionPage() {
		TestListener.setLog(LogStatus.INFO, "Verify Failed Transaction Message");
		orderSummaryPage.verifyFailedTransactionMessage();
	}

}
