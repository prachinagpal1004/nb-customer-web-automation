//package com.groupon.web.nearbuy.test;
//
//import com.groupon.web.common.test.BaseTest;
//import com.groupon.web.common.utils.TestListener;
//import com.groupon.web.nearbuy.data.MenuData;
//import com.groupon.web.nearbuy.data.PriceFilterData;
//import com.groupon.web.nearbuy.page.DealCatalogPage;
//import com.groupon.web.nearbuy.page.HeaderFooterPage;
//import com.groupon.web.nearbuy.page.HomePage;
//import com.relevantcodes.extentreports.LogStatus;
//
//import org.testng.annotations.BeforeMethod;
//import org.testng.annotations.Test;
///**
// * 
// * @author Prachi Nagpal
// *
// */
//@Test(groups = {"sanity", "staging"})
//public class FiltersTest extends BaseTest {
//
//	private HeaderFooterPage headerFooterPage;
//	private DealCatalogPage dealCatalogPage;
//	private HomePage homePage;
//
//	@BeforeMethod(alwaysRun=true)
//	public void init() {
//		headerFooterPage = new HeaderFooterPage();
//		dealCatalogPage = new DealCatalogPage();
//	}
//
//	/**
//	 * Navigate to particular Menu
//	 * @param menu
//	 * @param submenu
//	 * @throws Throwable
//	 */
//	@Test(priority = 1 , dataProvider="foodAndDrink" , dataProviderClass = MenuData.class )
//	public void test_Home_Pages(String menu, String submenu) throws Throwable {
//		TestListener.setLog(LogStatus.INFO, "Navigate to Deal Listing Page from Menu");
//		headerFooterPage.navigateToMenu(menu, submenu);
//	}
//
//	/**
//	 *  Test Left Small Image Widget
//	 */
//	@Test(priority = 2)
//	public void testLeftSmallImageWidget() throws Throwable {
//		TestListener.setLog(LogStatus.INFO, "Verify Left Small Image Widget");
//		dealCatalogPage.leftSmallWidgetImage();
//	}
//	
//	/**
//	 *  Test Scroll Down Link
//	 */
//	@Test(priority = 3 , dataProvider="minMaxPriceRange" , dataProviderClass = PriceFilterData.class )
//	public void testViewAllDealsLink(String minPrice, String maxPrice) throws Throwable {
//		TestListener.setLog(LogStatus.INFO, "Verify Filters Section");
//		dealCatalogPage.categoryFilter(minPrice, maxPrice);
//	}
//}
