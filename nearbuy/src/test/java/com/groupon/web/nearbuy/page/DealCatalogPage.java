package com.groupon.web.nearbuy.page;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.apache.pdfbox.TextToPDF;
import org.apache.poi.ss.util.ImageUtils;
import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.StaleElementReferenceException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import com.groupon.web.common.page.BasePage;
import com.groupon.web.common.utils.PageUtils;
import com.groupon.web.common.utils.WaitUtility;
import com.groupon.web.nearbuy.locator.DealCatalogLocator;
import com.groupon.web.nearbuy.utils.ImageServiceUtils;

public class DealCatalogPage extends BasePage {

	private static final Logger logger = Logger.getLogger(DealCatalogPage.class);
	private DealCatalogLocator locator;

	public DealCatalogPage() {
		locator = PageFactory.initElements(getWebDriver(), DealCatalogLocator.class);
	}

	/**
	 * Clicks on first featured deal
	 */
	public DealCatalogPage clickOnSpecifiedDealCard(int dealCardNo){

		waitVar.until(ExpectedConditions.visibilityOfAllElements(locator.dealCards));
		Assert.assertNotNull(locator.dealCards);
		for(WebElement deal : locator.dealCards){
			Assert.assertTrue(deal.isDisplayed());
			Assert.assertTrue(deal.isEnabled());
		}
		System.out.println(locator.dealCards.get(dealCardNo));
		locator.dealCards.get(dealCardNo).click();
		WaitUtility.waitForLoad(getWebDriver());
		waitVar.until(ExpectedConditions.visibilityOf(locator.dealDetailPage));
		logger.info("Deal Detail Page opens:"+getCurrentUrl());
		return this;
	}

	/**
	 * Scroll till the end of page
	 */
	public DealCatalogPage scrollDownToSeeAllDeals(){

		Assert.assertTrue(locator.moreDealsSection.isDisplayed());
		PageUtils.scrollToEnd(getWebDriver());
		Assert.assertTrue(locator.viewAllDealsLink.isDisplayed());
		Assert.assertTrue(locator.viewAllDealsLink.isEnabled());
		logger.info("View All deals Text : "+ locator.viewAllDealsLink.getText());
		return this;

	}

	/**
	 * Display all deal Card details
	 * @return
	 */
	public DealCatalogPage dealDetailsForEachDeal(){
		try{
			int i =1;
			for(WebElement eachImage : locator.dealImagesOnDealCards){
				Assert.assertTrue("Deal Card Image is not present", eachImage.isDisplayed());
				try {
					ImageServiceUtils.verifyImageURL(eachImage);
				} catch (IOException e1) {
					e1.printStackTrace();
				}
				logger.info("Deal Image " + i + " Link: " + eachImage.getAttribute("src"));
				i++;
			}
			int a =1;
			for(WebElement dealTitle : locator.dealTitlesOnDealCards){
				Assert.assertTrue("Deal Card Title is not present", dealTitle.isDisplayed());
				logger.info("Deal Title  " + a + " deal card : " + dealTitle.getText());
				a++;
			}
			int b =1;
			for(WebElement dealCardLocation : locator.dealLocationsOnDealCards){
				Assert.assertTrue("Deal Card Location is not present", dealCardLocation.isDisplayed());
				logger.info("Deal Location  " + b + " deal card : " + dealCardLocation.getText());
				b++;
			}
			int c =1;
			for(WebElement dealCardDescription : locator.dealDescriptionsOnDealCards){
				logger.info("Deal card no : " + c);
				Assert.assertTrue("Deal Card Description is not present", dealCardDescription.isDisplayed());
				logger.info(dealCardDescription.getText());
				c++;
			}
			int d =1;
			for(WebElement dealCardDiscountedPrice : locator.dealDiscountPricesOnDealCards){
				Assert.assertTrue("Deal Discounted Price is not present", dealCardDiscountedPrice.isDisplayed());
				logger.info("Deal Discounted Price  " + d + " deal card : " + dealCardDiscountedPrice.getText());
				d++;
			}
			int e =1;
			for(WebElement dealCardActualPrice : locator.dealActualPricesOnDealCards){
				Assert.assertTrue("Deal Actual Price is not present", dealCardActualPrice.isDisplayed());
				logger.info("Deal Actual Price  " + e + " deal card : " + dealCardActualPrice.getText());
				e++;
			}
			int y=1;
			for(WebElement deal : locator.dealBox){
				if(WaitUtility.isElementPresent(getWebDriver(), deal.findElement(By.cssSelector(".actual_price")))){
					Assert.assertTrue(deal.findElement(By.cssSelector(".card__discount")).isDisplayed());
					logger.info("Deal Discount " + y + " deal card : " + deal.findElement(By.cssSelector(".card__discount")).getText());
				}
				else if(WaitUtility.isElementPresent(getWebDriver(), deal.findElement(By.cssSelector(".bought")))){
					Assert.assertTrue("Deal Bought Counter is not present", deal.findElement(By.cssSelector(".bought")).isDisplayed());
					logger.info("Deal Bought Counter " + y + " deal card : " + deal.findElement(By.cssSelector(".bought")).getText());
				}
				y++;
			}
		}
		catch (NoSuchElementException ignored) {
			logger.info("Not present");
		}
		catch (StaleElementReferenceException ignored) {
			logger.info("Not present");
		}

		return this;
	}

	/**
	 * Scroll to top of the page
	 */
	public DealCatalogPage clickOnBackToTopButton(){

		waitVar.until(ExpectedConditions.visibilityOf(locator.scrollToTopButton));
		Assert.assertTrue(locator.scrollToTopButton.isDisplayed());
		locator.scrollToTopButton.click();
		HeaderFooterPage headerFooterPage = new HeaderFooterPage();
		//waitVar.until(ExpectedConditions.visibilityOf(headerFooterPage.getMenuNavigationBar()));
		//Assert.assertTrue(headerFooterPage.isMenuNavigationBarDisplayed());
		return this;

	}

	/**
	 * Verify Breadcrumbs
	 * @return
	 */
	public DealCatalogPage verifyBreadcrumbs(){

		waitVar.until(ExpectedConditions.visibilityOfAllElements(locator.breadcrumbsList));
		for(WebElement bread : locator.breadcrumbsList){

			Assert.assertTrue(bread.isDisplayed());
			Assert.assertTrue(bread.isEnabled());
			logger.info("Breadcrumbs are visible");
			logger.info("Breadcrumb: " + bread.getText());

			if(bread.getAttribute("class").equals("current")){
				Assert.assertTrue(getWebDriver().getCurrentUrl().contains(bread.getText().toLowerCase()));
				logger.info("Current URL contains the last breadcrumb");
				logger.info("Breadcrumb for current url : " + bread.getText());
			}
		}

		String lastBreadcrumbText = locator.lastBreadcrumb.getText();
		logger.info("Last breadcrumb text : " + lastBreadcrumbText);

		if(getWebDriver().getCurrentUrl().contains(lastBreadcrumbText.toLowerCase()))
		{
			logger.info("Breadcrumbs are working");
		} else
			logger.info("Issue with breadcrumb");

		return this;
	}

	/**
	 * Category Filter And Price filter
	 * @return
	 */
	public DealCatalogPage categoryFilter(String minPrice, String maxPrice){

		Assert.assertTrue(locator.categorySection.isDisplayed());

		for(WebElement categoryName : locator.categoryNames){

			if(categoryName.getAttribute("data-type").equals("subCategory")){
				Assert.assertTrue(categoryName.isDisplayed());
				logger.info("Category Name: " + categoryName.getText());
			}
		}

		for(WebElement heading : locator.categoryAndPriceHeadings){

			Assert.assertTrue(heading.isDisplayed());
			logger.info("Filters Heading: " + heading.getText());
		}
		for(WebElement count : locator.categoryAndPriceCounts){

			if(!count.getAttribute("class").contains("hidden")){
				Assert.assertTrue(count.isDisplayed());
				logger.info("Count: " + count.getText());
			}
		}

		if(WaitUtility.isElementPresentByLocator(getWebDriver(), By.id("ul-price-filter"))){
			Assert.assertTrue(locator.priceFilterSection.isDisplayed());

			for(WebElement priceCheckbox : locator.priceFilterCheckboxes){

				Assert.assertTrue(priceCheckbox.isDisplayed());
				Assert.assertTrue(priceCheckbox.isEnabled());
				logger.info("Price Filter Checkbox :  " + priceCheckbox);
			}

		}

		if(WaitUtility.isElementPresentByLocator(getWebDriver(), By.cssSelector("#ul-price-filter label"))){

			Assert.assertTrue(locator.enterPriceRangeLabel.isDisplayed());
			logger.info("Enter Price Filter Label :  " + locator.enterPriceRangeLabel.getText());

			for(WebElement priceLabel : locator.priceFilterLabels){

				Assert.assertTrue(priceLabel.isDisplayed());
				logger.info("Price Filter Label :  " + priceLabel);
			}

			Assert.assertTrue(locator.minPriceRangeTextbox.isDisplayed());
			Assert.assertTrue(locator.minPriceRangeTextbox.isEnabled());

			Assert.assertTrue(locator.maxPriceRangeTextbox.isDisplayed());
			Assert.assertTrue(locator.maxPriceRangeTextbox.isEnabled());

			locator.minPriceRangeTextbox.sendKeys(minPrice);
			locator.maxPriceRangeTextbox.sendKeys(maxPrice);
			logger.info("Min Price Send is :  " + minPrice);
			logger.info("Max Price Send is :  " + maxPrice);

			locator.applyPriceRangeLink.click();
			logger.info("Apply Range Link is clicked");

		}

		if(WaitUtility.isElementPresentByLocator(getWebDriver(), By.cssSelector(".filter_box.location"))){

			Assert.assertTrue(locator.viewMoreLink.isDisplayed());
			Assert.assertTrue(locator.viewMoreLink.isEnabled());
			locator.viewMoreLink.click();

			waitVar.until(ExpectedConditions.visibilityOf(locator.locationPopup));

			for(WebElement location : locator.locationHeading){

				Assert.assertTrue(location.isDisplayed());
				logger.info("Location Heading :  " + location.getText());
			}

			Assert.assertTrue(locator.resetButton.isDisplayed());
			Assert.assertTrue(locator.resetButton.isEnabled());

			Assert.assertTrue(locator.applyButton.isDisplayed());
			Assert.assertTrue(locator.applyButton.isEnabled());

			Assert.assertTrue(locator.closeButton.isDisplayed());
			Assert.assertTrue(locator.closeButton.isEnabled());

			for(WebElement count : locator.countOfDealsInPopup){

				Assert.assertNotNull(count);
				logger.info("Count :  " + count.getText());
			}

			for(WebElement label : locator.labelOfDealsInPopup){

				Assert.assertNotNull(label);
				logger.info("Label :  " + label.getText());
			}

			for(WebElement checkbox : locator.checkboxForDealsInPopup){

				Assert.assertNotNull(checkbox);
				Assert.assertTrue(checkbox.isEnabled());
			}

		}

		return this;
	}

	/**
	 * Heading and Image on Left Widget
	 * @return
	 */
	public DealCatalogPage leftSmallWidgetImage(){

		waitVar.until(ExpectedConditions.visibilityOf(locator.leftSmallImageWidget));

		Assert.assertTrue(locator.leftSmallImageWidget.isDisplayed());
		Assert.assertTrue(locator.headingOnLeftSmallImageWidget.isDisplayed());
		logger.info("Heading on Widget : " + locator.headingOnLeftSmallImageWidget.getText());


		return this;
	}

	/**
	 * Open First Sub Category available
	 * @return
	 */
	public DealCatalogPage openFirstSubCategoryPage(){

		waitVar.until(ExpectedConditions.visibilityOf(locator.categorySection));

		for(WebElement category : locator.categoryNames){

			if(category.getAttribute("data-type").equals("subCategory")){
				category.click();
				logger.info("Sub Category is clciked");
				break;
			}
		}
		return this;
	}

	/**
	 * Open First Sub Sub Category available
	 * @return
	 */
	public DealCatalogPage openFirstSubSubCategoryPage(){

		waitVar.until(ExpectedConditions.visibilityOf(locator.categorySection));

		for(WebElement category : locator.categoryNames){

			if(category.getAttribute("data-type").equals("subSubCategory")){
				category.click();
				logger.info(" Sub Sub Category is clciked");
				break;
			}
		}
		return this;
	}


	/**
	 * Verify Data Layer
	 * @return
	 */
	public DealCatalogPage verifyDataLayerCatalog() {
		//prepare map by extracting values from html
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("category", locator.dlCategory.getAttribute("value"));
		map.put("city", locator.dlCity.getAttribute("value"));
		map.put("deal-1", locator.dlDeal1.getAttribute("value"));
		map.put("deal-2", locator.dlDeal2.getAttribute("value"));
		map.put("deal-3", locator.dlDeal3.getAttribute("value"));
		map.put("dealTitle1", locator.dlDealTitle1.getAttribute("value"));
		map.put("dealTitle2", locator.dlDealTitle2.getAttribute("value"));
		map.put("dealTitle3", locator.dlDealTitle3.getAttribute("value"));
		map.put("pageType", "CategoryPage");
		map.put("sitetype", "d");
		map.put("vertical", locator.dlVertical.getAttribute("data-vertical"));

		verifyDataLayer(map,"3");
		return this;
	}

	/**
	 * Verify Sub Category Data Layer
	 * @return
	 */
	public DealCatalogPage verifyDataLayerSubCategoryPage(){

		verifyDataLayerCatalog();
		//prepare map by extracting values from html
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("subcategory", locator.dlSubcategory.getAttribute("value").toLowerCase());

		return this;
	}

	/**
	 * Verify Sub Sub Category Data Layer
	 * @return
	 */
	public DealCatalogPage verifyDataLayerSubSubCategoryPage(){

		verifyDataLayerCatalog();
		//prepare map by extracting values from html
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("subsubcategory", locator.dlSubcategory.getAttribute("value").toLowerCase());

		return this;
	}

	public DealCatalogPage openURL(String URL){

		getWebDriver().navigate().to(URL);
		return this;
	}

	public DealCatalogPage verifyRatingAndFavIcon(){

		for(WebElement rating : locator.ratingsOnDealCards){
			Assert.assertTrue("Rating Div is NOT coming", rating.isDisplayed());
			WebElement nearbuyRating = rating.findElement(By.cssSelector(".rating-score"));
			if(rating.getAttribute("class").contains("card__rating--empty")){
				Assert.assertTrue(nearbuyRating.getText().equals("-"));
			}
			if(rating.getAttribute("class").equals("card__rating")){
				Assert.assertFalse(nearbuyRating.getText().equals("-"));
			}
		}
		for(WebElement card : locator.dealCards){
			Assert.assertTrue(card.findElement(By.cssSelector(".card__favourite")).isDisplayed());
			Assert.assertTrue(card.findElement(By.cssSelector(".card__favourite")).isEnabled());
			Assert.assertTrue(card.findElement(By.cssSelector(".icon-heart")).isDisplayed());
		}
		return this;
	}


	public DealCatalogPage verifyCategoryFilters(){

		for(WebElement tree : locator.categoryFilterTree){
			Assert.assertTrue("Category Filter Full Div is NOT coming", tree.isDisplayed());
			Assert.assertTrue("Category Filter Heading is NOT coming", tree.isDisplayed());
			Assert.assertTrue("Category Filter Tree is NOT coming", tree.isDisplayed());
			Assert.assertTrue("Category Filter Tree is NOT opened", tree.findElement(By.cssSelector("li")).getAttribute("class").equals("open"));
			WebElement mainCategory = tree.findElement(By.cssSelector(".menu-header span:first-child"));
			Assert.assertNotNull("Category Filter Main Menu is Empty", mainCategory.getText());
			Assert.assertTrue("Category Filter Main Menu is NOT displayed", mainCategory.isDisplayed());
			Assert.assertNotNull("Category Filter Main Menu Count is NOT null", tree.findElement(By.cssSelector(".menu-header .count")).getText());
			Assert.assertTrue("Category Filter Main Menu Count is NOT displayed", tree.findElement(By.cssSelector(".menu-header .count")).isDisplayed());
			Assert.assertTrue("Category Name in Page Heading is NOT matching with category opened", locator.categoryTextInPageHeading.getText().equals(mainCategory.getText()));
			Assert.assertTrue("Offers In text NOT coming in Page Heading", locator.offersInTextInPageHeading.getText().contains(" Offers in "));
			System.out.println(locator.locationTextInPageHeading.getText().toLowerCase());
			System.out.println(new HeaderFooterPage().getCurrentLocationSelected().toLowerCase());
			//TODO//			Assert.assertTrue("Location Name in Page Heading is NOT matching with the current Location Selected", locator.locationTextInPageHeading.getText().toLowerCase().equals(getCurrentUrl().substring(getCurrentUrl().indexOf("offers/"), getCurrentUrl().indexOf("/"))));

			if(tree.findElement(By.cssSelector(".menu-header")).getAttribute("class").contains("menu-header-arrow-hide")){
				System.out.println("No sub category Menu present for category name : " + tree.findElement(By.cssSelector("span")).getText());
				Assert.assertTrue("Category Menu which have no subcategory is NOT enabled", tree.isEnabled());
			}
			else if(tree.findElement(By.cssSelector(".menu-header")).getAttribute("class").equals("menu-header")) {
				System.out.println("More sub category Menu present for category name : " + tree.findElement(By.cssSelector("span")).getText());
				Assert.assertTrue("Category Menu which have subcategory is NOT enabled", tree.isEnabled());
				//tree.click();
				Assert.assertTrue(tree.findElement(By.cssSelector("li")).getAttribute("class").equals("open"));

				if(tree.findElement(By.cssSelector(".sub-menu-header")).getAttribute("class").equals("sub-menu-header")){
					String subCategoryName = tree.findElement(By.cssSelector(".sub-menu-header span")).getText();
					System.out.println("More sub sub category Menu present for sub category name : " + subCategoryName);
					tree.findElement(By.cssSelector(".sub-menu-header span")).click();
					try {
						Thread.sleep(1000);
					} catch (InterruptedException e) {
						e.printStackTrace();
					}
					waitVar.until(ExpectedConditions.urlContains(subCategoryName.toLowerCase()));
					System.out.println(subCategoryName + " : is clicked");
					waitVar.until(ExpectedConditions.presenceOfElementLocated(By.cssSelector("ul.list-block.sub-menu-options.active")));
					Assert.assertTrue(getWebDriver().findElement(By.cssSelector(".sub-menu-options")).getAttribute("class").contains("active"));
					Assert.assertTrue(getWebDriver().findElement(By.cssSelector(".sub-menu-options span")).isDisplayed());
					Assert.assertTrue("Sub Category Name in Page Heading is NOT matching with sub category opened", locator.categoryTextInPageHeading.getText().equals(subCategoryName));
					System.out.println(getCurrentUrl());
					getWebDriver().findElement(By.cssSelector(".open .sub-menu .open .sub-menu-options--header span")).click();
					try {
						Thread.sleep(1000);
					} catch (InterruptedException e) {
						e.printStackTrace();
					}

					System.out.println("Sub Sub category : " + getWebDriver().findElement(By.cssSelector(".open .sub-menu .open .sub-menu-options--header span")).getText() + " : is clicked");
					waitVar.until(ExpectedConditions.textToBePresentInElement(By.cssSelector(".city-name h1"), getWebDriver().findElements(By.cssSelector("a.sub-menu-options--header")).get(0).getText()));

					WaitUtility.waitForURLtoChange(getWebDriver(), waitVar);
					Assert.assertTrue("Sub Category Name in Page Heading is NOT matching with sub category opened", locator.categoryTextInPageHeading.getText().equals(getWebDriver().findElement(By.cssSelector(".open .sub-menu .open .sub-menu-options--header span")).getText()));
					break;
				}
				else if(tree.findElement(By.cssSelector(".sub-menu-header")).getAttribute("class").contains("sub-menu-header-arrow-hide")){
					Assert.assertTrue(tree.findElement(By.cssSelector(".sub-menu-header")).getAttribute("class").contains("sub-menu-header-arrow-hide"));
					Assert.assertTrue(tree.findElement(By.cssSelector(".sub-menu-header span")).isDisplayed());
					tree.findElement(By.cssSelector(".sub-menu-header")).click();
					WaitUtility.waitForURLtoChange(getWebDriver(), waitVar);
					break;

				}

			}
		}
		return this;
	}

	public DealCatalogPage verifyLocationFilter(String locationSentInSearchBox){

		Assert.assertTrue("Location Filter Main Heading is Displayed",locator.locationFilterMainHeading.isDisplayed());
		Assert.assertTrue("Location Filter SearchBox is Displayed",locator.locationFilterSearchbox.isDisplayed());
		Assert.assertTrue("Location Filter SearchBox is Enabled",locator.locationFilterSearchbox.isEnabled());

		for(WebElement checkbox : locator.allLocationCheckboxes){
			Assert.assertTrue("Location Checkbox is displayed", checkbox.isDisplayed());
			Assert.assertTrue("Location Checkbox is enabled", checkbox.isEnabled());
		}

		for(WebElement count : locator.locationFilterCounts){
			Assert.assertTrue("Location Filter Count is displayed", count.isDisplayed());
		}

		for(WebElement name : locator.locationFilterName){
			Assert.assertTrue("Location Filter Name is displayed", name.isDisplayed());
		}

		for(WebElement sublocation : locator.subLocationWrapper){
			Assert.assertTrue("Location Filter Name is displayed", sublocation.isDisplayed());
		}

		verifyViewMoreLinkInLocationFilters();

		for(WebElement location : locator.locationFilterHeadings){
			Assert.assertTrue("Location Heading is displayed", location.isDisplayed());
			if(location.findElement(By.cssSelector(".sublocation-wrapper li")).getSize().toString().length()>=5){
				Assert.assertTrue("View More link is not visible even if 5 or greater number of sublocations are present",location.findElement(By.cssSelector(".filter-location .view-more")).isDisplayed());
				Assert.assertTrue("View More link is not enabled",location.findElement(By.cssSelector(".filter-location .view-more")).isEnabled());
				locator.locationFilterSearchbox.sendKeys(locationSentInSearchBox);
				waitVar.until(ExpectedConditions.textToBePresentInElementLocated(By.cssSelector(".padding-left-xs"), locationSentInSearchBox));
				for(WebElement subLocation : locator.subLocationListAfterUserSearched){
					System.out.println("sublocation Text : " + subLocation.getText().substring(0, subLocation.getText().indexOf("(")).trim().toLowerCase());
					Assert.assertTrue("Location Searched in Text box doesn't matches with the location in the filter list",subLocation.getText().substring(0, subLocation.getText().indexOf("(")).trim().toLowerCase().contains(locationSentInSearchBox.toLowerCase()));
					break;
				}
				WebElement checkBoxUserWantsToClick = locator.subLocationListAfterUserSearched.get(0);
				String textOfSubLocationUserWantsToTick = checkBoxUserWantsToClick.getText().substring(0, checkBoxUserWantsToClick.getText().indexOf("("));
				int actualTextInCHecboxUserHasclicked = Integer.parseInt(checkBoxUserWantsToClick.findElement(By.cssSelector("span")).getText().substring(1, 2));
				checkBoxUserWantsToClick.click();
				try {
					Thread.sleep(2000);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
				waitVar.until(ExpectedConditions.urlContains("locations"));
				waitVar.until(ExpectedConditions.presenceOfElementLocated(By.cssSelector(".nb-checkbox input:checked+.nb-checkbox__bg")));
				waitVar.until(ExpectedConditions.refreshed(ExpectedConditions.presenceOfAllElementsLocatedBy(By.cssSelector("#card-clearfix .row"))));
				try {
					Thread.sleep(2000);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
				System.out.println(textOfSubLocationUserWantsToTick);
				Assert.assertEquals(actualTextInCHecboxUserHasclicked,locator.dealCards.size());

				//Assert.assertTrue("Checkbox is NOt marked as red", locator.checkedBoxInRedColor.getAttribute("background-color").equals("#ef534e"));
				//Assert.assertTrue("Checkbox is NOt marked as red", locator.checkedBoxInRedColor.getAttribute("border-color").equals("#ef534e"));
				for(WebElement card : locator.locationInEachCard){
					Assert.assertTrue("Selected Location is NOT coming in Deal Card", card.getText().contains(textOfSubLocationUserWantsToTick.trim()) || card.getText().contains("Locations"));
				}
			}
		}

		return this;
	}

	private void verifyViewMoreLinkInLocationFilters() {

		waitVar.until(ExpectedConditions.elementToBeClickable(locator.viewMoreLink));
		Assert.assertTrue("View More link is NOT displayed", locator.viewMoreLink.isDisplayed());
		Assert.assertTrue("View More link is NOT enabled", locator.viewMoreLink.isEnabled());
		locator.viewMoreLink.click();
		waitVar.until(ExpectedConditions.visibilityOf(locator.viewMoreContainerModal));
		Assert.assertTrue("Heading in view more popup is NOT coming", locator.viewMoreModalHeading.isDisplayed());
		Assert.assertTrue(locator.viewMoreModalHeading.getText().equals("Location"));
		Assert.assertTrue("View more Search box is NOT displayed", locator.viewMoreSearchBox.isDisplayed());
		Assert.assertTrue("View more Search box is NOT enabled", locator.viewMoreSearchBox.isEnabled());
		Assert.assertTrue("View more Search box placeholder is NOT coming", locator.viewMoreSearchBox.getAttribute("placeholder").equals("Search for a location"));

		for(WebElement heading : locator.viewMorePopupMainLocationHeadings){
			Assert.assertTrue(heading.isDisplayed());
		}
		for(WebElement text : locator.viewMorePopUpSubLocationText){
			Assert.assertTrue(text.isDisplayed());
			Assert.assertTrue(text.isEnabled());
		}
		for(WebElement count : locator.viewMorePopUpDealCountInSublocation){
			Assert.assertTrue(count.isDisplayed());
		}
		Assert.assertTrue("View more PopUp Close Button is NOT displayed", locator.viewMorePopUpCLoseButton.isDisplayed());
		Assert.assertTrue("View more PopUp Close Button is NOT enabled", locator.viewMorePopUpCLoseButton.isEnabled());

		Assert.assertTrue("View more PopUp Clear Button is NOT displayed", locator.viewMorePopUpClearButton.isDisplayed());
		Assert.assertTrue("View more PopUp Clear Button is NOT enabled", locator.viewMorePopUpClearButton.isEnabled());

		Assert.assertTrue("View more PopUp Apply Button is NOT displayed", locator.viewMorePopUpApplyButton.isDisplayed());
		Assert.assertTrue("View more PopUp Apply Button is NOT enabled", locator.viewMorePopUpApplyButton.isEnabled());

		locator.viewMorePopUpSubLocationText.get(0).click();
		locator.viewMorePopUpSubLocationText.get(1).click();
		locator.viewMorePopUpApplyButton.click();

		locator.viewMoreLink.click();
		waitVar.until(ExpectedConditions.visibilityOf(locator.viewMoreContainerModal));

		locator.viewMorePopUpSubLocationText.get(2).click();
		locator.viewMorePopUpSubLocationText.get(3).click();

		locator.viewMorePopUpClearButton.click();
		try {
			Thread.sleep(1000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		locator.viewMorePopUpApplyButton.click();

		waitVar.until(ExpectedConditions.invisibilityOfElementLocated(By.cssSelector("#locationContainer")));
	}

	public DealCatalogPage verifySort(String sortOptionToBeClicked){

		Assert.assertTrue("Sort Div is NOT visible", locator.sortDiv.isDisplayed());
		Assert.assertTrue("Popular Sort is NOT set by default", locator.activeSortOption.isDisplayed());
		Assert.assertTrue("Popular Sort is NOT set by default", locator.activeSortOption.getText().equals("Popular"));
		Assert.assertTrue(locator.sortOptions.get(0).getText().equals("Popular"));
		Assert.assertTrue(locator.sortOptions.get(1).getText().equals("What's New"));
		Assert.assertTrue(locator.sortOptions.get(2).getText().equals("Price (High to Low)"));
		Assert.assertTrue(locator.sortOptions.get(3).getText().equals("Price (Low to High)"));
		for(WebElement sort : locator.sortOptions){
			Assert.assertTrue(sort.isDisplayed());
			Assert.assertTrue(sort.isEnabled());
		}
		if(sortOptionToBeClicked.equals("What's New")){
			locator.sortOptions.get(1).click();
			waitVar.until(ExpectedConditions.elementToBeSelected(locator.sortOptions.get(1)));
			waitVar.until(ExpectedConditions.refreshed(ExpectedConditions.presenceOfAllElementsLocatedBy(By.cssSelector("#card-clearfix .row"))));
			Assert.assertTrue("What's New is NOT selected", locator.sortOptions.get(1).getAttribute("class").equals("active"));
		}
		boolean sorted = true;  
		if(sortOptionToBeClicked.equals("Price (High to Low)")){
			locator.sortOptions.get(2).click();
			waitVar.until(ExpectedConditions.elementToBeSelected(locator.sortOptions.get(2)));
			waitVar.until(ExpectedConditions.refreshed(ExpectedConditions.presenceOfAllElementsLocatedBy(By.cssSelector("#card-clearfix .row"))));
			Assert.assertTrue("Price (High to Low) is NOT selected", locator.sortOptions.get(2).getAttribute("class").equals("active"));
			for (int i = 1; i < locator.nearbuyPrices.size(); i++) {
				//TODO // if (locator.nearbuyPrices.get(i-1).compareTo(locator.nearbuyPrices.get(i)) > 0) sorted = false; 
			}
		}

		return this;
	}
}