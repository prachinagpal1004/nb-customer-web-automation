package com.groupon.web.nearbuy.data;

import com.groupon.web.common.config.Config;
import com.groupon.web.common.utils.DataProviderUtil;
import com.groupon.web.nearbuy.utils.ApiDataUtils;
import org.testng.annotations.DataProvider;

import java.util.Arrays;
import java.util.List;

/**
 *
 * @author prachi.nagpal
 *
 */
public class DealData {

    @DataProvider(name = "dealDetailData")
    public static Object[][] provideDealDetails(){

        //Fetch DealIds from excel
        String[][] dealIds = DataProviderUtil.provideData(Config.getInstance().getConfig("testData.file"), "nearbuy", "dealDetailData");

        int i = 0;
        String[][] dealData = new String[dealIds.length][]; //Initialize dealData result array

        //Iterate on each dealId, Fetch its details from API & set the details into dealData array
        for (String[] deal : dealIds) {
            String dealId = deal[0];
            String dealUrl = deal[1];
            List<String> properties = Arrays.asList(dealId, dealUrl);

            //Fetch Deal Details for Given DealId
           // properties.addAll(ApiDataUtils.getDealDetailByDealId(dealId));

            //Add list to result dealData array
            dealData[i++] = properties.toArray(new String[properties.size()]);
        }
        return dealData;
    }
}
