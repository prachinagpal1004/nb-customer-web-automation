package com.groupon.web.nearbuy.page;

import com.groupon.web.common.page.BasePage;
import com.groupon.web.common.utils.DatePickerFromCalenderUtility;
import com.groupon.web.common.utils.WaitUtility;
import com.groupon.web.nearbuy.locator.DealDetailLocator;
import com.groupon.web.nearbuy.locator.HeaderFooterLocator;

import java.io.IOException;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;


public class DealDetailPage extends BasePage {

	public DealDetailLocator locator;
	public HeaderFooterLocator header;
	private static final Logger logger = Logger.getLogger(DealDetailPage.class);

	public DealDetailPage() {
		locator = PageFactory.initElements(getWebDriver(), DealDetailLocator.class);
		header = PageFactory.initElements(getWebDriver(), HeaderFooterLocator.class);
	}

	/**
	 * Deal Heading
	 * @return
	 */
	public DealDetailPage dealDetailHeading(){
		return dealDetailHeading(null);
	}

	public DealDetailPage dealDetailHeading(String expectedTitle){

		waitVar.until(ExpectedConditions.visibilityOf(locator.dealImageDiv));
		//waitVar.until(ExpectedConditions.visibilityOf(locator.dealHeading));
		Assert.assertNotNull("Deal Heading is Not Present",locator.dealHeading);
		Assert.assertTrue("Deal Heading is Not Displayed",locator.dealHeading.isDisplayed());
		String text = locator.dealHeading.getText();
		logger.info("Deal Title on Detail Page:"+text);
		if(expectedTitle != null) {
			Assert.assertEquals(text.trim().toLowerCase(), expectedTitle.trim().toLowerCase());
		}
		return this;
	}

	/**
	 * Click on Book Now Button
	 * @return
	 */
	public DealDetailPage clickOnBuyNowButton(){

		waitVar.until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector(".block--secondary.o-available")));
		Assert.assertTrue(locator.buyNowButton.isEnabled());
		locator.buyNowButton.click();
		logger.info("Nuy Now Button on Deal Detail Page is clicked");
		waitVar.until(ExpectedConditions.visibilityOf(locator.loginModal));

		return this;
	}

	/**
	 * Verify Offer Label , Offer Text And Select Quantity & Total Amount Text [OLD UI CODE]
	 * @return
	 */
	//	public DealDetailPage verifyTextsInEachLocalOffer(){
	//
	//		verifyHeadingTextOnOptionsOrOffers();
	//
	//		for(WebElement label : locator.offerLabels){
	//			label.isDisplayed();
	//			logger.info("Offer label : " + label.getText());
	//		}
	//
	//		for(WebElement heading : locator.offerHeading){
	//			heading.isDisplayed();
	//			logger.info("Offer Heading : " + heading.getText());
	//		}
	//
	//		for(WebElement qtyAndTotal : locator.selectQtyAndTotalText){
	//			qtyAndTotal.isDisplayed();
	//			logger.info("Select Quantity & Total Text : " + qtyAndTotal.getText());
	//		}
	//
	//		for(WebElement discount : locator.percentageDiscounts){
	//			discount.isDisplayed();
	//			logger.info("Percentage Discount : " + discount.getText());
	//		}
	//
	//		for(WebElement actual : locator.actualPrice){
	//			actual.isDisplayed();
	//			logger.info("Actual Price : " + actual.getText());
	//		}
	//
	//		for(WebElement price : locator.priceAfterDiscount){
	//			price.isDisplayed();
	//			logger.info("Price After Discount : " + price.getText());
	//		}
	//
	//		for(WebElement totPrice : locator.totalPrice){
	//			totPrice.isDisplayed();
	//			logger.info("Total Price : " + totPrice.getText());
	//		}
	//
	//		return this;
	//	}

	/**
	 * Verify the heading text on Offers/Options [OLD UI CODE]
	 * @return
	 */
	//	public DealDetailPage verifyHeadingTextOnOptionsOrOffers() {
	//
	//		if(WaitUtility.isElementPresent(getWebDriver(), header.categoryLabelName)){
	//			if(header.categoryLabelName.getText().equals("TRAVEL")){
	//				Assert.assertTrue(locator.chooseOfferHeading.getText().contains("Choose Your Dates"));
	//			}
	//
	//			else if(header.categoryLabelName.getText().equals("PRODUCTS")){
	//				logger.info("Label name :" + header.categoryLabelName.getText());
	//				if(locator.numberOfOptions.size()>1)
	//					Assert.assertTrue(locator.chooseOfferHeading.getText().contains("Select an Option"));
	//				logger.info("Text : " + locator.selectOptionText.getText());
	//				if(locator.numberOfOptions.size()==1)
	//					Assert.assertTrue(locator.chooseOfferHeading.getText().contains("Offer Details"));
	//				logger.info("Text : " + locator.selectOptionText.getText());
	//			}
	//		}
	//
	//		else {
	//			Assert.assertTrue(locator.chooseOfferHeading.getText().contains("Choose An Offer"));
	//		}
	//
	//		return this;
	//	}


	/**
	 * Verify quantity Increase and Decrease Function [OLD UI CODE]
	 * @return
	 */
	//	public DealDetailPage verifyQuantityIncDec(){
	//
	//		Assert.assertTrue(locator.quantity.getText().equals("0"));
	//		logger.info("No of Minus Icons: " + locator.quantityMinusIcons.size());
	//		logger.info("No of Plus Icons : " + locator.quantityPlusIcons.size());
	//
	//
	//		a: for(WebElement minus : locator.quantityMinusIcons){
	//
	//			Assert.assertTrue(minus.getAttribute("class").contains("quantity-btn-deactive"));
	//			logger.info("Minus Text : " + minus.getText());
	//
	//			scrollDownIfLeftNavigationOnOfferPresent();
	//			continue a;
	//		}
	//
	//
	//
	//		b:	for(WebElement plus : locator.quantityPlusIcons){
	//
	//			Assert.assertFalse(plus.getAttribute("class").contains("quantity-btn-deactive"));
	//			logger.info("Plus Text : " + plus.getText());
	//
	//			scrollDownIfLeftNavigationOnOfferPresent();
	//			continue b;
	//		}
	//
	//		return this;
	//	}

	private void scrollDownIfLeftNavigationOnOfferPresent() {
		if(locator.verticalDraggerOnOffers.isDisplayed()){
			Assert.assertTrue(locator.verticalDraggerOnOffers.isDisplayed());
			//new Actions(getWebDriver()).moveToElement(locator.offerSection).keyDown(Keys.CONTROL).sendKeys(Keys.END).perform();

			JavascriptExecutor js;
			js = (JavascriptExecutor) getWebDriver();
			js.executeScript("window.scrollBy(0,250)",locator.verticalDraggerOnOffers);
			return;

		}
		else{
			Assert.assertFalse(locator.verticalDraggerOnOffers.isDisplayed());
			logger.info("Vertical Dragger Not Present");
		}
	}

	/**
	 * Inc/Dec Qty And Verify Price [OLD UI CODE BUT CAN BE USED AGAIN]
	 * @param qtyToBeInc
	 * @param qtyToBeDec
	 * @return
	 */
	public DealDetailPage incOrDecQtyAndVerifyPrice(int qtyToBeInc, int qtyToBeDec){

		for(int i=1; i<=qtyToBeInc; i++){

			locator.activeQuantityPlusIcons.click();
			try {
				Thread.sleep(1000);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			logger.info("Clicked Time : "+ i);

			//			for(WebElement plus : locator.quantityPlusIcons){
			//
			//				plus.click();
			//				logger.info("First Offers Qty increased");
			//				break;
			//			}
		}


		return this;
	}

	/**
	 * Select date from calender [OLD UI]
	 * @return
	 */
	//	public DealDetailPage selectDateFromCalender(){
	//
	//		try {
	//			new DatePickerFromCalenderUtility().datePickerFromCalender();
	//		} catch (IOException e) {
	//			// TODO Auto-generated catch block
	//			e.printStackTrace();
	//		}
	//		return this;
	//	}

	/**
	 * Select from options available [OLD UI CODE]
	 * @return
	 */
	//	public DealDetailPage selectFromOptions(int optionToBeSelected, int qtyToBeInc, int qtyToBeDec){
	//
	//
	//		verifyHeadingTextOnOptionsOrOffers();
	//		logger.info("Heading Text : " + locator.chooseOfferHeading.getText());
	//		String productPrice = locator.productTotal.getText();
	//
	//		if(locator.numberOfOptions.size()>1){
	//			locator.numberOfOptions.get(optionToBeSelected).click();
	//		}
	//		if(locator.numberOfOptions.size()==1){
	//			Assert.assertTrue(locator.radioButtonCheckbox.getAttribute("class").contains("checked"));
	//		}
	//
	//
	//		Assert.assertFalse(productPrice.equals(0));
	//
	//		for(WebElement text : locator.selectQtyAndTotalText){
	//
	//			Assert.assertTrue(text.isDisplayed());
	//			logger.info("Text : " + text.getText());
	//		}
	//		Assert.assertTrue(locator.productDiscount.isDisplayed());
	//		logger.info("Product Discount : " + locator.productDiscount.getText());
	//
	//		Assert.assertTrue(locator.productActualPrice.isDisplayed());
	//		logger.info("Product Actual Price : " + locator.productActualPrice.getText());
	//
	//		Assert.assertTrue(locator.productDiscountPrice.isDisplayed());
	//		logger.info("Product Discount Price : " + locator.productDiscountPrice.getText());
	//
	//		clickOnBuyNowButton();
	//
	//		// Assertions for Error Message
	//		waitVar.until(ExpectedConditions.visibilityOf(locator.qtyErrorMessage));
	//		Assert.assertEquals("Please select quantity", locator.qtyErrorMessage.getText());
	//		logger.info("Error Text : " + locator.qtyErrorMessage.getText());
	//
	//		// If more than one options are present
	//		if(locator.optionsCheckbox.size()>1){
	//
	//			locator.optionsCheckbox.get(optionToBeSelected).click();
	//
	//			// Display text of label that is selected
	//			for(WebElement option : locator.optionsLabelText){
	//
	//				if(option.isSelected()){
	//					logger.info("Selected option : " + option.getText());
	//				}
	//			}
	//
	//			incOrDecQtyAndVerifyPrice(qtyToBeInc, qtyToBeDec);
	//			clickOnBuyNowButton();
	//
	//		}
	//
	//		else
	//		{
	//			incOrDecQtyAndVerifyPrice(qtyToBeInc, qtyToBeDec);
	//			clickOnBuyNowButton();
	//		}
	//
	//		return this;
	//	}

	/**
	 * Verify Image Slider And Carousel
	 * @return
	 */
	public DealDetailPage verifySliderAndCarousel(){

		//PageUtils.validateInvalidImages(getWebDriver());
		logger.info("Verified all the images in page");

		for(WebElement mainImage : locator.mainImageSlider){

			if(mainImage.getAttribute("class").contains("flex-active-slide")){
				Assert.assertTrue("No Active Deal Image is present", mainImage.getAttribute("class").contains("flex-active-slide"));

				//				for(WebElement carousel : locator.carouselSlider){
				//					if(carousel.getAttribute("class").contains("flex-active-slide")){
				//						Assert.assertTrue(carousel.getAttribute("class").contains("flex-active-slide"));
				//					}
				//				}
			}

		}

		if(WaitUtility.isElementPresent(getWebDriver(), locator.sliderNextButton)){

			logger.info("More than one image is present");
			for(WebElement next : locator.sliderNextPrevIcons){

				if(next.getAttribute("class").contains("flex-prev")){
					Assert.assertTrue(next.isDisplayed());
					Assert.assertTrue(next.isEnabled());
					next.click();
					logger.info("Prev icon is clicked");
				}
				else if(next.getAttribute("class").contains("flex-next")){
					Assert.assertTrue(next.isDisplayed());
					Assert.assertTrue(next.isEnabled());
					next.click();
					logger.info("Next icon is clicked");
				}
			}

			//			for(WebElement next : locator.carouselNextPrevIcons){
			//
			//				if(next.getAttribute("class").contains("flex-prev")){
			//					Assert.assertTrue(next.isDisplayed());
			//					Assert.assertTrue(next.isEnabled());
			//					next.click();
			//					logger.info("Carousel Prev icon is clicked");
			//				}
			//				else if(next.getAttribute("class").contains("flex-next")){
			//					Assert.assertTrue(next.isDisplayed());
			//					Assert.assertTrue(next.isEnabled());
			//					next.click();
			//					logger.info("Carousel Next icon is clicked");
			//				}
			//			}
		}

		return this;
	}

	/**
	 * More Deal Section
	 * @return
	 */
	public DealDetailPage moreDealSections(){

		for(WebElement heading : locator.moreDealHeadings){

			Assert.assertTrue("Heading is not displayed", heading.isDisplayed());
			logger.info("Heading : " + heading);
		}

		for(WebElement block : locator.moreDealBlocks){

			Assert.assertTrue("More deal block is not dispayed", block.isDisplayed());
			Assert.assertTrue("More deal block is not enabled",block.isEnabled());
		}

		for(WebElement text : locator.moreDealBlocksTextOnHover){

			//new Actions(getWebDriver()).moveToElement(text).build().perform();
			//waitVar.until(ExpectedConditions.visibilityOf(text));
			Assert.assertNotNull("More deal block is not dispayed", text);
			logger.info("Block Text On Hover : " + text.getText());
		}

		return this;
	}

	/**
	 * Verify all Offers are not sold out
	 * @return
	 */
	public DealDetailPage verifyNotAllLocalOffersAreSoldOut(){

		logger.info("No. Of Offers Present : " + locator.offersList.size());

		int count = 0 ;
		for(WebElement offer : locator.offersList){

			Assert.assertTrue("Offer is not Displayed", offer.isDisplayed());
			logger.info("Offer is displayed");
			Assert.assertTrue("Offer is not Enabled",offer.isEnabled());
			logger.info("Offer is enabled");
			if(offer.getAttribute("class").contains("sold_out disabled")){
				count++;
				logger.info("Sold Out Offer is Present");
			}
		}
		logger.info("No. of Sold Out Offers : " + count);
		if(locator.offersList.size() == count){
			logger.info("All Offers are Sold Out which should never be the case");
		}

		return this;
	}

	/**
	 * Verify Tags
	 * @return
	 * @throws MalformedURLException
	 * @throws IOException
	 */
	public DealDetailPage verifyTags(){

		for(WebElement tag : locator.nearbuyTags){

			Assert.assertTrue(tag.isDisplayed());
			Assert.assertTrue(tag.isEnabled());
			logger.info("Tag Name : " + tag.getText());

		}

		//PageUtils.findBrokenLinks(getWebDriver());

		return this;
	}
	/**
	 * Sharing social the deals
	 * @return
	 */
	public DealDetailPage shareThisDeal(){

		Assert.assertTrue(locator.shareThisDealSection.isDisplayed());
		Assert.assertTrue(locator.shareThisDealLabel.isDisplayed());
		logger.info("Text Present : " + locator.shareThisDealLabel.getText());

		for(WebElement share : locator.shareThisDealIcons){

			Assert.assertTrue(share.isDisplayed());
			Assert.assertTrue(share.isEnabled());

			//PageUtils.findBrokenLinks(getWebDriver());

			// Store the current window handle
			String winHandleBefore = getWebDriver().getWindowHandle();

			logger.info("Parent window : " + winHandleBefore);

			String anchorAttribute = share.getAttribute("title");
			if(anchorAttribute.equals("Facebook")){
				// Perform the click operation that opens new window
				share.click();
				logger.info(anchorAttribute + " : Link is clicked");
				// Switch to new window opened
				for(String winHandle : getWebDriver().getWindowHandles()){
					getWebDriver().switchTo().window(winHandle);
					if(getWebDriver().getTitle().contains("Facebook")){
						System.out.println(" You are in Facebook window");
						//break;
					}
				}
				logger.info("Asserting new opened window");
				Assert.assertTrue(getWebDriver().getTitle().contains(anchorAttribute));
				logger.info("New window URL : " + getWebDriver().getCurrentUrl());
				logger.info("New Window Title : " + getWebDriver().getTitle());

				// Close the new window, if that window no more required
				getWebDriver().close();

				getWebDriver().switchTo().window(winHandleBefore);
				logger.info(getWebDriver().getCurrentUrl());
				// Switch back to original browser (first window)

			}

			else if(anchorAttribute.equals("Twitter")){
				// Perform the click operation that opens new window
				share.click();
				logger.info(anchorAttribute + " : Link is clicked");
				// Switch to new window opened
				for(String winHandle : getWebDriver().getWindowHandles()){
					getWebDriver().switchTo().window(winHandle);
					if(getWebDriver().getTitle().contains("Twitter")){
						System.out.println(" You are in Twitter window");
					}
				}
				Assert.assertTrue(getWebDriver().getTitle().contains(anchorAttribute));
				logger.info("New window URL : " + getWebDriver().getCurrentUrl());
				logger.info("New Window Title : " + getWebDriver().getTitle());

				// Close the new window, if that window no more required
				getWebDriver().close();

				getWebDriver().switchTo().window(winHandleBefore);
				logger.info(getWebDriver().getCurrentUrl());
				// Switch back to original browser (first window)

			}
		}

		return this;
	}

	/**
	 * Verify Maps
	 * @return
	 */
	public DealDetailPage verifyMaps(){
		if(WaitUtility.isElementPresent(getWebDriver(), locator.mapsLocationContainer)){

			Assert.assertTrue("Maps Heading is Not Displayed",locator.mapsLocationHeading.isDisplayed());
			Assert.assertEquals("Redemption locations", locator.mapsLocationHeading.getText());
			Assert.assertTrue("Maps Container is Not Displayed",locator.mapsLocationContainer.isDisplayed());
			Assert.assertTrue("Maps Container is Not Enabled",locator.mapsLocationContainer.isEnabled());
			Assert.assertTrue("Location Address on Map is Not Displayed",locator.mapsLocationAddress.get(0).isDisplayed());
			Assert.assertTrue("Location Address on Map is Not Enabled",locator.mapsLocationAddress.get(0).isEnabled());
			logger.info("Map Location Heading : " + locator.mapsLocationHeading.getText());
			if(locator.mapsLocationAddress.size()>2){
				Assert.assertTrue(locator.viewAllLocationsButotn.isDisplayed());
				Assert.assertTrue(locator.viewAllLocationsButotn.isEnabled());
			}

			locator.mapsLocationContainer.click();
			waitVar.until(ExpectedConditions.presenceOfElementLocated(By.cssSelector(".location__cont__map")));
			Assert.assertTrue(locator.locationPopUp.getAttribute("class").contains("modal-show"));
			Assert.assertTrue(locator.mapsLocationAddress.get(0).getAttribute("class").equals("active"));
			if(locator.mapsLocationAddress.size()>1){
				locator.mapsLocationAddress.get(1).click();
			}
			locator.locationPopUpCloseButton.click();
			waitVar.until(ExpectedConditions.invisibilityOfElementLocated(By.cssSelector(".location__cont__map")));
			Assert.assertTrue(locator.locationPopUp.getAttribute("class").contains("modal-hide"));
		}
		return this;
	}

	/**
	 * Verify Deal Content 
	 * @return
	 */
	public DealDetailPage dealDetailOptionTitles(){

		Assert.assertTrue(locator.dealDetailsSection.isDisplayed());

		for(WebElement title :  locator.dealDetailOptionTitles){
			Assert.assertTrue("SubHeading is Not Displayed", title.isDisplayed());
			String subHeadingText = title.getText();
			logger.info(subHeadingText);
		}

		return this;
	}

	/**
	 * View Groupon Menu/View Services List/View Location List
	 * @return
	 */
	public DealDetailPage viewGrouponMenuLocationMenuServicesMenu(){

		// For Location Menu
		if(WaitUtility.isElementPresent(getWebDriver(), locator.viewLocationList)){

			logger.info("Location Menu is present.Lets assert it");
			Assert.assertTrue(locator.viewLocationList.isDisplayed());
			Assert.assertTrue(locator.viewLocationList.isEnabled());
			locator.viewLocationList.click();
			waitVar.until(ExpectedConditions.visibilityOf(locator.viewMenuPopUpDialogBox));
			Assert.assertTrue(locator.viewMenuPopUpDialogBox.isDisplayed());
			for(WebElement location : locator.locationListInPopUp){
				logger.info(location.getText());
			}
			locator.closeButtonOnMenuPopUp.click();
			waitVar.until(ExpectedConditions.invisibilityOfElementLocated(By.cssSelector("#menu_modal")));
			logger.info("PopUp is closed");
		}

		// For Food Or Services Menu 
		else if(WaitUtility.isElementPresent(getWebDriver(), locator.viewServicesOrGrouponMenu)){

			logger.info("Services/Groupon Menu is present.Lets assert it");
			Assert.assertTrue(locator.viewServicesOrGrouponMenu.isDisplayed());
			Assert.assertTrue(locator.viewServicesOrGrouponMenu.isEnabled());
			locator.viewServicesOrGrouponMenu.click();
			waitVar.until(ExpectedConditions.visibilityOf(locator.viewMenuPopUpDialogBox));
			Assert.assertTrue(locator.viewMenuPopUpDialogBox.isDisplayed());
			for(WebElement food : locator.foodMenuOrServicesListInPopUp){
				logger.info(food.getText());
			}
			locator.closeButtonOnMenuPopUp.click();
			waitVar.until(ExpectedConditions.invisibilityOfElementLocated(By.cssSelector("#menu_modal")));
			logger.info("PopUp is closed");
		}

		// For no Menu
		else
			logger.info("No Groupon/Services/Location Menu is present on page");

		return this;
	}

	/**
	 * Assert all the deal content on Deal Detail Page {NEW UI}
	 * @return
	 */
	public DealDetailPage dealContent(){

		waitVar.until(ExpectedConditions.visibilityOf(locator.dealDetailsSection));
		for(WebElement heading : locator.dealContentHeadings){
			Assert.assertNotNull("Heading in the Content is not present", heading);
			logger.info(heading.getText());
		}

		for(WebElement list : locator.dealContentLists){
			Assert.assertNotNull("List Points in the Content is not present", list);
			logger.info(list.getText());
		}

		// If Paragraphs are present in Deal Content
		if(WaitUtility.isElementPresentByLocator(getWebDriver(), By.cssSelector(".deal_details p"))){

			logger.info("Paragraphs are present in Content");

			for(WebElement para : locator.dealContentParagraphs){
				Assert.assertNotNull("Paragraph Tag's Text in the Content is not displayed", para);
				logger.info(para.getText());
			}
		}

		// If View More Link is present in Deal Content
		if(WaitUtility.isElementPresentByLocator(getWebDriver(), By.cssSelector(".nb-freedom .view_all"))){

			logger.info("View More Link is present in Content");

			for(WebElement link : locator.viewMoreLink){
				Assert.assertNotNull(link);
				Assert.assertTrue(link.isEnabled());
				logger.info(link.getText());

			}
		}

		// If Buttons For Menu or TnC are present in Deal Content
		if(WaitUtility.isElementPresentByLocator(getWebDriver(), By.cssSelector(".nb-freedom .deal_details a.btn_green"))){

			logger.info("TnC/MenuForTheDeal Button is present in Content");

			for(WebElement button : locator.tnCandMenuForTheDealButton){
				Assert.assertTrue(button.isDisplayed());
				Assert.assertTrue(button.isEnabled());
				logger.info(button.getText());

				// Click On Terms and Conditions Button
				if(button.getText().equals("Terms and Conditions")){
					button.click();
					waitVar.until(ExpectedConditions.visibilityOf(locator.tNcPopUp));
					Assert.assertTrue(locator.tNcPopUpHeading.isDisplayed());
					logger.info(locator.tNcPopUpHeading.getText());
					if(WaitUtility.isElementPresentByLocator(getWebDriver(), By.cssSelector("#nearbuy-terms-conditions strong"))){
						for(WebElement bold : locator.tNcPopUpBoldText){
							Assert.assertNotNull(bold);
							logger.info(bold.getText());
						}
					}
					for(WebElement list : locator.tNcPopUpListPoints){
						Assert.assertTrue(list.isDisplayed());
						logger.info(list.getText());
					}

					Assert.assertTrue(locator.tNcCloseButton.isDisplayed());
					Assert.assertTrue(locator.tNcCloseButton.isEnabled());
					try {
						Thread.sleep(1000);
					} catch (InterruptedException e) {
						e.printStackTrace();
					}
					locator.tNcCloseButton.click();
					waitVar.until(ExpectedConditions.invisibilityOfElementLocated(By.id("nearbuy-terms-conditions")));
					logger.info("Assertions dne for TnC popUp and closed the pop up");
				}

				// CLick on Menu For The Deal Button
				else if(button.getText().equals("Menu for the deal")){
					button.click();
					waitVar.until(ExpectedConditions.visibilityOf(locator.menuPopUp));
					Assert.assertTrue(locator.menuPopUpHeading.isDisplayed());
					logger.info(locator.menuPopUpHeading.getText());
					for(WebElement bold : locator.menuPopUpBoldPoints){
						Assert.assertTrue(bold.isDisplayed());
						logger.info(bold.getText());
					}
					for(WebElement list : locator.menuPopUpListPoints){
						Assert.assertTrue(list.isDisplayed());
						logger.info(list.getText());
					}
					for(WebElement list : locator.menuPopUpBoldText){
						Assert.assertTrue(list.isDisplayed());
						logger.info(list.getText());
					}
					Assert.assertTrue(locator.menuCloseButton.isDisplayed());
					Assert.assertTrue(locator.menuCloseButton.isEnabled());
					try {
						Thread.sleep(1000);
					} catch (InterruptedException e) {
						e.printStackTrace();
					}
					locator.menuCloseButton.click();
					waitVar.until(ExpectedConditions.invisibilityOfElementLocated(By.id("nearbuy_menu")));
					logger.info("Assertions dne for Menu popUp and closed the pop up");
				}


			}
		}

		return this;
	}

	/**
	 * Veirfy Offer Section
	 * @return
	 */
	public DealDetailPage verifyOfferSectionAndClickOnOption(int optionNumber, int quantityOfOptionToBeAdded){
		
		// If Book Now Button is Enabled
		if(WaitUtility.isElementPresent(getWebDriver(), locator.emptyOrderCart)){

			System.out.println("If cart is empty");

			// By Default Assertions on Offer Section
			Assert.assertTrue(locator.emptyOrderBuyNowButton.isDisplayed());
			Assert.assertTrue(locator.emptyOrderBuyNowButton.isEnabled());

			for(WebElement eachPlusSign : locator.optionPlusSigns){
				Assert.assertTrue(eachPlusSign.isDisplayed());
				Assert.assertTrue(eachPlusSign.isEnabled());
			}
			for(WebElement eachMinusSign : locator.optionMinusSigns){
				Assert.assertTrue(eachMinusSign.isDisplayed());
				Assert.assertTrue(eachMinusSign.getAttribute("class").contains("btn--disabled"));
			}
			for(WebElement option : locator.optionTitle){
				Assert.assertTrue(option.isDisplayed());
				logger.info("Option Name : " + option.getText());
			}
			for(WebElement nbPrice : locator.nearbuyPrice){
				Assert.assertFalse(nbPrice.getText().contains(".")); // Verify no decimal value in Price is coming on UI
				logger.info("Option Price : " + nbPrice.getText());
			}

			Assert.assertTrue(locator.totalAmount.isDisplayed());

			Assert.assertTrue(locator.totalPriceText.isDisplayed());
			Assert.assertTrue(locator.totalPriceText.getText().contains("Total"));
			Assert.assertTrue(locator.totalAmount.getText().contains("-"));

			int j=0;
			for(WebElement panel : locator.optionPanelList){
				Assert.assertTrue(panel.isDisplayed());
				// For Sold Out Offers
				if(WaitUtility.isElementPresent(getWebDriver(), locator.continueShoppingButton)){
					Assert.assertTrue(getWebDriver().findElement(By.cssSelector(".option .block__inner b")).getText().equals("Sold out"));
					logger.info("Option number : " + j + " : is Sold Out");
					logger.info("Number of sold out offers : " + locator.soldOutOffer.size());
					Assert.assertTrue(locator.offerStatusDiv.isDisplayed());
					Assert.assertTrue(locator.offerStatusHeading.getText().equals("Sorry, this offer is"));
					Assert.assertTrue(locator.offerStatusSoldOutText.getText().equals("Sold Out"));
					Assert.assertTrue(locator.continueShoppingButton.isDisplayed());
					Assert.assertTrue(locator.continueShoppingButton.isEnabled());
				}
			}
			logger.info("No. of Options in the deal : " + locator.optionPanelList.size());

			for(WebElement qty : locator.quantityCount){
				Assert.assertTrue(qty.isDisplayed());
				logger.info(qty.getText());
			}

			try {
				Thread.sleep(3000);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			// Perform some operations and verify if offer list > 1
			locator.optionPlusSigns.get(0).click();
			System.out.println("Added one item");
			waitVar.until(ExpectedConditions.elementToBeClickable(locator.optionMinusSigns.get(0)));
			Assert.assertFalse(locator.optionMinusSigns.get(0).getAttribute("class").contains("btn--disabled"));
			Assert.assertTrue(locator.optionMinusSigns.get(0).isDisplayed());
			Assert.assertTrue(locator.optionMinusSigns.get(0).isEnabled());
			Assert.assertTrue(locator.inclusiveOfAllText.isDisplayed());
			totalPriceVerification();
			locator.optionMinusSigns.get(0).click();
			logger.info("Minus Sign is clicked");
			try {
				Thread.sleep(1000);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			Assert.assertTrue(locator.quantityCount.get(0).getText().equals("0"));

			for(int i=1; i <= quantityOfOptionToBeAdded; i++){
				Assert.assertTrue(locator.optionPlusSigns.get(optionNumber).isEnabled());
				locator.optionPlusSigns.get(optionNumber).click();
				waitVar.until(ExpectedConditions.invisibilityOfElementLocated(By.cssSelector(".orders__total--empty")));
				System.out.println("User has put one offer in cart");
				totalPriceVerification();
			}


		}

		// If all offers are sold out
		else if(WaitUtility.isElementPresent(getWebDriver(), locator.continueShoppingButton)){
			try{

				Assert.assertFalse(locator.continueShoppingButton.isEnabled());
				for(WebElement eachOffer : locator.optionPanelList){
					Assert.assertTrue(eachOffer.findElement(By.cssSelector("b")).getText().equals("Sold out"));
				}
				for(WebElement soldOut : locator.soldOutOffer){
					Assert.assertTrue(soldOut.isDisplayed());
					logger.info(soldOut.getText());
				}
			}
			catch(AssertionError ae) {
				throw ae;
			}
		}
		return this;
	}

	private void totalPriceVerification() {

		Assert.assertNotNull(locator.nameOfOffersInCart);
		for(WebElement l : locator.nameOfOffersInCart){
			for(WebElement a : locator.nameOfOptionsUserHasSelected){
				System.out.println(a.getText());
				System.out.println(l.getText());
				Assert.assertTrue(l.getText().contains(a.getText()));
			}
		}
		for(WebElement l : locator.quantityOfOptionsInCart){
			for(WebElement a : locator.quantityOfOptionsUserHasSelected){
				System.out.println(a.getText());
				System.out.println(l.getText().substring(2));
				Assert.assertTrue(l.getText().substring(2).contains(a.getText()));
			}
		}
		for(WebElement l : locator.priceOfOptionsInCart){
			for(WebElement a : locator.nearbuyPriceOfOptionsUserHasSelected){
				for(WebElement b : locator.quantityOfOptionsInCart){
					String eachPriceInCart = l.getText().trim();
					String eachPriceInOption = a.getText().trim();
					if( eachPriceInCart.contains(",")){
						eachPriceInCart = eachPriceInCart.replace(",", "");
					}
					if(eachPriceInOption.contains(",")){
						eachPriceInOption = eachPriceInOption.replace(",", "");
					}
					logger.info(eachPriceInOption);
					logger.info(eachPriceInCart);
					Assert.assertEquals(Integer.parseInt(eachPriceInCart) ,(Integer.parseInt(eachPriceInOption) * Integer.parseInt(b.getText().substring(2))));
				}
			}
		}

		int expectedTotalPrice=0;
		for(WebElement eachPriceInCart : locator.priceOfOptionsInCart){
			String eachPriceAmountOfOptionIncart = eachPriceInCart.getText().trim();
			if(eachPriceAmountOfOptionIncart.contains(",")){
				eachPriceAmountOfOptionIncart = eachPriceAmountOfOptionIncart.replace(",", "");
			}
			expectedTotalPrice = expectedTotalPrice + Integer.parseInt(eachPriceAmountOfOptionIncart);
		}

		String totalAmount = locator.totalAmount.getText().trim();
		if(totalAmount.contains(",")){
			totalAmount = totalAmount.replace(",", "");
		}
		Assert.assertEquals(expectedTotalPrice, Integer.parseInt(totalAmount));

		if(WaitUtility.isElementPresentByLocator(getWebDriver(), By.xpath(".//*[@class='quantity__dec']//..//..//*[contains(@class,'actual-price')]"))){

			Assert.assertTrue(locator.orderSavingText.isDisplayed());
			int totalActualPrice=0;
			for(WebElement eachActualPrice : locator.actualPriceOfOptionsUserHasSelected){
				for(WebElement b : locator.quantityOfOptionsInCart){
					String eachActualPriceAmount = eachActualPrice.getText().trim();
					if(eachActualPriceAmount.contains(",")){
						eachActualPriceAmount = eachActualPriceAmount.replace(",", "");
						logger.info(eachActualPriceAmount);
					}
					totalActualPrice = totalActualPrice + (Integer.parseInt(eachActualPriceAmount) * Integer.parseInt(b.getText().substring(2)));
					System.out.println(totalActualPrice);
				}
			}
			System.out.println("Total Actual Price : " + totalActualPrice);
			System.out.println("Total Amount  : " + totalAmount);
			if(totalAmount.contains(",")){
				totalAmount = totalAmount.replace(",", "");
			}
			int ecpectedSavings = totalActualPrice - Integer.parseInt(totalAmount);
			System.out.println("Expected Savings : " + ecpectedSavings);

			String savingsAmount = locator.orderSavingPrice.getText().trim();
			if(savingsAmount.contains(",")){
				savingsAmount = savingsAmount.replace(",", "");
			}
			Assert.assertEquals(ecpectedSavings, Integer.parseInt(savingsAmount));
		}		
	}

	/**
	 * Data Layer on Deal Detail Page
	 * @return
	 */
	public DealDetailPage verifyDataLayerDealDetail() {
		//prepare map by extracting values from html
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("title", locator.dlTitle.getAttribute("value"));
		map.put("city", locator.dlCity.getAttribute("value"));
		map.put("dealId", locator.dlDealId.getAttribute("value"));
		map.put("dealPrice", locator.dlPrice.getAttribute("value"));
		map.put("pageType", "Deal");
		map.put("saleCounter", locator.dlSoldQuantity.getAttribute("value"));
		map.put("permalink", locator.dlPermalink.getAttribute("value"));
		map.put("sitetype", "d");
		map.put("vertical", locator.dlVertical.getAttribute("value"));

		verifyDataLayer(map, "2");
		return this;
	}


	public DealDetailPage nearbuyPromise(){

		Assert.assertTrue("Nearbuy Promise COntent Is Not displayed", locator.nearbuyPromiseContent.isDisplayed());
		Assert.assertTrue("Nearbuy Promise Image Is Not displayed", locator.nearbuyPromiseImage.isDisplayed());
		Assert.assertTrue("Nearbuy Promise COntent Is Not correct", locator.nearbuyPromiseContent.getText().contains("Your purchase is "));
		Assert.assertTrue("Nearbuy Promise Is Not displayed", locator.nearbuyPromiseTooltip.isDisplayed());
		Assert.assertTrue("Nearbuy Promise Is Not enabled", locator.nearbuyPromiseTooltip.isEnabled());
		locator.nearbuyPromiseTooltip.click();
		waitVar.until(ExpectedConditions.visibilityOf(locator.nearbuyPromisePopUp));

		for(WebElement tagLines : locator.nearbuyPromiseTagLinesInPopUp){
			Assert.assertTrue(tagLines.isDisplayed());
			if(WaitUtility.isElementPresentByLocator(getWebDriver(), By.cssSelector("travel-deal-view"))){
				Assert.assertEquals(locator.nearbuyPromiseTagLinesInPopUp.size(),2);
				Assert.assertTrue(locator.nearbuyPromiseTagLinesInPopUp.get(0).getText().equalsIgnoreCase("CANCEL BEFORE YOUR BOOKING IS CONFIRMED?"));
				Assert.assertTrue(locator.nearbuyPromiseTagLinesInPopUp.get(1).getText().equalsIgnoreCase("This is a non cancellable offer, but you won’t have a bad experience"));
			}
		}
		for(WebElement img : locator.nearbuyPromiseImageInPopUp){
			Assert.assertTrue(img.isDisplayed());
			if(WaitUtility.isElementPresentByLocator(getWebDriver(), By.cssSelector("travel-deal-view"))){
				Assert.assertEquals(locator.nearbuyPromiseImageInPopUp.size(),2);
			}
		}
		locator.nearbuyPromiseCloseIcon.click();
		waitVar.until(ExpectedConditions.invisibilityOfElementLocated(By.cssSelector(".promise-content+.tooltip-popover-wrapper .tooltip-popover.show")));

		return this;
	}

	public DealDetailPage cancellationPolicy(){
		Assert.assertTrue(locator.blockSectionOnDealDetail.get(2).findElement(By.cssSelector("p")).isDisplayed());
		return this;
	}

	public DealDetailPage thingstoRemember(){
		Assert.assertTrue(locator.thingsToRememberHeading.isDisplayed());
		Assert.assertTrue(locator.thingsToRememberHeading.getText().equals("Things to remember"));
		for(WebElement each : locator.thingsToRememberPoints){
			Assert.assertTrue(each.isDisplayed());
		}
		return this;
	}

	public DealDetailPage howToUseOffer(){
		for(WebElement each : locator.howToUseOfferListPoints){
			Assert.assertTrue(each.isDisplayed());
		}
		Assert.assertTrue(locator.howToUseOfferHeading.isDisplayed());
		Assert.assertTrue(locator.howToUseOfferHeading.getText().equals("How to use offer"));
		return this;
	}

	public DealDetailPage bankOffersSection(){
		if(WaitUtility.isElementPresent(getWebDriver(), locator.bankOfferSection)){
			Assert.assertTrue(locator.bankOfferSectionHeading.isDisplayed());
			Assert.assertTrue(locator.bankOfferSectionHeading.getText().equals("Save More With Payment Offers"));
			for(WebElement eachBankOffer : locator.bankOffersList){
				Assert.assertTrue(eachBankOffer.isDisplayed());
				Assert.assertTrue(eachBankOffer.isEnabled());
			}
			locator.bankOffersList.get(0).click();
			waitVar.until(ExpectedConditions.visibilityOf(locator.bankOffersPopUp));
			for(WebElement each : locator.bankOffersListInPopUp){
				Assert.assertTrue(each.findElement(By.cssSelector("img")).isDisplayed());
				Assert.assertTrue(each.findElement(By.cssSelector(".h6")).isDisplayed());
				Assert.assertTrue(each.findElement(By.cssSelector(".offer-card__right p")).isDisplayed());
				Assert.assertTrue(each.findElement(By.cssSelector(".offer-card__right p")).getText().equals("Terms & Conditions"));
				Assert.assertTrue(each.findElement(By.cssSelector(".offer-card__right li")).isDisplayed());
			}
			Assert.assertTrue(locator.bankOfferPopUpCloseButton.isDisplayed());
			Assert.assertTrue(locator.bankOfferPopUpCloseButton.isEnabled());
			locator.bankOfferPopUpCloseButton.click();
			waitVar.until(ExpectedConditions.invisibilityOfElementLocated(By.cssSelector(".bank-offer-modal.modal-show")));
		}
		return this;
	}

	public DealDetailPage promoSection(){

		if(WaitUtility.isElementPresent(getWebDriver(), locator.promoContainer)){
			System.out.println("Promo Container is present");
			Assert.assertTrue(locator.promoContainer.isDisplayed());
			for(WebElement each : locator.allPromoList){
				Assert.assertTrue(each.isDisplayed());
				Assert.assertTrue(each.findElement(By.cssSelector("h3")).isDisplayed());
				Assert.assertTrue(each.findElement(By.cssSelector(".promo__title p")).isDisplayed());
				Assert.assertTrue(each.findElement(By.cssSelector(".promo__code__text")).isDisplayed());
				Assert.assertTrue(each.findElement(By.cssSelector(".promo__code__copy")).isDisplayed());
				Assert.assertTrue(each.findElement(By.cssSelector(".promo .block__footer")).isDisplayed());
				Assert.assertTrue(each.findElement(By.cssSelector(".promo__validity")).isDisplayed());
				Assert.assertTrue(each.findElement(By.cssSelector(".promo__more button")).isDisplayed());
				Assert.assertTrue(each.findElement(By.cssSelector(".promo__more button")).isEnabled());
				each.findElement(By.cssSelector(".promo__more button")).click();
				waitVar.until(ExpectedConditions.visibilityOf(locator.promoPopUp));
				Assert.assertTrue(locator.promoPopUpCloseButton.isDisplayed());
				Assert.assertTrue(locator.promoPopUpCloseButton.isEnabled());
				Assert.assertTrue(locator.promoPopUpHeading.isDisplayed());
				Assert.assertTrue(locator.promoPopUpHeading.getText().equals("Available Offers"));

				for(WebElement eachOfferCard : locator.promoPopUpOffersCards){
					Assert.assertTrue(eachOfferCard.findElement(By.cssSelector(".offers-card__heading")).isDisplayed());
					Assert.assertTrue(eachOfferCard.findElement(By.cssSelector(".offers-card__txt")).isDisplayed());
					Assert.assertTrue(eachOfferCard.findElement(By.cssSelector(".coupon__code")).isDisplayed());
					Assert.assertTrue(eachOfferCard.findElement(By.cssSelector(".coupon__btn")).isDisplayed());
					eachOfferCard.findElement(By.cssSelector(".coupon__btn")).click();
					waitVar.until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector(".tooltip .tooltip--left")));
					Assert.assertTrue(eachOfferCard.findElement(By.cssSelector(".offers-card__right")).isDisplayed());
					Assert.assertTrue(eachOfferCard.findElement(By.cssSelector(".offers-card__right p")).isDisplayed());
					Assert.assertTrue(eachOfferCard.findElement(By.cssSelector(".offers-card__right p")).getText().equals("Terms & Conditions"));
					Assert.assertTrue(eachOfferCard.findElement(By.cssSelector(".offers-card__right li")).isDisplayed());

				}
				Assert.assertTrue(locator.promoPopUpCloseButton.isEnabled());			
				locator.promoPopUpCloseButton.click();
				waitVar.until(ExpectedConditions.invisibilityOfElementLocated(By.cssSelector(".promo-offer-modal.modal-show")));
			}
		}
		return this;
	}


	/**
	 * Get Title of the deal detail page and assert
	 * @return
	 */
	public DealDetailPage getTitleOfTheDealDetailPage(){

		String dealHeading = locator.dealHeading.getText();
		System.out.println(dealHeading);
		String currentTitle = getWebDriver().getTitle();
		System.out.println(currentTitle);
		Assert.assertTrue("Deal Page Title is not containng Deal Heading", currentTitle.contains(dealHeading));
		//Assert.assertEquals(dealHeading , currentTitle);
		logger.info("Asserted the title of home page");
		return this;
	}

	public DealDetailPage pickDates(){

		//if(WaitUtility.isElementPresent(getWebDriver(), webelement))

		return this;
	}

	public String getCurrentDealId() {
		waitVar.until(ExpectedConditions.urlContains("offer"));
		waitVar.until(ExpectedConditions.visibilityOf(locator.dealHeading));

		String currentUrl = getWebDriver().getCurrentUrl();
		String dealIdFromUrl = currentUrl.substring(currentUrl.lastIndexOf("/") + 1, currentUrl.indexOf("?"));
		logger.info("DealId from Url:"+dealIdFromUrl);
		WebElement localDealDetailSection = locator.localDealDetailSection;
		Assert.assertNotNull(localDealDetailSection);
		String dealIdFromPage = localDealDetailSection.getAttribute("data-dealid");
		logger.info("DealId from Page:" + dealIdFromPage);
		Assert.assertEquals(dealIdFromUrl, dealIdFromPage);

		return dealIdFromPage;
	}

	public DealDetailPage clickOnPlusToIncreaseOptionQuantity(int optionNumber, int quantityToBeIncreased){

		waitVar.until(ExpectedConditions.visibilityOfAllElements(locator.plusSign));
		try {
			Thread.sleep(2000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		for(int j=0;j<=quantityToBeIncreased-1 ; j++){
			locator.plusSign.get(optionNumber-1).click();
		}
		logger.info("option number : " + optionNumber + " :: increased to quantity : " + quantityToBeIncreased);
		return this;
	}

}
