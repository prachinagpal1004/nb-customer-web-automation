package com.groupon.web.nearbuy.test;

import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import com.groupon.web.common.test.BaseTest;
import com.groupon.web.common.utils.TestListener;
import com.groupon.web.nearbuy.data.AuthenticationData;
import com.groupon.web.nearbuy.page.AuthenticationPage;
import com.relevantcodes.extentreports.LogStatus;
/**
 * 
 * @author Prachi Nagpal
 *
 */
@Test(groups = {"sanity","loginSignUp"})
public class AuthenticationTest extends BaseTest {

	private AuthenticationPage authenticationPage;

	@BeforeMethod(alwaysRun=true)
	public void init() {
		authenticationPage = new AuthenticationPage();
	}
	/**
	 * Test Click on Login Link
	 */
	@Test(priority = 1)
	public void testNavigateToHome() throws Throwable {
		TestListener.setLog(LogStatus.INFO, "Navigate to Home Page");
		//authPopup();
		//authenticationPage.navigateToHome();
	}
	/**
	 * Test Enter Credentials
	 */
	@Test(priority = 2 , dataProvider="userCredentialsForErrorMessage" ,dataProviderClass = AuthenticationData.class )
	public void testUserEntersCredentialsAndLogin(String username, String password, String message, String bool) throws Throwable {
		TestListener.setLog(LogStatus.INFO, "Enter Credentials & Verify Error Messages");
		authenticationPage.navigateToLoginPage();
		//	authenticationPage.verifyLoginAndErrorMessages(username, password, message, Boolean.valueOf(bool));
	}

	//	/**
	//	 * Test Forgot Password Link
	//	 */
	//	@Test(priority = 3, dataProvider="ForgotPasswordOtpScreenErrorMessages" , dataProviderClass = AuthenticationData.class)
	//	public void testForgotPasswordLink(String emailOrmobile, String errorMessage) throws Throwable {
	//		TestListener.setLog(LogStatus.INFO, "Verify Forgot Password Link");
	//		authenticationPage.navigateToLoginPage().forgotPasswordLink(emailOrmobile, errorMessage);
	//	}
	//	
	//	/**
	//	 * Test SignUp
	//	 */
	//	@Test(priority = 4, dataProvider="signUpErrorMessages" , dataProviderClass = AuthenticationData.class)
	//	public void testSignUp(String errorMessage) throws Throwable {
	//		TestListener.setLog(LogStatus.INFO, "Enter Details for SignUp");
	//		authenticationPage.navigateToLoginPage().clickOnSignUpLink().enterDetailsForSignUp(errorMessage);
	//	}
	//
	////	/**
	////	 * Test Already a user Link
	////	 */
	////	@Test(priority = 4)
	////	public void testAlreadyAregisteredLink() throws Throwable {
	////		TestListener.setLog(LogStatus.INFO, "Verify Already a User Link");
	////		authenticationPage.alreadyAUserLink();
	////	}
	//
	//
	//	/**
	//	 * Test Success Number Login
	//	 */
	//	@Test(priority = 6 , dataProvider="successNumberLoginLogout" ,dataProviderClass = AuthenticationData.class )
	//	public void testUserDoesSuccessNumberLogin(String username, String password, String message, String bool) throws Throwable {
	//		
	//		TestListener.setLog(LogStatus.INFO, "Enter Credentials for Successful Number login");
	//		authenticationPage.clickOnLoginLink().verifyLoginAndErrorMessages(username, password, message, Boolean.valueOf(bool));
	//	}
	//	
	//	/**
	//	 * Test Success Email Id Login
	//	 */
	//	
	//	@Test(priority = 7 , dataProvider="successEmailIdLoginLogout" ,dataProviderClass = AuthenticationData.class )
	//	public void testUserDoesSuccessEmailIdLogin(String username, String password, String message, String bool) throws Throwable {
	//		
	//		TestListener.setLog(LogStatus.INFO, "Enter Credentials for Successful Email ID login");
	//		authenticationPage.navigateToLoginPage().verifyLoginAndErrorMessages(username, password, message, Boolean.valueOf(bool));
	//	}
}
