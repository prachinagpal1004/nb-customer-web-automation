package com.groupon.web.nearbuy.test;

import com.groupon.web.common.test.BaseTest;
import com.groupon.web.common.utils.TestListener;
import com.groupon.web.nearbuy.data.MenuData;
import com.groupon.web.nearbuy.page.DealCatalogPage;
import com.groupon.web.nearbuy.page.HeaderFooterPage;
import com.relevantcodes.extentreports.LogStatus;

import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
/**
 * 
 * @author Prachi Nagpal
 *
 */
@Test(groups = {"sanity","localListingFiltersAndSort"})
public class ListingFiltersAndSortTest extends BaseTest {

	private HeaderFooterPage headerFooterPage;
	private DealCatalogPage dealCatalogPage;

    @BeforeMethod(alwaysRun=true)
    public void init() {
		headerFooterPage = new HeaderFooterPage();
		dealCatalogPage = new DealCatalogPage();
	}

	/**
	 * Navigate to particular Menu
	 * @param menu
	 * @param submenu
	 * @throws Throwable
	 */
	@Test(priority = 1 , dataProvider="foodAndDrink" , dataProviderClass = MenuData.class)
	public void testCategoryAndLocationFilters(String menu, String submenu, String subSubMenu) throws Throwable {
		TestListener.setLog(LogStatus.INFO, "Navigate to Deal Listing Page from Menu");
		headerFooterPage.navigateToMenu(menu, submenu, subSubMenu);
		TestListener.setLog(LogStatus.INFO, "Verify Filters");
		dealCatalogPage.verifyCategoryFilters();
		dealCatalogPage.verifyLocationFilter("Saket");
	}

	/**
	 *  Test Deal Details for each deal
	 */
	@Test(priority = 2 )
	public void testCategorySort() throws Throwable {
		TestListener.setLog(LogStatus.INFO, "Verify Sort");
		//dealCatalogPage.
	}

}
