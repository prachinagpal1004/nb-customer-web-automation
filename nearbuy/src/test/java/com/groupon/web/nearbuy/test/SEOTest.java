package com.groupon.web.nearbuy.test;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import com.groupon.web.common.test.BaseTest;
import com.groupon.web.common.utils.GoogleSheetUtils;
import com.groupon.web.common.utils.TestListener;
import com.groupon.web.nearbuy.data.SeoData;
import com.groupon.web.nearbuy.page.HeaderFooterPage;
import com.groupon.web.nearbuy.page.SEOpage;
import com.relevantcodes.extentreports.LogStatus;
/**
 * 
 * @author Prachi Nagpal
 *
 */
@Test(groups = {"seo"})
public class SEOTest extends BaseTest {

	private HeaderFooterPage headerFooterPage;
	private SEOpage seo;
	private List<SEOResult> seoResults = new ArrayList<SEOResult>();

	public SEOResult getHeader() {
		SEOResult result = new SEOResult();
		result.city = "city";
		result.category = "category";
		result.subCategory = "subCategory";
		result.expectedTitle = "expectedTitle";
		result.actualTitle = "actualTitle";
		result.titleMatch = "titleMatch";
		result.expectedMetaDesc = "expectedMetaDesc";
		result.actualMetaDesc = "actualMetaDesc";
		result.metaDescMatch = "metaDescMatch";
		return result;
	}

	@BeforeMethod(alwaysRun=true)
	public void init() {
		headerFooterPage = new HeaderFooterPage();
		seo = new SEOpage();
	}

	/**
	 * Navigate to particular Menu
	 * @param menu
	 * @param submenu
	 * @throws Throwable
	 */
	
	@Test(priority = 1 , dataProvider="fetchSeoInputData" , dataProviderClass = SeoData.class )
	public void navigateToCategoryAndAssertPageTitle(String mainMenu, String cat, String subCat, String subCat2, String expectedTitle, String expectedMeta) throws Throwable {
		try{
		System.out.println("Test Started with input:"+StringUtils.join(mainMenu, cat, subCat, subCat2, expectedTitle, expectedMeta));
		TestListener.setLog(LogStatus.INFO, "Navigate to Menu");
		headerFooterPage.navigateToMenu(mainMenu, cat, subCat);
		if(StringUtils.isNotBlank(subCat2)) {
			headerFooterPage.clickOnSubSubCategory(subCat, subCat2);
		}
		String city = getCity();
		expectedTitle = prepareExpected(city, cat, subCat, subCat2, expectedTitle);
		expectedMeta = prepareExpected(city, cat, subCat, subCat2, expectedMeta);
		SEOResult seoResult = prepareSEOResult(city, cat, subCat, subCat2);
		seo.assertTitleOfThePage(expectedTitle, seoResult);
		seo.assertMetaOfThePage(expectedMeta, seoResult);
		seoResults.add(seoResult);
		}
		catch(Exception e){
			e.printStackTrace();
			seoResults.add(new SEOResult());
		}
	}

	private SEOResult prepareSEOResult(String city, String cat, String subCat, String subCat2) {
		SEOResult result = new SEOResult();
		result.city = city;
		result.category = cat;
		result.subCategory = subCat;
		result.subSubCategory = subCat2;
		return result;
	}

	private String prepareExpected(String city, String cat, String subCat, String subCat2, String expected) {
		expected = expected.replaceAll("<City Name>", city);
		expected = expected.replaceAll("<Min-Max%>", "20-80%");
		//expected = expected.replaceAll("<Category>", cat+"s");
		expected = expected.replaceAll("<Category>", cat);
		if(StringUtils.isNotBlank(subCat)) {
			expected = expected.replaceAll("<Sub-Category>", subCat);
		}
		if(StringUtils.isNotBlank(subCat2)) {
			expected = expected.replaceAll("<Sub-Sub-Category>", subCat2);
		}
		return expected;
	}

	@AfterClass
	public void writeToFile() throws ParseException{
		try {
			File file = new File(System.getProperty("user.dir")+File.separator+"target"+File.separator+"SeoMatchResults.csv");
			// if file doesn't exists, then create it
			if (file.exists()) {
				file.delete();
			}
			file.createNewFile();

			FileWriter fw = new FileWriter(file.getAbsoluteFile());
			BufferedWriter bw = new BufferedWriter(fw);
			bw.write(getHeader().toString());
			bw.newLine();

			List<List<Object>> sheetOutput = new ArrayList<List<Object>>(seoResults.size());
			for(SEOResult l : seoResults) {
				sheetOutput.add(l.toList());
				bw.write(l.toString());
				bw.newLine();
			}
			bw.close();

			String spreadsheetId = "1mACzxSkG_xJi3Ej4v-YaQIEvmSEUTTM6NnOL2h4cd0Q";
			String range = "Sheet1!G2";
			Map<String, Object> response = GoogleSheetUtils.updateValues(spreadsheetId, range, sheetOutput);
			System.out.println(response);
		} catch(IOException ie) {
			ie.printStackTrace();
		}
	}

	public static class SEOResult {

		public String city = "";
		public String category = "";
		public String subCategory = "";
		public String subSubCategory = "";
		public String expectedTitle = "";
		public String actualTitle = "";
		public String titleMatch = "";
		public String expectedMetaDesc = "";
		public String actualMetaDesc = "";
		public String metaDescMatch = "";

		@Override
		public String toString() {
			return city + "," + category + "," 
					+ subCategory + "," + subSubCategory + "," 
					+ expectedTitle + "," + actualTitle + "," + titleMatch + "," 
					+ expectedMetaDesc + "," + actualMetaDesc + "," + metaDescMatch;
		}

		public List<Object> toList() {
			List<Object> list = new ArrayList<Object>();
			list.add(actualTitle);
			list.add(titleMatch);
			list.add(actualMetaDesc);
			list.add(metaDescMatch);
			return list;
		}
	}

}
