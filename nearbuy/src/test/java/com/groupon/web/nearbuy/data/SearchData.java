package com.groupon.web.nearbuy.data;

import org.testng.annotations.DataProvider;

/**
 * 
 * @author prachi.nagpal
 *
 */
public class SearchData {

	@DataProvider(name = "enterSearchKeywrod")
	public static Object[][] providesearchKeyword(){
		return new Object[][] {

				{"food", true}
		};
	}
	
	@DataProvider(name = "enterNoResultSearchKeywrod")
	public static Object[][] provideNoResultSearchKeyword(){
		return new Object[][] {
			
			{"@#$%@$jkdgwvd",false},
		};
	}
	@DataProvider(name = "searchMore")
	public static Object[][] provideHighPriceProduct(){
		return new Object[][] {
			
			{"clothes", true},
		};
	}
	
}
