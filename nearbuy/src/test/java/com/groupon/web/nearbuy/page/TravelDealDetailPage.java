package com.groupon.web.nearbuy.page;

import com.groupon.web.common.page.BasePage;
import com.groupon.web.common.utils.WaitUtility;
import com.groupon.web.nearbuy.locator.TravelDealDetailLocator;
import org.apache.log4j.Logger;
import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;

public class TravelDealDetailPage extends BasePage {

	private static final Logger logger = Logger.getLogger(TravelDealDetailPage.class);
	private TravelDealDetailLocator locator;

	public TravelDealDetailPage() {
		locator = PageFactory.initElements(getWebDriver(), TravelDealDetailLocator.class);
	}

	public TravelDealDetailPage clickOnBookThisOptionButton(int buttonNumber){

		waitVar.until(ExpectedConditions.elementToBeClickable(locator.bookThisOptionAllButtons.get(buttonNumber)));
		locator.bookThisOptionAllButtons.get(buttonNumber).click();
		waitVar.until(ExpectedConditions.visibilityOf(locator.bookingFormModal));

		return this;
	}

	public TravelDealDetailPage clickOnBookThisOptionButtonForErrorMessage(int buttonNumber){

		waitVar.until(ExpectedConditions.elementToBeClickable(locator.bookThisOptionAllButtons.get(buttonNumber)));
		locator.bookThisOptionAllButtons.get(buttonNumber).click();
		waitVar.until(ExpectedConditions.visibilityOf(locator.errorMessage));

		return this;
	}

	public TravelDealDetailPage selectNumberOfRoomsCheckInAndCheckOutDates(int availableCheckInDate, int availableCheckOutDate, String numberOfRooms){

		waitVar.until(ExpectedConditions.elementToBeClickable(locator.checkInDateBox));
		locator.checkInDateBox.click();
		waitVar.until(ExpectedConditions.visibilityOf(locator.calenderDiv));
		locator.allAvailableDates.get(availableCheckInDate).click();
		waitVar.until(ExpectedConditions.visibilityOf(locator.calenderDiv));
		locator.allAvailableDates.get(availableCheckOutDate).click();
		waitVar.until(ExpectedConditions.invisibilityOfElementLocated(By.id("ui-datepicker-div")));

		Select numberOfRoomsBox = new Select(locator.numberOfRoomsDropDown);

		numberOfRoomsBox.selectByValue(numberOfRooms);

		return this;
	}

	public TravelDealDetailPage clickOnClearDatesLink(){

		Assert.assertTrue(locator.clearDatesLink.isEnabled());
		locator.clearDatesLink.click();
		waitVar.until(ExpectedConditions.presenceOfElementLocated(By.xpath(".//input[@placeholder='Check-out date '][@disabled]")));

		return this;
	}
	public TravelDealDetailPage clickOnProceedToPayButtonInTravelForm(){

		Assert.assertTrue(locator.proceedToPayButtonInTravelForm.isEnabled());
		locator.proceedToPayButtonInTravelForm.click();
		waitVar.until(ExpectedConditions.urlContains("order/placeorder?"));

		return this;
	}

	public TravelDealDetailPage verifyTripAdvisorLogo(){

		Assert.assertTrue(locator.tripAdvisorLogo.isDisplayed());

		return this;
	}

	public TravelDealDetailPage verifyEachOptionLeftSide(){

		for(WebElement each : locator.allOptionsDescription){
			Assert.assertTrue(each.findElement(By.cssSelector(".option__title")).isDisplayed());
			Assert.assertTrue(each.findElement(By.cssSelector(".icon-s")).isDisplayed());
			Assert.assertEquals(each.findElement(By.cssSelector(".icon-s")).getText(), "Non Cancellable");
			Assert.assertTrue(each.findElement(By.cssSelector(".option__offer i")).isDisplayed());
			Assert.assertTrue(each.findElement(By.cssSelector(".option__offer p")).isDisplayed());
			if(WaitUtility.isElementPresentByLocator(getWebDriver(), By.cssSelector(".deal-detail-travel .option__desc a"))){
				for(WebElement eachMore : locator.allMoreLinks){
					Assert.assertTrue(eachMore.isDisplayed());
					Assert.assertTrue(eachMore.isEnabled());
					eachMore.click();
					waitVar.until(ExpectedConditions.presenceOfElementLocated(By.cssSelector(".inclusion-modal.modal-show")));
					Assert.assertTrue(locator.inclusionHeading.isDisplayed());
					for(WebElement eachPoint : locator.inclusionsBulletPoints){
						Assert.assertTrue(eachPoint.isDisplayed());
					}
					Assert.assertTrue(locator.inclusionsPopUpCloseButton.isDisplayed());
					Assert.assertTrue(locator.inclusionsPopUpCloseButton.isEnabled());
					locator.inclusionsPopUpCloseButton.click();
					waitVar.until(ExpectedConditions.invisibilityOfElementLocated(By.cssSelector(".inclusion-modal.modal-show")));

					break;
				}
			}

		}

		return this;
	}

	public TravelDealDetailPage verifyEachOptionRightSide(){

		for(WebElement each : locator.optionsRightSide){
			if(each.findElement(By.cssSelector("button")).isDisplayed()){
				Assert.assertTrue(each.findElement(By.cssSelector(".price + p")).isDisplayed());
				Assert.assertEquals(each.findElement(By.cssSelector(".price + p")).getText(), "Inclusive of all taxes");
			}
			Assert.assertTrue(each.findElement(By.cssSelector(".option__availability a")).isDisplayed());
			Assert.assertEquals(each.findElement(By.cssSelector(".option__availability a")).getText(), "Rates & Availability");
			each.findElement(By.cssSelector(".option__availability a")).click();
			waitVar.until(ExpectedConditions.visibilityOf(locator.ratesAndAvailaibilityPopUp));
			Assert.assertTrue(locator.ratesAndAvailaibilityPopUp.findElement(By.cssSelector(".calender-heading p")).isDisplayed());
			Assert.assertEquals(locator.ratesAndAvailaibilityPopUp.findElement(By.cssSelector(".calender-heading p")).getText(), "Rates & Availability");
			Assert.assertTrue(locator.ratesAndAvailaibilityPopUp.findElement(By.cssSelector("button.close")).isDisplayed());
			Assert.assertTrue(locator.ratesAndAvailaibilityPopUp.findElement(By.cssSelector("button.close")).isEnabled());
			if(locator.calenderInnerData.getAttribute("class").equals(".calendar__inner-data")){
				Assert.assertTrue(locator.calenderInnerData.findElement(By.cssSelector(" tr:nth-child(2) td")).isDisplayed());
			}

			verifyCalenderArrowsAndExactlySixMonthsRatesComing();
			locator.ratesAndAvailaibilityPopUp.findElement(By.cssSelector("button.close")).click();
			waitVar.until(ExpectedConditions.invisibilityOfElementLocated(By.cssSelector(".calender-container.modal-show")));
			logger.info("Rates & availibility popup is closed");
			break;
		}
		return this;
	}

	private void verifyCalenderArrowsAndExactlySixMonthsRatesComing() {

		Assert.assertTrue(locator.calenderLeftArrow.isDisplayed());
		Assert.assertTrue(locator.calenderLeftArrow.getAttribute("class").contains("btn--dis"));
		Assert.assertTrue(locator.calenderRightArrow.isDisplayed());
		Assert.assertTrue(locator.calenderRightArrow.isEnabled());
		Assert.assertEquals(locator.calenderBody.size(), 2);

		locator.calenderRightArrow.click();
		logger.info("Calender Right Arrow is clicked");
		Assert.assertEquals(locator.calenderBody.size(), 2);
		Assert.assertTrue(locator.calenderLeftArrow.isEnabled());
		Assert.assertTrue(locator.calenderInnerData.findElement(By.cssSelector("tr:nth-child(2) td")).isDisplayed());

		while(true){
			locator.calenderRightArrow.click();
			if(locator.calenderRightArrow.getAttribute("class").contains("btn--dis")){
				break;
			}
		}
		logger.info("Calender Right Arrow is clicked again");
		Assert.assertEquals(locator.calenderBody.size(), 2);
		Assert.assertTrue(locator.calenderLeftArrow.isEnabled());
		Assert.assertTrue(locator.calenderRightArrow.getAttribute("class").contains("btn--dis"));
		Assert.assertTrue(locator.calenderInnerData.findElement(By.cssSelector("tr:nth-child(2) td")).isDisplayed());

		logger.info("RATES & Availaibility popup is verified");

	}

	public TravelDealDetailPage verifyBottomRightOfTravelDetail(){

		Assert.assertEquals(locator.rightHandHeadings.get(0).getText().trim().toLowerCase(), "Timings".toLowerCase());
		Assert.assertEquals(locator.rightHandHeadings.get(1).getText().trim().toLowerCase(), "Cancellation policy".toLowerCase());
		Assert.assertEquals(locator.rightHandHeadings.get(2).getText().trim().toLowerCase(), "Location(s)".toLowerCase());
		//Assert.assertEquals(locator.rightHandHeadings.get(1).findElement(By.cssSelector(" + p")).getText(), "No refund if booking is confirmed");
		Assert.assertEquals(locator.checkIncheckoutLabels.get(0).getText().trim().toLowerCase(), "Check in :".toLowerCase());
		Assert.assertTrue(locator.checkIncheckoutTimings.get(0).isDisplayed());
		Assert.assertTrue(locator.checkIncheckoutTimings.get(1).isDisplayed());
		Assert.assertEquals(locator.checkIncheckoutLabels.get(1).getText().trim().toLowerCase(), "Check out :".toLowerCase());

		return this;
	}

	public TravelDealDetailPage verifyTravelMaps(){

		Assert.assertTrue(locator.mapsImage.isDisplayed());
		Assert.assertTrue(locator.mapsImage.isEnabled());
		locator.mapsImage.click();

		waitVar.until(ExpectedConditions.presenceOfElementLocated(By.cssSelector(".location__cont__map")));
		Assert.assertTrue(locator.locationPopUp.getAttribute("class").contains("modal-show"));
		Assert.assertTrue(locator.mapsLocationAddress.get(0).getAttribute("class").equals("active"));

		Assert.assertEquals(locator.mapsLocationAddress.size(), 1);
		locator.locationPopUpCloseButton.click();
		waitVar.until(ExpectedConditions.invisibilityOfElementLocated(By.cssSelector(".location__cont__map")));
		Assert.assertTrue(locator.locationPopUp.getAttribute("class").contains("modal-hide"));

		return this;
	}

	public TravelDealDetailPage verifyFacilities(){

		Assert.assertEquals(locator.facilitiesHeading.getText(), "Facilities");

		if(locator.listOfFacilities.size()>6){
			Assert.assertTrue(locator.moreOrLessLinkInFacilities.isDisplayed());
			Assert.assertTrue(locator.moreOrLessLinkInFacilities.isEnabled());
			Assert.assertEquals("More".toLowerCase(),locator.moreOrLessLinkInFacilities.getText().trim().toLowerCase());
			Assert.assertTrue(locator.facilitiesContentDiv.getAttribute("class").contains("show-less"));
			locator.moreOrLessLinkInFacilities.click();
			waitVar.until(ExpectedConditions.presenceOfElementLocated(By.cssSelector(".facalities__content.show-more")));
			Assert.assertTrue(locator.facilitiesContentDiv.getAttribute("class").contains("show-more"));
			Assert.assertEquals("Less".toLowerCase(),locator.moreOrLessLinkInFacilities.getText().trim().toLowerCase());

		}

		for(WebElement each : locator.listOfFacilities){
			Assert.assertTrue(each.isDisplayed());
			Assert.assertTrue(each.findElement(By.cssSelector("img")).isDisplayed());
			Assert.assertTrue(each.findElement(By.cssSelector("p")).isDisplayed());
		}

		return this;
	}

	public TravelDealDetailPage howToUseOfferAndThingsToRemember(){

		Assert.assertEquals(locator.headings.get(1).getText().trim(), "How to use the offer");
		Assert.assertEquals(locator.headings.get(2).getText().trim(), "Things to remember");
		for(WebElement each : locator.listBulletPoints){
			Assert.assertTrue(each.isDisplayed());
		}

		return this;
	}
}
