package com.groupon.web.nearbuy.data;

import org.testng.annotations.DataProvider;

/**
 * 
 * @author prachi.nagpal
 *
 */
public class ChangeYourLocationData {

	@DataProvider(name = "changeLocation")
	public static Object[][] provideLocation(){
		return new Object[][] {

				{"Goa"},
		};
	}
	
}
