package com.groupon.web.nearbuy.page;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;

import com.groupon.web.common.page.BasePage;
import com.groupon.web.common.utils.WaitUtility;
import com.groupon.web.nearbuy.locator.HomeLocator;
import com.groupon.web.nearbuy.utils.ImageServiceUtils;

public class HomePage extends BasePage {

	private static final Logger logger = Logger.getLogger(HomePage.class);
	public HomeLocator locator;

	public HomePage() {
		this.locator = PageFactory.initElements(getWebDriver(), HomeLocator.class);
	}

	/**
	 * Crousel & Image changes correctly
	 */
	public HomePage upperCrousel(){
		final List<WebElement> upperCarousalElements = locator.upperCrousel;
		final By activeSlideBy = By.cssSelector("#slider1 li.flex-active-slide");
		for(int i = upperCarousalElements.size() - 1; i >= 0; i--) {
			WebElement each = upperCarousalElements.get(i);
			String cText = each.getText();
			logger.info("carousel text : " +cText);
			final WebElement activeSliderElement = getWebDriver().findElement(activeSlideBy);
			logger.info("last selected slider caption:"+activeSliderElement.findElement(By.cssSelector(".product_image .deal_caption p")).getText());
			each.click();
			WaitUtility.waitForAttributeNotContainsGivenValue(activeSliderElement, "class", "flex-active-slide", getWebDriver(), waitVar);
			WebElement caption = getWebDriver().findElement(By.cssSelector("#slider1 li.flex-active-slide .product_image .deal_caption p"));
			logger.info("Deal Caption Text: " + caption.getText());
			Assert.assertEquals(cText.trim().toLowerCase(), caption.getText().trim().toLowerCase());
		}
		return this;
	}

	/**
	 * Verify "What Are You Looking For" Section
	 */
	public HomePage category_slider(){

		for(WebElement each : locator.categorySlider){
			Assert.assertTrue(each.isDisplayed());
			Assert.assertTrue(each.isEnabled());
			System.out.println("Category Slider  : " + each.getText());
		}
		for(WebElement img : locator.categorySliderImage){
			Assert.assertTrue(img.isDisplayed());
			System.out.println("Image of category Slider  :" + img.getText());
		}
		return this;
	}

	/**
	 * Verify Headings and blocks for each
	 */
	public HomePage verify_blocks_headings_and_anchorTags(){

		for(WebElement each : locator.allBlocksHeading){
			Assert.assertTrue(each.isDisplayed());
			Assert.assertTrue(each.isEnabled());
			System.out.println("Headings : " + each.getText());
		}
		for(WebElement img : locator.allBlocksAchorTags){
			Assert.assertTrue(img.isDisplayed());
			Assert.assertTrue(img.isEnabled());
			System.out.println("Anchor Tags : " + img.getText());
		}
		for(WebElement seeAllLink : locator.seeAllLinks){
			Assert.assertTrue(seeAllLink.isDisplayed());
			Assert.assertTrue(seeAllLink.isEnabled());
			System.out.println("See All Link Text: " + seeAllLink.getText());
		}
		return this;
	}

	/**
	 * Verify Ads Section on Home page
	 * @return
	 */
	public HomePage verifyAds(){

		if(WaitUtility.isElementPresentByLocator(getWebDriver(), By.cssSelector(".ad_banner1 .alert-box a"))){
			for(WebElement ad : locator.adsSection){

				Assert.assertTrue(ad.isDisplayed());
				Assert.assertTrue(ad.isEnabled());
			}

			for(WebElement adClose : locator.adsCloseButtons){

				Assert.assertTrue(adClose.isDisplayed());
				Assert.assertTrue(adClose.isEnabled());
			}
		}
		else 
			logger.info("No Ads present in the listing page");

		return this;
	}

	/**
	 * Verify View All Link
	 * @return
	 */
	public HomePage viewAllOffersButton(){

		Assert.assertTrue(locator.viewAllOffersButton.isDisplayed());
		Assert.assertTrue(locator.viewAllOffersButton.isEnabled());
		locator.viewAllOffersButton.click();
		waitVar.until(ExpectedConditions.urlContains("all-offers"));
		Assert.assertTrue(locator.currentBreadcrumb.getText().contains("ALL OFFERS"));

		return this;
	}

	/**
	 * Migrate to Nearbuy PopUp
	 * @return
	 */
	public HomePage migratePopUp(){

		Assert.assertTrue(locator.driftBox.isDisplayed());
		Assert.assertTrue(locator.driftBoxText.isDisplayed());
		logger.info("Text on Drift Box : " + locator.driftBoxText.getText());
		Assert.assertTrue(locator.driftBoxNearbuyImage.isDisplayed());

		for(WebElement each : locator.driftBoxYesNoText){
			Assert.assertTrue(each.isDisplayed());
			Assert.assertTrue(each.isEnabled());
			logger.info("Text :" + each.getText());
		}

		//Click on yes option & validate
		for(WebElement each : locator.driftBoxYesNoText){
			if(each.getText().equals("Yes")){
				each.click();
				logger.info("User has clicked on Yes option");
				waitVar.until(ExpectedConditions.urlContains("gpmerge"));
				logger.info("Migration window has opened");
				break;
			}
		}

		//Navigate back
		getWebDriver().navigate().back();
		waitVar.until(ExpectedConditions.visibilityOf(locator.driftBox));
		Assert.assertTrue(locator.driftBox.isDisplayed());

		//Click on no option & validate
		for(WebElement each : locator.driftBoxYesNoText){
			if(each.getText().equals("No")){
				each.click();
				logger.info("User has clicked on No option");
				waitVar.until(ExpectedConditions.invisibilityOfElementLocated(By.id("drift-box")));
				logger.info("Migration Box disappears");
				break;
			}
		}

		return this;
	}

	/**
	 * New Search box on local home page
	 * @return
	 */
	public HomePage verifySearchBoxPresence(){

		Assert.assertTrue(locator.heading.isDisplayed());
		Assert.assertEquals(locator.heading.getText(), "Experience the world around you");
		Assert.assertTrue(locator.newSearchBoxDiv.isDisplayed());
		Assert.assertTrue(locator.searchLocationIcon.isDisplayed());
		Assert.assertTrue(locator.searchLocationName.isDisplayed());
		Assert.assertTrue(locator.searchLocationName.isEnabled());
		logger.info("Selected location : " + locator.searchLocationName.getText());
		Assert.assertTrue(locator.searchBox.isDisplayed());
		Assert.assertTrue(locator.searchBox.isEnabled());
		Assert.assertTrue(locator.searchBox.getAttribute("placeholder").equals("Search restaurants, spa, events, things to do..."));
		Assert.assertTrue(locator.searchButton.isDisplayed());
		Assert.assertTrue(locator.searchButton.isEnabled());

		return this;
	}

	public HomePage verifyIconsClickabilityAndImagePresence(){

		logger.info("No of Icons present : " + locator.iconsURLs.size());
		Assert.assertEquals(locator.iconsURLs.size(), 7);
		waitVar.until(ExpectedConditions.visibilityOf(locator.backgroundImage));
		String mainBackgroundImageUrl = locator.backgroundImage.getCssValue("background");
		System.out.println("Image URL : " + mainBackgroundImageUrl);
		//		String url = mainBackgroundImageUrl.substring(mainBackgroundImageUrl.indexOf("\""), mainBackgroundImageUrl.lastIndexOf("\""));
		//		System.out.println("URL : " + url);

		for (WebElement iconImg : locator.iconsImages){
			Assert.assertTrue(iconImg.isDisplayed());
			Assert.assertTrue(iconImg.isEnabled());

		}

		for (WebElement iconURL : locator.iconsURLs){
			Assert.assertTrue(iconURL.isDisplayed());
			Assert.assertTrue(iconURL.isEnabled());
		}

		for (WebElement iconLabel : locator.iconsLabels){
			Assert.assertTrue(iconLabel.isDisplayed());
			logger.info(iconLabel.getText());
		}

		return this;
	}

	public HomePage verifyTopPicksData(String dealId, String dealTitle){

		waitVar.until(ExpectedConditions.visibilityOf(locator.searchBox));
		WebElement deal = getWebDriver().findElement(By.xpath(".//*[contains(@class,'deals')]//h3[contains(text(),'Top')]/..//div[@data-dealid='"+dealId+"']"));
		WebElement dealTitleElement = deal.findElement(By.className("card__title"));
		Assert.assertEquals(dealTitle.trim().toLowerCase(), dealTitleElement.getText().trim().toLowerCase());

		return this;
	}

	/**
	 * Fetch Values from HTML and verify on Data Layer
	 * @return
	 */
	public HomePage verifyDataLayerHome() {
		//prepare map by extracting values from html
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("city", locator.dlCity.getAttribute("value"));
		map.put("dealTitle1", locator.dlDealTitle1.getAttribute("value"));
		map.put("dealTitle2", locator.dlDealTitle2.getAttribute("value"));
		map.put("dealTitle3", locator.dlDealTitle3.getAttribute("value"));
		map.put("deal-1", locator.dlDeal1.getAttribute("value"));
		map.put("deal-2", locator.dlDeal2.getAttribute("value"));
		map.put("deal-3", locator.dlDeal3.getAttribute("value"));
		map.put("pageType", "City Home Page");
		map.put("vertical", locator.dlVertical.getAttribute("data-vertical"));
		map.put("sitetype", "d");

		verifyDataLayer(map,"2");
		return this;
	}

	public HomePage travelRowVerificationInCitySelectionPopUp() {
		locator.locationSelectedInSearchBox.click();
		waitVar.until(ExpectedConditions.visibilityOf(locator.cityLocationModal));
		locator.travelRowLeftLink.click();
		waitVar.until(ExpectedConditions.urlContains("travel"));
		locator.nearbuyLogo.click();
		locator.locationSelectedInSearchBox.click();
		locator.travelRowRightLink.click();
		waitVar.until(ExpectedConditions.urlContains("travel"));

		return this;
	}

	public HomePage categoryIcons(){

		try {
			Thread.sleep(2000);
		} catch (InterruptedException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}

		waitVar.until(ExpectedConditions.visibilityOf(locator.secondaryNavWrapper));

		for(WebElement icon:locator.categoryIcons){

			Assert.assertTrue("Category Icons is NOT enabled",icon.isEnabled());
		}

		clickOnMoreAndVerifyCategoryIcons();

		for(WebElement href : locator.categoryIconsHref)
		{
			String hrefText = href.getAttribute("href");
			Assert.assertFalse("Href in Category Icon is EMPTY",hrefText.isEmpty());
			href.click();
			WaitUtility.waitForURLtoChange(getWebDriver(), waitVar);
			waitVar.until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector(".breadcrumbs")));
			String newURL = getWebDriver().getCurrentUrl();
			Assert.assertTrue("New URL & Category Icon href URL Doesn't matches",newURL.contains(hrefText));
			break;
		}

		try {
			Thread.sleep(2000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return this;
	}

	private void clickOnMoreAndVerifyCategoryIcons() {
		if(locator.categoryIcons.size()>=9){

			waitVar.until(ExpectedConditions.elementToBeClickable(locator.moreIcon));
			Assert.assertTrue(locator.moreIcon.isDisplayed());
			Assert.assertTrue(locator.moreIcon.isEnabled());
			Assert.assertTrue("Text More is NOT coming",locator.moreIconText.getText().equals("More"));
			locator.moreIcon.click();
			//WaitUtility.waitForAttributeContainsGivenValue(By.cssSelector(".secondary-nav"), "class", "secondary-nav--open", getWebDriver(), waitVar);
			waitVar.until(ExpectedConditions.visibilityOfAllElements(locator.categoryIcons));
			Assert.assertTrue("",getWebDriver().findElement(By.cssSelector(".secondary-nav")).getAttribute("class").contains("secondary-nav--open"));
			Assert.assertTrue("Text Less is NOT coming",locator.moreIconText.getText().equals("Less"));
			for(WebElement icon:locator.categoryIcons){

				Assert.assertTrue("Category Icons is NOT displayed",icon.isDisplayed());
				Assert.assertTrue("Category Icons is NOT enabled",icon.isEnabled());
			}
			for(WebElement text:locator.categoryIconsText){

				Assert.assertTrue("Category Icons Text is NOT displayed",text.isDisplayed());
			}

			for(WebElement href : locator.categoryIconsHref)
			{
				String hrefText = href.getAttribute("href");
				Assert.assertFalse("Href in Category Icon is EMPTY",hrefText.isEmpty());
			}
			int i=1;
			for(WebElement img : locator.categoryIconsImage){
				Assert.assertFalse("Category Icon Image is NOT displayed" , img.getCssValue("background-position").equals("0px 0px"));
				i++;
			}
			System.out.println("Image is not displayed for category : " + (i+1));
		}
		else
		{
			Boolean flag = WaitUtility.isElementPresent(getWebDriver(), locator.moreIcon);
			Assert.assertTrue("More Icon is Present even in case number of Category Icons < 9",!flag);
		}
	}


	public HomePage verifyLocalStorefront(){

		verifySectionsSequenceOnLocalStorefront();
		verifyRHRN();
		verifyTrendingRightNow();
		verifyPopularBrandsNewOnAndHotelsOnNearbuy();
		verifyTopRatedInYourCity();
		verifyFreeOffers();
		verifyMostSearchedFrom();

		return this;
	}

	private void verifyMostSearchedFrom() {

		Assert.assertTrue("Location Div Prev Arrow is NOT displayed", locator.mostSearchedFromPrevArrow.isDisplayed());
		Assert.assertFalse("Location Div Prev Arrow is enabled", locator.mostSearchedFromPrevArrow.isEnabled());

		Assert.assertTrue("Location Div is NOT displayed", locator.mostSearchedFromNextArrow.isDisplayed());
		Assert.assertTrue("Location Div is NOT enabled", locator.mostSearchedFromNextArrow.isEnabled());
		while(locator.mostSearchedFromNextArrow.getAttribute("class").contains("next--disable")){
			locator.mostSearchedFromNextArrow.click();
			break;
		}

		for(WebElement img : locator.RHRNimages){
			try {
				ImageServiceUtils.verifyImageURL(img);
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		Assert.assertTrue(locator.sectionHeadings.get(6).getText().equals("Most Searched From "+ new HeaderFooterPage().getCurrentLocationSelected()));
	}

	private void verifyFreeOffers() {

		Assert.assertTrue("Free Offers Div is NOT displayed", locator.freeOffersDiv.isDisplayed());
		for(WebElement card : locator.rafflesCards){
			Assert.assertTrue("Raffles Cards is NOT displayed", card.isDisplayed());
		}
		for(WebElement img : locator.rafflesImages){
			Assert.assertTrue("Raffles image is NOT displayed", img.isDisplayed());
			try {
				ImageServiceUtils.verifyImageURL(img);
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		for(WebElement title : locator.rafflesTitles){
			Assert.assertTrue("Raffles Cards Title is NOT displayed", title.isDisplayed());
		}
		for(WebElement subTitle : locator.rafflesSubTitles){
			Assert.assertTrue("Raffles Cards Title is NOT displayed", subTitle.isDisplayed());
		}
		for(WebElement price : locator.rafflesPrice){
			Assert.assertTrue("Raffles Price is NOT displayed", price.isDisplayed());
			Assert.assertTrue("Raffles Price is NOT Zero", price.getText().equals("Free"));
		}

		Assert.assertTrue("Free Prev Arrow is NOT displayed", locator.rafflesPrevArrow.isDisplayed());
		Assert.assertFalse("Free Prev Arrow is enabled", locator.rafflesPrevArrow.isEnabled());

		Assert.assertTrue("Free Next Arrow is NOT displayed", locator.rafflesNextArrow.isDisplayed());
		Assert.assertTrue("Free Next Arrow is NOT enabled", locator.rafflesNextArrow.isEnabled());
		while(locator.rafflesNextArrow.getAttribute("class").contains("next--disable")){
			locator.rafflesNextArrow.click();
			break;
		}

	}

	private void verifyTopRatedInYourCity() {
		Assert.assertTrue("See All Link in Top Rated is NOT displayed", locator.seeAllLinkForToRatedInYourCity.isDisplayed());
		Assert.assertTrue("See All Link in Top Rated is NOT displayed", locator.seeAllLinkForToRatedInYourCity.isEnabled());
	}

	private void verifyPopularBrandsNewOnAndHotelsOnNearbuy() {

		for(WebElement next : locator.nextArrowButtonForNavPanels){

			Assert.assertTrue("By default next arrow is NOT enabled", next.isEnabled());
			Assert.assertTrue("By default next arrow is NOT displayed", next.isDisplayed());

			while(next.getAttribute("class").contains("next--disable")){
				next.click();
			}
		}
		for(WebElement prev : locator.prevArrowButtonForNavPanels){

		//	Assert.assertFalse("By default prev arrow is enabled", prev.isEnabled());
			Assert.assertTrue("By default prev arrow is NOT displayed", prev.isDisplayed());

		}
		if(!getCurrentUrl().contains("travel")){
			String mostSearchedHeadingText = locator.sectionHeadings.get(6).getText();
			System.out.println(mostSearchedHeadingText);
			Assert.assertTrue(mostSearchedHeadingText.equals("Most Searched From " + new HeaderFooterPage().getCurrentLocationSelected()));
		}
		if(getCurrentUrl().contains("travel")){
			Assert.assertTrue(locator.sectionHeadings.get(1).getText().equals("Hotels In " + new HeaderFooterPage().getCurrentLocationSelected()));
			Assert.assertTrue(locator.sectionHeadings.get(0).getText().equals("Most Searched From " + new HeaderFooterPage().getCurrentLocationSelected()));
		}

	}

	private void verifyTrendingRightNow() {

		Dimension numberOfTrendingDeals = locator.slideSections.get(1).findElement(By.cssSelector("div")).getSize();
		System.out.println("Number of Trending Deals : " + numberOfTrendingDeals);

	}

	private void verifySectionsSequenceOnLocalStorefront() {
		
		waitVar.until(ExpectedConditions.visibilityOf(locator.searchBox));
		
		for(int i=1; i<=10; i++){
			getWebDriver().findElement(By.cssSelector("body")).sendKeys(Keys.PAGE_DOWN);
		}
		waitVar.until(ExpectedConditions.visibilityOf(locator.viewAllOffersButton));

		for(WebElement heading : locator.sectionHeadings){

			Assert.assertTrue("Heading is NOT displayed",heading.isDisplayed());
			System.out.println(heading.getText());
		}
		Assert.assertTrue(locator.sectionHeadings.get(0).getText().contains("Things To Do In"));
		Assert.assertTrue(locator.sectionHeadings.get(1).getText().equals("Trending Right Now"));
		Assert.assertTrue(locator.sectionHeadings.get(2).getText().equals("Popular Brands On Nearbuy"));
		Assert.assertTrue(locator.sectionHeadings.get(3).getText().equals("Top Rated In Your City"));
		Assert.assertTrue(locator.sectionHeadings.get(4).getText().equals("Free Offers"));
		//Assert.assertTrue(locator.sectionHeadings.get(5).getText().equals("Top Picks"));
		Assert.assertTrue(locator.sectionHeadings.get(5).getText().equals("New On Nearbuy"));
		Assert.assertTrue(locator.sectionHeadings.get(6).getText().contains("Most Searched From"));
		Assert.assertTrue(locator.sectionHeadings.get(7).getText().contains("Hotels In"));
		verifyViewAllOffersButton();

	}

	private void verifyViewAllOffersButton() {
		Assert.assertTrue(locator.viewAllOffersButton.isDisplayed());
		Assert.assertTrue(locator.viewAllOffersButton.isEnabled());
		Assert.assertTrue(locator.viewAllOffersButton.getText().equals("VIEW ALL OFFERS"));
	}

	private void verifyRHRN(){

		int numberOfRHRNcards = locator.RHRNcollectionCards.size();
		System.out.println(numberOfRHRNcards);
		String RHRNheading = locator.sectionHeadings.get(0).getText();
		//TO DO - BREAKING
		//Assert.assertTrue("RHRN Heading NOT correct",RHRNheading.equals((numberOfRHRNcards+2) + " Things To Do In " + new HeaderFooterPage().getCurrentLocationSelected()));

		Assert.assertTrue("By default RHRN prev arrow is NOT displayed", locator.RHRNprevArrowButton.isDisplayed());
		//Assert.assertFalse("By default RHRN prev arrow is enabled", locator.RHRNprevArrowButton.isEnabled());
		Assert.assertTrue("By default RHRN prev arrow is NOT containing class disbaled", locator.RHRNprevArrowButton.getAttribute("class").contains("prev--disable"));

		Assert.assertTrue("By default RHRN next arrow is NOT displayed", locator.RHRNnextArrowButton.isDisplayed());
		Assert.assertTrue("By default RHRN next arrow is NOT enabled", locator.RHRNnextArrowButton.isEnabled());

		// TO DO - BREAKING
		//verifyCollectionCardUI();

//		try {
//			for(WebElement img : locator.RHRNimages){
//				ImageServiceUtils.verifyImageURL(img);
//			}
//		} catch (IOException e) {
//			e.printStackTrace();
//		}
	}

	private void verifyCollectionCardUI() {
		
		for(int i=1;i<=3;i++){
			locator.RHRNnextArrowButton.click();
		}

		for(WebElement img : locator.RHRNimages){
			Assert.assertTrue("RHRN images are NOT displayed", img.isDisplayed());
		}
		for(WebElement title : locator.collectionTitle){
			Assert.assertTrue("Collection Titles are NOT displayed", title.isDisplayed());
		}
		for(WebElement icon : locator.collectionIcons){
			Assert.assertTrue("Collection Icons are NOT displayed", icon.isDisplayed());
		}
		for(WebElement number : locator.numberOfOffersInCollectionCards){
			Assert.assertTrue("Collection Number of offers are NOT displayed", number.isDisplayed());
		}
		for(WebElement range : locator.priceRangeInCollectionCards){
			Assert.assertTrue("Collection Price Range are NOT displayed", range.isDisplayed());
		}

	}

	public HomePage verifyTravelStorefront(){

		verifyTravelSearchBox();
		verifyMostSearchedFrom();
		verifyPopularBrandsNewOnAndHotelsOnNearbuy();
		verifyPopularBrandsNewOnAndHotelsOnNearbuy();
		verifyPopularDestinations();
		verifySectionsSequenceOnTravelStorefront();

		return this;
	}

	private void verifySectionsSequenceOnTravelStorefront() {
		Assert.assertTrue(locator.sectionHeadings.get(0).getText().contains("Most Searched From " + new HeaderFooterPage().getCurrentLocationSelected()));
		Assert.assertTrue(locator.sectionHeadings.get(1).getText().contains("Hotels In " + new HeaderFooterPage().getCurrentLocationSelected()));
		Assert.assertTrue(locator.sectionHeadings.get(2).getText().contains("Popular Destinations"));
		Assert.assertTrue(locator.sectionHeadings.get(3).getText().contains("Popular Brands On Nearbuy"));
		verifyViewAllOffersButton();
	}

	private void verifyPopularDestinations() {

		for(WebElement img : locator.RHRNimages){
			try {
				ImageServiceUtils.verifyImageURL(img);
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		for(WebElement title : locator.travelLocationCardTitles){
			Assert.assertTrue(title.isDisplayed());
		}
	}

	private void verifyTravelSearchBox() {
		Assert.assertTrue("Travel Search Box is NOT displayed", locator.travelSearchDiv.isDisplayed());
		Assert.assertTrue("Travel Search Heading is NOT displayed", locator.travelSearchHeading.isDisplayed());
		Assert.assertTrue("Travel Search Right Content is NOT displayed", locator.travelSearchRightContent.isDisplayed());
		for(WebElement box : locator.travelSearchBoxes){
			Assert.assertTrue(box.isDisplayed());
			Assert.assertTrue(box.isEnabled());
		}
		Assert.assertTrue("Search box placeholder is NOT correct",locator.travelSearchBoxes.get(0).findElement(By.cssSelector("p")).getText().equals("Search for a city or hotel"));		
		Assert.assertTrue("Search box placeholder is NOT correct",locator.travelSearchBoxes.get(1).findElement(By.cssSelector("p")).getText().equals("Check-in date"));		
		Assert.assertTrue("Search box placeholder is NOT correct",locator.travelSearchBoxes.get(2).findElement(By.cssSelector("p")).getText().equals("Check-out date"));

		for(WebElement icon : locator.travelCalenderIcon){
			Assert.assertTrue(icon.isDisplayed());
		}

		Assert.assertTrue("Find Hotels button is NOT displayed",locator.findHotelsButton.isDisplayed());
		Assert.assertTrue("Find Hotels button is NOT enabled",locator.findHotelsButton.isDisplayed());
		Assert.assertTrue("Find Hotels button text is NOT correct",locator.findHotelsButton.getText().equals(" Find hotels "));
	}


}
