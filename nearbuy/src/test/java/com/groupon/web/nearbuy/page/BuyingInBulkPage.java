package com.groupon.web.nearbuy.page;

import com.groupon.web.common.page.BasePage;
import com.groupon.web.nearbuy.locator.BuyingInBulkLocator;
import junit.framework.Assert;
import org.apache.log4j.Logger;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;

import java.util.ArrayList;

public class BuyingInBulkPage extends BasePage {

	public BuyingInBulkLocator locator;
	private static final Logger logger = Logger.getLogger(BuyingInBulkPage.class);
	String oldTab ;


	public BuyingInBulkPage() {
		locator = PageFactory.initElements(getWebDriver(), BuyingInBulkLocator.class);
	}


	/**
	 * Click on Buying in Bulk Button
	 * @return
	 */
	public BuyingInBulkPage clickOnBuyingInBulkButton(){

		oldTab = getWebDriver().getWindowHandle();
        DealDetailPage dealDetailPage = new DealDetailPage();
		return this;
	}

	/**
	 * Fill in the Buying In Bulk form
	 * @param name
	 * @param email
	 * @param qty
	 * @param organization
	 * @param phone
	 * @return
	 */
	public BuyingInBulkPage fillForm(String name, String email, String qty, String organization, String phone){

		ArrayList<String> newTab = new ArrayList<String>(getWebDriver().getWindowHandles());

		// change focus to new tab
		getWebDriver().switchTo().window(newTab.get(0));

		//write code to open new link

		for( int k= 0 ; k< newTab.size(); k++  ){
			for(String winHandle : getWebDriver().getWindowHandles()){
				getWebDriver().switchTo().window(newTab.get(k));

			}}


		Assert.assertTrue(locator.dealHeading.isDisplayed());
		Assert.assertTrue(locator.formHeading.isDisplayed());
		logger.info("Form Heading : " + locator.formHeading.getText());
		logger.info("Deal Heading : " + locator.dealHeading.getText());

		Assert.assertTrue(locator.nameTextBox.isDisplayed());
		Assert.assertTrue(locator.nameTextBox.isEnabled());
		locator.nameTextBox.sendKeys(name);

		Assert.assertTrue(locator.emailTextBox.isDisplayed());
		Assert.assertTrue(locator.emailTextBox.isEnabled());
		locator.emailTextBox.sendKeys(email);

		Assert.assertTrue(locator.quantityTextBox.isDisplayed());
		Assert.assertTrue(locator.quantityTextBox.isEnabled());
		locator.quantityTextBox.sendKeys(qty);

		Assert.assertTrue(locator.organizationTextBox.isDisplayed());
		Assert.assertTrue(locator.organizationTextBox.isEnabled());
		locator.organizationTextBox.sendKeys(organization);

		Assert.assertTrue(locator.phoneTextBox.isDisplayed());
		Assert.assertTrue(locator.phoneTextBox.isEnabled());
		locator.phoneTextBox.sendKeys(phone);

		Assert.assertTrue(locator.submitButton.isDisplayed());
		Assert.assertTrue(locator.submitButton.isEnabled());
		locator.submitButton.submit();

		return this;
	}

	/**
	 * Thank You Message Verification And Buttons Verification after Filling Booking Form
	 *
     * @param menuUrl
     * @param productUrl
     * @return
	 */
	public BuyingInBulkPage thanksMessageAndButtonsVerification(String menuUrl, String productUrl, String buttonText){

		Assert.assertTrue(locator.thanksMessage.isDisplayed());
		logger.info("Thank You Message Heading : " + locator.thanksMessage.getText());

		for(WebElement button : locator.continueShoppingAndBackToDealButtons){

			Assert.assertTrue(button.isDisplayed());
			Assert.assertTrue(button.isEnabled());
			logger.info("Button Text : " + button.getText());
		}

		for(WebElement clickOnButton : locator.continueShoppingAndBackToDealButtons){

            final String text = clickOnButton.getText();
            if(text.equals(buttonText) && buttonText.equals("Continue Shopping")){
				clickOnButton.click();
				waitVar.until(ExpectedConditions.urlContains("deals"));
				logger.info(text + " is clicked");
				getWebDriver().navigate().back();
				waitVar.until(ExpectedConditions.urlContains("bulk-order"));
				logger.info("Navigated back to thank you page of bulk order form");
                break;
			}
			else if(text.equals(buttonText) && buttonText.equals("Back to Offer")){
				clickOnButton.click();
				waitVar.until(ExpectedConditions.urlToBe(productUrl));
				logger.info(text + " is clicked");
				getWebDriver().navigate().back();
				waitVar.until(ExpectedConditions.urlContains("bulk-order"));
				logger.info("Navigated back to thank you page of bulk order form");
                break;
			}
		}

		return this;
	}

}
