package com.groupon.web.nearbuy.page;

import com.groupon.web.common.page.BasePage;
import com.groupon.web.nearbuy.locator.ThankYouLocator;

import java.util.HashMap;
import java.util.Map;

import org.apache.log4j.Logger;
import org.junit.Assert;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;

public class ThankYouPage extends BasePage {

	private static final Logger logger = Logger.getLogger(ThankYouPage.class);
	private ThankYouLocator locator;

	public ThankYouPage() {
		locator = PageFactory.initElements(getWebDriver(), ThankYouLocator.class);
	}

	/**
	 * Verify Congrats Message
	 * @return
	 */
	public ThankYouPage verifyCongratsMessage(){

		waitVar.until(ExpectedConditions.urlContains("payment-vendor"));
		Assert.assertTrue(locator.congratulationsHeading.isDisplayed());
		logger.info("Heading Text : " + locator.congratulationsHeading.getText());
		Assert.assertTrue(locator.userName.isDisplayed());
		logger.info("Message : " + locator.userName.getText());

		return this;
	}

	/**
	 * Verify Order Details
	 * @return
	 */
//	public ThankYouPage verifyOrderDetails(){
//
//		OrderSummaryPage orderSummaryPage = new OrderSummaryPage();
//		for(WebElement each : orderSummaryPage.getOrderTableHeadings()){
//
//			Assert.assertTrue(each.isDisplayed());
//			logger.info("Order Heading : " + each.getText());
//		}
//
//		for(WebElement bold : locator.boldText){
//
//			Assert.assertTrue(bold.isDisplayed());
//			logger.info("Bold text : " + bold.getText());
//		}
//
//		for(WebElement detail : orderSummaryPage.getOrderTableDetails()){
//
//			Assert.assertTrue(detail.isDisplayed());
//			logger.info("Detail : " + detail.getText());
//		}
//
////		Assert.assertTrue(locator.viewDealLink.isDisplayed());
////		Assert.assertTrue(locator.viewDealLink.isEnabled());
////		logger.info("Link Text : " + locator.viewDealLink.getText());
//
//		Assert.assertTrue(locator.dealImage.isDisplayed());
//
//		return this;
//	}

	/**
	 * Verify Data Layer
	 * @return
	 */
	public ThankYouPage verifyDataLayerThankYou() {
		//prepare map by extracting values from html
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("dealId", locator.dlDealId.getAttribute("data-dealid"));
		map.put("dealPrice", locator.dlDealPrice.getAttribute("data-offeramount"));
		map.put("dealTitle", locator.dlDealTitle.getAttribute("data-title"));
		map.put("finalAmountPaid", locator.dlFinalAmountPaid.getAttribute("data-finalamount"));
		map.put("grossBookingValue", locator.dlGrossBookingValue.getAttribute("data-totalofferamount"));
		map.put("numberOfVouchers", locator.dlNumberOfVouchers.getAttribute("data-quantity"));
		map.put("orderId", locator.dlOrderId.getAttribute("data-orderid"));
		map.put("permalink", locator.dlPermalink.getAttribute("data-permalink"));
		map.put("sku", locator.dlSku.getAttribute("data-offerid"));
		map.put("vertical", locator.dlVertical.getAttribute("data-vertical"));
		map.put("pageType", "nearbuy confirmation page");
		map.put("sitetype", "d");

		verifyDataLayer(map,"3");
		return this;
	}
	
	/**
	 * Get Title of Thank You Page
	 * @param currentLocationSelected 
	 * @return
	 */
	public ThankYouPage getTitleOfThankYouPage(String currentLocationSelected){
		HeaderFooterPage headerFooterPage = new HeaderFooterPage();
		String currentTitle = getWebDriver().getTitle();
		Assert.assertEquals("nearbuy (formerly Groupon) | Best deals in " + currentLocationSelected + " - Restaurants, Spa & Salon, Fitness, Movies, Activities and Travel", currentTitle);
		return this;
	}
	

}
