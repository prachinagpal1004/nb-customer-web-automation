//package com.groupon.web.nearbuy.test;
//
//import com.groupon.web.common.test.BaseTest;
//import com.groupon.web.common.utils.TestListener;
//import com.groupon.web.nearbuy.data.AuthenticationData;
//import com.groupon.web.nearbuy.data.MenuData;
//import com.groupon.web.nearbuy.data.SearchData;
//import com.groupon.web.nearbuy.page.AuthenticationPage;
//import com.groupon.web.nearbuy.page.DealCatalogPage;
//import com.groupon.web.nearbuy.page.DealDetailPage;
//import com.groupon.web.nearbuy.page.HeaderFooterPage;
//import com.groupon.web.nearbuy.page.MyAccountPage;
//import com.groupon.web.nearbuy.page.OrderSummaryPage;
//import com.groupon.web.nearbuy.page.PaytmWalletPage;
//import com.groupon.web.nearbuy.page.ThankYouPage;
//import com.relevantcodes.extentreports.LogStatus;
//
//import org.testng.annotations.BeforeMethod;
//import org.testng.annotations.Test;
///**
// * 
// * @author Prachi Nagpal
// *
// */
//@Test(groups = {"sanity", "staging"})
//public class SignOutTest extends BaseTest {
//
//	private AuthenticationPage authenticationPage;
//	private MyAccountPage myAccountPage;
//	private HeaderFooterPage headerFooterPage;
//	private DealDetailPage dealDetailPage;
//	private DealCatalogPage dealCatalogPage;
//
//	@BeforeMethod(alwaysRun=true)
//	public void init() {
//
//		authenticationPage = new AuthenticationPage();
//		headerFooterPage = new HeaderFooterPage();
//		dealDetailPage = new DealDetailPage();
//		dealCatalogPage = new DealCatalogPage();
//		myAccountPage = new MyAccountPage();
//	}
//	
//	
//	
//
//	/**
//	 * Test Success Email Id Login
//	 */
//	@Test(priority = 1 , dataProvider="successEmailIdLogin" ,dataProviderClass = AuthenticationData.class )
//	public void testSignOutFromHomePage(String username, String password, String message, String bool) throws Throwable {
//
//		TestListener.setLog(LogStatus.INFO, "Enter Credentials for Successful Email ID login");
//		authenticationPage.navigateToHome().clickOnLoginLink();
//		authenticationPage.verifyLoginAndErrorMessages(username, password, message, Boolean.valueOf(bool));
//	}
//
//	@Test(priority = 2, dataProvider="enterSubmenuForDealListingPage" , dataProviderClass = MenuData.class )
//	public void testSignOutFromListingPage(String mainMenu, String cat, String subCat) throws Throwable {
//		TestListener.setLog(LogStatus.INFO, "Navigate to Deal Listing Page from Menu");
//		headerFooterPage.navigateToMenu(mainMenu, cat, subCat);
//		TestListener.setLog(LogStatus.INFO, "Click on Sign Out Link & Verify");
//		myAccountPage.clickOnLink("Sign Out");
//	}
//	
//	@Test(priority = 4 , dataProvider="successEmailIdLogin" ,dataProviderClass = AuthenticationData.class )
//	public void testLoginFromHomePage(String username, String password, String message, String bool) throws Throwable {
//
//		TestListener.setLog(LogStatus.INFO, "Enter Credentials for Successful Email ID login");
//		authenticationPage.navigateToHome().clickOnLoginLink();
//		authenticationPage.verifyLoginAndErrorMessages(username, password, message, Boolean.valueOf(bool));
//	}
//
//	@Test(priority = 5, dataProvider="enterSubmenuForDealListingPage" , dataProviderClass = MenuData.class )
//	public void testSignOutFromDetailPage(String mainMenu, String cat, String subCat) throws Throwable {
//		TestListener.setLog(LogStatus.INFO, "Navigate to Deal Listing Page from Menu");
//		headerFooterPage.navigateToMenu(mainMenu, cat, subCat);
//		dealCatalogPage.clickOnSpecifiedDealCard(3);
//		TestListener.setLog(LogStatus.INFO, "Click on Sign Out Link & Verify");
//		myAccountPage.clickOnLink("Sign Out");
//	}
//
//	@Test(priority = 7 , dataProvider="successEmailIdLogin" ,dataProviderClass = AuthenticationData.class )
//	public void testSignOutFromDetailPage(String username, String password, String message, String bool) throws Throwable {
//
//		TestListener.setLog(LogStatus.INFO, "Enter Credentials for Successful Email ID login");
//		authenticationPage.clickOnLoginLink();
//		authenticationPage.verifyLoginAndErrorMessages(username, password, message, Boolean.valueOf(bool));
//	}
//	
//	@Test(priority = 8, dataProvider="enterSubmenuForDealListingPage" , dataProviderClass = MenuData.class )
//	public void testLandingOnOrderSummaryPaeg(String menu, String submenu) {
//		TestListener.setLog(LogStatus.INFO, "Navigate to Deal Listing Page from Menu");
//		headerFooterPage.navigateToMenu(menu, submenu);
//		dealCatalogPage.clickOnSpecifiedDealCard(1);
//		dealDetailPage.clickOnBuyNowButton();
//	}
//	
//	@Test(priority = 9 )
//	public void testSignOutFromOrderSummaryPage() throws Throwable {
//
//		TestListener.setLog(LogStatus.INFO, "Click on Sign Out Link & Verify");
//		myAccountPage.clickOnLink("Sign Out");
//	}
//
//}
//
