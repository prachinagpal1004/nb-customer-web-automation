package com.groupon.web.nearbuy.test;

import java.util.List;

import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import com.groupon.web.common.test.BaseTest;
import com.groupon.web.common.utils.TestListener;
import com.groupon.web.nearbuy.data.MenuData;
import com.groupon.web.nearbuy.dto.DealDetailDTO;
import com.groupon.web.nearbuy.page.DealCatalogPage;
import com.groupon.web.nearbuy.page.DealDetailPage;
import com.groupon.web.nearbuy.page.HeaderFooterPage;
import com.groupon.web.nearbuy.page.HomePage;
import com.groupon.web.nearbuy.utils.ApiDataUtils;
import com.relevantcodes.extentreports.LogStatus;

public class DataVerificationTest extends BaseTest {

	private HomePage homePage;
    private HeaderFooterPage headerFooterPage;
    private DealCatalogPage dealCatalogPage;
    private DealDetailPage dealDetailPage;

    @BeforeMethod(alwaysRun=true)
    public void init() {
		homePage = new HomePage();
        headerFooterPage = new HeaderFooterPage();
        dealCatalogPage = new DealCatalogPage();
        dealDetailPage = new DealDetailPage();
    }

//    @Test(priority = 1 , dataProvider="topPicks" , dataProviderClass = HomeData.class )
//	public void verifyTopPicksData(String dealId, String dealTitle){
//    	homePage.verifyTopPicksData(dealId,dealTitle);
//	}

    @Test(priority = 2, dataProvider="foodAndDrink" , dataProviderClass = MenuData.class )
    public void verifyDealDetailData(String menu, String submenu,String subsubmenu){
        TestListener.setLog(LogStatus.INFO, "Navigate to Deal Listing Page from Menu");
        headerFooterPage.navigateToMenu(menu, submenu, subsubmenu);

        TestListener.setLog(LogStatus.INFO, "Click on Featured Deal");
        dealCatalogPage.clickOnSpecifiedDealCard(1);

        TestListener.setLog(LogStatus.INFO, "Fetch DealId from Page");
        String dealId = dealDetailPage.getCurrentDealId();

        TestListener.setLog(LogStatus.INFO, "Fetch Deal Details using Rest API");
        DealDetailDTO expedtedDealDetails = ApiDataUtils.getDealDetailByDealId(dealId);

        TestListener.setLog(LogStatus.INFO, "Verify Deal Detail Data");
        dealDetailPage.dealDetailHeading(expedtedDealDetails.getTitle());
    }
}
