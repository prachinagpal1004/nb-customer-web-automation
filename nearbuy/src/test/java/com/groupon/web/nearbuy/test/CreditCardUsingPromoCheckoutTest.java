package com.groupon.web.nearbuy.test;

import com.groupon.web.common.test.BaseTest;
import com.groupon.web.common.utils.TestListener;
import com.groupon.web.nearbuy.data.AuthenticationData;
import com.groupon.web.nearbuy.data.CreditCardData;
import com.groupon.web.nearbuy.data.CreditsData;
import com.groupon.web.nearbuy.data.IncDecQtyData;
import com.groupon.web.nearbuy.data.MenuData;
import com.groupon.web.nearbuy.data.PaymentModeData;
import com.groupon.web.nearbuy.data.PromoData;
import com.groupon.web.nearbuy.page.AuthenticationPage;
import com.groupon.web.nearbuy.page.DealCatalogPage;
import com.groupon.web.nearbuy.page.DealDetailPage;
import com.groupon.web.nearbuy.page.HeaderFooterPage;
import com.groupon.web.nearbuy.page.OrderSummaryPage;
import com.groupon.web.nearbuy.page.PayUcheckoutPage;
import com.groupon.web.nearbuy.page.ThankYouPage;
import com.relevantcodes.extentreports.LogStatus;

import org.openqa.selenium.Keys;
import org.openqa.selenium.interactions.Actions;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
/**
 * 
 * @author Prachi Nagpal
 *
 */
@Test(groups = {"sanity", "staging","creditCardTransactionUsingPromocode"})
public class CreditCardUsingPromoCheckoutTest extends BaseTest {


	private AuthenticationPage authenticationPage;
	private HeaderFooterPage headerFooterPage;
	private DealDetailPage dealDetailPage;
	private DealCatalogPage dealCatalogPage;
	private OrderSummaryPage orderSummaryPage;
	private PayUcheckoutPage payUcheckoutPage;
	private ThankYouPage thankYouPage;

    @BeforeMethod(alwaysRun=true)
    public void init() {

		authenticationPage = new AuthenticationPage();
		headerFooterPage = new HeaderFooterPage();
		dealDetailPage = new DealDetailPage();
		dealCatalogPage = new DealCatalogPage();
		orderSummaryPage = new OrderSummaryPage();
		payUcheckoutPage = new PayUcheckoutPage();
		thankYouPage = new ThankYouPage();
	}

	/**
	 * Traverse to Deal Catalog Page and click on featured Deal
	 * @param menu
	 * @param submenu
	 */
	@Test(priority = 2,dataProvider="foodAndDrink" , dataProviderClass = MenuData.class  )
	public void testLandingOnDealDetailPage(String mainMenu, String cat, String subCat) {
		TestListener.setLog(LogStatus.INFO, "Navigate to Deal Listing Page from Menu");
		headerFooterPage.navigateToMenu(mainMenu, cat, subCat);
		TestListener.setLog(LogStatus.INFO, "Click on Featured Deal");
		dealCatalogPage.clickOnSpecifiedDealCard(7);
	}
	

	/**
	 * Increase the quantity
	 * @param inc
	 * @param dec
	 */
	@Test(priority = 3)
	public void clickOnAnyOptionAndVerify() {
		TestListener.setLog(LogStatus.INFO, "Verify Deal Details & OfferSection");
		dealDetailPage.dealDetailHeading().dealDetailOptionTitles().verifyOfferSectionAndClickOnOption(0, 1);
	}


	/**
	 * Click on Buy Now Button of Deal
	 */
	@Test(priority = 4)
	public void testClickOnBuyNowButton(){
		TestListener.setLog(LogStatus.INFO, "Click on Buy Now Button");
		dealDetailPage.clickOnBuyNowButton();
	}
	
	/**
	 * Do Success Login
	 */
	@Test(priority = 5 , dataProvider="successNumberLogin" , dataProviderClass = AuthenticationData.class )
	public void testSuccessLogin(String username, String password, String message, String bool) throws Throwable {
		TestListener.setLog(LogStatus.INFO, "Enter Nearbuy Credentials for Login");
		authenticationPage.verifyLoginAndErrorMessages(username, password, message, Boolean.valueOf(bool));
	}
	
	/**
	 * Verify Order Details
	 */
	@Test(priority = 6 )
	public void testOrderDetails() {
		TestListener.setLog(LogStatus.INFO, "Verify Order Details & Price");
		orderSummaryPage.verifyOrderSummaryDetails().priceVerifications();
	}

	/**
	 * Select the credits
	 * @param bool
	 */
	@Test(priority = 7, dataProvider="userDontWantToUseCredits" , dataProviderClass = CreditsData.class )
	public void testCredits(String bool) {
		TestListener.setLog(LogStatus.INFO, "Deselect the credits checkbox");
		orderSummaryPage.selectDeselectCredits(Boolean.valueOf(bool));
	}
	
	/**
	 * Enter Valid Promocode
	 * @param promo
	 * @param validPromo
	 */
	@Test(priority = 8, dataProvider="enterValidPromo" , dataProviderClass = PromoData.class )
	public void testPromoValid(String promo, String validPromo, String removePromo) {
		TestListener.setLog(LogStatus.INFO, "Apply Valid Promocode");
		orderSummaryPage.enterPromoCode(promo, Boolean.valueOf(validPromo), Boolean.valueOf(removePromo));
	}

	@Test(priority = 9,  dataProvider="selectCreditCard" , dataProviderClass = PaymentModeData.class )
	public void testSelectingPaymentModes(String mode) {
		TestListener.setLog(LogStatus.INFO, "Select Debit Card as Mode of Transaction");
		orderSummaryPage.selectModeOfPayment(mode);
	}
	
	@Test(priority = 10)
	public void testAvailOffersDuringPayment() {
		orderSummaryPage.availOffersDuringPayment();
	}
	
	@Test(priority = 11,  dataProvider="enterCreditCardDetails" , dataProviderClass = CreditCardData.class )
	public void testCreditCardDetails(String cardNo, String name, String cvv, String date) {
		TestListener.setLog(LogStatus.INFO, "Enter Credit Card Details Credentials");
		payUcheckoutPage.enterCreditCardDetailsAndClickOnPayNowButton(cardNo, name, cvv, date);
	}

	//		@Test(priority = 12)
	//	public void testThankYouPage() {
	//		thankYouPage.verifyCongratsMessage();
	//	}


}
