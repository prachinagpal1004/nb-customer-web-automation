package com.groupon.web.nearbuy.test;

import com.groupon.web.common.test.BaseTest;
import com.groupon.web.common.utils.TestListener;
import com.groupon.web.nearbuy.data.ChangeYourLocationData;
import com.groupon.web.nearbuy.data.MenuData;
import com.groupon.web.nearbuy.page.HeaderFooterPage;
import com.groupon.web.nearbuy.page.HomePage;
import com.relevantcodes.extentreports.LogStatus;

import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
/**
 * 
 * @author Prachi Nagpal
 *
 */
@Test(groups = {"sanity", "localAndTravelCategoryIcons"})
public class CategoryIconsVerificationTest extends BaseTest {

	private HomePage home;
	private HeaderFooterPage menu;

	@BeforeMethod(alwaysRun=true)
	public void init() {
		home = new HomePage();
		menu = new HeaderFooterPage();
	}

	@Test(priority = 1 )
	public void testCategoryIconsForLocal() throws Throwable {
		TestListener.setLog(LogStatus.INFO, "Verify Category Icons For Local");
		home.categoryIcons();
		
	}
	@Test(priority = 2 )
	public void testCategoryIconsForTravel() throws Throwable {
		TestListener.setLog(LogStatus.INFO, "Verify Category Icons For Travel");
		menu.navigateToMenu("Hotels", "", "");
		home.categoryIcons();
		
	}

}

