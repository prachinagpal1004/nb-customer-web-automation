//package com.groupon.web.nearbuy.test;
//
//import com.groupon.web.common.test.BaseTest;
//import com.groupon.web.common.utils.TestListener;
//import com.groupon.web.nearbuy.data.AuthenticationData;
//import com.groupon.web.nearbuy.data.CreditsData;
//import com.groupon.web.nearbuy.data.IncDecQtyData;
//import com.groupon.web.nearbuy.data.MenuData;
//import com.groupon.web.nearbuy.data.PayTmData;
//import com.groupon.web.nearbuy.data.PaymentModeData;
//import com.groupon.web.nearbuy.data.PromoData;
//import com.groupon.web.nearbuy.page.AuthenticationPage;
//import com.groupon.web.nearbuy.page.DealCatalogPage;
//import com.groupon.web.nearbuy.page.DealDetailPage;
//import com.groupon.web.nearbuy.page.HeaderFooterPage;
//import com.groupon.web.nearbuy.page.OrderSummaryPage;
//import com.groupon.web.nearbuy.page.PaytmWalletPage;
//import com.groupon.web.nearbuy.page.ThankYouPage;
//import com.relevantcodes.extentreports.LogStatus;
//
//import org.testng.annotations.BeforeMethod;
//import org.testng.annotations.Test;
///**
// * 
// * @author Prachi Nagpal
// *
// */
//@Test(groups = {"sanity", "staging"})
//public class ReferralCodeTest extends BaseTest {
//
//
//	private AuthenticationPage authenticationPage;
//	private HeaderFooterPage headerFooterPage;
//	private DealDetailPage dealDetailPage;
//	private DealCatalogPage dealCatalogPage;
//	private OrderSummaryPage orderSummaryPage;
//	private PaytmWalletPage payTMcheckoutPage;
//	private ThankYouPage thankYouPage;
//
//    @BeforeMethod(alwaysRun=true)
//    public void init() {
//
//		authenticationPage = new AuthenticationPage();
//		headerFooterPage = new HeaderFooterPage();
//		dealDetailPage = new DealDetailPage();
//		dealCatalogPage = new DealCatalogPage();
//		orderSummaryPage = new OrderSummaryPage();
//		payTMcheckoutPage = new PaytmWalletPage();
//		thankYouPage = new ThankYouPage();
//	}
//
//
//	/**
//	 * Traverse to Deal Catalog Page and click on featured Deal
//	 * @param menu
//	 * @param submenu
//	 */
//	@Test(priority = 1, dataProvider="enterSubmenuForDealListingPage" , dataProviderClass = MenuData.class )
//	public void testLandingOnDealDetailPage(String menu, String submenu) {
//		TestListener.setLog(LogStatus.INFO, "Navigate to Deal Listing Page from Menu");
//		headerFooterPage.navigateToMenu(menu, submenu);
//		TestListener.setLog(LogStatus.INFO, "Click on Featured Deal");
//		dealCatalogPage.clickOnSpecifiedDealCard(2);
//	}
//
//	/**
//	 * Increase the quantity
//	 * @param inc
//	 * @param dec
//	 */
////	@Test(priority = 2, dataProvider="incDecQtyCount" , dataProviderClass = IncDecQtyData.class )
////	public void testIncDecQty(String inc, String dec) {
////		TestListener.setLog(LogStatus.INFO, "Verify Deal Details & Increase Quantity of the deal");
////		dealDetailPage.dealDetailHeading().incOrDecQtyAndVerifyPrice(Integer.valueOf(inc), Integer.valueOf(dec));
////	}
//
//	/**
//	 * Click on Buy Now Button of Deal
//	 */
//	@Test(priority = 3)
//	public void testClickOnBuyNowButton(){
//		TestListener.setLog(LogStatus.INFO, "Click on Buy Now Button");
//		dealDetailPage.clickOnBuyNowButton();
//	}
//
//	/**
//	 * Do Success Login
//	 */
//	@Test(priority = 4 , dataProvider="successEmailIdLogin" , dataProviderClass = AuthenticationData.class )
//	public void testSuccessLogin(String username, String password, String message, String bool) throws Throwable {
//		TestListener.setLog(LogStatus.INFO, "Enter Credentials for Successful Login");
//		authenticationPage.verifyLoginAndErrorMessages(username, password, message, Boolean.valueOf(bool));
//	}
//
//	/**
//	 * Verify Order Details
//	 */
//	@Test(priority = 5 )
//	public void testOrderDetails() {
//		TestListener.setLog(LogStatus.INFO, "Verify Price Details on Order Summary page");
//		orderSummaryPage.verifyOrderDetails().priceVerifications();
//	}
//	
//	/**
//	 * Enter Valid Promocode
//	 * @param promo
//	 * @param validPromo
//	 */
//	@Test(priority = 6, dataProvider="selfReferralCode" , dataProviderClass = PromoData.class )
//	public void testSelfReferralCode(String promo, String validPromo, String removePromo) {
//		TestListener.setLog(LogStatus.INFO, "Apply Self Referral Code");
//		orderSummaryPage.enterPromoCode(promo, Boolean.valueOf(validPromo), Boolean.valueOf(removePromo));
//	}
//	
//	@Test(priority = 7, dataProvider="notFirstTransactionReferralCode" , dataProviderClass = PromoData.class )
//	public void testNotFirstTransactionReferralCode(String promo, String validPromo, String removePromo) {
//		TestListener.setLog(LogStatus.INFO, "Apply Referral Code on transaction which is not user's first");
//		orderSummaryPage.enterPromoCode(promo, Boolean.valueOf(validPromo), Boolean.valueOf(removePromo));
//	}
//
//	/**
//	 * Select the credits
//	 * @param bool
//	 */
//	@Test(priority = 8, dataProvider="userDontWantToUseCredits" , dataProviderClass = CreditsData.class )
//	public void testCredits(String bool) {
//		TestListener.setLog(LogStatus.INFO, "Deselect the credits checkbox");
//		orderSummaryPage.selectDeselectCredits(Boolean.valueOf(bool));
//	}
//
//	/**
//	 * Enter Valid Promocode
//	 * @param promo
//	 * @param validPromo
//	 */
//	@Test(priority = 9, dataProvider="enterValidPromo" , dataProviderClass = PromoData.class )
//	public void testPromoValid(String promo, String validPromo, String removePromo) {
//		TestListener.setLog(LogStatus.INFO, "Apply Valid Promocode");
//		orderSummaryPage.enterPromoCode(promo, Boolean.valueOf(validPromo), Boolean.valueOf(removePromo));
//	}
//
//	@Test(priority = 8, dataProvider="selectPaytmRadioButton" , dataProviderClass = PaymentModeData.class )
//	public void testSelectingPaymentModes(String mode) {
//		TestListener.setLog(LogStatus.INFO, "Select Paytm Mode of Transaction");
//		orderSummaryPage.selectModeOfPayment(mode);
//	}
//	
////	@Test(priority = 9)
////	public void testAvailOffersDuringPayment() {
////		orderSummaryPage.availOffersDuringPayment();
////	}
//
//	@Test(priority = 10)
//	public void testProceedToPayment() {
//		TestListener.setLog(LogStatus.INFO, "Click on 'Proceed To Payment' Button");
//		orderSummaryPage.clickOnProceedToPaymentButton();
//	}
//	
//}
