package com.groupon.web.nearbuy.page;

import org.apache.log4j.Logger;

import com.groupon.web.common.page.BasePage;

public class HulkToDealDetailPage extends BasePage {

	private static final Logger logger = Logger.getLogger(HulkToDealDetailPage.class);

	public HulkToDealDetailPage() {
	}

	/**
	 * Build Local URL of the deal
	 * @param dealId 
	 * @return
	 */
	//    public HulkToDealDetailPage buildLocalURL(String city, String dealName, String dealId){
	public HulkToDealDetailPage buildLocalURL(String dealId){

		logger.info("Building Local URL");
		String currentUrl = getCurrentUrl();
		String url = currentUrl.substring(0,currentUrl.lastIndexOf("/")+1)+dealId;
		logger.info("Running for URL : " + url);
		getWebDriver().get(url);

		return this;
	}

	/**
	 * Build Product URL of the deal
	 * @return
	 */
	public HulkToDealDetailPage buildProductURL(String dealName, String dealID){

		logger.info("Building Product URL");
		logger.info("Running for URL : " + baseUrl + "/deal/products/" + dealName + "/" + dealID);
		getWebDriver().get(baseUrl + "/deal/products/" + dealName + "/" + dealID);

		return this;
	}
}
