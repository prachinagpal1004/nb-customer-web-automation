package com.groupon.web.nearbuy.data;

import org.testng.annotations.DataProvider;

import com.groupon.web.common.config.Config;
import com.groupon.web.common.utils.DataProviderUtil;

/**
 * 
 * @author prachi.nagpal
 *
 */
public class PaymentModeData {

	@DataProvider(name = "selectSavedCards")
	public static Object[][] selectSavedCards(){
		return DataProviderUtil.provideData(Config.getInstance().getConfig("testData.file"), "nearbuy","selectSavedCards");
	}
	@DataProvider(name = "selectDebitCard")
	public static Object[][] selectDebitCard(){
		return DataProviderUtil.provideData(Config.getInstance().getConfig("testData.file"), "nearbuy","selectDebitCard");
	}
	@DataProvider(name = "selectCreditCard")
	public static Object[][] selectCreditCard(){
		return DataProviderUtil.provideData(Config.getInstance().getConfig("testData.file"), "nearbuy","selectCreditCard");
	}
	@DataProvider(name = "selectNetBanking")
	public static Object[][] selectNetBanking(){
		return DataProviderUtil.provideData(Config.getInstance().getConfig("testData.file"), "nearbuy","selectNetBanking");
	}
	@DataProvider(name = "selectWallets")
	public static Object[][] selectWallets(){
		return DataProviderUtil.provideData(Config.getInstance().getConfig("testData.file"), "nearbuy","selectWallets");
	}
}
