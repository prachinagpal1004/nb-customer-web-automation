package com.groupon.web.nearbuy.test;
//package com.groupon.web.nearbuy.test;
//
//import com.groupon.web.common.test.BaseTest;
//import com.groupon.web.common.utils.TestListener;
//import com.groupon.web.nearbuy.data.AuthenticationData;
//import com.groupon.web.nearbuy.data.CreditsData;
//import com.groupon.web.nearbuy.data.IncDecQtyData;
//import com.groupon.web.nearbuy.data.MenuData;
//import com.groupon.web.nearbuy.data.PayTmData;
//import com.groupon.web.nearbuy.data.PaymentModeData;
//import com.groupon.web.nearbuy.data.PromoData;
//import com.groupon.web.nearbuy.data.SearchData;
//import com.groupon.web.nearbuy.page.AuthenticationPage;
//import com.groupon.web.nearbuy.page.DealCatalogPage;
//import com.groupon.web.nearbuy.page.DealDetailPage;
//import com.groupon.web.nearbuy.page.HeaderFooterPage;
//import com.groupon.web.nearbuy.page.OrderSummaryPage;
//import com.groupon.web.nearbuy.page.PaytmWalletPage;
//import com.groupon.web.nearbuy.page.ThankYouPage;
//import com.relevantcodes.extentreports.LogStatus;
//
//import org.testng.annotations.BeforeMethod;
//import org.testng.annotations.Test;
///**
// * 
// * @author Prachi Nagpal
// *
// */
//@Test(groups = {"sanity", "staging"})
//public class PayTmTransactionMoreThan10K extends BaseTest {
//
//
//	private AuthenticationPage authenticationPage;
//	private HeaderFooterPage headerFooterPage;
//	private DealDetailPage dealDetailPage;
//	private DealCatalogPage dealCatalogPage;
//	private OrderSummaryPage orderSummaryPage;
//
//    @BeforeMethod(alwaysRun=true)
//    public void init() {
//
//		authenticationPage = new AuthenticationPage();
//		headerFooterPage = new HeaderFooterPage();
//		dealDetailPage = new DealDetailPage();
//		dealCatalogPage = new DealCatalogPage();
//		orderSummaryPage = new OrderSummaryPage();
//	}
//
//
//	/**
//	 * Traverse to Deal Catalog Page and click on featured Deal
//	 * @param menu
//	 * @param submenu
//	 */
//	@Test(priority = 1, dataProvider="enterSubmenuForDealListingPage" , dataProviderClass = MenuData.class )
//	public void testLandingOnDealDetailPage(String menu, String submenu) {
//		TestListener.setLog(LogStatus.INFO, "Navigate to Deal Listing Page from Menu");
//		headerFooterPage.navigateToMenu(menu, submenu);
//	}
//
////	@Test(priority = 2 , dataProvider="searchMoreThan10Kprice" , dataProviderClass = SearchData.class )
////	public void testSearchForProductMoreThan10K(String searchKeyword) throws Throwable {
////		TestListener.setLog(LogStatus.INFO, "Search a product of price more than 10K");
////		headerFooterPage.searchBox(searchKeyword).verifyAutocomplete("1");
////	}
//	
//	@Test(priority = 3 )
//	public void user_clicks_on_featuredDeal() throws Throwable {
//		TestListener.setLog(LogStatus.INFO, "Click on Featured Deal");
//		dealCatalogPage.clickOnSpecifiedDealCard(2);
//	}
//	/**
//	 * Increase the quantity
//	 * @param inc
//	 * @param dec
//	 */
//	@Test(priority = 4, dataProvider="incDecQtyCountTo10" , dataProviderClass = IncDecQtyData.class )
//	public void testIncDecQty(String inc, String dec) {
//		TestListener.setLog(LogStatus.INFO, "Verify Deal Details & Increase Quantity of the deal");
//		dealDetailPage.dealDetailHeading().verifyOfferSection();
//		dealDetailPage.incOrDecQtyAndVerifyPrice(Integer.valueOf(inc), Integer.valueOf(dec));
//	}
//
//	/**
//	 * Click on Buy Now Button of Deal
//	 */
//	@Test(priority = 5)
//	public void testClickOnBuyNowButton(){
//		TestListener.setLog(LogStatus.INFO, "Click on Buy Now Button");
//		dealDetailPage.clickOnBuyNowButton();
//	}
//
//	/**
//	 * Do Success Login
//	 */
//	@Test(priority = 6 , dataProvider="nearbuyCredentialsForPayTMtransaction" , dataProviderClass = AuthenticationData.class )
//	public void testSuccessLogin(String username, String password, String message, String bool) throws Throwable {
//		TestListener.setLog(LogStatus.INFO, "Enter Credentials for Successful Login");
//		authenticationPage.verifyLoginAndErrorMessages(username, password, message, Boolean.valueOf(bool));
//	}
//
//	/**
//	 * Verify Order Details
//	 */
//	@Test(priority = 7 )
//	public void testOrderDetails() {
//		TestListener.setLog(LogStatus.INFO, "Verify Order Details & Price");
//		orderSummaryPage.verifyOrderSummaryDetails().priceVerifications();
//	}
//
//	/**
//	 * Select the credits
//	 * @param bool
//	 */
//	@Test(priority = 8, dataProvider="userDontWantToUseCredits" , dataProviderClass = CreditsData.class )
//	public void testCredits(String bool) {
//		TestListener.setLog(LogStatus.INFO, "Deselect the credits checkbox");
//		orderSummaryPage.selectDeselectCredits(Boolean.valueOf(bool));
//	}
//
//	/**
//	 * Enter Valid Promocode
//	 * @param promo
//	 * @param validPromo
//	 */
//	@Test(priority = 9, dataProvider="enterValidPromo" , dataProviderClass = PromoData.class )
//	public void testPromoValid(String promo, String validPromo, String removePromo) {
//		TestListener.setLog(LogStatus.INFO, "Apply Valid Promocode");
//		orderSummaryPage.enterPromoCode(promo, Boolean.valueOf(validPromo), Boolean.valueOf(removePromo));
//	}
//
//	@Test(priority = 10, dataProvider="selectPaytmRadioButton" , dataProviderClass = PaymentModeData.class )
//	public void testSelectingPaymentModes(String mode) {
//		TestListener.setLog(LogStatus.INFO, "Select Paytm Mode of Transaction");
//		orderSummaryPage.selectModeOfPayment(mode);
//	}
//	
////	@Test(priority = 11)
////	public void testAvailOffersDuringPayment() {
////		orderSummaryPage.availOffersDuringPayment();
////	}
//
//	@Test(priority = 12)
//	public void testProceedToPayment() {
//		TestListener.setLog(LogStatus.INFO, "Click on 'Proceed To Payment' Button");
//		orderSummaryPage.clickOnProceedToPaymentButton("cc-paynow-btn");
//	}
//
//	@Test(priority = 13)
//	public void testFailedPaytmTransactionPage() {
//		TestListener.setLog(LogStatus.INFO, "Verify Failed Transaction Message");
//		orderSummaryPage.verifyFailedTransactionMessage();
//	}
//
//
//}
