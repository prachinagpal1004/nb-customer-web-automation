package com.groupon.web.nearbuy.data;

import org.testng.annotations.DataProvider;

/**
 * 
 * @author prachi.nagpal
 *
 */
public class  MenuData {

	@DataProvider(name = "enterMenuAndSubmenu")
	public static Object[][] provideMenuAndSubmenu(){
		return new Object[][] {

				{"Around You","Around You"},
                {"HOTELS","HOTELS"},
        };
	}
	
	@DataProvider(name = "allDealsSubmenu")
	public static Object[][] provideAllDealsSubmenu(){
		return new Object[][] {
			
			{"Around You","all-deals"},
		};
	}

	@DataProvider(name = "travelSubMenu")
	public static Object[][] provideTravelSubmenu(){
		return new Object[][] {
			
			{"Hotels","Short Getaways",""},
		};
	}
	
	@DataProvider(name = "productSubMenu")
	public static Object[][] provideProductSubmenu(){
		return new Object[][] {
			
			{"PRODUCTS","Electronics"},
		};
	}
	@DataProvider(name = "foodAndDrink")
	public static Object[][] provideFoodSubmenu(){
		return new Object[][] {
			
			{"Around You","Food & Drink",""},
		};
	}
	@DataProvider(name = "spaAndMassage")
	public static Object[][] provideSpaSubmenu(){
		return new Object[][] {
			
			{"Around You","Spa & Massage",""},
		};
	}
	@DataProvider(name = "activities")
	public static Object[][] provideActivitiesSubmenu(){
		return new Object[][] {
			
			{"Around You","Activities",""},
		};
	}
	@DataProvider(name = "beautyAnsSalon")
	public static Object[][] provideSalonSubmenu(){
		return new Object[][] {
			
			{"Around You","Beauty & Salon",""},
		};
	}
}
