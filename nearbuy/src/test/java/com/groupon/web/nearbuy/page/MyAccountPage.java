package com.groupon.web.nearbuy.page;

import com.groupon.web.common.page.BasePage;
import com.groupon.web.nearbuy.page.OrderSummaryPage;
import com.groupon.web.common.utils.BrowserAuthenticationWindow;
import com.groupon.web.common.utils.PDFUtil;
import com.groupon.web.common.utils.WaitUtility;
import com.groupon.web.nearbuy.locator.MyAccountLocator;
import org.apache.log4j.Logger;
import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;

import java.awt.AWTException;
import java.awt.Robot;
import java.io.IOException;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

public class MyAccountPage extends BasePage {

	private static final Logger logger = Logger.getLogger(MyAccountPage.class);
	private MyAccountLocator locator;

	public MyAccountPage() {
		locator = PageFactory.initElements(getWebDriver(), MyAccountLocator.class);
	}

	/**
	 * Click on Link
	 * @param linkText
	 * @return
	 */
	public MyAccountPage clickOnLink(String linkText){

		waitVar.until(ExpectedConditions.visibilityOf(locator.loggedInUser));
		waitVar.until(ExpectedConditions.elementToBeClickable(locator.loggedInUser));
		locator.loggedInUser.click();
		WaitUtility.waitForAttributeContainsGivenValue(locator.myAccountDropdown, "class", "show", getWebDriver(), waitVar);

		for(WebElement link : locator.dropDownList){

			if(link.getText().contains(linkText)){
				logger.info(link.getText() + " will be clicked");
				if(linkText.contains("Sign Out")){
					waitVar.until(ExpectedConditions.elementToBeClickable(link));
					link.click();
					waitVar.until(ExpectedConditions.elementToBeClickable(new AuthenticationPage().getLoginLink()));
					logger.info("User Logged out");
					Assert.assertTrue(locator.loginLink.isDisplayed());
					Assert.assertTrue(locator.loginLink.isEnabled());
					waitVar.until(ExpectedConditions.visibilityOf(locator.mainPageHeader));
				}
				else{
					link.click();
					WaitUtility.waitForURLtoChange(getWebDriver(), waitVar);
					WaitUtility.waitForLoad(getWebDriver());
//					if(getCurrentUrl().contains("/user/refer")){
//
//						waitVar.until(ExpectedConditions.visibilityOf(locator.continueShoppingButton));
//					}
//					else{
//						waitVar.until(ExpectedConditions.visibilityOf(locator.pageHeading));
//					}
					logger.info("User Traversed to : " + linkText );
				}
				break;
			}
		}
		return this;
	}


	/**
	 * Verify Credits Section
	 * @return
	 */
	public MyAccountPage myCredits() {
		String pageHeadingText = locator.pageHeading.getText();
		waitVar.until(ExpectedConditions.visibilityOf(locator.pageHeading));
		Assert.assertTrue(locator.pageHeading.isDisplayed());
		Assert.assertEquals(pageHeadingText , "My Credits");
		logger.info("Credits Heading Text : " + pageHeadingText);

		if(locator.currentCredits.getText().equals("Current Credits: 0")){

			logger.info("Current Credits : " + locator.currentCredits.getText());
			logger.info("No Credits with User");

		}
		else if(!locator.currentCredits.getText().equals("Current Credits: 0")){

			waitVar.until(ExpectedConditions.visibilityOf(locator.creditsSection));
			Assert.assertTrue(locator.creditsSection.isDisplayed());
			Assert.assertEquals(pageHeadingText, "My Credits");
			logger.info("Credits Heading Text : " + pageHeadingText);
			logger.info(locator.currentCredits.getText());
			//final OrderSummaryPage orderSummaryPage = new OrderSummaryPage();
			//			for(WebElement each : orderSummaryPage.getOrderTableHeadings()){
			//
			//				Assert.assertTrue(each.isDisplayed());
			//				logger.info("Heading: " + each.getText());
			//			}
			//
			//			for(WebElement each : orderSummaryPage.getOrderTableDetails()){
			//
			//				Assert.assertTrue(each.isDisplayed());
			//				logger.info("Details: " + each.getText());
			//			}
			//
			//			logger.info("No. Of Rows:  " + (orderSummaryPage.getNumberOfRows().size() - 1));

		}

		return this;
	}

	/**
	 * Verify My Profile Section
	 * @param nameToBeChanged
	 * @return
	 */
	public MyAccountPage myProfile(String nameToBeChanged){

		waitVar.until(ExpectedConditions.elementToBeClickable(locator.myProfileSaveChangesButton));

		Assert.assertTrue(locator.profileSection.isDisplayed());
		Assert.assertTrue(locator.pageHeading.getText().contains("My Profile"));
		for(WebElement label : locator.profileLabels){
			Assert.assertTrue(label.isDisplayed());
		}

		Assert.assertTrue(locator.nameTextbox.isEnabled());
		//	logger.info("Name Value: " + locator.nameTextbox.getAttribute("value"));

		Assert.assertTrue(locator.emailTextbox.isDisplayed());
		//	Assert.assertTrue(locator.emailTextbox.getAttribute("readonly").contains("readonly"));
		//logger.info("Email Text : " + locator.emailText.getText());
		//	logger.info("Email Value: " + locator.emailTextbox.getAttribute("value"));
		//	logger.info("Mobile Text : " + locator.mobileText.getText());
		Assert.assertTrue(locator.mobileTextbox.isDisplayed());
		//	Assert.assertTrue(locator.mobileTextbox.getAttribute("readonly").contains("readonly"));
		//	logger.info("Mobile Value: " + locator.mobileTextbox.getAttribute("value"));

		locator.nameTextbox.clear();
		logger.info("Name to be changed is : " + nameToBeChanged);
		locator.nameTextbox.sendKeys(nameToBeChanged);
		locator.myProfileSaveChangesButton.click();
		waitVar.until(ExpectedConditions.visibilityOf(locator.successMessageDiv));
		Assert.assertTrue(locator.successMessageDiv.isDisplayed());
		Assert.assertTrue(locator.successMessageDiv.getText().contains("Your profile has been successfully updated"));
		waitVar.until(ExpectedConditions.invisibilityOfElementLocated(By.cssSelector(".txt-right.txt-primary.icon-s")));

		logger.info("Verified Name has been saved successfully as provided by the user");

		return this;
	}

	/**
	 * Verify My Email Subscription Section
	 * @return
	 */
	public MyAccountPage myNewsletters(){
		String pageHeadingText = locator.pageHeading.getText();
		waitVar.until(ExpectedConditions.elementToBeClickable(locator.subscribeButton));
		Assert.assertTrue(locator.subscriptionSection.isDisplayed());
		Assert.assertTrue(locator.newsLetterSection.isDisplayed());

		Assert.assertTrue(locator.pageHeading.isDisplayed());
		logger.info(pageHeadingText);

		Assert.assertTrue(locator.mySubscriptionSubHeading.isDisplayed());
		logger.info(locator.mySubscriptionSubHeading.getText());

		Assert.assertTrue(locator.disabledEmailTextbox.getAttribute("class").contains("form-readonly"));
		logger.info("Email Label Text : " + locator.disabledEmailLabel.getText());

		Assert.assertTrue(locator.cityNewsletterLabel.isDisplayed());
		logger.info("Newsletter Label Text : " + locator.cityNewsletterLabel.getText());

		waitVar.until(ExpectedConditions.visibilityOf(locator.allCitiesBox));
		locator.locationCheckboxes.get(4).click();
		logger.info("Current city clicked and all is clicked");
		locator.subscribeButton.click();
		waitVar.until(ExpectedConditions.visibilityOf(locator.updateSuccessMessage));
		waitVar.until(ExpectedConditions.elementToBeClickable(locator.unsubscribeLink));
		logger.info(locator.updateSuccessMessage.getText());

		logger.info("Total Cities for Subscription present : " + locator.allLocationLabels.size());

		for(WebElement allCheck : locator.locationCheckboxes){
			//	Assert.assertTrue(allCheck.isDisplayed());
			Assert.assertTrue(allCheck.isEnabled());

		}
		for(WebElement allLabel : locator.allLocationLabels){
			Assert.assertTrue(allLabel.isDisplayed());
			Assert.assertTrue(allLabel.isEnabled());
			logger.info(allLabel.getText());
		}

		//To get all the options that are selected in the drop down.
		for (WebElement check : locator.allCheckedLocations)
		{
			Assert.assertTrue(check.isDisplayed());
			Assert.assertTrue(check.isEnabled());
		}
		for (WebElement label : locator.allCheckedLocationsLabel)
		{
			Assert.assertTrue(label.isDisplayed());
			Assert.assertTrue(label.isEnabled());
			logger.info("Checked Locations : " + label.getText());
		}

		for(WebElement all : locator.allLocationLabels){
			Assert.assertTrue(all.isDisplayed());
			Assert.assertTrue(all.isEnabled());
			if(all.getText().equals("All")){
				all.click();
				logger.info("All Clicked");
				locator.subscribeButton.click();
				waitVar.until(ExpectedConditions.visibilityOf(locator.updateSuccessMessage));
				waitVar.until(ExpectedConditions.elementToBeClickable(locator.unsubscribeLink));
				logger.info(locator.updateSuccessMessage.getText());

				break;
			}
		}

		// Unsubscribe Functionality
		locator.unsubscribeLink.click();
		System.out.println("Unsuscribe from all & verify No & Yes option");
		waitVar.until(ExpectedConditions.visibilityOf(locator.unsubscribePopUp));
		Assert.assertTrue("Unsubscribe No Option is dispalyed", locator.unsubscribeNoOption.isDisplayed());
		Assert.assertTrue("Unsubscribe No is enabled", locator.unsubscribeNoOption.isEnabled());
		locator.unsubscribeNoOption.click();
		waitVar.until(ExpectedConditions.invisibilityOfElementLocated(By.cssSelector("unsubscribe-modal .wrapper.show")));
		locator.unsubscribeLink.click();
		System.out.println("clicked on No");
		waitVar.until(ExpectedConditions.visibilityOf(locator.unsubscribePopUp));
		Assert.assertTrue("Unsubscribe Yes Option is dispalyed", locator.unsubscribeYesOption.isDisplayed());
		Assert.assertTrue("Unsubscribe Yes Option is enabled", locator.unsubscribeYesOption.isEnabled());
		locator.unsubscribeYesOption.click();
		System.out.println("Yes unsuscribed");
		waitVar.until(ExpectedConditions.invisibilityOfElementLocated(By.cssSelector("news-letter .row a")));

		// Select deselect one or two three cities and save
		waitVar.until(ExpectedConditions.visibilityOf(locator.allCitiesBox));

		for(WebElement location : locator.allLocationLabels){

			if(location.getText().equals("Delhi/NCR")){
				location.click();
				System.out.println("Clicked : " + location.getText());
			}
			if(location.getText().equals("Gurgaon")){
				location.click();
				System.out.println("Clicked : " + location.getText());
			}
		}

		locator.subscribeButton.click();
		System.out.println("clicked on subscribe button");
		waitVar.until(ExpectedConditions.visibilityOf(locator.updateSuccessMessage));

		return this;
	}


	public String getLoggedInUserText() {
		return locator.loggedInUser.getText();
	}

	/**
	 * Verify PDF text
	 * @return
	 */
	public MyAccountPage clickAndverifyPDFvoucherText(){

		waitVar.until(ExpectedConditions.visibilityOfAllElements(locator.listOfPDFlinks));

		int maxCount = 2;
		int count = 0;
		for(WebElement pdf : locator.listOfPDFlinks){

			String mwh=getWebDriver().getWindowHandle();

			Assert.assertTrue(pdf.isDisplayed());
			Assert.assertTrue(pdf.isEnabled());
			pdf.click();

			Set s=getWebDriver().getWindowHandles();
			//this method will gives you the handles of all opened windows

			Iterator ite=s.iterator();

			while(ite.hasNext())
			{
				String popupHandle=ite.next().toString();
				if(!popupHandle.contains(mwh))
				{
					getWebDriver().switchTo().window(popupHandle);

					try {
						final String output = new PDFUtil().readPDF();
						Assert.assertTrue(output.contains("Use at"));
						Assert.assertTrue(output.contains("Purchase Id:"));
						Assert.assertTrue(output.contains("Next steps"));
						Assert.assertTrue(output.contains("Things to remember"));
						Assert.assertTrue(output.contains("Cancellation policy"));
						Assert.assertTrue(output.contains("We're here to help"));
						Assert.assertTrue(output.contains("080-66010101 | support@nearbuy.com | Mon - Sun 9AM-10PM"));

					} catch (IOException e) {
						throw new RuntimeException(e);
					}
					getWebDriver().close();
					getWebDriver().switchTo().window(mwh);
					break;
				}
			}

			if(++count == maxCount) {
				break;
			}
		}
		return this;
	}

	/**
	 * Refer A Friend
	 * @return
	 */
	public MyAccountPage referAfriend(String username, String password){

		waitVar.until(ExpectedConditions.urlContains("user/refer"));


		if(WaitUtility.isElementPresent(getWebDriver(), locator.continueShoppingButton)){
			System.out.println("No referral page for this user");
			Assert.assertTrue("No Referral Message is nOT present", locator.noReferralMessage.isDisplayed());
			Assert.assertEquals("There are no active referral campaigns right now. Keep coming back", locator.noReferralMessage.getText());
			Assert.assertTrue(locator.continueShoppingButton.isDisplayed());
			Assert.assertTrue(locator.continueShoppingButton.isEnabled());
			locator.continueShoppingButton.click();
			WaitUtility.waitForURLtoChange(getWebDriver(), waitVar);
			waitVar.until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector(".secondary-nav")));

		}

		else{
			System.out.println("Referral is swicthed on for this user");

			Assert.assertTrue(locator.referAfriendHeading.isDisplayed());
			Assert.assertTrue(locator.referAfriendSubHeading.isDisplayed());
			Assert.assertTrue(locator.shareReferralCodeDiv.isDisplayed());
			Assert.assertTrue(locator.shareReferralText.isDisplayed());
			Assert.assertTrue(locator.shareReferralCode.isDisplayed());
			Assert.assertTrue(locator.referralAmount.isDisplayed());
			Assert.assertTrue(locator.howItWorksHeading.isDisplayed());
			Assert.assertTrue(locator.howItWorksDetails.isDisplayed());
			Assert.assertTrue(locator.TnClink.isDisplayed());
			Assert.assertTrue(locator.TnClink.isEnabled());

			logger.info(locator.referAfriendHeading.getText());
			logger.info(locator.shareReferralText.getText());
			logger.info(locator.referAfriendSubHeading.getText());
			logger.info(locator.howItWorksHeading.getText());
			logger.info(locator.howItWorksDetails.getText());
			logger.info("Referral Amount :" + locator.referralAmount.getText());
			logger.info("Referral Code :" + locator.shareReferralCode.getText());
			logger.info("Referral Code Length :" + locator.shareReferralCode.getText().length());
			Assert.assertEquals(6,locator.shareReferralCode.getText().length());

			locator.TnClink.click();
			waitVar.until(ExpectedConditions.visibilityOf(locator.TnCpopup));
			Assert.assertTrue(locator.popUpText.isDisplayed());
			Assert.assertTrue(locator.popUpBoldText.isDisplayed());
			logger.info(locator.popUpText.getText());
			logger.info(locator.popUpBoldText.getText());
			waitVar.until(ExpectedConditions.elementToBeClickable(locator.closeButton));
			Assert.assertTrue(locator.closeButton.isDisplayed());
			Assert.assertTrue(locator.closeButton.isEnabled());

			try {
				Thread.sleep(1000);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

			locator.closeButton.click();
			logger.info("Cross Icon in Popup is clicked");
			waitVar.until(ExpectedConditions.invisibilityOfElementLocated(By.cssSelector(".reveal-modal-bg")));

			// Store the current window handle
			String winHandleBefore = getWebDriver().getWindowHandle();

			logger.info("Parent window : " + winHandleBefore);


			for(WebElement share : locator.shareReferral){
				Assert.assertTrue(share.isDisplayed());
				Assert.assertTrue(share.isEnabled());
				if(share.getAttribute("href").contains("twitter")){
					share.click();
					logger.info("Link is clicked");
					// Switch to new window opened
					for(String winHandle : getWebDriver().getWindowHandles()){
						getWebDriver().switchTo().window(winHandle);
						if(getWebDriver().getTitle().contains("Twitter")){
							System.out.println(" You are in Twitter window");
							//break;
						}
					}
					logger.info("Entering Credentials in Twitter Window");
					enterTwitterCredentials(username, password);

					// Close the new window, if that window no more required
					waitVar.until(ExpectedConditions.invisibilityOfElementLocated(By.id("password")));

					getWebDriver().switchTo().window(winHandleBefore);
					logger.info("Switched back to the refer's screen :" + getWebDriver().getCurrentUrl());
					// Switch back to original browser (first window)
					break;
				}
				else if(share.getAttribute("href").contains("mailto")){
					logger.info("Currently not covering for email share");

				}
			}
		}

		return this;
	}
	
	public MyAccountPage myPromos(){
		
		waitVar.until(ExpectedConditions.urlContains("promos"));
		Assert.assertTrue(getCurrentUrl().contains("promos"));
		Assert.assertTrue(locator.pageHeading.getText().equals("My Promos"));
		Assert.assertTrue(locator.currentCredits.getText().equals("Currently there are no active promotions"));
		
		return this;
	}

	/**
	 * Enter Credentials
	 * @param username
	 * @param password
	 * @return
	 */
	public MyAccountPage enterTwitterCredentials(String username, String password){
		waitVar.until(ExpectedConditions.elementToBeClickable(locator.twitterUsername));
		String statusText = locator.statusText.getText();
		Assert.assertEquals("Enjoy ₹300 OFF on your first purchase using code RNTFZ9. Explore nearbuy to discover places to dine,shop and unwind- bit.ly/22vQ0P6", statusText);
		locator.twitterUsername.sendKeys(username);
		locator.twitterPassword.sendKeys(password);
		locator.loginAndTweetButton.submit();

		return this;
	}

	public MyAccountPage enterEmailIdToShare() throws AWTException{

		new BrowserAuthenticationWindow().enterText("automation.groupon@gmail.com", false);

		return this;
	}

	/**
	 * Verify Data Layer Orders Page
	 * @return
	 */
	public MyAccountPage verifyDataLayerMyOrders() {
		//prepare map by extracting values from html
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("pageType", "Orders");
		map.put("sitetype", "d");

		verifyDataLayer(map,"4");
		return this;
	}

	/**
	 * Verify Data Layer Credits Page
	 * @return
	 */
	public MyAccountPage verifyDataLayerMyCredits() {
		//prepare map by extracting values from html
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("pageType", "Credit");
		map.put("sitetype", "d");

		verifyDataLayer(map,"4");
		return this;
	}

	/**
	 * Verify Data Layer Credits Page
	 * @return
	 */
	public MyAccountPage verifyDataLayerMyProfile() {
		//prepare map by extracting values from html
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("pageType", "Profile");
		map.put("sitetype", "d");

		verifyDataLayer(map,"4");
		return this;
	}

	/**
	 * Verify Data Layer Newsletter Page
	 * @return
	 */
	public MyAccountPage verifyDataLayerMyNewsletters() {
		//prepare map by extracting values from html
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("pageType", "Email Subscriptions");

		verifyDataLayer(map,"4");
		return this;
	}

}
