package com.groupon.web.nearbuy.test;

import com.groupon.web.common.test.BaseTest;
import com.groupon.web.common.utils.TestListener;
import com.groupon.web.nearbuy.data.AuthenticationData;
import com.groupon.web.nearbuy.data.MenuData;
import com.groupon.web.nearbuy.page.AuthenticationPage;
import com.groupon.web.nearbuy.page.DealCatalogPage;
import com.groupon.web.nearbuy.page.DealDetailPage;
import com.groupon.web.nearbuy.page.HeaderFooterPage;
import com.groupon.web.nearbuy.page.HomePage;
import com.groupon.web.nearbuy.page.OrderSummaryPage;
import com.groupon.web.nearbuy.page.ThankYouPage;
import com.relevantcodes.extentreports.LogStatus;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
/**
 * 
 * @author Prachi Nagpal
 *
 */
@Test(groups = {"sanity", "staging"})
public class DataLayerAndGTMscriptTest extends BaseTest {

	private HomePage homePage;
	private HeaderFooterPage header;
	private DealCatalogPage list;
	private DealDetailPage detail;
	private AuthenticationPage login;
	private OrderSummaryPage order;
	private ThankYouPage thank;

	@BeforeMethod(alwaysRun=true)
	public void init() {
		homePage = new HomePage();
		header = new HeaderFooterPage();
		list = new DealCatalogPage();
		detail = new DealDetailPage();
		login = new AuthenticationPage();
		order = new OrderSummaryPage();
		thank = new ThankYouPage();
	}

	@Test(priority = 2)
	public void testGTMscriptAndDataLayerObjectOnHomePage() {
		TestListener.setLog(LogStatus.INFO, "Test dataLayer Object on Home Page");
		homePage.verifyDataLayerHome();
	}

	@Test(priority = 3 , dataProvider="foodAndDrink" , dataProviderClass = MenuData.class)
	public void testGTMscriptAndDataLayerObjectOnListingPage(String menu, String submenu, String subsubmenu) {


		TestListener.setLog(LogStatus.INFO, "Navigate to Listing Page");
		header.navigateToMenu(menu, submenu,subsubmenu);

		TestListener.setLog(LogStatus.INFO, "Test dataLayer Object on Listing Page");
		list.verifyDataLayerCatalog();

		list.openFirstSubCategoryPage();
		TestListener.setLog(LogStatus.INFO, "Test dataLayer Object on Sub Category Listing Page");
		list.verifyDataLayerSubCategoryPage();

		list.openFirstSubSubCategoryPage();
		TestListener.setLog(LogStatus.INFO, "Test dataLayer Object on Sub Sub Category Listing Page");
		list.verifyDataLayerSubSubCategoryPage();
	}

	@Test(priority = 4)
	public void testGTMscriptAndDataLayerObjectOnDealDetailPage(){


		TestListener.setLog(LogStatus.INFO, "Click on Featured Deal");
		list.clickOnSpecifiedDealCard(2);

		TestListener.setLog(LogStatus.INFO, "Test dataLayer Object on Deal Detail Page");
		detail.verifyDataLayerDealDetail();
		
	}

	@Test(priority = 5 , dataProvider="successNumberLogin" , dataProviderClass = AuthenticationData.class)
	public void testGTMscriptAndDataLayerObjectOnLoginPage(String username, String password, String message, String bool) throws Throwable{

		TestListener.setLog(LogStatus.INFO, "Verify Deal Details & OfferSection");
		detail.dealDetailHeading().dealDetailOptionTitles().verifyOfferSectionAndClickOnOption(1, 1);
		
		TestListener.setLog(LogStatus.INFO, "Click on Buy Now Button");
		detail.clickOnBuyNowButton();

		login.verifyLoginAndErrorMessages(username, password, message, Boolean.valueOf(bool));
	}

	@Test(priority = 6 )
	public void testGTMscriptAndDataLayerObjectOnOrderPage() {


		TestListener.setLog(LogStatus.INFO, "Verify Order Details & Price");
		order.verifyOrderSummaryDetails().priceVerifications();


		TestListener.setLog(LogStatus.INFO, "Test dataLayer Object on Order Page");
		order.verifyDataLayerOrderSummary();
	}

	@Test(priority = 7 )
	public void testGTMscriptAndDataLayerObjectOnThankYouPage() {



		TestListener.setLog(LogStatus.INFO, "Verify Thank You Page");
		order.clickOnProceedButton();

		TestListener.setLog(LogStatus.INFO, "Test dataLayer Object on Thank You Page");
		thank.verifyDataLayerThankYou();
	}
}