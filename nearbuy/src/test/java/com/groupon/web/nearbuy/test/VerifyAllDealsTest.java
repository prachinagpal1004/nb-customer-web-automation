package com.groupon.web.nearbuy.test;

import java.util.HashMap;
import java.util.Map;

import org.apache.log4j.Logger;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import com.groupon.web.common.test.BaseTest;
import com.groupon.web.common.utils.TestListener;
import com.groupon.web.nearbuy.data.AuthenticationData;
import com.groupon.web.nearbuy.data.CreditsData;
import com.groupon.web.nearbuy.data.HulkToDealDetailData;
import com.groupon.web.nearbuy.data.IncDecQtyData;
import com.groupon.web.nearbuy.data.MenuData;
import com.groupon.web.nearbuy.page.AuthenticationPage;
import com.groupon.web.nearbuy.page.DealCatalogPage;
import com.groupon.web.nearbuy.page.DealDetailPage;
import com.groupon.web.nearbuy.page.HeaderFooterPage;
import com.groupon.web.nearbuy.page.HulkToDealDetailPage;
import com.groupon.web.nearbuy.page.OrderSummaryPage;
import com.relevantcodes.extentreports.LogStatus;
/**
 * 
 * @author Prachi Nagpal
 *
 */
@Test(groups = {"regression"})
public class VerifyAllDealsTest extends BaseTest {

	private static final Logger logger = Logger.getLogger(VerifyAllDealsTest.class);
	private Map<String, String> dealLogs = new HashMap<String,String>();

	private HulkToDealDetailPage buildURL;
	private DealDetailPage dealDetail;
	private AuthenticationPage authenticationPage;
	private HeaderFooterPage headerFooterPage;
	private DealCatalogPage dealCatalogPage;
	private OrderSummaryPage orderSummaryPage;

	@BeforeMethod(alwaysRun=true)
	public void init() {
		logger.info("Before Method");
		buildURL = new HulkToDealDetailPage();
		dealDetail = new DealDetailPage();
		authenticationPage = new AuthenticationPage();
		orderSummaryPage = new OrderSummaryPage();
		headerFooterPage = new HeaderFooterPage();
		dealCatalogPage = new DealCatalogPage();
	}

	@Test(priority = 1, dataProvider="enterSubmenuForDealListingPage" , dataProviderClass = MenuData.class )
	public void testLandingOnDealDetailPage(String menu, String submenu, String subsubmenu) {
		//TestListener.setLog(LogStatus.INFO, "Navigate to Deal Listing Page from Menu");
		headerFooterPage.navigateToMenu(menu, submenu,subsubmenu);
		//TestListener.setLog(LogStatus.INFO, "Click on Featured Deal");
		dealCatalogPage.clickOnSpecifiedDealCard(1);
	}

	@Test(priority = 2 , dataProvider="hulkToLocalDealDetail" , dataProviderClass = HulkToDealDetailData.class )
	//public void testBuildURLAndVerifyDealDetailPage(String city, String dealName) {
	public void testBuildURLAndVerifyDealDetailPage(String dealId) {
		//TestListener.setLog(LogStatus.INFO, "Build Local URL");
		//		int lastIndexOf = dealName.lastIndexOf("-");
		//		String dealId = dealName.substring(lastIndexOf+1);
		//		dealName = dealName.substring(0, lastIndexOf);

		//buildURL.buildLocalURL(city, dealName, dealId);
		buildURL.buildLocalURL(dealId);
		//TestListener.setLog(LogStatus.INFO, "Verify Page");
		try{
		//	dealDetail.waitForBuyInBulkButtonToBeClickable();
			// Verify the Deal Heading,More Deals Section, Quantity Inc/Dec, Slider Crousel & Text in Each Offer List 
			dealDetail.dealDetailHeading()
			.moreDealSections()
			//.verifyOfferSection()
			//.verifyQuantityIncDec()
			.verifySliderAndCarousel()
			//.verifyTextsInEachLocalOffer()
			.verifyMaps()
			.dealDetailOptionTitles()
			.viewGrouponMenuLocationMenuServicesMenu()
			.shareThisDeal();

			dealDetail.verifyNotAllLocalOffersAreSoldOut();

			dealLogs.put(dealId, "{'verification.status':'PASS’}");
		}catch(AssertionError ae) {
			dealLogs.put(dealId, "{'verification.status':'FAIL’,'reason':'"+ae.getMessage()+"'}");
			throw ae;
		}
	}

	@AfterMethod(alwaysRun=true)
	public void afterTest() {
		logger.info("After Method");
		TestListener.setLog(LogStatus.INFO, dealLogs.toString());
	}

	@Test(priority = 3)
	public void testClickOnBuyNowButton() {
		TestListener.setLog(LogStatus.INFO, "Click on Buy Now Button");
		dealDetail.clickOnBuyNowButton();
	}
	/**
	 * Do Success Login
	 */
	@Test(priority = 4 , dataProvider="successEmailIdLogin" , dataProviderClass = AuthenticationData.class )
	public void testSuccessLogin(String username, String password, String message, String bool) throws Throwable {
		TestListener.setLog(LogStatus.INFO, "Enter Credentials for Successful Login");
		authenticationPage.verifyLoginAndErrorMessages(username, password, message, Boolean.valueOf(bool));
	}

	/**
	 * Verify Order Details
	 */
	@Test(priority = 5 )
	public void testOrderDetails() {
		TestListener.setLog(LogStatus.INFO, "Verify Price Details on Order Summary page");
		orderSummaryPage.verifyOrderSummaryDetails().priceVerifications();
	}

	/**
	 * Select the credits
	 * @param bool
	 */
	@Test(priority = 6, dataProvider="userDontWantToUseCredits" , dataProviderClass = CreditsData.class )
	public void testCredits(String bool) {
		TestListener.setLog(LogStatus.INFO, "Deselect the credits checkbox");
		orderSummaryPage.selectDeselectCredits(Boolean.valueOf(bool));
	}

//	@Test(priority = 10)
//	public void testProceedToPayment() {
//		orderSummaryPage.clickOnProceedToPaymentButton("dc-paynow-btn");
//	}

}

