package com.groupon.web.nearbuy.test;

import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import com.groupon.web.common.test.BaseTest;
import com.groupon.web.common.utils.TestListener;
import com.groupon.web.nearbuy.data.AuthenticationData;
import com.groupon.web.nearbuy.page.AuthenticationPage;
import com.relevantcodes.extentreports.LogStatus;
/**
 * 
 * @author Prachi Nagpal
 *
 */
@Test(groups = {"sanity", "loginSignUpInPopUp"})
public class AuthenticationPopUpTest extends BaseTest {

	private AuthenticationPage authenticationPage;

	@BeforeMethod(alwaysRun=true)
	public void init() {
		authenticationPage = new AuthenticationPage();
	}
	/**
	 * Test Click on Login Link
	 */
	@Test(priority = 1)
	public void testUserClickOnLoginLink() throws Throwable {
		TestListener.setLog(LogStatus.INFO, "Click on Login Link");
		//authPopup();
		authenticationPage.navigateToHome().clickOnLoginLink();
	}
	/**
	 * Test Enter Credentials
	 */
	@Test(priority = 2 , dataProvider="userCredentialsForErrorMessage" ,dataProviderClass = AuthenticationData.class )
	public void testUserEntersCredentialsAndLogin(String username, String password, String message, String bool) throws Throwable {
		TestListener.setLog(LogStatus.INFO, "Enter Credentials & Verify Error Messages");
		authenticationPage.verifyLoginAndSignUpPopUp().verifyLoginAndErrorMessagesInLoginPopUp(username, password, message, Boolean.valueOf(bool));
	}

	/**
	 * Test Forgot Password Link
	 */
	@Test(priority = 3, dataProvider="ForgotPasswordOtpScreenErrorMessages" , dataProviderClass = AuthenticationData.class)
	public void testForgotPasswordLink(String emailOrmobile, String errorMessage) throws Throwable {
		TestListener.setLog(LogStatus.INFO, "Verify Forgot Password Link");
		authenticationPage.forgotPasswordLink(emailOrmobile, errorMessage);
	}
	
	
	/**
	 * Test SignUp
	 */
	@Test(priority = 4, dataProvider="signUpErrorMessages" , dataProviderClass = AuthenticationData.class)
	public void testSignUp(String errorMessage) throws Throwable {
		TestListener.setLog(LogStatus.INFO, "Enter Details for SignUp");
		authenticationPage.clickOnSignUpLinkAfterClosingLoginPopUp().alreadyAUserLink().enterDetailsForSignUp(errorMessage);
	}

	/**
	 * Test Success Number Login
	 */
	@Test(priority = 6 , dataProvider="successNumberLoginLogout" ,dataProviderClass = AuthenticationData.class )
	public void testUserDoesSuccessNumberLogin(String username, String password, String message, String bool) throws Throwable {
		
		TestListener.setLog(LogStatus.INFO, "Enter Credentials for Successful Number login");
		authenticationPage.clickOnLoginTabFromSignUpTab();
		//authPopup();
		authenticationPage.verifyLoginAndErrorMessagesInLoginPopUp(username, password, message, Boolean.valueOf(bool));
	}
	
	/**
	 * Test Success Email Id Login
	 */
	
	@Test(priority = 7 , dataProvider="successEmailIdLoginLogout" ,dataProviderClass = AuthenticationData.class )
	public void testUserDoesSuccessEmailIdLogin(String username, String password, String message, String bool) throws Throwable {
		
		TestListener.setLog(LogStatus.INFO, "Enter Credentials for Successful Email ID login");
		authenticationPage.clickOnLoginLink();
		//authPopup();
		authenticationPage.verifyLoginAndErrorMessagesInLoginPopUp(username, password, message, Boolean.valueOf(bool));
	}
	

}
