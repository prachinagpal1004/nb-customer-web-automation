//package com.groupon.web.nearbuy.test;
//
//import java.io.File;
//import java.net.MalformedURLException;
//import java.net.URL;
//import java.util.HashMap;
//
//import org.openqa.selenium.By;
//import org.openqa.selenium.Dimension;
//import org.openqa.selenium.JavascriptExecutor;
//import org.openqa.selenium.WebElement;
//import org.openqa.selenium.interactions.Actions;
//import org.openqa.selenium.remote.DesiredCapabilities;
//import org.openqa.selenium.remote.RemoteWebElement;
//import org.openqa.selenium.support.ui.ExpectedConditions;
//import org.openqa.selenium.support.ui.WebDriverWait;
//import org.testng.annotations.AfterClass;
//import org.testng.annotations.BeforeClass;
//import org.testng.annotations.Test;
//import io.appium.java_client.MobileElement;
//import io.appium.java_client.android.AndroidDriver;
//
//@Test(groups = {"android"})
//public class NearMeSelectWithLocationOffTest{
//
//	public AndroidDriver<MobileElement> driver;
//	public WebDriverWait wait ;
//
//	@BeforeClass
//	public void prepareAndroidForAppium() throws MalformedURLException {
//		appiumStart();
//	}
//	
//	@Test
//	public void testNearMeWithLocationOff() throws Exception {
//		stepsToReachHomePage();
//		wait = new WebDriverWait(driver, 2000);
//		MobileElement nearMeSelectInHeader = driver.findElementById("my_location");
//		nearMeSelectInHeader.tap(1, 1000);
//		wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("com.android.settings:id/switch_widget")));
//		MobileElement locationSwitchButton = driver.findElementById("com.android.settings:id/switch_widget");
//		locationSwitchButton.click();
//		driver.navigate().back();
//		wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("store_front_cat_menu")));
//		
//		// java
////		JavascriptExecutor js = (JavascriptExecutor) driver;
////		HashMap<String, String> scrollObject = new HashMap<String, String>();
////		scrollObject.put("direction", "down");
////		scrollObject.put("element", ((MobileElement) driver.findElementByXPath("//android.widget.CheckedTextView[contains(@resource-id,'tv_offer_details') and @text='Body N Shape, 3 Locations']")).getId());
////		js.executeScript("mobile: scroll", scrollObject);
//		
//		
////		HashMap<String, String> scrollObject = new HashMap<String, String>();
////		//Must be an element with scrollable  property; Android (ListView,ScrollabeView) IOS (UIAScrollableView
////		RemoteWebElement element = (RemoteWebElement) driver.findElement(By.id("gradient_overlay"));
////		JavascriptExecutor js = (JavascriptExecutor) driver;
////		String widId = ((RemoteWebElement) element).getId();
////		 //Text for search on the screen
//////		scrollObject.put("text", text);
//////		scrollObject.put("element", widId);
////		js.executeScript("mobile: scrollTo", scrollObject);
//		
////		driver.swipe(400, 600, 400, 100, 1000);
//		driver.swipe(37, -301, 1, -2, 3000);
//		
//		Thread.sleep(4000);
//
//	}
//
//	@AfterClass
//	public void teardown(){
//		//close the app
//		driver.closeApp();
//		driver.resetApp();
//		driver.quit();
//	}
//
//	public void appiumStart() throws MalformedURLException {
//		File appDir = new File("/Users/Prachi/Documents/adt-bundle-mac-x86_64-20140702/sdk/tools");
//		File app = new File(appDir, "nb-android-staging-stag-release.apk");
//		DesiredCapabilities capabilities = new DesiredCapabilities();
//		// capabilities.setCapability("device","Android");
//		capabilities.setCapability("platformName", "Android");
//
//		//mandatory capabilities
//		capabilities.setCapability("deviceName","emulator-5554");
//		capabilities.setCapability("platformVersion","5.1");
//		// Set android appPackage desired capability.
//		capabilities.setCapability("appPackage", "com.nearbuy.nearbuymobile.stag");
//
//		// Set android appActivity desired capability.
//		capabilities.setCapability("appActivity", "com.nearbuy.nearbuymobile.activity.LauncherActivity");
//
//		//   capabilities.setCapability("udid", Properties.udid);
//
//		//other caps
//		capabilities.setCapability("app", app.getAbsolutePath());
//		driver =  new AndroidDriver<MobileElement>(new URL("http://127.0.0.1:4723/wd/hub"), capabilities);
//	}
//
//	public void stepsToReachHomePage() {
//		wait = new WebDriverWait(driver, 4000);
//		wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//android.widget.TextView[@text='Skip']")));
//		driver.findElementByXPath("//android.widget.TextView[@text='Skip']").click();
//		WebElement searchBox=driver.findElementByXPath("//android.widget.EditText[@text='Search']");
//		wait.until(ExpectedConditions.elementToBeClickable(searchBox));
////		searchBox.click();
////		searchBox.sendKeys("De");
////		wait.until(ExpectedConditions.elementToBeClickable(driver.findElementByXPath("//android.widget.CheckedTextView[contains(@resource-id,'txtVwLocationName') and @text='Delhi/NCR']")));
////		driver.findElementByXPath("//android.widget.CheckedTextView[contains(@resource-id,'txtVwLocationName') and @text='Delhi/NCR']").click();
////		wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//android.widget.TextView[@text='Around You']")));
////		wait.until(ExpectedConditions.elementToBeClickable(By.id("store_front_cat_menu_item_cat_name")));
//	}
//
//}
