package com.groupon.web.nearbuy.data;

import org.testng.annotations.DataProvider;
import com.groupon.web.common.config.Config;
import com.groupon.web.common.utils.DataProviderUtil;

/**
 * 
 * @author prachi.nagpal
 *
 */
public class AuthenticationData {

	@DataProvider(name = "userCredentialsForErrorMessage")
	public static Object[][] provideCredentials(){
		return DataProviderUtil.provideData(Config.getInstance().getConfig("testData.file"), "nearbuy","userCredentialsForErrorMessage");
	}
	@DataProvider(name = "signUpErrorMessages")
	public static Object[][] provideSignUpErrorMessages(){
		return DataProviderUtil.provideData(Config.getInstance().getConfig("testData.file"), "nearbuy","signUpErrorMessages");
	}
	@DataProvider(name = "ForgotPasswordOtpScreenErrorMessages")
	public static Object[][] provideOtpScreenErrorMessages(){
		return DataProviderUtil.provideData(Config.getInstance().getConfig("testData.file"), "nearbuy","ForgotPasswordOtpScreenErrorMessages");
	}
	
	@DataProvider(name = "successNumberLogin")
	public static Object[][] provideSuccessNumberCredentials(){
		return DataProviderUtil.provideData(Config.getInstance().getConfig("testData.file"), "nearbuy","successNumberLogin");
	}
	@DataProvider(name = "successNumberLoginForMyAccountDetails")
	public static Object[][] provideSuccessNumberLoginForMyAccountDetails(){
		return DataProviderUtil.provideData(Config.getInstance().getConfig("testData.file"), "nearbuy","successNumberLoginForMyAccountDetails");
	}
	
	@DataProvider(name = "successEmailIdLogin")
	public static Object[][] provideSuccessEmailIdCredentials(){
		return DataProviderUtil.provideData(Config.getInstance().getConfig("testData.file"), "nearbuy","successEmailIdLogin");
	}
	
	@DataProvider(name = "successNumberLoginLogout")
	public static Object[][] provideSuccessNumberLoginLogoutCredentials(){
		return DataProviderUtil.provideData(Config.getInstance().getConfig("testData.file"), "nearbuy","successNumberLoginLogout");
	}
	
	@DataProvider(name = "successEmailIdLoginLogout")
	public static Object[][] provideSuccessEmailIdLoginLogoutCredentials(){
		return DataProviderUtil.provideData(Config.getInstance().getConfig("testData.file"), "nearbuy","successEmailIdLoginLogout");
	}
	@DataProvider(name = "nearbuyCredentialsForPayTMtransaction")
	public static Object[][] provideSuccessNumberPayTmCredentials(){
		return DataProviderUtil.provideData(Config.getInstance().getConfig("testData.file"), "nearbuy","nearbuyCredentialsForPayTMtransaction");
	}
}
