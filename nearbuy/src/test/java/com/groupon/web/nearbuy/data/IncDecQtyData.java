package com.groupon.web.nearbuy.data;

import org.testng.annotations.DataProvider;

import com.groupon.web.common.config.Config;
import com.groupon.web.common.utils.DataProviderUtil;

/**
 * 
 * @author prachi.nagpal
 *
 */
public class IncDecQtyData {

	@DataProvider(name = "incDecQtyCount")
	public static Object[][] provideCount(){
		return DataProviderUtil.provideData(Config.getInstance().getConfig("testData.file"), "nearbuy","incDecQtyCount");
	}
	
	@DataProvider(name = "incDecQtyCountTo10")
	public static Object[][] provideMoreCount(){
		return DataProviderUtil.provideData(Config.getInstance().getConfig("testData.file"), "nearbuy","incDecQtyCountTo10");
	}
	
	@DataProvider(name = "incDecQtyCountAndOptionToBeSelected")
	public static Object[][] provideOption(){
		return DataProviderUtil.provideData(Config.getInstance().getConfig("testData.file"), "nearbuy","incDecQtyCountAndOptionToBeSelected");
	}
}
