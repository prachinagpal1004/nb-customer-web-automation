package com.groupon.web.nearbuy.test;

import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import com.groupon.web.common.test.BaseTest;
import com.groupon.web.common.utils.TestListener;
import com.groupon.web.nearbuy.data.MenuData;
import com.groupon.web.nearbuy.page.DealCatalogPage;
import com.groupon.web.nearbuy.page.DealDetailPage;
import com.groupon.web.nearbuy.page.HeaderFooterPage;
import com.groupon.web.nearbuy.page.HomePage;
import com.groupon.web.nearbuy.page.TravelDealDetailPage;
import com.relevantcodes.extentreports.LogStatus;
/**
 * 
 * @author Prachi Nagpal
 *
 */
@Test(groups = {"sanity", "staging","travelDealDetail"})
public class TravelDealDetailTest extends BaseTest {


	private HeaderFooterPage headerFooterPage;
	private HomePage homePage;
	private DealCatalogPage dealCatalogPage;
	private DealDetailPage dealDetailPage;
	private TravelDealDetailPage travelDealDetailPage;

	@BeforeMethod(alwaysRun=true)
	public void init() {
		headerFooterPage = new HeaderFooterPage();
		dealCatalogPage = new DealCatalogPage();
		dealDetailPage = new DealDetailPage();
		homePage = new HomePage();
		travelDealDetailPage = new TravelDealDetailPage();
	}

	@Test(priority = 1, dataProvider="travelSubMenu" , dataProviderClass = MenuData.class )
	public void testLandingOnDealDetailPage(String mainMenu, String cat, String subCat) {
		TestListener.setLog(LogStatus.INFO, "Navigate to Travel Deal Listing Page from Menu");
		headerFooterPage.navigateToMenu(mainMenu, cat, subCat);
		TestListener.setLog(LogStatus.INFO, "Click on Featured Deal");
		dealCatalogPage.clickOnSpecifiedDealCard(3);
	}

	@Test(priority = 2 )
	public void testDealHeading() throws Throwable {
		TestListener.setLog(LogStatus.INFO, "Verify Deal Detail Heading");
		dealDetailPage.dealDetailHeading().dealDetailOptionTitles();
	}

	/**
	 * Click on Buy Now Button of Deal
	 */
	@Test(priority = 3)
	public void testClickOnBuyNowButtonForErrorMessage(){
		TestListener.setLog(LogStatus.INFO, "Click on Buy Now Button");
		travelDealDetailPage.clickOnBookThisOptionButtonForErrorMessage(0);
	}

	/**
	 * Click on Buy Now Button of Deal
	 */
	@Test(priority = 4)
	public void testSelectDatesAndVerifyClearDatesLink(){
		TestListener.setLog(LogStatus.INFO, "Select Dates");
		travelDealDetailPage.selectNumberOfRoomsCheckInAndCheckOutDates(0, 0, "1");
		TestListener.setLog(LogStatus.INFO, "Clear Dates");
		travelDealDetailPage.clickOnClearDatesLink();
		TestListener.setLog(LogStatus.INFO, "Select Dates Again");
		travelDealDetailPage.selectNumberOfRoomsCheckInAndCheckOutDates(0, 0, "1");

	}

	@Test(priority = 5 )
	public void testLeftSideOfOptions(){
		TestListener.setLog(LogStatus.INFO, "Verify Left Of Each Option");
		travelDealDetailPage.verifyEachOptionLeftSide();
	}

	@Test(priority = 6 )
	public void testRightSideOfOptions(){
		TestListener.setLog(LogStatus.INFO, "Verify Right Of Each Option");
		travelDealDetailPage.verifyEachOptionRightSide();
	}

	@Test(priority = 7 )
	public void testMaps() throws Throwable {
		TestListener.setLog(LogStatus.INFO, "Verify Travel Maps");
		travelDealDetailPage.verifyTravelMaps();
	}
	
	@Test(priority = 8 )
	public void testFacilities() throws Throwable {
		TestListener.setLog(LogStatus.INFO, "Verify Facilities");
		travelDealDetailPage.verifyFacilities();
	}

	@Test(priority = 9 )
	public void testTags() throws Throwable {
		TestListener.setLog(LogStatus.INFO, "Verify Tags");
		dealDetailPage.verifyTags();
	}
	@Test(priority = 10 )
	public void testHowToUseOffer() throws Throwable {
		TestListener.setLog(LogStatus.INFO, "Verify How To Use Offer & Things to remember Section");
		travelDealDetailPage.howToUseOfferAndThingsToRemember();
	}
	@Test(priority = 11 )
	public void testNearbuyPromiseSection() throws Throwable {
		TestListener.setLog(LogStatus.INFO, "Verify Nearbuy Promise Section");
		dealDetailPage.nearbuyPromise();
	}
	@Test(priority = 12 )
	public void testNearbuyCancellationPolicy() throws Throwable {
		TestListener.setLog(LogStatus.INFO, "Verify Nearbuy cancellation Section");
		travelDealDetailPage.verifyBottomRightOfTravelDetail();
	}
	//
	//	@Test(priority = 14 )
	//	public void testShareThisDeal() throws Throwable {
	//		TestListener.setLog(LogStatus.INFO, "Verify Share This Deal Section");
	//		dealDetailPage.shareThisDeal();
	//	}

}
