package com.groupon.web.nearbuy.test;

import com.groupon.web.common.test.BaseTest;
import com.groupon.web.common.utils.TestListener;
import com.groupon.web.nearbuy.data.ChangeYourLocationData;
import com.groupon.web.nearbuy.data.MenuData;
import com.groupon.web.nearbuy.page.HeaderFooterPage;
import com.groupon.web.nearbuy.page.HomePage;
import com.relevantcodes.extentreports.LogStatus;

import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
/**
 * 
 * @author Prachi Nagpal
 *
 */
@Test(groups = {"sanity","citySelector"})
public class CitySelectorTest extends BaseTest {

	private HeaderFooterPage headerFooterPage;

	@BeforeMethod(alwaysRun=true)
	public void init() {
		headerFooterPage = new HeaderFooterPage();
	}

	@Test(priority = 1 , dataProvider="changeLocation" , dataProviderClass = ChangeYourLocationData.class )
	public void testCitySelectorAndChangeLocationOnHomePage(String location) throws Throwable {
		TestListener.setLog(LogStatus.INFO, "Verify 'City Selector' Functionality on Home Page");
		headerFooterPage.changeYourLocation(location);
	}
	@Test(priority = 2 , dataProvider="changeLocation" , dataProviderClass = ChangeYourLocationData.class )
	public void testCitySelectorAndChangeLocationOnSearchPage(String location) throws Throwable {
		TestListener.setLog(LogStatus.INFO, "Verify 'City Selector' Functionality on Search Page");
		headerFooterPage.clickOnSearchBox();
		headerFooterPage.verifyCitySelectorFromSearchBox(location);
	}

}

