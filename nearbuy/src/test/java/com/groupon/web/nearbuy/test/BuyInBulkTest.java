//package com.groupon.web.nearbuy.test;
//
//import com.groupon.web.common.page.BasePage;
//import com.groupon.web.common.test.BaseTest;
//import com.groupon.web.common.utils.TestListener;
//import com.groupon.web.nearbuy.data.BuyingInBulkData;
//import com.groupon.web.nearbuy.data.MenuData;
//import com.groupon.web.nearbuy.page.BuyingInBulkPage;
//import com.groupon.web.nearbuy.page.DealCatalogPage;
//import com.groupon.web.nearbuy.page.HeaderFooterPage;
//import com.relevantcodes.extentreports.LogStatus;
//
//import org.testng.annotations.BeforeMethod;
//import org.testng.annotations.Test;
///**
// * 
// * @author Prachi Nagpal
// *
// */
//@Test(groups = {"sanity", "staging"})
//public class BuyInBulkTest extends BaseTest {
//
//
//	private HeaderFooterPage headerFooterPage;
//	private BuyingInBulkPage buyingInBulkPage;
//	private DealCatalogPage dealCatalogPage;
//
//	@BeforeMethod(alwaysRun=true)
//	public void init() {
//		headerFooterPage = new HeaderFooterPage();
//		buyingInBulkPage = new BuyingInBulkPage();
//		dealCatalogPage = new DealCatalogPage();
//	}
//
//	@Test(priority = 1, dataProvider="enterSubmenuForDealListingPage" , dataProviderClass = MenuData.class )
//	public void testLandingOnDealDetailPage(String menu, String submenu) throws Throwable {
//		TestListener.setLog(LogStatus.INFO, "Navigate to Deal Listing Page");
//		headerFooterPage.navigateToMenu(menu, submenu);
//	}
//
//	@Test(priority = 2, dataProvider="enterDetailsInForm" , dataProviderClass = BuyingInBulkData.class )
//	public void testBulkBuy(String name,String email,String qty,String organization,String phone) throws Throwable {
//		final String subMenuUrl = BasePage.getCurrentUrl();
//		final String menuUrl = subMenuUrl.substring(0, subMenuUrl.substring(0, subMenuUrl.length()-1).lastIndexOf("/"));
//		System.out.println(subMenuUrl);
//		System.out.println(menuUrl);
//		TestListener.setLog(LogStatus.INFO, "Click on Featured Deal");
//		dealCatalogPage.clickOnFeaturedDeal();
//		final String productUrl = BasePage.getCurrentUrl();
//		System.out.println(productUrl);
//		TestListener.setLog(LogStatus.INFO, "Click on Buy In Bulk Button");
//		buyingInBulkPage.clickOnBuyingInBulkButton().fillForm(name, email, qty, organization, phone);
//		TestListener.setLog(LogStatus.INFO, "Buy In Bulk Form is filled");
//		TestListener.setLog(LogStatus.INFO, "Verify Continue Shopping Button");
//		buyingInBulkPage.thanksMessageAndButtonsVerification(menuUrl, productUrl, "Continue Shopping");
//		TestListener.setLog(LogStatus.INFO, "Verify Back To Deal Button");
//		buyingInBulkPage.thanksMessageAndButtonsVerification(menuUrl, productUrl, "Back to Deal");
//	}
//
//}
//
