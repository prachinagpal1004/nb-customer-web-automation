package com.groupon.web.nearbuy.data;

import org.testng.annotations.DataProvider;

import com.groupon.web.common.config.Config;
import com.groupon.web.common.utils.DataProviderUtil;

/**
 * 
 * @author prachi.nagpal
 *
 */
public class HulkToDealDetailData {

	@DataProvider(name = "hulkToLocalDealDetail")
	public static Object[][] hulkToLocalDealDetail(){
		return DataProviderUtil.provideData(Config.getInstance().getConfig("hulkData.file"), "nearbuy","hulkToLocalDealDetail");
	}
	
	@DataProvider(name = "hulkToProductDealDetail")
	public static Object[][] hulkToProductDealDetail(){
		return DataProviderUtil.provideData(Config.getInstance().getConfig("testData.file"), "nearbuy","hulkToProductDealDetail");
	}
}
