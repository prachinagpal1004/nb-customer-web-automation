package com.groupon.web.nearbuy.test;

import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import com.groupon.web.common.test.BaseTest;
import com.groupon.web.common.utils.TestListener;
import com.groupon.web.nearbuy.data.MenuData;
import com.groupon.web.nearbuy.page.DealCatalogPage;
import com.groupon.web.nearbuy.page.DealDetailPage;
import com.groupon.web.nearbuy.page.HeaderFooterPage;
import com.groupon.web.nearbuy.page.HomePage;
import com.relevantcodes.extentreports.LogStatus;
/**
 * 
 * @author Prachi Nagpal
 *
 */
@Test(groups = {"sanity", "staging"})
public class LocalDealDetailTest extends BaseTest {


	private HeaderFooterPage headerFooterPage;
	private HomePage homePage;
	private DealCatalogPage dealCatalogPage;
	private DealDetailPage dealDetailPage;

	@BeforeMethod(alwaysRun=true)
	public void init() {
		headerFooterPage = new HeaderFooterPage();
		dealCatalogPage = new DealCatalogPage();
		dealDetailPage = new DealDetailPage();
		homePage = new HomePage();
	}

	@Test(priority = 1, dataProvider="foodAndDrink" , dataProviderClass = MenuData.class )
	public void testLandingOnDealDetailPage(String mainMenu, String cat, String subCat) {
		TestListener.setLog(LogStatus.INFO, "Navigate to Deal Listing Page from Menu");
		headerFooterPage.navigateToMenu(mainMenu, cat, subCat);
		TestListener.setLog(LogStatus.INFO, "Click on Featured Deal");
		dealCatalogPage.clickOnSpecifiedDealCard(3);
	}

	@Test(priority = 2 )
	public void testDealHeading() throws Throwable {
		TestListener.setLog(LogStatus.INFO, "Verify Deal Detail Heading");
		dealDetailPage.dealDetailHeading();
	}

	@Test(priority = 3 )
	public void testDealContent() {
		TestListener.setLog(LogStatus.INFO, "Verify Deal Detail Content");
		dealDetailPage.dealContent();
	}

	@Test(priority = 4 )
	public void testVerifyOfferSection() throws Throwable {
		TestListener.setLog(LogStatus.INFO, "Verify Each Offer Section");
		dealDetailPage.verifyOfferSectionAndClickOnOption(1,2);
	}

		@Test(priority = 5 )
		public void testMaps() throws Throwable {
			TestListener.setLog(LogStatus.INFO, "Verify Maps");
			dealDetailPage.verifyMaps();
		}

	//	@Test(priority = 6 )
	//	public void testDealDetailSection() throws Throwable {
	//		TestListener.setLog(LogStatus.INFO, "Verify Deal Detail Text Section");
	//		dealDetailPage.dealDetailSection();
	//	}

	
//	@Test(priority = 10 )
//	public void testImageSliderAndCarousel() throws Throwable {
//		TestListener.setLog(LogStatus.INFO, "Verify Image SLider & Carousel");
//		dealDetailPage.verifySliderAndCarousel();
//	}
//
	@Test(priority = 11 )
	public void testTags() throws Throwable {
		TestListener.setLog(LogStatus.INFO, "Verify Tags");
		dealDetailPage.verifyTags();
	}
//
//	@Test(priority = 12 )
//	public void testMoreDealSection() throws Throwable {
//		TestListener.setLog(LogStatus.INFO, "Verify More Deal Section");
//		dealDetailPage.moreDealSections();
//	}
//
	@Test(priority = 12 )
	public void testHowToUseOffer() throws Throwable {
		TestListener.setLog(LogStatus.INFO, "Verify How To Use Offer Section");
		dealDetailPage.howToUseOffer();
	}
	@Test(priority = 13 )
	public void testNearbuyPromiseSection() throws Throwable {
		TestListener.setLog(LogStatus.INFO, "Verify Nearbuy Promise Section");
		dealDetailPage.nearbuyPromise();
	}
	@Test(priority = 14 )
	public void testNearbuyCancellationPolicy() throws Throwable {
		TestListener.setLog(LogStatus.INFO, "Verify Nearbuy cancellation Section");
		dealDetailPage.cancellationPolicy();
	}
	@Test(priority = 15 )
	public void testNearbuyThingsToRemember() throws Throwable {
		TestListener.setLog(LogStatus.INFO, "Verify Nearbuy Things To rememeber Section");
		dealDetailPage.thingstoRemember();
	}
	@Test(priority = 16 )
	public void testPromoSectionIfPresent() throws Throwable {
		TestListener.setLog(LogStatus.INFO, "Verify Nearbuy Promo Section");
		dealDetailPage.promoSection();
	}
	@Test(priority = 17 )
	public void testBankOffersSectionIfPresent() throws Throwable {
		TestListener.setLog(LogStatus.INFO, "Verify Nearbuy Bank Offers Section");
		dealDetailPage.bankOffersSection();
	}
//
//	@Test(priority = 14 )
//	public void testShareThisDeal() throws Throwable {
//		TestListener.setLog(LogStatus.INFO, "Verify Share This Deal Section");
//		dealDetailPage.shareThisDeal();
//	}

}
