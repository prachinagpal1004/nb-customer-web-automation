	package com.groupon.web.nearbuy.test;

import com.groupon.web.common.test.BaseTest;
import com.groupon.web.common.utils.TestListener;
import com.groupon.web.nearbuy.data.MenuData;
import com.groupon.web.nearbuy.page.DealCatalogPage;
import com.groupon.web.nearbuy.page.HeaderFooterPage;
import com.relevantcodes.extentreports.LogStatus;

import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
/**
 * 
 * @author Prachi Nagpal
 *
 */
@Test(groups = {"sanity"})
public class BreadcrumbsTest extends BaseTest {

	private HeaderFooterPage headerFooterPage;
	private DealCatalogPage dealCatalogPage;

	@BeforeMethod(alwaysRun=true)
	public void init() {
		headerFooterPage = new HeaderFooterPage();
		dealCatalogPage = new DealCatalogPage();
	}

	@Test(priority = 1 , dataProvider="foodAndDrink" , dataProviderClass = MenuData.class )
	public void testBreadCrumbs(String mainMenu, String cat, String subCat) {
		TestListener.setLog(LogStatus.INFO, "Navigate to Deal Listing Page from Menu");
		headerFooterPage.navigateToMenu(mainMenu, cat, subCat);
		TestListener.setLog(LogStatus.INFO, "Verify Breadcrumbs Funcationality");
		dealCatalogPage.verifyBreadcrumbs();
	}
}
