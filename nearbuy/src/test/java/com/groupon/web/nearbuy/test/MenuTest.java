//package com.groupon.web.nearbuy.test;
//
//import com.groupon.web.common.test.BaseTest;
//import com.groupon.web.nearbuy.page.HeaderFooterPage;
//import org.testng.annotations.BeforeMethod;
//import org.testng.annotations.Test;
//
//public class MenuTest extends BaseTest {
//
//	private HeaderFooterPage headerFooterPage;
//
//    @BeforeMethod(alwaysRun=true)
//    public void init() {
//		headerFooterPage = new HeaderFooterPage();
//	}
//
//	@Test(priority = 1 )
//	public void test_Menu() throws Throwable {
//		headerFooterPage.hoverMainMenuAndVerifyAllSubmenuLinksAndImages();
//	}
//
//}
//
