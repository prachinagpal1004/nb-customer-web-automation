package com.groupon.web.nearbuy.test;

import com.groupon.web.common.test.BaseTest;
import com.groupon.web.common.utils.TestListener;
import com.groupon.web.nearbuy.data.AuthenticationData;
import com.groupon.web.nearbuy.page.AuthenticationPage;
import com.groupon.web.nearbuy.page.MyAccountPage;
import com.relevantcodes.extentreports.LogStatus;

import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
/**
 * 
 * @author Prachi Nagpal
 *
 */
@Test(groups = {"sanity", "staging","pdfVoucher"})
public class PDFvoucherTest extends BaseTest {

	private AuthenticationPage authenticationPage;
	private MyAccountPage myAccountPage;

    @BeforeMethod(alwaysRun=true)
    public void init() {
		authenticationPage = new AuthenticationPage();
		myAccountPage = new MyAccountPage();
	}


	/**
	 * Test Click on Login Link
	 */
    @Test(priority = 1 , dataProvider="successNumberLogin" ,dataProviderClass = AuthenticationData.class )
	public void testPDFvouchersContent(String username, String password, String message, String bool) throws Throwable {
    	TestListener.setLog(LogStatus.INFO, "Click on Login Link & Enter Credentials for Successful Login");
//    	String authPopupEnabled = Config.getInstance().getConfig("auth.popup.enabled");
//		if(authPopupEnabled != null && Boolean.valueOf(authPopupEnabled)) {
//			(new Thread(new BrowserAuthenticationWindow())).start();
//		}
		authenticationPage.clickOnLoginLink().verifyLoginAndErrorMessages(username, password, message, Boolean.valueOf(bool));
		TestListener.setLog(LogStatus.INFO, "Click on My Orders Link");
		myAccountPage.clickOnLink("My Orders");
		TestListener.setLog(LogStatus.INFO, "Click on PDF Vouchers & Verify Content");
		myAccountPage.clickAndverifyPDFvoucherText();
	}
}
