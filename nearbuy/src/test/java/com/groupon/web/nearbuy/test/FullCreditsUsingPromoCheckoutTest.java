package com.groupon.web.nearbuy.test;

import com.groupon.web.common.test.BaseTest;
import com.groupon.web.common.utils.TestListener;
import com.groupon.web.nearbuy.data.AuthenticationData;
import com.groupon.web.nearbuy.data.CreditsData;
import com.groupon.web.nearbuy.data.MenuData;
import com.groupon.web.nearbuy.data.PromoData;
import com.groupon.web.nearbuy.page.AuthenticationPage;
import com.groupon.web.nearbuy.page.DealCatalogPage;
import com.groupon.web.nearbuy.page.DealDetailPage;
import com.groupon.web.nearbuy.page.HeaderFooterPage;
import com.groupon.web.nearbuy.page.OrderSummaryPage;
import com.groupon.web.nearbuy.page.ThankYouPage;
import com.relevantcodes.extentreports.LogStatus;

import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

@Test(groups = {"sanity", "staging"})
public class FullCreditsUsingPromoCheckoutTest extends BaseTest {

	private AuthenticationPage authenticationPage;
	private HeaderFooterPage headerFooterPage;
	private DealDetailPage dealDetailPage;
	private DealCatalogPage dealCatalogPage;
	private OrderSummaryPage orderSummaryPage;
	private ThankYouPage thankYouPage;
	private String currentLocationSelected;

	@BeforeMethod(alwaysRun=true)
	public void init() {
		authenticationPage = new AuthenticationPage();
		headerFooterPage = new HeaderFooterPage();
		dealDetailPage = new DealDetailPage();
		dealCatalogPage = new DealCatalogPage();
		orderSummaryPage = new OrderSummaryPage();
		thankYouPage = new ThankYouPage();
	}


	/**
	 * Traverse to Deal Catalog Page and click on featured Deal
	 * @param menu
	 * @param submenu
	 */
	@Test(priority = 1, dataProvider="enterSubmenuForDealListingPage" , dataProviderClass = MenuData.class )
	public void testLandingOnDealDetailPage(String menu, String submenu, String subsubmenu) {
		TestListener.setLog(LogStatus.INFO, "Verify Home Page Title");
		headerFooterPage.getTitleOfTheHomePage();
		currentLocationSelected = headerFooterPage.getCurrentLocationSelected();
		TestListener.setLog(LogStatus.INFO, "Navigate to Deal Listing Page from Menu");
		headerFooterPage.navigateToMenu(menu, submenu,subsubmenu);
		TestListener.setLog(LogStatus.INFO, "Verify Deal Catalog Title");
		headerFooterPage.getTitleOfTheCatalogPage();
		TestListener.setLog(LogStatus.INFO, "Click on Featured Deal");
		dealCatalogPage.clickOnSpecifiedDealCard(5);
	}

	/**
	 * Increase the quantity
	 * @param inc
	 * @param dec
	 */
	@Test(priority = 2)
	public void testOfferSection() {
		TestListener.setLog(LogStatus.INFO, "Verify Deal Details & OfferSection");
		dealDetailPage.dealDetailHeading().dealDetailOptionTitles().verifyOfferSectionAndClickOnOption(1, 1);
		TestListener.setLog(LogStatus.INFO, "Verify Deal Detail Title");
		dealDetailPage.getTitleOfTheDealDetailPage();
	}

	/**
	 * Click on Buy Now Button of Deal
	 */
	@Test(priority = 3)
	public void testClickOnBuyNowButton(){
		TestListener.setLog(LogStatus.INFO, "Click on Book Now Button");
		//authPopup();
		dealDetailPage.clickOnBuyNowButton();
	}

	/**
	 * Do Success Login
	 */
	@Test(priority = 4 , dataProvider="successNumberLogin" , dataProviderClass = AuthenticationData.class )
	public void testSuccessLogin(String username, String password, String message, String bool) throws Throwable {
		TestListener.setLog(LogStatus.INFO, "Verify Login Title");
		authenticationPage.getTitleOfTheLoginPage();
		TestListener.setLog(LogStatus.INFO, "Enter Credentials for Successful Login");
		authenticationPage.verifyLoginAndErrorMessages(username, password, message, Boolean.valueOf(bool));
	}

	/**
	 * Verify Order Details
	 */
	@Test(priority = 5 )
	public void testOrderDetails() {
		TestListener.setLog(LogStatus.INFO, "Verify Order Details & Price");
		orderSummaryPage.verifyOrderSummaryDetails().priceVerifications();
		TestListener.setLog(LogStatus.INFO, "Verify Order Summary Title");
		orderSummaryPage.getTitleOfOrderSummaryPage(currentLocationSelected);
	}

	/**
	 * Select the credits
	 * @param bool
	 */
	@Test(priority = 6, dataProvider="userWantsToUseCredits" , dataProviderClass = CreditsData.class )
	public void testCredits(String bool) {
		TestListener.setLog(LogStatus.INFO, "Deselect the credits checkbox");
		orderSummaryPage.selectDeselectCredits(Boolean.valueOf(bool));
	}

	/**
	 * Enter Valid Promocode
	 * @param promo
	 * @param validPromo
	 */
	@Test(priority = 7, dataProvider="enterValidPromo" , dataProviderClass = PromoData.class )
	public void testPromoValid(String promo, String validPromo, String removePromo) {
		TestListener.setLog(LogStatus.INFO, "Apply Valid Promocode");
		orderSummaryPage.enterPromoCode(promo, Boolean.valueOf(validPromo), Boolean.valueOf(removePromo));
	}

	@Test(priority = 8)
	public void testProceedToPayment() {
		TestListener.setLog(LogStatus.INFO, "Click on 'Proceed To Payment' Button");
		orderSummaryPage.clickOnProceedButton();
	}

	@Test(priority = 9)
	public void testThankYouPage() {
		TestListener.setLog(LogStatus.INFO, "Verify Thank You Page Messages");
		thankYouPage.verifyCongratsMessage();
		TestListener.setLog(LogStatus.INFO, "Verify Thank You Page Title");
		thankYouPage.getTitleOfThankYouPage(currentLocationSelected);
	}

}
