package com.groupon.web.nearbuy.test;

import com.groupon.web.common.test.BaseTest;
import com.groupon.web.common.utils.TestListener;
import com.groupon.web.nearbuy.data.AuthenticationData;
import com.groupon.web.nearbuy.data.CreditsData;
import com.groupon.web.nearbuy.data.IncDecQtyData;
import com.groupon.web.nearbuy.data.MenuData;
import com.groupon.web.nearbuy.data.PayTmData;
import com.groupon.web.nearbuy.data.PaymentModeData;
import com.groupon.web.nearbuy.data.PromoData;
import com.groupon.web.nearbuy.page.AuthenticationPage;
import com.groupon.web.nearbuy.page.DealCatalogPage;
import com.groupon.web.nearbuy.page.DealDetailPage;
import com.groupon.web.nearbuy.page.HeaderFooterPage;
import com.groupon.web.nearbuy.page.OrderSummaryPage;
import com.groupon.web.nearbuy.page.PaytmWalletPage;
import com.groupon.web.nearbuy.page.ThankYouPage;
import com.groupon.web.nearbuy.page.TravelDealDetailPage;
import com.relevantcodes.extentreports.LogStatus;

import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
/**
 * 
 * @author Prachi Nagpal
 *
 */
@Test(groups = {"sanity", "staging","travelWalletTransaction"})
public class TravelWalletCheckoutTest extends BaseTest {


	private AuthenticationPage authenticationPage;
	private HeaderFooterPage headerFooterPage;
	private DealDetailPage dealDetailPage;
	private TravelDealDetailPage travelDealDetailPage;
	private DealCatalogPage dealCatalogPage;
	private OrderSummaryPage orderSummaryPage;
	private PaytmWalletPage payTMcheckoutPage;
	private ThankYouPage thankYouPage;

    @BeforeMethod(alwaysRun=true)
    public void init() {

		authenticationPage = new AuthenticationPage();
		headerFooterPage = new HeaderFooterPage();
		dealDetailPage = new DealDetailPage();
		travelDealDetailPage = new TravelDealDetailPage();
		dealCatalogPage = new DealCatalogPage();
		orderSummaryPage = new OrderSummaryPage();
		payTMcheckoutPage = new PaytmWalletPage();
		thankYouPage = new ThankYouPage();
	}

    /**
	 * Test Click on Login Link
	 */
	@Test(priority = 1)
	public void testUserClickOnLoginLink() throws Throwable {
		TestListener.setLog(LogStatus.INFO, "Click on Login Link");
		authenticationPage.clickOnLoginLink();
	}
	
	/**
	 * Do Success Login
	 */
	@Test(priority = 2 , dataProvider="successNumberLogin" , dataProviderClass = AuthenticationData.class )
	public void testSuccessLogin(String username, String password, String message, String bool) throws Throwable {
		TestListener.setLog(LogStatus.INFO, "Enter Nearbuy Credentials for Login");
		authenticationPage.verifyLoginAndErrorMessages(username, password, message, Boolean.valueOf(bool));
	}

	/**
	 * Traverse to Deal Catalog Page and click on featured Deal
	 * @param menu
	 * @param submenu
	 */
	@Test(priority = 3, dataProvider="travelSubMenu" , dataProviderClass = MenuData.class )
	public void testLandingOnDealDetailPage(String menu, String submenu, String subsubmenu) {
		TestListener.setLog(LogStatus.INFO, "Navigate to Travel Deal Listing Page from Menu");
		headerFooterPage.navigateToMenu(menu, submenu,subsubmenu);
		TestListener.setLog(LogStatus.INFO, "Click on any Travel Deal");
		dealCatalogPage.clickOnSpecifiedDealCard(5);
	}

	/**
	 * Increase the quantity
	 * @param inc
	 * @param dec
	 */
	@Test(priority = 4)
	public void testOfferSection() {
		TestListener.setLog(LogStatus.INFO, "Verify Deal Details & OfferSection");
		dealDetailPage.dealDetailHeading().dealDetailOptionTitles();
	}
	
	/**
	 * Click on Buy Now Button of Deal
	 */
	@Test(priority = 5)
	public void testSelectDatesAndVerifyClearDatesLink(){
		TestListener.setLog(LogStatus.INFO, "Select Dates");
		travelDealDetailPage.selectNumberOfRoomsCheckInAndCheckOutDates(0, 0, "1");

	}

	/**
	 * Click on Buy Now Button of Deal
	 */
	@Test(priority = 6)
	public void testClickOnBookNowButton(){
		TestListener.setLog(LogStatus.INFO, "Click On Book Now Button");
		travelDealDetailPage.clickOnBookThisOptionButton(0);
	}
	@Test(priority = 7)
	public void testClickOnProceedToPayButtonInTravelForm(){
		TestListener.setLog(LogStatus.INFO, "Click On Proceed To Pay In Booking Form");
		travelDealDetailPage.clickOnProceedToPayButtonInTravelForm();
	}

	/**
	 * Verify Order Details
	 */
	@Test(priority = 8)
	public void testOrderDetails() {
		TestListener.setLog(LogStatus.INFO, "Verify Order Details & Price");
		orderSummaryPage.verifyTravelOrderSummaryDetails().travelPriceVerifications();
	}

	/**
	 * Select the credits
	 * @param bool
	 */
	@Test(priority = 9, dataProvider="userDontWantToUseCredits" , dataProviderClass = CreditsData.class )
	public void testCredits(String bool) {
		TestListener.setLog(LogStatus.INFO, "Deselect the credits checkbox");
		orderSummaryPage.selectDeselectCredits(Boolean.valueOf(bool));
	}

	@Test(priority = 10, dataProvider="selectWallets" , dataProviderClass = PaymentModeData.class )
	public void testSelectingPaymentModes(String mode) {
		TestListener.setLog(LogStatus.INFO, "Select Paytm Mode of Transaction");
		orderSummaryPage.selectModeOfPayment(mode);
	}
	
	@Test(priority = 11)
	public void testAvailOffersDuringPayment() {
		orderSummaryPage.availOffersDuringPayment();
	}

	@Test(priority = 12)
	public void testSelectWalletsAndClickPayNow() {
		TestListener.setLog(LogStatus.INFO, "Select Wallets & Click on 'Pay Now' Button");
		payTMcheckoutPage.selectWalletsAndVisitRetailer();
	}
}
